<h1><img alt="NLX" src="logo.png" width="200"> System</h1>

NLX is an open source inter-organizational system facilitating federated authentication, secure connecting and protocolling in a large-scale, dynamic API landscape.

This repository contains all of the components required to act out the [NLX Product Vision](https://docs.nlx.io/understanding-the-basics/product-vision/).

## Developing on NLX

Please find the latest documentation for using NLX on [docs.nlx.io](https://docs.nlx.io). This is a good place to start if you would like to develop an application or service that uses or provides API access over NLX.

## Questions and contributions

Read more on how to ask questions, file bugs and contribute code and documentation in [`CONTRIBUTING.md`](CONTRIBUTING.md).

## Troubleshooting

If you are running into other issues, please [post an Issue on GitLab](https://gitlab.com/commonground/nlx/nlx/issues).

## Building and running an NLX network locally

The NLX project consists of multiple components that together make up the entire NLX platform. Some components run as centralized NLX services, others run on-premise at organizations. All components are maintained in a single repository. This means that a developer has all the tools and code to build and test the complete NLX platform in a single repository. It simplifies version and dependency management and allows changes that affect multiple components to be combined in a single feature branch and merge-request.

If you want to develop locally, or run your own NLX network, you will likely want to start all the components.

### Cloning

Clone NLX in your workspace.

```bash
git clone https://gitlab.com/commonground/nlx/nlx
cd nlx
```

### Development setup

Make sure you have installed the following tools:

- [Docker Desktop / Docker engine](https://docs.docker.com/install/)
- [Docker Compose](https://docs.docker.com/compose/install/)
- [Golang](https://golang.org/doc/install)
- [NodeJS LTS](https://nodejs.org/en/download/)
- [Sass](https://sass-lang.com/install)
- [modd](https://github.com/cortesi/modd)

Install the npm dependencies by running:

```bash
(cd docs/website && npm install)
(cd controller/ports/ui/asssets && npm install)
(cd directory-ui/ports/ui/asssets && npm install)
```

Make sure the TLS key files have the correct permissions to run the NLX components

```bash
./pki/fix-permissions.sh
```

Update the `/etc/hosts` file on your system:

<details>
  <summary>Show hosts</summary>

```
# NLX
127.0.0.1     directory.shared.nlx.local
127.0.0.1     txlog-api.directory.nlx.local

127.0.0.1     controller.organization-a.nlx.local
127.0.0.1     controller-api.organization-a.nlx.local
127.0.0.1     inway.organization-a.nlx.local
127.0.0.1     outway.organization-a.nlx.local
127.0.0.1     outway-2.organization-a.nlx.local
127.0.0.1     txlog-api.organization-a.nlx.local
127.0.0.1     manager.organization-a.nlx.local

127.0.0.1     controller.organization-b.nlx.local
127.0.0.1     controller-api.organization-b.nlx.local
127.0.0.1     inway.organization-b.nlx.local
127.0.0.1     auth.organization-b.nlx.local
127.0.0.1     txlog-api.organization-b.nlx.local
127.0.0.1     manager.organization-b.nlx.local

127.0.0.1     controller.organization-c.nlx.local
127.0.0.1     controller-api.organization-c.nlx.local
127.0.0.1     inway.organization-c.nlx.local
127.0.0.1     outway.organization-c.nlx.local
127.0.0.1     auth.organization-c.nlx.local
127.0.0.1     txlog-api.organization-c.nlx.local
127.0.0.1     manager.organization-c.nlx.local

::1           controller-api.organization-a.nlx.local
::1           inway.organization-a.nlx.local
::1           txlog-api.organization-a.nlx.local

::1           controller.organization-b.nlx.local
::1           controller-api.organization-b.nlx.local
::1           inway.organization-b.nlx.local
::1           auth.organization-b.nlx.local

::1           controller.organization-c.nlx.local
::1           controller-api.organization-c.nlx.local
::1           inway.organization-c.nlx.local
::1           auth.organization-c.nlx.local
```
</details>
</br>

Before you can start the services you are required to install [delve](https://github.com/go-delve/delve).
Run `go install github.com/go-delve/delve/cmd/dlv@latest`.

To start the services in development daemons with up-to-date databases, run: `./scripts/start-development.sh`. Make sure Docker is running.

During the starting routines of the services, you might see a few services erroring that are dependent on a service that has not yet been started.
This is expected behavior and will resolve itself within 5 seconds.

Services will reload automatically when the code changes and is saved.

Visit the Controllers:

- [Gemeente Stijns (Organization A)](http://controller.organization-a.nlx.local:3011) (HTTP: 3011)
- [RvRD (Organization B)](http://controller.organization-b.nlx.local:3021) (HTTP: 3021)
- [Vergunningsoftware BV (Organization C)](http://controller.organization-c.nlx.local:3031) (HTTP: 3031)

The Directory:

- [directory-ui](http://localhost:3001) (HTTP: 3001)

To start the documentation website locally:

- `(cd docs/website && npm start)` to run the [docs](http://localhost:3002) (HTTP: 3002)

## Deploying and releasing

The [CI system of GitLab](https://gitlab.com/commonground/nlx/nlx/pipelines) builds every push to the main branch and creates a release to Docker, tagging it with the short git commit hash.
When a release is successful, it also gets deployed to the acceptance environment.

When a git tag is pushed, GitLab builds and allows you to manually deploy it to the demo environment.

For branches prefixed with `review/` (f.e. `review/feature-name`), a review environment will automatically be created for a shared testing environment.

### Semantic Release

In order for Semantic Release to be able to create a Git tag and push the updated changelog, a GitLab Access Token should be created.

The token can be created via https://gitlab.com/commonground/nlx/fsc-nlx/-/settings/access_tokens

```text
Token name: semantic-release
Role: Maintainer
Scopes: api, write_repository
```

Create a GitLab CI/CD variable named 'GITLAB_TOKEN' via https://gitlab.com/commonground/nlx/fsc-nlx/-/settings/ci_cd using the generated Access Token as the value.

Please don't forget to store the Access Token in the VNG 1Password account.

## Live environments

There are multiple environments for NLX

- `acceptance`: follows the main branch automatically
- `demo`, `pre-production` and `production`: updated after manually triggering a release

Overview of available links per environment:

- [links.demo.nlx.io](https://links.demo.nlx.io)
- [links.acc.nlx.io](https://links.acc.nlx.io)


## Generated files

Te execute the commands in the following sections, you will need to install [Earthly](https://earthly.dev/get-earthly).

### Proto

gRPC protobuf files can be regenerated using:

```shell
earthly +proto
```

You might want to regenerate the mocks after recompiling the protobuf files.

### Mocks

Mocks can be regenerated using:

```shell
earthly +mocks
```

### Sqlc

Sqlc can be regenerated using:

```shell
earthly +sqlc
```

### OpenAPI Specifications

OAS can be regenerated using:

```shell
earthly +oas
```

### All tasks

If you want to regenerate all files, use the command:

```shell
earthly +all
```

## SOPS

Sops will encrypt your local value using the public key of the cluster.
This encrypted value can only be decrypted using the private key which is on the cluster.

brew install sops
brew install gpg

git clone git@gitlab.com:commonground/haven/internal/configuration.git
cd configuration/flux
gpg --import .sops.pub.asc

Let's add a secret that we want to encrypt:

`secret.yml`

```yaml
apiVersion: v1
kind: Secret
metadata:
    name: gitlab-agent-token
    namespace: gitlab-agent-nlx
type: Opaque
data:
    token: <insert-value-here>
```

Example token value:

```yaml
tls.crt: foo
tls.key: bar
```

Encrypt the token

```shell
sops -e -i azure-core-prod/fsc-nlx-pre-production/internal-ca-issuer-secret.yaml
```

Let's generate a new Key Pair.

cd pki
sh init.sh

open pki/internal/directory/ca

copy contents of intermediate-1.pem to a file
add contents of root-1.pem to that same file

encode the content of that file

cat the-file.txt | base64

add the encoded contet to the secret property `tls.crt`:

For the key, only copy the contents of the intermediate key the `tls.key` property

copy contents of intermediate-1.key.pem

## Further documentation

* [Technical notes](technical-docs/notes.md)
* [Official documentation website](https://docs.nlx.io)

## License

Copyright © VNG Realisatie 2017

[Licensed under the EUPL](LICENCE.md)
