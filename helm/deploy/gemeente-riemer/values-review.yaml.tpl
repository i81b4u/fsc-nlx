###########
## Chart ##
###########
postgresql:
  storageSize: 256Mi

outway:
  ingress:
    enabled: true
    host: nlx-outway-gemeente-riemer-{{DOMAIN_SUFFIX}}

################
## Sub-charts ##
################
fsc-nlx-manager:
  config:
    selfAddress: "https://gemeente-riemer-nlx-manager:8443"
    directoryPeerManagerAddress: "https://gemeente-riemer-fsc-nlx-manager-external:8443"

fsc-nlx-controller:
  config:
    directoryAddress: "https://gemeente-riemer-fsc-nlx-manager-external:8443"
  ingress:
    hosts:
      - "nlx-controller-gemeente-riemer-{{DOMAIN_SUFFIX}}"
