###########
## Chart ##
###########
postgresql:
  storageSize: 256Mi

outway:
  ingress:
    enabled: true
    host: nlx-outway-gemeente-stijns-{{DOMAIN_SUFFIX}}

outway-2:
  ingress:
    enabled: true
    host: nlx-outway-2-gemeente-stijns-{{DOMAIN_SUFFIX}}

################
## Sub-charts ##
################
fsc-nlx-manager:
  config:
    selfAddress: "https://gemeente-stijns-nlx-manager:8443"
    directoryPeerManagerAddress: "https://gemeente-stijns-fsc-nlx-manager-external:8443"

fsc-nlx-controller:
  config:
    directoryAddress: "https://gemeente-stijns-fsc-nlx-manager-external:8443"
  ingress:
    hosts:
      - nlx-controller-gemeente-stijns-{{DOMAIN_SUFFIX}}

parkeerrechten-api:
  enabled: true
