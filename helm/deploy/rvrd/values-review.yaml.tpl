###########
## Chart ##
###########
postgresql:
  storageSize: 256Mi

opa:
  enabled: true

################
## Sub-charts ##
################
fsc-nlx-manager:
  config:
    selfAddress: "https://rvrd-nlx-manager:8443"
    directoryPeerManagerAddress: "https://shared-fsc-nlx-manager-external:8443"

fsc-nlx-controller:
  config:
    directoryAddress: "https://shared-fsc-nlx-manager-external:8443"
  ingress:
    hosts:
      - "nlx-controller-rvrd-{{DOMAIN_SUFFIX}}"
