###########
## Chart ##
###########
postgresql:
  storageSize: 256Mi

outway:
  ingress:
    enabled: true
    host: outway-vgs-bv-{{DOMAIN_SUFFIX}}

opa:
  enabled: true

################
## Sub-charts ##
################
fsc-nlx-manager:
  config:
    selfAddress: "https://vgs-bv-nlx-manager:8443"
    directoryPeerManagerAddress: "https://vgs-bv-fsc-nlx-manager-external:8443"

fsc-nlx-controller:
  config:
    directoryAddress: "https://vgs-bv-fsc-nlx-manager-external:8443"
  ingress:
    hosts:
      - "nlx-controller-vgs-bv-{{DOMAIN_SUFFIX}}"

parkeerrechten-admin-fsc:
  enabled: true
  organizationName: "Vergunningsoftware BV"
  municipalities: "Stijns"
  outwayAddress: "http://vergunningsoftware-bv-nlx-outway"
  ingress:
    enabled: true
    hosts:
      # abbreviated name, because https://gitlab.com/commonground/nlx/fsc-nlx/-/blob/main/technical-docs/notes.md#1215-rename-current-organizations
      - nlx-pa-fsc-vgs-bv-{{DOMAIN_SUFFIX}}
