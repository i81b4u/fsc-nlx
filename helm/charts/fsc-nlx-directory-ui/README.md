# nlx-directory-ui

The NLX Directory provides an overview of the Peers who are participant in an NLX Group.

## Prerequisites

- Kubernetes 1.11+

## Installing the Chart

To install the Chart with the release name `nlx-directory-ui`:

```console
## add the Common Ground Helm repository
$ helm repo add commonground https://charts.commonground.nl

## Install the nlx-directory-ui helm Chart
$ helm install nlx-directory-ui commonground/nlx-directory-ui
```

> **Tip**: List all releases using `helm list`

## Upgrading the Chart

Currently, our Helm charts use the same release version as the FSC NLX release version.
To know what has changed for the Helm charts, look at the changes in our [CHANGELOG](https://gitlab.com/commonground/nlx/fsc-nlx/-/blob/main/CHANGELOG.md)
that are prefixed with 'Helm'.

## Uninstalling the Chart

To uninstall or delete the `nlx-directory-ui` deployment:

```console
$ helm delete nlx-directory-ui
```

## Parameters

The following table lists the configurable parameters of the nlx-directory Chart and its default values.

### Global parameters

| Parameter                                    | Description                                                                                        | Default | Required (yes/no) |
|----------------------------------------------|----------------------------------------------------------------------------------------------------|---------|-------------------|
| `global.imageRegistry`                       | Global Docker Image registry                                                                       | `nil`   | no                |
| `global.imageTag`                            | Global Docker Image tag                                                                            | `true`  | no                |
| `global.certificates.group.caCertificatePEM` | Global Group CA Certificate. If not set the value of `certificates.group.caCertificatePEM` is used | `nil`   | no                |

### Common parameters

| Parameter          | Description                   | Default | Required (yes/no) |
|--------------------|-------------------------------|---------|-------------------|
| `nameOverride`     | Override deployment name      | `""`    | no                |
| `fullnameOverride` | Override full deployment name | `""`    | no                |

### NLX directory parameters

| Parameter                             | Description                                                                                                                                                        | Default | Required (yes/no) |
|---------------------------------------|--------------------------------------------------------------------------------------------------------------------------------------------------------------------|---------|-------------------|
| `config.logType`                      | Possible values: **live**, **local**. Affects the log output. See NewProduction and NewDevelopment at https://godoc.org/go.uber.org/zap#Logger.                    | `live`  | no                |
| `config.logLevel`                     | Possible values: **debug**, **warn**, **info**. Override the default loglevel set by `config.logType`                                                              | `info`  | no                |

### Deployment parameters

| Parameter                     | Description                                                                                                                                                                         | Default                         | Required (yes/no) |
|-------------------------------|-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|---------------------------------|-------------------|
| `image.registry`              | Image registry (ignored if `global.imageRegistry` is set)                                                                                                                           | `docker.io`                     | no                |
| `image.repository`            | Image repository                                                                                                                                                                    | `fscnlx/directory-api`          | no                |
| `image.monitorRepository`     | Image repository                                                                                                                                                                    | `fscnlx/directory-monitor`      | no                |
| `image.tag`                   | Image tag (ignored if `global.imageTag` is set). When set to null, the AppVersion from the Chart is used                                                                            | `The appVersion from the chart` | no                |
| `image.pullPolicy`            | Image pull policy                                                                                                                                                                   | `Always`                        | no                |
| `image.pullSecrets`           | Secrets for the image repository                                                                                                                                                    | `[]`                            | no                |
| `affinity`                    | Node affinity for pod assignment                                                                                                                                                    | `{}`                            | no                |
| `nodeSelector`                | Node labels for pod assignment                                                                                                                                                      | `{}`                            | no                |
| `replicaCount`                | Number of management replicas                                                                                                                                                       | `1`                             | no                |
| `resources`                   | Pod resource requests & limits                                                                                                                                                      | `{}`                            | no                |
| `tolerations`                 | Node tolerations for pod assignment                                                                                                                                                 | `[]`                            | no                |
| `serviceAccount.create`       | If `true`, create a new service account                                                                                                                                             | `true`                          | no                |
| `serviceAccount.name`         | Service account to be used. If not set and `serviceAccount.create` is `true`, a name is generated using the fullName template                                                       | `""`                            | no                |
| `serviceAccount.annotations`  | Annotations to add to the service account                                                                                                                                           | `{}`                            | no                |
| `securityContext`             | Optional security context. The YAML block should adhere to the [SecurityContext spec](https://kubernetes.io/docs/reference/generated/kubernetes-api/v1.16/#securitycontext-v1-core) | `{}`                            | no                |
| `podSecuritiyContext.fsGroup` | Group ID under which the pod should be started                                                                                                                                      | `1001`                          | no                |

### NLX TLS parameters

| Parameter                             | Description                                                                                                                                                   | Default | Required (yes/no)                |
|---------------------------------------|---------------------------------------------------------------------------------------------------------------------------------------------------------------|---------|----------------------------------|
| `certificates.group.caCertificatePEM` | The CA certificate of the Group                                                                                                                               | `""`    | yes (if global value is not set) |
| `certificates.group.certificatePEM`   | The Group certificate                                                                                                                                         | `""`    | yes (if global value is not set) |
| `certificates.group.keyPEM`           | Private Key of `certificates.group.certificatePEM`                                                                                                            | `""`    | yes (if global value is not set) |
| `certificates.group.existingSecret`   | Use existing secret with your NLX keypair (`certificates.group.certificatePEM` and `certificates.group.keyPEM` will be ignored and picked up from the secret) | `""`    | x                                |

### Ingress parameters

| Parameter             | Description               | Default               | Required (yes/no) |
|-----------------------|---------------------------|-----------------------|-------------------|
| `ingress.class`       | Ingress class             | `""`                  | no                |
| `ingress.annotations` | Ingress annotations       | `{}`                  | no                |
| `ingress.hosts`       | Ingress accepted hostname | `chart-example.local` | no                |
| `ingress.tls`         | Ingress TLS configuration | `[]`                  | no                |

### Exposure parameters

| Parameter             | Description                                         | Default     | Required (yes/no) |
|-----------------------|-----------------------------------------------------|-------------|-------------------|
| `service.type`        | Service type (ClusterIP, NodePort or LoadBalancer)  | `ClusterIP` | no                |
| `service.port`        | Port exposed by the service for the Directory API   | `443`       | no                |
| `service.annotations` | Annotations for directory API                       | `{}`        | no                |

Specify each parameter using the `--set key=value[,key=value]` argument to `helm install`.

Alternatively, a YAML file that specifies the values for the above parameters can be provided while installing the chart.

```console
$ helm install nlx-directory-ui -f values.yaml .
```
> **Tip**: You can use the default [values.yaml](https://gitlab.com/commonground/nlx/fsc-nlx/blob/main/helm/charts/nlx-directory/values.yaml)
