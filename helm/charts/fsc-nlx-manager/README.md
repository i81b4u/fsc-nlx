# Manager

FSC NLX Manager.

## Prerequisites

-   Kubernetes 1.11+

## Installing the Chart

To install the Chart with the release name `manager`:

```console
## add the Common Ground Helm repository
$ helm repo add commonground https://charts.commonground.nl

## Install the nlx-manager helm Chart
$ helm install manager commonground/nlx-manager
```

> **Tip**: List all releases using `helm list`

## Upgrading the Chart

Currently, our Helm charts use the same release version as the NLX release version.
To know what has changed for the Helm charts, look at the changes in our [CHANGELOG](https://gitlab.com/commonground/nlx/fsc-nlx/-/blob/main/CHANGELOG.md)
that are prefixed with 'Helm'.

## Uninstalling the Chart

To uninstall or delete the `manager` deployment:

```console
$ helm delete manager
```

## Parameters

The following table lists the configurable parameters of the nlx-manager Chart and its default values.

### Global parameters

| Parameter                                       | Description                                                                                                          | Default | Required (yes/no) |
| ----------------------------------------------- | -------------------------------------------------------------------------------------------------------------------- | ------- | ----------------- |
| `global.imageRegistry`                          | Global Docker Image registry                                                                                         | `nil`   | no                |
| `global.imageTag`                               | Global Docker Image tag                                                                                              | `true`  | no                |
| `global.certificates.group.caCertificatePEM`    | Global CA certificate of the NLX Group. If not set the value of `certificates.group.caCertificatePEM` is used        | `nil`   | no                |
| `global.certificates.internal.caCertificatePEM` | Global CA certificate of your internal PKI. If not set the value of `certificates.internal.caCertificatePEM` is used | `nil`   | no                |

### Common parameters

| Parameter          | Description                   | Default | Required (yes/no) |
| ------------------ | ----------------------------- | ------- | ----------------- |
| `nameOverride`     | Override deployment name      | `""`    | no                |
| `fullnameOverride` | Override full deployment name | `""`    | no                |

### Deployment parameters

| Parameter                     | Description                                                                                                                                                                         | Default                         | Required (yes/no) |
|-------------------------------|-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|---------------------------------|-------------------|
| `image.registry`              | Image registry (ignored if `global.imageRegistry` is set)                                                                                                                           | `docker.io`                     | no                |
| `image.repository`            | Image repository for the txlog API                                                                                                                                                  | `fscnlx/manager`                | no                |
| `image.tag`                   | Image tag (ignored if `global.imageTag` is set). When set to null, the AppVersion from the Chart is used                                                                            | `The appVersion from the chart` | no                |
| `image.pullPolicy`            | Image pull policy                                                                                                                                                                   | `Always`                        | no                |
| `image.pullSecrets`           | Secrets for the image repository                                                                                                                                                    | `[]`                            | no                |
| `affinity`                    | Node affinity for pod assignment                                                                                                                                                    | `{}`                            | no                |
| `nodeSelector`                | Node labels for pod assignment                                                                                                                                                      | `{}`                            | no                |
| `replicaCount`                | Number of management replicas                                                                                                                                                       | `1`                             | no                |
| `resources`                   | Pod resource requests & limits                                                                                                                                                      | `{}`                            | no                |
| `tolerations`                 | Node tolerations for pod assignment                                                                                                                                                 | `[]`                            | no                |
| `serviceAccount.create`       | If `true`, create a new service account                                                                                                                                             | `true`                          | no                |
| `serviceAccount.name`         | Service account to be used. If not set and `serviceAccount.create` is `true`, a name is generated using the fullname template                                                       | `""`                            | no                |
| `serviceAccount.annotations`  | Annotations to add to the service account                                                                                                                                           | `{}`                            | no                |
| `securityContext`             | Optional security context. The YAML block should adhere to the [SecurityContext spec](https://kubernetes.io/docs/reference/generated/kubernetes-api/v1.16/#securitycontext-v1-core) | `{}`                            | no                |
| `podSecuritiyContext.fsGroup` | Group ID under which the pod should be started                                                                                                                                      | `1001`                          | no                |

### NLX Manager parameters

| Parameter                            | Description                                                                                                                                     | Default  | Required (yes/no) |
|--------------------------------------|-------------------------------------------------------------------------------------------------------------------------------------------------|----------| ----------------- |
| `config.logType`                     | Possible values: **live**, **local**. Affects the log output. See NewProduction and NewDevelopment at https://godoc.org/go.uber.org/zap#Logger. | `live`   | no                |
| `config.logLevel`                    | Possible values: **debug**, **warn**, **info**. Override the default loglevel set by `config.logType`                                           | `info`   | no                |
| `config.selfAddres`                  | The self address of the Manager                                                                                                                 | `""`     | yes               |
| `config.controllerAPIAddress`        | The address of the Controller API                                                                                                               | `""`     | yes               |
| `config.txLogAPIAddress`             | The address of the Transaction Log API                                                                                                          | `""`     | yes               |
| `config.directoryPeerID`             | The ID of the Peer acting as the Directory                                                                                                      | `""`     | yes               |
| `config.directoryPeerManagerAddress` | The address of the Manager acting as the Directory                                                                                              | `""`     | yes               |
| `config.autoSignGrants`              | The contracts containing grants which should be signed automatically.                                                                           | `[]`     | no                |

### Certificates parameters

Certificates used by the Manage component.

| Parameter                                     | Description                                                                                                                                                                    | Default | Required (yes/no)                                                |
| --------------------------------------------- | ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------ | ------- | ---------------------------------------------------------------- |
| `certificates.internal.caCertificatePEM`      | The CA certificate of your internal PKI                                                                                                                                        | `""`    | yes (if global value is not set)                                 |
| `certificates.internal.certificatePEM`        | The certificate signed by your internal PKI                                                                                                                                    | `""`    | yes                                                              |
| `certificates.internal.keyPEM`                | The private key of `certificates.internal.certificatePEM`                                                                                                                      | `""`    | yes                                                              |
| `certificates.internal.existingSecret`        | Use existing secret with your keypair (`certificates.internal.certificatePEM` and `certificates.internal.keyPEM` will be ignored and picked up from this secret)               | `""`    | no                                                               |
| `certificates.group.caCertificatePEM`         | The CA certificate of the Group                                                                                                                                                | `""`    | yes (if global value is not set)                                 |
| `certificates.group.peer.certificatePEM`      | The certificate signed by the Group CA                                                                                                                                         | `""`    | yes                                                              |
| `certificates.group.peer.keyPEM`              | The private key of `tls.peer.certificatePEM`                                                                                                                                   | `""`    | yes                                                              |
| `certificates.group.peer.existingSecret`      | Use existing secret with your keypair (`certificates.group.peer.certificatePEM` and `certificates.group.peer.keyPEM` will be ignored and picked up from this secret)           | `""`    | no                                                               |
| `certificates.group.token.certificatePEM`     | The certificate used to sign access tokens                                                                                                                                     | `""`    | yes (if config.certificates.token.existingSecret is not set)     |
| `certificates.group.token.keyPEM`             | The private key of `certificates.group.token.certificatePEM`                                                                                                                   | `""`    | yes (if config.certificates.token.existingSecret is not set)     |
| `certificates.group.token.existingSecret`     | Use existing secret with your keypair (`certificates.group.token.certificatePEM` and `certificates.group.token.keyPEM` will be ignored and picked up from this secret)         | `""`    | no                                                               |
| `certificates.group.signature.certificatePEM` | The certificate used to create signatures for Contracts                                                                                                                        | `""`    | yes (if config.certificates.signature.existingSecret is not set) |
| `certificates.group.signature.keyPEM`         | The private key of `certificates.group.signature.certificatePEM`                                                                                                               | `""`    | yes (if config.certificates.signature.existingSecret is not set) |
| `certificates.group.signature.existingSecret` | Use existing secret with your keypair (`certificates.group.signature.certificatePEM` and `certificates.group.signature.keyPEM` will be ignored and picked up from this secret) | `""`    | no                                                               |

### NLX Manager PostgreSQL parameters

| Parameter                               | Description                                                                                                                               | Default            | Required (yes/no)                              |
| --------------------------------------- | ----------------------------------------------------------------------------------------------------------------------------------------- | ------------------ | ---------------------------------------------- |
| `postgresql.hostname`                   | PostgreSQL hostname                                                                                                                       | `"postgresql"`     | yes                                            |
| `postgresql.connectTimeout`             | The connection timeout for PostgreSQL                                                                                                     | `10`               | no                                             |
| `postgresql.port`                       | PostgreSQL port                                                                                                                           | `5432`             | yes                                            |
| `postgresql.sslMode`                    | PostgreSQL SSL mode                                                                                                                       | `required`         | yes                                            |
| `postgresql.database`                   | PostgreSQL database                                                                                                                       | `"nlx_management"` | yes                                            |
| `postgresql.username`                   | PostgreSQL username. Will be stored in a Kubernetes secret                                                                                | `""`               | yes (if not using `postgresql.existingSecret`) |
| `postgresql.password`                   | PostgreSQL password. Will be stored in a Kubernetes secret                                                                                | `""`               | yes (if not using `postgresql.existingSecret`) |
| `postgresql.existingSecret.name`        | Use existing secret for password details (`postgresql.username` and `postgresql.password` will be ignored and picked up from this secret) | `""`               | no                                             |
| `postgresql.existingSecret.usernameKey` | Key for username value in aforementioned existingSecret                                                                                   | `username`         | no                                             |
| `postgresql.existingSecret.passwordKey` | Key for password value in aforementioned existingSecret                                                                                   | `password`         | no                                             |

Specify each parameter using the `--set key=value[,key=value]` argument to `helm install`.

Alternatively, a YAML file that specifies the values for the above parameters can be provided while installing the chart.

```console
$ helm install manager -f values.yaml .
```

> **Tip**: You can use the default [values.yaml](https://gitlab.com/commonground/nlx/fsc-nlx/blob/main/helm/charts/nlx-manager/values.yaml)
