{
  "$schema": "http://json-schema.org/schema#",
  "type": "object",
  "properties": {
    "affinity": {
      "type": "object",
      "description": "Node affinity for pod assignment"
    },
    "config": {
      "type": "object",
      "properties": {
        "authorizationService": {
          "type": "object",
          "properties": {
            "enabled": {
              "type": "boolean"
            },
            "url": {
              "type": "string"
            }
          }
        },
        "managerInternalAddress": {
          "type": "string",
          "description": "Internal address of the Manager"
        },
        "name": {
          "type": "string",
          "description": "Unique identifier of this Outway."
        },
        "logLevel": {
          "type": "string",
          "description": "Override the default loglevel set by config.logType",
          "enum": ["debug", "warn", "info"]
        },
        "logType": {
          "type": "string",
          "description": "Affects the log output. See NewProduction and NewDevelopment at https://godoc.org/go.uber.org/zap#Logger.",
          "enum": ["live", "local"]
        },
        "controllerApiAddress": {
          "type": "string",
          "description": "The config address of the Controller API."
        },
        "transactionLogApiAddress": {
          "type": "string",
          "description": "The address of the Transaction Log API"
        }
      },
      "required": [
        "name",
        "managerInternalAddress",
        "controllerApiAddress",
        "transactionLogApiAddress"
      ]
    },
    "fullnameOverride": {
      "type": "string",
      "description": "Override full deployment name"
    },
    "global": {
      "type": "object",
      "properties": {
        "imageRegistry": {
          "type": "string",
          "name": "Global Docker Image registry"
        },
        "imageTag": {
          "type": "string",
          "description": "Global Docker Image tag"
        },
        "certificates": {
          "type": "object",
          "properties": {
            "internal": {
              "type": "object",
              "properties": {
                "caCertificatePEM": {
                  "type": "string",
                  "description": "Global CA certificate of your internal PKI. If not set the value of certificates.internal.caCertificatePEM is used"
                }
              }
            },
            "group": {
              "type": "object",
              "properties": {
                "caCertificatePEM": {
                  "type": "string",
                  "description": "Global NLX CA certificate. If not set the value of certificates.group.caCertificatePEM is used"
                }
              }
            }
          }
        }
      }
    },
    "https": {
      "type": "object",
      "properties": {
        "certificatePEM": {
          "type": "string",
          "description": "TLS certificate as PEM. Required if https.enabled is true\t"
        },
        "enabled": {
          "type": "boolean",
          "description": "If true, HTTPs will be enabled"
        },
        "keyPEM": {
          "type": "string",
          "description": "Private key of https.certificatePEM as PEM. Required if https.enabled is true"
        }
      }
    },
    "image": {
      "type": "object",
      "properties": {
        "pullPolicy": {
          "type": "string",
          "description": "Image pull policy",
          "enum": ["IfNotPresent", "Never", "Always"]
        },
        "pullSecrets": {
          "type": "array",
          "description": "Secrets for the image repository"
        },
        "registry": {
          "type": "string",
          "description": "Image registry (ignored if global.imageRegistry is set)"
        },
        "repository": {
          "type": "string",
          "description": "Image repository"
        },
        "tag": {
          "type": "string",
          "description": "Image tag (ignored if global.imageTag is set). When set to null, the AppVersion from the Chart is used"
        }
      }
    },
    "nameOverride": {
      "type": "string",
      "description": "Override deployment name"
    },
    "nodeSelector": {
      "type": "object",
      "description": "Node labels for pod assignment"
    },
    "podSecurityContext": {
      "type": "object",
      "properties": {
        "fsGroup": {
          "type": "integer",
          "description": "Group ID under which the pod should be started"
        }
      }
    },
    "replicaCount": {
      "type": "integer",
      "description": "Number of management replicas"
    },
    "resources": {
      "type": "object",
      "description": "Pod resource requests & limits"
    },
    "securityContext": {
      "type": "object",
      "description": "Optional security context. The YAML block should adhere to the SecurityContext spec"
    },
    "service": {
      "type": "object",
      "properties": {
        "httpPort": {
          "type": "integer",
          "description": "Port exposed by the Outway service"
        },
        "httpsPort": {
          "type": "integer",
          "description": "Port exposed by the Outway service if https.enabled is true"
        },
        "type": {
          "type": "string",
          "description": "Service type",
          "enum": ["ClusterIP", "NodePort", "LoadBalancer"]
        }
      }
    },
    "serviceAccount": {
      "type": "object",
      "properties": {
        "annotations": {
          "type": "object",
          "description": "Annotations to add to the service account"
        },
        "create": {
          "type": "boolean",
          "description": "If true, create a new service account"
        },
        "name": {
          "type": "string",
          "description": "Service account to be used. If not set and serviceAccount.create is true, a name is generated using the fullname template"
        }
      }
    },
    "certificates": {
      "type": "object",
      "properties": {
        "internal": {
          "type": "object",
          "properties": {
            "certificatePEM": {
              "type": "string",
              "description": "The certificate signed by your internal PKI"
            },
            "existingSecret": {
              "type": "string",
              "description": "The private key of tls.internal.certificatePEM"
            },
            "keyPEM": {
              "type": "string",
              "description": "Use existing secret with your NLX keypair (certificates.internal.certificatePEM and certificates.internal.keyPEM will be ignored and picked up from this secret)"
            },
            "caCertificatePEM": {
              "type": "string",
              "description": "The CA certificate of your internal PKI"
            }
          },
          "required": ["certificatePEM", "keyPEM"]
        },
        "group": {
          "type": "object",
          "properties": {
            "certificatePEM": {
              "type": "string",
              "description": "Your NLX certificate"
            },
            "existingSecret": {
              "type": "string",
              "description": "Use existing secret with your NLX keypair (certificates.group.certificatePEM and certificates.group.keyPEM will be ignored and picked up from the secret)"
            },
            "keyPEM": {
              "type": "string",
              "description": "The private key of certificates.group.certificatePEM"
            },
            "caCertificatePEM": {
              "type": "string",
              "description": "The NLX CA certificate"
            }
          },
          "required": ["certificatePEM", "keyPEM"]
        }
      }
    },
    "tolerations": {
      "type": "array",
      "description": "Node tolerations for pod assignment"
    }
  }
}
