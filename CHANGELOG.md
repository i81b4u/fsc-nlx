## [0.2.2](https://gitlab.com/commonground/nlx/fsc-nlx/compare/v0.2.1...v0.2.2) (2023-09-15)

## [0.2.1](https://gitlab.com/commonground/nlx/fsc-nlx/compare/v0.2.0...v0.2.1) (2023-09-15)

# [0.2.0](https://gitlab.com/commonground/nlx/fsc-nlx/compare/v0.1.1...v0.2.0) (2023-09-14)


### Features

* **directory:** remove preprod and prod options from the Directory UI filter list ([86e36e5](https://gitlab.com/commonground/nlx/fsc-nlx/commit/86e36e5bf24651503ff9cfe4091932fb88622799)), closes [fsc-nlx#24](https://gitlab.com/fsc-nlx/issues/24)
* **directory:** update NLX content to reflect FSC NLX ([82d4a5e](https://gitlab.com/commonground/nlx/fsc-nlx/commit/82d4a5e2ad0c4bf6eb56b42e62d73b86586df033)), closes [fsc-nlx#24](https://gitlab.com/fsc-nlx/issues/24)

## [0.1.1](https://gitlab.com/commonground/nlx/fsc-nlx/compare/v0.1.0...v0.1.1) (2023-09-13)

# [0.1.0](https://gitlab.com/commonground/nlx/fsc-nlx/compare/v0.0.5...v0.1.0) (2023-09-12)


### Features

* rename listen-address to listen-address-ui in controller ([5faf247](https://gitlab.com/commonground/nlx/fsc-nlx/commit/5faf247dcaa39b2ca5d1ac0d2e227e1475b96882)), closes [fsc-nlx#12](https://gitlab.com/fsc-nlx/issues/12)


### Reverts

* update node.js to v20.6.0 ([803abd9](https://gitlab.com/commonground/nlx/fsc-nlx/commit/803abd9c9ab9283f63438e196c0d95b8f87c9c5b)), closes [nlx#1818](https://gitlab.com/nlx/issues/1818)

## [0.0.5](https://gitlab.com/commonground/nlx/fsc-nlx/compare/v0.0.4...v0.0.5) (2023-09-08)


### Bug Fixes

* **helm:** add fsc- prefix to hostnames for the Demo deployment ([7f718a8](https://gitlab.com/commonground/nlx/fsc-nlx/commit/7f718a8297887655aee726b976844d5effda758a)), closes [nlx#1818](https://gitlab.com/nlx/issues/1818)
* **manager:** show message if Directory Peer Manager address is not passed to Manager ([13756fc](https://gitlab.com/commonground/nlx/fsc-nlx/commit/13756fc37ac462f47d9ede167b5a13f1dda911de)), closes [nlx#1818](https://gitlab.com/nlx/issues/1818)
* **manager:** show message if Self Address is not passed to Manager ([cbb6693](https://gitlab.com/commonground/nlx/fsc-nlx/commit/cbb6693557b7f6c49322cdc85b3ae00a3f756ff5)), closes [nlx#1818](https://gitlab.com/nlx/issues/1818)

## [0.0.4](https://gitlab.com/commonground/nlx/fsc-nlx/compare/v0.0.3...v0.0.4) (2023-09-07)

## [0.0.3](https://gitlab.com/commonground/nlx/fsc-nlx/compare/v0.0.2...v0.0.3) (2023-09-07)

## [0.0.2](https://gitlab.com/commonground/nlx/fsc-nlx/compare/v0.0.1...v0.0.2) (2023-09-07)

## [0.0.1](https://gitlab.com/commonground/nlx/fsc-nlx/compare/v0.0.0...v0.0.1) (2023-09-07)
