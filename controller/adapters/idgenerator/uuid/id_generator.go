// Copyright © VNG Realisatie 2023
// Licensed under the EUPL

package uuidgenerator

import (
	"fmt"

	"github.com/google/uuid"
)

type IDGenerator struct{}

func (s *IDGenerator) New() ([]byte, error) {
	uuidBytes, err := uuid.New().MarshalBinary()
	if err != nil {
		return nil, fmt.Errorf("%w: could not get bytes from uuid", err)
	}

	return uuidBytes, nil
}

func New() *IDGenerator {
	return &IDGenerator{}
}
