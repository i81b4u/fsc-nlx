// Copyright © VNG Realisatie 2023
// Licensed under the EUPL

package postgresstorage

import (
	"context"
	"fmt"

	"go.nlx.io/nlx/controller/adapters/storage"
	"go.nlx.io/nlx/controller/adapters/storage/postgres/queries"
)

func (p *PostgresStorage) CreateService(ctx context.Context, args *storage.CreateServiceArgs) error {
	err := p.queries.CreateService(ctx, &queries.CreateServiceParams{
		GroupID:      args.GroupID,
		Name:         args.Name,
		EndpointUrl:  args.EndpointURL,
		InwayAddress: args.InwayAddress,
	})
	if err != nil {
		return fmt.Errorf("could not create service in database: %w", err)
	}

	return nil
}
