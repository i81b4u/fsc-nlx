// Copyright © VNG Realisatie 2023
// Licensed under the EUPL

package postgresstorage

import (
	"context"
	"fmt"

	"go.nlx.io/nlx/controller/adapters/storage/postgres/queries"
)

func (p *PostgresStorage) RegisterOutway(ctx context.Context, groupID, name, certificateThumbprint string) error {
	err := p.queries.RegisterOutway(ctx, &queries.RegisterOutwayParams{
		GroupID:               groupID,
		Name:                  name,
		CertificateThumbprint: certificateThumbprint,
	})
	if err != nil {
		return fmt.Errorf("could not register outway in database: %w", err)
	}

	return nil
}
