// Copyright © VNG Realisatie 2023
// Licensed under the EUPL

package postgresstorage

import (
	"context"
	"fmt"
)

func (p *PostgresStorage) ListInwayAddresses(ctx context.Context, groupID string) ([]string, error) {
	rows, err := p.queries.ListInwayAddresses(ctx, groupID)
	if err != nil {
		return nil, fmt.Errorf("could not list inway addresses from database: %w", err)
	}

	return rows, nil
}
