-- Copyright © VNG Realisatie 2023
-- Licensed under the EUPL

-- name: RegisterOutway :exec
INSERT INTO controller.outways (
    group_id,
    name,
    certificate_thumbprint
)
VALUES ($1, $2, $3)
ON CONFLICT DO NOTHING;
