-- Copyright © VNG Realisatie 2023
-- Licensed under the EUPL

-- name: GetInwayAddressForService :one
SELECT
    s.inway_address
FROM
    controller.services as s
WHERE
    group_id = $1
    AND name = $2;
