-- Copyright © VNG Realisatie 2023
-- Licensed under the EUPL

-- name: ListOutwayThumbprints :many
SELECT
    name,
    certificate_thumbprint
FROM
    controller.outways
WHERE
    group_id = $1
GROUP BY
    certificate_thumbprint, name;
