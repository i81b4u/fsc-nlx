-- Copyright © VNG Realisatie 2023
-- Licensed under the EUPL

-- name: GetServiceEndpointURL :one
SELECT
    s.endpoint_url
FROM
    controller.services as s
WHERE
    group_id = $1
    AND name = $2
    AND inway_address = $3;
