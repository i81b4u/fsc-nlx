// Code generated by sqlc. DO NOT EDIT.
// versions:
//   sqlc v1.19.1
// source: list_outway_thumbprints.sql

package queries

import (
	"context"
)

const listOutwayThumbprints = `-- name: ListOutwayThumbprints :many

SELECT
    name,
    certificate_thumbprint
FROM
    controller.outways
WHERE
    group_id = $1
GROUP BY
    certificate_thumbprint, name
`

type ListOutwayThumbprintsRow struct {
	Name                  string
	CertificateThumbprint string
}

// Copyright © VNG Realisatie 2023
// Licensed under the EUPL
func (q *Queries) ListOutwayThumbprints(ctx context.Context, groupID string) ([]*ListOutwayThumbprintsRow, error) {
	rows, err := q.query(ctx, q.listOutwayThumbprintsStmt, listOutwayThumbprints, groupID)
	if err != nil {
		return nil, err
	}
	defer rows.Close()
	items := []*ListOutwayThumbprintsRow{}
	for rows.Next() {
		var i ListOutwayThumbprintsRow
		if err := rows.Scan(&i.Name, &i.CertificateThumbprint); err != nil {
			return nil, err
		}
		items = append(items, &i)
	}
	if err := rows.Close(); err != nil {
		return nil, err
	}
	if err := rows.Err(); err != nil {
		return nil, err
	}
	return items, nil
}
