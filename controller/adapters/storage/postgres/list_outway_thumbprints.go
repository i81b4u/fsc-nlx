// Copyright © VNG Realisatie 2023
// Licensed under the EUPL

package postgresstorage

import (
	"context"
	"fmt"
)

func (p *PostgresStorage) ListOutwayThumbprints(ctx context.Context, groupID string) (map[string][]string, error) {
	rows, err := p.queries.ListOutwayThumbprints(ctx, groupID)
	if err != nil {
		return nil, fmt.Errorf("could not list outway thumbprints from database: %w", err)
	}

	thumbprints := make(map[string][]string)

	for _, r := range rows {
		_, ok := thumbprints[r.CertificateThumbprint]
		if !ok {
			thumbprints[r.CertificateThumbprint] = make([]string, 0)
		}

		thumbprints[r.CertificateThumbprint] = append(thumbprints[r.CertificateThumbprint], r.Name)
	}

	return thumbprints, nil
}
