// Copyright © VNG Realisatie 2022
// Licensed under the EUPL

package postgresstorage

import (
	"context"
	"errors"
	"fmt"
	"sync"
	"time"

	"github.com/golang-migrate/migrate/v4"
	_ "github.com/golang-migrate/migrate/v4/database/postgres"
	"github.com/jmoiron/sqlx"
	_ "github.com/lib/pq" // postgres driver

	"go.nlx.io/nlx/controller/adapters/storage/postgres/migrations"
	"go.nlx.io/nlx/controller/adapters/storage/postgres/queries"
)

const migratorDriverName = "embed"

var registerDriverOnce sync.Once

type PostgresStorage struct {
	db      *sqlx.DB
	queries *queries.Queries
}

func New(db *sqlx.DB) (*PostgresStorage, error) {
	if db == nil {
		return nil, fmt.Errorf("db is required")
	}

	querier, err := queries.Prepare(context.Background(), db)
	if err != nil {
		return nil, err
	}

	return &PostgresStorage{
		db:      db,
		queries: querier,
	}, nil
}

func NewConnection(dsn string) (*sqlx.DB, error) {
	db, err := sqlx.Open("postgres", dsn)
	if err != nil {
		return nil, fmt.Errorf("could not open connection to postgres: %s", err)
	}

	const (
		FiveMinutes        = 5 * time.Minute
		MaxIdleConnections = 2
	)

	db.SetConnMaxLifetime(FiveMinutes)
	db.SetMaxIdleConns(MaxIdleConnections)

	return db, nil
}

func (p *PostgresStorage) Close() error {
	err := p.queries.Close()
	if err != nil {
		return err
	}

	return p.db.Close()
}

func setupMigrator(dsn string) (*migrate.Migrate, error) {
	registerDriverOnce.Do(func() {
		migrations.RegisterDriver(migratorDriverName)
	})

	return migrate.New(fmt.Sprintf("%s://", migratorDriverName), dsn)
}

func PerformMigrations(dsn string) error {
	migrator, err := setupMigrator(dsn)
	if err != nil {
		return err
	}

	err = migrator.Up()
	if err != nil && !errors.Is(err, migrate.ErrNoChange) {
		return fmt.Errorf("running migrations: %v", err)
	}

	return nil
}

func MigrationStatus(dsn string) (version uint, dirty bool, err error) {
	migrator, err := setupMigrator(dsn)
	if err != nil {
		return 0, false, err
	}

	return migrator.Version()
}
