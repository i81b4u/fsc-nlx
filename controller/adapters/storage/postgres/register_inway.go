// Copyright © VNG Realisatie 2023
// Licensed under the EUPL

package postgresstorage

import (
	"context"
	"fmt"
	"net/url"

	"go.nlx.io/nlx/controller/adapters/storage/postgres/queries"
)

func (p *PostgresStorage) RegisterInway(ctx context.Context, groupID, name string, address *url.URL) error {
	err := p.queries.RegisterInway(ctx, &queries.RegisterInwayParams{
		GroupID: groupID,
		Name:    name,
		Address: address.String(),
	})
	if err != nil {
		return fmt.Errorf("could not register inway in database: %w", err)
	}

	return nil
}
