// Copyright © VNG Realisatie 2023
// Licensed under the EUPL

package postgresstorage

import (
	"context"
	"fmt"
	"net/url"

	"go.nlx.io/nlx/controller/adapters/storage/postgres/queries"
)

func (p *PostgresStorage) GetServiceEndpointURL(ctx context.Context, groupID, serviceName, inwayAddress string) (*url.URL, error) {
	address, err := p.queries.GetServiceEndpointURL(ctx, &queries.GetServiceEndpointURLParams{
		GroupID:      groupID,
		Name:         serviceName,
		InwayAddress: inwayAddress,
	})
	if err != nil {
		return nil, fmt.Errorf("could not get service endpoint URL for inway address from database: %w", err)
	}

	u, err := url.Parse(address)
	if err != nil {
		return nil, fmt.Errorf("invalid endpoint URL in database: %w", err)
	}

	return u, nil
}
