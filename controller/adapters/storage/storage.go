// Copyright © VNG Realisatie 2023
// Licensed under the EUPL

package storage

import (
	"context"
	"net/url"
)

type Storage interface {
	// API
	ListServices(ctx context.Context, groupID string) (Services, error)
	CreateService(ctx context.Context, args *CreateServiceArgs) error

	// Internal
	RegisterInway(ctx context.Context, groupID, name string, address *url.URL) error
	RegisterOutway(ctx context.Context, groupID, name, certificateThumbprint string) error
	GetInwayAddressForService(ctx context.Context, groupID, serviceName string) (*url.URL, error)
	GetServiceEndpointURL(ctx context.Context, groupID, serviceName, inwayAddress string) (*url.URL, error)

	// UI
	ListInwayAddresses(ctx context.Context, groupID string) ([]string, error)
	ListOutwayThumbprints(ctx context.Context, groupID string) (map[string][]string, error)
}

type Services []*Service

type Service struct {
	Name         string
	EndpointURL  *url.URL
	InwayAddress string
}

type CreateServiceArgs struct {
	GroupID      string
	Name         string
	EndpointURL  string
	InwayAddress string
}
