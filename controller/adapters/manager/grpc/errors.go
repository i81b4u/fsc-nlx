// Copyright © VNG Realisatie 2023
// Licensed under the EUPL

package grpcmanager

import (
	"fmt"

	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"

	"go.nlx.io/nlx/controller/adapters/manager"
)

func mapError(err error, message string) error {
	st, ok := status.FromError(err)
	if !ok {
		return fmt.Errorf("%s: %s: %w", message, err, manager.ErrInternalError)
	}

	if st.Code() == codes.InvalidArgument {
		return fmt.Errorf("%s: %s: %w", message, manager.NewValidationError(st.Message()), err)
	}

	return fmt.Errorf("%s: %s: %w", message, err, manager.ErrInternalError)
}
