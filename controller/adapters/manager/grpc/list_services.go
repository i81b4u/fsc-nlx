// Copyright © VNG Realisatie 2023
// Licensed under the EUPL

package grpcmanager

import (
	"context"
	"fmt"

	"github.com/pkg/errors"

	"go.nlx.io/nlx/controller/adapters/manager"
	"go.nlx.io/nlx/manager/ports/int/grpc/api"
)

func (m *grpcManager) ListServices(ctx context.Context) (manager.Services, error) {
	resp, err := m.client.ListServices(ctx, &api.ListServicesRequest{})
	if err != nil {
		return nil, errors.Wrap(err, "could not list services from grpc manager")
	}

	services := make(manager.Services, len(resp.Services))

	for i, s := range resp.Services {
		switch service := s.Data.(type) {
		case *api.ListServicesResponse_Service_Service_:
			services[i] = &manager.Service{
				Name:     service.Service.Name,
				PeerID:   service.Service.Provider.Id,
				PeerName: service.Service.Provider.Name,
			}

		case *api.ListServicesResponse_Service_DelegatedService_:
			services[i] = &manager.DelegatedService{
				Name:             service.DelegatedService.Name,
				ProviderPeerID:   service.DelegatedService.Provider.Id,
				ProviderPeerName: service.DelegatedService.Provider.Name,
				DelegateeID:      service.DelegatedService.Delegator.Id,
				DelegateeName:    service.DelegatedService.Delegator.Name,
			}

		default:
			m.logger.Info(fmt.Sprintf("unknown service type %v", service))
		}
	}

	return services, nil
}
