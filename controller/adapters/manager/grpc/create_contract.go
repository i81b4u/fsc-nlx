// Copyright © VNG Realisatie 2023
// Licensed under the EUPL

package grpcmanager

import (
	"context"

	"go.nlx.io/nlx/controller/adapters/manager"
	"go.nlx.io/nlx/manager/ports/int/grpc/api"
)

func (m *grpcManager) CreateContract(ctx context.Context, args *manager.CreateContractArgs) error {
	grants := []*api.Grant{}

	peerRegistrationGrants := mapPeerRegistrationGrants(args.PeerRegistrationGrants)
	grants = append(grants, peerRegistrationGrants...)

	servicePublicationGrants := mapServicePublicationGrants(args.ServicePublicationGrants)
	grants = append(grants, servicePublicationGrants...)

	serviceConnectionGrants := mapServiceConnectionGrants(args.ServiceConnectionGrants)
	grants = append(grants, serviceConnectionGrants...)

	delegatedServiceConnectionGrants := mapDelegatedServiceConnectionGrants(args.DelegatedServiceConnectionGrants)
	grants = append(grants, delegatedServiceConnectionGrants...)

	delegatedServicePublicationGrants := mapDelegatedServicePublicationGrants(args.DelegatedServicePublicationGrants)
	grants = append(grants, delegatedServicePublicationGrants...)

	_, err := m.client.CreateContract(ctx, &api.CreateContractRequest{
		ContractContent: &api.ContractContent{
			Id:      args.ID,
			GroupId: args.GroupID,
			Validity: &api.Validity{
				NotBefore: args.ContractNotBefore.Unix(),
				NotAfter:  args.ContractNotAfter.Unix(),
			},
			Grants:        grants,
			HashAlgorithm: api.HashAlgorithm(args.HashAlgorithm),
			CreatedAt:     args.CreatedAt.Unix(),
		},
	})
	if err != nil {
		return mapError(err, "could not create contract in grpc manager")
	}

	return nil
}

func mapPeerRegistrationGrants(grants []*manager.PeerRegistrationGrant) []*api.Grant {
	result := []*api.Grant{}

	for _, grant := range grants {
		result = append(result, &api.Grant{
			Type: api.GrantType_GRANT_TYPE_PEER_REGISTRATION,
			Data: &api.Grant_PeerRegistration{
				PeerRegistration: &api.GrantPeerRegistration{
					Directory: &api.GrantPeerRegistration_Directory{
						PeerId: grant.PeerID,
					},
					Peer: &api.GrantPeerRegistration_Peer{
						Id:   grant.PeerID,
						Name: grant.PeerName,
					},
				},
			},
		})
	}

	return result
}

func mapServicePublicationGrants(grants []*manager.ServicePublicationGrant) []*api.Grant {
	result := []*api.Grant{}

	for _, grant := range grants {
		result = append(result, &api.Grant{
			Type: api.GrantType_GRANT_TYPE_SERVICE_PUBLICATION,
			Data: &api.Grant_ServicePublication{
				ServicePublication: &api.GrantServicePublication{
					Directory: &api.GrantServicePublication_Directory{
						PeerId: grant.DirectoryPeerID,
					},
					Service: &api.GrantServicePublication_Service{
						PeerId: grant.ServicePeerID,
						Name:   grant.ServiceName,
					},
				},
			},
		})
	}

	return result
}

func mapServiceConnectionGrants(grants []*manager.ServiceConnectionGrant) []*api.Grant {
	result := []*api.Grant{}

	for _, grant := range grants {
		result = append(result, &api.Grant{
			Type: api.GrantType_GRANT_TYPE_SERVICE_CONNECTION,
			Data: &api.Grant_ServiceConnection{
				ServiceConnection: &api.GrantServiceConnection{
					Outway: &api.GrantServiceConnection_Outway{
						PeerId:                grant.OutwayPeerID,
						CertificateThumbprint: grant.OutwayCertificateThumbprint,
					},
					Service: &api.GrantServiceConnection_Service{
						PeerId: grant.ServicePeerID,
						Name:   grant.ServiceName,
					},
				},
			},
		})
	}

	return result
}

func mapDelegatedServiceConnectionGrants(grants []*manager.DelegatedServiceConnectionGrant) []*api.Grant {
	result := []*api.Grant{}

	for _, grant := range grants {
		result = append(result, &api.Grant{
			Type: api.GrantType_GRANT_TYPE_DELEGATED_SERVICE_CONNECTION,
			Data: &api.Grant_DelegatedServiceConnection{
				DelegatedServiceConnection: &api.GrantDelegatedServiceConnection{
					Delegator: &api.GrantDelegatedServiceConnection_Delegator{PeerId: grant.DelegatorPeerID},
					Outway: &api.GrantDelegatedServiceConnection_Outway{
						PeerId:                grant.OutwayPeerID,
						CertificateThumbprint: grant.OutwayCertificateThumbprint,
					},
					Service: &api.GrantDelegatedServiceConnection_Service{
						PeerId: grant.ServicePeerID,
						Name:   grant.ServiceName,
					},
				},
			},
		})
	}

	return result
}

func mapDelegatedServicePublicationGrants(grants []*manager.DelegatedServicePublicationGrant) []*api.Grant {
	result := []*api.Grant{}

	for _, grant := range grants {
		result = append(result, &api.Grant{
			Type: api.GrantType_GRANT_TYPE_DELEGATED_SERVICE_PUBLICATION,
			Data: &api.Grant_DelegatedServicePublication{
				DelegatedServicePublication: &api.GrantDelegatedServicePublication{
					Directory: &api.GrantDelegatedServicePublication_Directory{
						PeerId: grant.DirectoryPeerID,
					},
					Service: &api.GrantDelegatedServicePublication_Service{
						PeerId: grant.ServicePeerID,
						Name:   grant.ServiceName,
					},
					Delegator: &api.GrantDelegatedServicePublication_Delegator{
						PeerId: grant.DelegatorPeerID,
					},
				},
			},
		})
	}

	return result
}
