// Copyright © VNG Realisatie 2023
// Licensed under the EUPL

package grpcmanager

import (
	"context"

	"github.com/pkg/errors"

	"go.nlx.io/nlx/controller/adapters/manager"
	"go.nlx.io/nlx/manager/ports/int/grpc/api"
)

func (m *grpcManager) GetPeerInfo(ctx context.Context) (*manager.Peer, error) {
	resp, err := m.client.GetPeerInfo(ctx, &api.GetPeerInfoRequest{})
	if err != nil {
		return nil, errors.Wrap(err, "could not get peer info from grpc manager")
	}

	return &manager.Peer{
		ID:   resp.Peer.Id,
		Name: resp.Peer.Name,
	}, nil
}
