// Copyright © VNG Realisatie 2023
// Licensed under the EUPL

package grpcmanager

import (
	"context"
	"fmt"
	"time"

	"go.nlx.io/nlx/controller/adapters/manager"
	"go.nlx.io/nlx/manager/ports/int/grpc/api"
	"go.nlx.io/nlx/txlog-api/domain/record"
)

func (m *grpcManager) ListTXLogRecords(ctx context.Context, req *manager.ListTXLogRecordsRequest) ([]*manager.TXLogRecord, error) {
	resp, err := m.client.ListTXLogRecords(ctx, reqToProto(req))
	if err != nil {
		return nil, mapError(err, "could not get tx log records from grpc manager")
	}

	return protoToRecords(resp)
}

func protoToRecords(resp *api.ListTXLogRecordsResponse) ([]*manager.TXLogRecord, error) {
	records := make([]*manager.TXLogRecord, len(resp.Records))

	for i, r := range resp.Records {
		var src interface{}
		switch s := r.Source.Data.(type) {
		case *api.TransactionLogRecord_Source_Source_:
			src = &manager.TXLogRecordSource{
				OutwayPeerID: s.Source.OutwayPeerId,
			}
		case *api.TransactionLogRecord_Source_DelegatedSource_:
			src = &manager.TXLogRecordDelegatedSource{
				OutwayPeerID:    s.DelegatedSource.OutwayPeerId,
				DelegatorPeerID: s.DelegatedSource.DelegatorPeerId,
			}
		default:
			return nil, fmt.Errorf("unknown source type: %T", s)
		}

		var dest interface{}
		switch d := r.Destination.Data.(type) {
		case *api.TransactionLogRecord_Destination_Destination_:
			dest = &manager.TXLogRecordDestination{
				ServicePeerID: d.Destination.ServicePeerId,
			}
		case *api.TransactionLogRecord_Destination_DelegatedDestination_:
			dest = &manager.TXLogRecordDelegatedDestination{
				ServicePeerID:   d.DelegatedDestination.ServicePeerId,
				DelegatorPeerID: d.DelegatedDestination.DelegatorPeerId,
			}
		default:
			return nil, fmt.Errorf("unknown destination type: %T", d)
		}

		records[i] = &manager.TXLogRecord{
			TransactionID: r.TransactionId,
			GrantHash:     r.GrantHash,
			Direction:     directionProtoToModel(r.Direction),
			ServiceName:   r.ServiceName,
			Source:        src,
			Destination:   dest,
			CreatedAt:     time.Unix(r.CreatedAt, 0),
		}
	}

	return records, nil
}

func directionProtoToModel(d api.TransactionLogRecord_Direction) record.Direction {
	switch d {
	case api.TransactionLogRecord_DIRECTION_IN:
		return record.DirectionIn
	case api.TransactionLogRecord_DIRECTION_OUT:
		return record.DirectionOut
	default:
		return record.DirectionUnspecified
	}
}

func reqToProto(req *manager.ListTXLogRecordsRequest) *api.ListTXLogRecordsRequest {
	return &api.ListTXLogRecordsRequest{
		Pagination: &api.Pagination{
			StartId: req.Pagination.StartID,
			Limit:   req.Pagination.Limit,
			Order:   sortOrderToProto(req.Pagination.SortOrder),
		},
		DataSourcePeerId: req.DataSourcePeerID,
	}
}

func sortOrderToProto(o manager.SortOrder) api.SortOrder {
	switch o {
	case manager.SortOrderAscending:
		return api.SortOrder_SORT_ORDER_ASCENDING
	case manager.SortOrderDescending:
		return api.SortOrder_SORT_ORDER_DESCENDING
	default:
		return api.SortOrder_SORT_ORDER_UNSPECIFIED
	}
}
