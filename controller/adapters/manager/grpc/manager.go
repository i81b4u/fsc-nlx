// Copyright © VNG Realisatie 2023
// Licensed under the EUPL

package grpcmanager

import (
	"fmt"

	"go.nlx.io/nlx/controller/adapters/logger"

	mclient "go.nlx.io/nlx/controller/pkg/manager"
)

type grpcManager struct {
	client mclient.Client
	logger logger.Logger
}

func New(client mclient.Client, l logger.Logger) (*grpcManager, error) {
	if client == nil {
		return nil, fmt.Errorf("manager grpc client is required")
	}

	if l == nil {
		return nil, fmt.Errorf("logger is required")
	}

	return &grpcManager{
		client: client,
		logger: l,
	}, nil
}
