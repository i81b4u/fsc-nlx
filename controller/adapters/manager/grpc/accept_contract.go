// Copyright © VNG Realisatie 2023
// Licensed under the EUPL

package grpcmanager

import (
	"context"
	"fmt"

	"go.nlx.io/nlx/controller/adapters/manager"
	"go.nlx.io/nlx/manager/ports/int/grpc/api"
)

func (m *grpcManager) AcceptContract(ctx context.Context, contentHash string) error {
	_, err := m.client.AcceptContract(ctx, &api.AcceptContractRequest{
		ContentHash: contentHash,
	})
	if err != nil {
		return fmt.Errorf("could not accept contract with grpc manager: %s: %w", err, manager.ErrInternalError)
	}

	return nil
}
