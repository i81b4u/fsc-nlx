// Copyright © VNG Realisatie 2023
// Licensed under the EUPL

package grpcmanager

import (
	"context"
	"time"

	"github.com/pkg/errors"

	"go.nlx.io/nlx/controller/adapters/manager"
	"go.nlx.io/nlx/manager/ports/int/grpc/api"
)

// nolint:gocyclo,funlen // unable to make less complex
func (m *grpcManager) ListContracts(ctx context.Context, contentHashes []string, grantTypes []manager.GrantType) (manager.Contracts, error) {
	filters := make([]*api.ListContractsRequest_Filter, len(contentHashes))

	for i, hash := range contentHashes {
		filters[i] = &api.ListContractsRequest_Filter{ContentHash: hash}
	}

	for _, grantType := range grantTypes {
		filters = append(filters, &api.ListContractsRequest_Filter{GrantType: convertGrantType(grantType)})
	}

	resp, err := m.client.ListContracts(ctx, &api.ListContractsRequest{
		Filters: filters,
	})
	if err != nil {
		return nil, errors.Wrap(err, "could not get contracts from grpc manager")
	}

	contracts := make(manager.Contracts, len(resp.Contracts))

	for i, c := range resp.Contracts {
		peers := make(map[string]*manager.Peer)

		for peerID, peer := range c.Peers {
			peers[peerID] = &manager.Peer{
				ID:   peer.Id,
				Name: peer.Name,
			}
		}

		var peerRegistrationGrant *manager.PeerRegistrationGrant

		servicePublicationGrants := make([]*manager.ServicePublicationGrant, 0)
		serviceConnectionGrants := make([]*manager.ServiceConnectionGrant, 0)
		delegatedServiceConnectionGrants := make([]*manager.DelegatedServiceConnectionGrant, 0)
		delegatedServicePublicationGrants := make([]*manager.DelegatedServicePublicationGrant, 0)

		for _, grant := range c.Content.Grants {
			switch convertedGrant := grant.Data.(type) {
			case *api.Grant_PeerRegistration:
				peerRegistrationGrant = &manager.PeerRegistrationGrant{
					Hash:            convertedGrant.PeerRegistration.Hash,
					DirectoryPeerID: convertedGrant.PeerRegistration.Directory.PeerId,
					PeerID:          convertedGrant.PeerRegistration.Peer.Id,
					PeerName:        convertedGrant.PeerRegistration.Peer.Name,
				}
			case *api.Grant_ServicePublication:
				servicePublicationGrants = append(servicePublicationGrants, &manager.ServicePublicationGrant{
					Hash:            convertedGrant.ServicePublication.Hash,
					DirectoryPeerID: convertedGrant.ServicePublication.Directory.PeerId,
					ServicePeerID:   convertedGrant.ServicePublication.Service.PeerId,
					ServiceName:     convertedGrant.ServicePublication.Service.Name,
				})

			case *api.Grant_ServiceConnection:
				serviceConnectionGrants = append(serviceConnectionGrants, &manager.ServiceConnectionGrant{
					Hash:                        convertedGrant.ServiceConnection.Hash,
					ServicePeerID:               convertedGrant.ServiceConnection.Service.PeerId,
					ServiceName:                 convertedGrant.ServiceConnection.Service.Name,
					OutwayPeerID:                convertedGrant.ServiceConnection.Outway.PeerId,
					OutwayCertificateThumbprint: convertedGrant.ServiceConnection.Outway.CertificateThumbprint,
				})

			case *api.Grant_DelegatedServiceConnection:
				delegatedServiceConnectionGrants = append(delegatedServiceConnectionGrants, &manager.DelegatedServiceConnectionGrant{
					Hash:                        convertedGrant.DelegatedServiceConnection.Hash,
					DelegatorPeerID:             convertedGrant.DelegatedServiceConnection.Delegator.PeerId,
					ServicePeerID:               convertedGrant.DelegatedServiceConnection.Service.PeerId,
					ServiceName:                 convertedGrant.DelegatedServiceConnection.Service.Name,
					OutwayPeerID:                convertedGrant.DelegatedServiceConnection.Outway.PeerId,
					OutwayCertificateThumbprint: convertedGrant.DelegatedServiceConnection.Outway.CertificateThumbprint,
				})

			case *api.Grant_DelegatedServicePublication:
				delegatedServicePublicationGrants = append(delegatedServicePublicationGrants, &manager.DelegatedServicePublicationGrant{
					Hash:            convertedGrant.DelegatedServicePublication.Hash,
					DirectoryPeerID: convertedGrant.DelegatedServicePublication.Directory.PeerId,
					DelegatorPeerID: convertedGrant.DelegatedServicePublication.Delegator.PeerId,
					ServicePeerID:   convertedGrant.DelegatedServicePublication.Service.PeerId,
					ServiceName:     convertedGrant.DelegatedServicePublication.Service.Name,
				})
			}
		}

		contracts[i] = &manager.Contract{
			ID:                                c.Content.Id,
			Hash:                              c.Hash,
			HashAlgorithm:                     manager.HashAlg(c.Content.HashAlgorithm),
			GroupID:                           c.Content.GroupId,
			CreatedAt:                         time.Unix(c.Content.CreatedAt, 0),
			Peers:                             peers,
			AcceptSignatures:                  convertSignatures(c.Signatures.Accept),
			RejectSignatures:                  convertSignatures(c.Signatures.Reject),
			RevokeSignatures:                  convertSignatures(c.Signatures.Revoke),
			ValidFrom:                         time.Unix(c.Content.Validity.NotBefore, 0),
			ValidUntil:                        time.Unix(c.Content.Validity.NotAfter, 0),
			HasRejected:                       c.HasRejected,
			HasAccepted:                       c.HasAccepted,
			HasRevoked:                        c.HasRevoked,
			PeerRegistrationGrant:             peerRegistrationGrant,
			ServicePublicationGrants:          servicePublicationGrants,
			ServiceConnectionGrants:           serviceConnectionGrants,
			DelegatedServicePublicationGrants: delegatedServicePublicationGrants,
			DelegatedServiceConnectionGrants:  delegatedServiceConnectionGrants,
		}
	}

	return contracts, nil
}

func convertSignatures(signatures map[string]*api.ListContractsResponse_Signature) map[string]manager.Signature {
	convertedSignatures := make(map[string]manager.Signature)

	for peerID, signature := range signatures {
		convertedSignatures[peerID] = manager.Signature{
			SignedAt: time.Unix(signature.SignedAt, 0),
		}
	}

	return convertedSignatures
}

func convertGrantType(g manager.GrantType) api.GrantType {
	switch g {
	case manager.GrantTypePeerRegistration:
		return api.GrantType_GRANT_TYPE_PEER_REGISTRATION
	case manager.GrantTypeServicePublication:
		return api.GrantType_GRANT_TYPE_SERVICE_PUBLICATION
	case manager.GrantTypeServiceConnection:
		return api.GrantType_GRANT_TYPE_SERVICE_CONNECTION
	case manager.GrantTypeDelegatedServiceConnection:
		return api.GrantType_GRANT_TYPE_DELEGATED_SERVICE_CONNECTION
	case manager.GrantTypeDelegatedServicePublication:
		return api.GrantType_GRANT_TYPE_DELEGATED_SERVICE_PUBLICATION
	default:
		return api.GrantType_GRANT_TYPE_UNSPECIFIED
	}
}
