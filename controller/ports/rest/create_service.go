// Copyright © VNG Realisatie 2023
// Licensed under the EUPL

package rest

import (
	"context"

	"github.com/pkg/errors"

	"go.nlx.io/nlx/controller/app/apiapp/command"
	"go.nlx.io/nlx/controller/ports/rest/api"
)

func (s *Server) CreateService(ctx context.Context, req api.CreateServiceRequestObject) (api.CreateServiceResponseObject, error) {
	err := s.app.Commands.CreateService.Handle(ctx, &command.CreateServiceArgs{
		Name:         req.Body.Name,
		EndpointURL:  req.Body.EndpointUrl,
		InwayAddress: req.Body.InwayAddress,
	})
	if err != nil {
		return nil, errors.Wrap(err, "could not create service")
	}

	return api.CreateService201Response{}, nil
}
