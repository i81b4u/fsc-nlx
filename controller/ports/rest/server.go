// Copyright © VNG Realisatie 2023
// Licensed under the EUPL

package rest

import (
	"context"
	"fmt"
	"net/http"
	"time"

	"github.com/go-chi/chi/v5"

	common_tls "go.nlx.io/nlx/common/tls"
	"go.nlx.io/nlx/controller/app/apiapp"
	"go.nlx.io/nlx/controller/ports/rest/api"
	"go.nlx.io/nlx/manager/adapters/logger"
)

type Server struct {
	app    *apiapp.Application
	logger logger.Logger
	cert   *common_tls.CertificateBundle
	server *http.Server
}

type NewArgs struct {
	Logger        logger.Logger
	App           *apiapp.Application
	Cert          *common_tls.CertificateBundle
	ListenAddress string
}

func New(args *NewArgs) (*Server, error) {
	if args == nil {
		return nil, fmt.Errorf("args required")
	}

	if args.Logger == nil {
		return nil, fmt.Errorf("logger required")
	}

	if args.Cert == nil {
		return nil, fmt.Errorf("cert required")
	}

	if args.App == nil {
		return nil, fmt.Errorf("app required")
	}

	if args.ListenAddress == "" {
		return nil, fmt.Errorf("listen address required")
	}

	s := &Server{
		app:    args.App,
		logger: args.Logger,
		cert:   args.Cert,
	}

	r := chi.NewRouter()
	api.HandlerFromMux(api.NewStrictHandler(s, []api.StrictMiddlewareFunc{}), r)

	var readHeaderTimeout = 5 * time.Second

	s.server = &http.Server{
		ReadHeaderTimeout: readHeaderTimeout,
		Handler:           r,
		Addr:              args.ListenAddress,
		TLSConfig:         args.Cert.TLSConfig(args.Cert.WithTLSClientAuth()),
	}

	return s, nil
}

func (s *Server) ListenAndServeTLS(certFilePath, keyFilePath string) error {
	return s.server.ListenAndServeTLS(certFilePath, keyFilePath)
}

func (s *Server) Shutdown(ctx context.Context) error {
	return s.server.Shutdown(ctx)
}
