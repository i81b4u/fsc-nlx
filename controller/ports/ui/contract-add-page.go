// Copyright © VNG Realisatie 2023
// Licensed under the EUPL

package uiport

import (
	"net/http"

	"github.com/pkg/errors"
)

type addContractsPage struct {
	*BasePage
	BaseAuthenticatedPage

	SuccessMessage               string
	ErrorMessage                 string
	HashAlgorithms               HashAlgorithms
	Groups                       []string
	OutwayCertificateThumbprints map[string][]string
	Form                         *AddContractsPageForm
}

type HashAlgorithms []string

type AddContractsPageForm struct {
	Action        string
	HashAlgorithm string
	GroupID       string
	ValidFrom     string
	ValidUntil    string

	PeerRegistrationGrants            []*AddContractsPageFormPeerRegistrationGrant
	ServicePublicationGrants          []*AddContractsPageFormServicePublicationGrant
	ServiceConnectionGrants           []*AddContractsPageFormServiceConnectionGrant
	DelegatedServiceConnectionGrants  []*AddContractsPageFormDelegatedServiceConnectionGrant
	DelegatedServicePublicationGrants []*AddContractsPageFormDelegatedServicePublicationGrant
}

type AddContractsPageFormPeerRegistrationGrant struct {
	DirectoryPeerID string
	PeerID          string
	PeerName        string
}

type AddContractsPageFormServicePublicationGrant struct {
	DirectoryPeerID string
	ServicePeerID   string
	ServiceName     string
}

type AddContractsPageFormServiceConnectionGrant struct {
	ServicePeerID               string
	ServiceName                 string
	OutwayPeerID                string
	OutwayCertificateThumbprint string
}

type AddContractsPageFormDelegatedServiceConnectionGrant struct {
	DelegatorPeerID             string
	ServicePeerID               string
	ServiceName                 string
	OutwayPeerID                string
	OutwayCertificateThumbprint string
}

type AddContractsPageFormDelegatedServicePublicationGrant struct {
	DirectoryPeerID string
	DelegatorPeerID string
	ServicePeerID   string
	ServiceName     string
}

const (
	FormActionAddPeerRegistrationGrant            = "add-peer-registration-grant"
	FormActionAddServicePublicationGrant          = "add-service-publication-grant"
	FormActionAddServiceConnectionGrant           = "add-service-connection-grant"
	FormActionAddDelegatedServiceConnectionGrant  = "add-delegated-service-connection-grant"
	FormActionAddDelegatedServicePublicationGrant = "add-delegated-service-publication-grant"
)

func (p *addContractsPage) renderWithError(w http.ResponseWriter, err error) {
	statusCode, message := mapError(err)

	w.WriteHeader(statusCode)

	p.ErrorMessage = message

	p.render(w)
}

func (p *addContractsPage) renderWithSuccess(w http.ResponseWriter, successMessage string) {
	p.SuccessMessage = successMessage
	p.render(w)
}

func decodeAddContractPageForm(r *http.Request) (*AddContractsPageForm, error) {
	err := r.ParseForm()
	if err != nil {
		return nil, errors.Wrap(err, "failed to parse form")
	}

	var result AddContractsPageForm

	err = decoder.Decode(&result, r.Form)
	if err != nil {
		return nil, errors.Wrap(err, "failed to decode form values")
	}

	return &result, nil
}

func (p *addContractsPage) render(w http.ResponseWriter) {
	baseTemplate := p.TemplateWithHelpers()

	t, err := baseTemplate.
		ParseFS(
			tplFolder,
			"templates/base-authenticated.html",
			"templates/add-contract.html",
		)
	if err != nil {
		http.Error(w, http.StatusText(http.StatusInternalServerError), http.StatusInternalServerError)
		return
	}

	err = t.ExecuteTemplate(w, "base-authenticated.html", p)
	if err != nil {
		http.Error(w, http.StatusText(http.StatusInternalServerError), http.StatusInternalServerError)
		return
	}
}
