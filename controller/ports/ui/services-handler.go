// Copyright © VNG Realisatie 2023
// Licensed under the EUPL

package uiport

import (
	"net/http"

	"go.nlx.io/nlx/controller/app/apiapp/query"
)

//nolint:dupl // looks the same as directoryHandler but is different.
func (s *Server) servicesHandler(w http.ResponseWriter, r *http.Request) {
	ctx := r.Context()

	resp, err := s.apiApp.Queries.ListServices.Handle(ctx)
	if err != nil {
		s.logger.Error("could not render services page", err)
		http.Error(w, http.StatusText(http.StatusInternalServerError), http.StatusInternalServerError)

		return
	}

	services := mapQueryToUIServices(resp)

	page := servicesPage{
		BasePage: s.basePage,
		BaseAuthenticatedPage: BaseAuthenticatedPage{
			PrimaryNavigationActivePath: PathServicesPage,
			Title:                       s.i18n.Translate("Services"),
			Description:                 "",
			Username:                    "admin",
			Peer:                        s.getPeerInfo(ctx),
		},
		AmountOfServices: len(services),
		Services:         services,
	}

	err = page.render(w)
	if err != nil {
		s.logger.Error("could not render services page", err)
		http.Error(w, http.StatusText(http.StatusInternalServerError), http.StatusInternalServerError)

		return
	}
}

func mapQueryToUIServices(qServices query.Services) []*servicesPageService {
	contracts := make([]*servicesPageService, len(qServices))

	for i, s := range qServices {
		contracts[i] = &servicesPageService{
			Name: s.Name,
		}
	}

	return contracts
}
