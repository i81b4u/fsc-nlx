/**
 * Copyright © VNG Realisatie 2023
 * Licensed under the EUPL
 */

import 'htmx.org'
import _hyperscript from 'hyperscript.org/src/_hyperscript.js';
window._hyperscript = _hyperscript.browserInit();
