// Copyright © VNG Realisatie 2023
// Licensed under the EUPL

package uiport

import (
	"context"
	"fmt"
	"net/http"
)

const (
	formActionContractAccept = "accept"
	formActionContractRevoke = "revoke"
	formActionContractReject = "reject"
)

func (s *Server) signContractPostHandler(w http.ResponseWriter, r *http.Request) {
	err := r.ParseForm()
	if err != nil {
		s.logger.Error("accept contract parse form", err)
		http.Error(w, http.StatusText(http.StatusInternalServerError), http.StatusInternalServerError)

		return
	}

	contentHash := r.FormValue("contentHash")
	action := r.Form.Get("action")

	switch action {
	case formActionContractAccept:
		err = s.apiApp.Commands.AcceptContract.Handle(context.Background(), contentHash)
	case formActionContractRevoke:
		err = s.apiApp.Commands.RevokeContract.Handle(context.Background(), contentHash)
	case formActionContractReject:
		err = s.apiApp.Commands.RejectContract.Handle(context.Background(), contentHash)
	default:
		s.logger.Error(fmt.Sprintf("unknown form action: %s", action), nil)

		http.Error(w, http.StatusText(http.StatusInternalServerError), http.StatusInternalServerError)

		return
	}

	if err != nil {
		s.logger.Error(fmt.Sprintf("failed to preform %s action on contract contract", action), err)

		statusCode, message := mapError(err)

		w.WriteHeader(statusCode)

		_, err = w.Write([]byte(message))
		if err != nil {
			s.logger.Error("failed to write response", err)

			http.Error(w, http.StatusText(http.StatusInternalServerError), http.StatusInternalServerError)

			return
		}

		return
	}

	w.Header().Add("Location", fmt.Sprintf("/contract-modal/%s", contentHash))
	w.WriteHeader(http.StatusSeeOther)
}
