// Copyright © VNG Realisatie 2023
// Licensed under the EUPL

package uiport

import (
	"context"
	"embed"
	"fmt"
	"net/http"
	"time"

	"github.com/go-chi/chi/v5"
	"github.com/go-chi/chi/v5/middleware"
	"github.com/pkg/errors"
	"github.com/zitadel/schema"
	"go.nlx.io/nlx/common/clock"

	"go.nlx.io/nlx/controller/adapters/logger"
	"go.nlx.io/nlx/controller/app/apiapp"
	"go.nlx.io/nlx/controller/app/uiapp"
	"go.nlx.io/nlx/controller/ports/ui/i18n"
	jsoni18n "go.nlx.io/nlx/controller/ports/ui/i18n/json"
)

//go:embed templates/**
var tplFolder embed.FS

var decoder = schema.NewDecoder()

type Server struct {
	locale     string
	staticPath string
	apiApp     *apiapp.Application
	uiApp      *uiapp.Application
	i18n       i18n.I18n
	logger     logger.Logger
	httpServer *http.Server
	basePage   *BasePage
	clock      clock.Clock
	peerInfo   *PeerInfo
}

type NewServerArgs struct {
	Locale     string
	StaticPath string
	GroupID    string
	Logger     logger.Logger
	APIApp     *apiapp.Application
	UIApp      *uiapp.Application
	Clock      clock.Clock
}

func New(ctx context.Context, args *NewServerArgs) (*Server, error) {
	if args.Locale == "" {
		return nil, fmt.Errorf("locale must be set (nl/en)")
	}

	if args.Logger == nil {
		return nil, fmt.Errorf("logger cannot be nil")
	}

	if args.APIApp == nil {
		return nil, fmt.Errorf("API app cannot be nil")
	}

	if args.UIApp == nil {
		return nil, fmt.Errorf("UI app cannot be nil")
	}

	if args.Clock == nil {
		return nil, fmt.Errorf("clock cannot be nil")
	}

	translations, err := jsoni18n.New(args.Locale)
	if err != nil {
		return nil, errors.Wrap(err, "unable to create new json i18n instance")
	}

	basePage, err := NewBasePage(args.StaticPath, translations, args.GroupID)
	if err != nil {
		return nil, errors.Wrap(err, "failed to create base page")
	}

	server := &Server{
		clock:      args.Clock,
		locale:     args.Locale,
		staticPath: args.StaticPath,
		logger:     args.Logger,
		i18n:       translations,
		apiApp:     args.APIApp,
		uiApp:      args.UIApp,
		basePage:   basePage,
	}

	return server, nil
}

const compressionLevel = 5

func (s *Server) ListenAndServe(address string) error {
	r := chi.NewRouter()
	r.Use(middleware.Compress(compressionLevel))
	r.Use(middleware.Logger)
	r.Get("/", func(w http.ResponseWriter, r *http.Request) {
		http.Redirect(w, r, "/directory", http.StatusTemporaryRedirect)
	})
	r.Get("/services", s.servicesHandler)
	r.Get("/services/add-service", s.addServiceGetHandler)
	r.Post("/services/add-service", s.addServicePostHandler)
	r.Get("/directory", s.directoryHandler)
	r.Get("/contracts", s.contractsHandler)
	r.Get("/contracts/add", s.addContractGetHandler)
	r.Post("/contracts/add", s.addContractPostHandler)
	r.Post("/contracts/sign", s.signContractPostHandler)
	r.Get("/contract-modal/{hash}", s.contractModalHandler)
	r.Get("/transaction-logs", s.transactionLogsHandler)

	filesDir := http.Dir(s.staticPath)
	r.Handle("/*", http.FileServer(filesDir))

	const readHeaderTimeout = 5 * time.Second

	s.httpServer = &http.Server{
		Addr:              address,
		Handler:           r,
		ReadHeaderTimeout: readHeaderTimeout,
	}

	err := s.httpServer.ListenAndServe()
	if err != http.ErrServerClosed {
		return err
	}

	return nil
}

func (s *Server) Shutdown(ctx context.Context) error {
	err := s.httpServer.Shutdown(ctx)
	if err != http.ErrServerClosed {
		return err
	}

	return nil
}
