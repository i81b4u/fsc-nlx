// Copyright © VNG Realisatie 2023
// Licensed under the EUPL

package uiport

import (
	"net/http"
)

type contractModalPage struct {
	*BasePage
	ID                                string
	State                             string
	Hash                              string
	HashAlgorithm                     string
	GroupID                           string
	CreatedAt                         string
	ValidFrom                         string
	ValidUntil                        string
	PeerRegistrationGrants            []*PeerRegistrationGrant
	ServicePublicationGrants          []*ServicePublicationGrant
	ServiceConnectionGrants           []*ServiceConnectionGrant
	DelegatedServicePublicationGrants []*DelegatedServicePublicationGrant
	DelegatedServiceConnectionGrants  []*DelegatedServiceConnectionGrant
	Organizations                     []*Peer
	DisableRejectButton               bool
	DisableAcceptButton               bool
	DisableRevokeButton               bool
	IsActive                          bool
}

type Peer struct {
	Name                    string
	PeerID                  string
	RejectSignatureSignedAt string
	AcceptSignatureSignedAt string
	RevokeSignatureSignedAt string
}

type PeerRegistrationGrant struct {
	Hash            string
	DirectoryPeerID string
	PeerID          string
	PeerName        string
}

type ServicePublicationGrant struct {
	Hash            string
	DirectoryPeerID string
	ServicePeerID   string
	ServiceName     string
}

type ServiceConnectionGrant struct {
	Hash                        string
	ServicePeerID               string
	ServiceName                 string
	OutwayPeerID                string
	OutwayCertificateThumbprint string
}

type DelegatedServiceConnectionGrant struct {
	Hash                        string
	DelegatorPeerID             string
	OutwayPeerID                string
	OutwayCertificateThumbprint string
	ServicePeerID               string
	ServiceName                 string
}

type DelegatedServicePublicationGrant struct {
	Hash            string
	DirectoryPeerID string
	DelegatorPeerID string
	ServicePeerID   string
	ServiceName     string
}

func (p *contractModalPage) render(w http.ResponseWriter) error {
	baseTemplate := p.TemplateWithHelpers()

	t, err := baseTemplate.
		ParseFS(
			tplFolder,
			"templates/contract-modal.html",
		)
	if err != nil {
		return err
	}

	err = t.ExecuteTemplate(w, "contract-modal.html", p)
	if err != nil {
		return err
	}

	return nil
}
