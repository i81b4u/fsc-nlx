// Copyright © VNG Realisatie 2023
// Licensed under the EUPL

package uiport

import (
	"context"
	"net/http"
	"time"

	"go.nlx.io/nlx/controller/app/apiapp/command"
)

const Day = 24 * time.Hour

func (s *Server) addContractGetHandler(w http.ResponseWriter, _ *http.Request) {
	page := getDefaultPageSetup(s.basePage, &AddContractsPageForm{
		ValidFrom:  time.Now().Format("2006-01-02"),
		ValidUntil: time.Now().Add(Day).Format("2006-01-02"),
	})

	page.render(w)
}

func (s *Server) addContractPostHandler(w http.ResponseWriter, r *http.Request) {
	ctx := r.Context()

	formValues, err := decodeAddContractPageForm(r)
	if err != nil {
		s.logger.Error("failed to read form values", err)
		http.Error(w, http.StatusText(http.StatusInternalServerError), http.StatusInternalServerError)

		return
	}

	page := getDefaultPageSetup(s.basePage, formValues)

	owThumbprints, err := s.uiApp.Queries.ListOutwayThumbprints.Handle(ctx)
	if err != nil {
		s.logger.Error("get outway thumbprints for form", err)
		http.Error(w, http.StatusText(http.StatusInternalServerError), http.StatusInternalServerError)

		return
	}

	page.OutwayCertificateThumbprints = owThumbprints

	if formValues.Action == FormActionAddPeerRegistrationGrant {
		page.Form.PeerRegistrationGrants = append(page.Form.PeerRegistrationGrants, &AddContractsPageFormPeerRegistrationGrant{})

		page.render(w)

		return
	}

	if formValues.Action == FormActionAddServicePublicationGrant {
		page.Form.ServicePublicationGrants = append(page.Form.ServicePublicationGrants, &AddContractsPageFormServicePublicationGrant{})

		page.render(w)

		return
	}

	if formValues.Action == FormActionAddServiceConnectionGrant {
		page.Form.ServiceConnectionGrants = append(page.Form.ServiceConnectionGrants, &AddContractsPageFormServiceConnectionGrant{})

		page.render(w)

		return
	}

	if formValues.Action == FormActionAddDelegatedServiceConnectionGrant {
		page.Form.DelegatedServiceConnectionGrants = append(page.Form.DelegatedServiceConnectionGrants, &AddContractsPageFormDelegatedServiceConnectionGrant{})

		page.render(w)

		return
	}

	if formValues.Action == FormActionAddDelegatedServicePublicationGrant {
		page.Form.DelegatedServicePublicationGrants = append(page.Form.DelegatedServicePublicationGrants, &AddContractsPageFormDelegatedServicePublicationGrant{})

		page.render(w)

		return
	}

	err = s.apiApp.Commands.CreateContract.Handle(context.Background(), &command.CreateContractArgs{
		HashAlgorithm:                     formValues.HashAlgorithm,
		GroupID:                           formValues.GroupID,
		ContractNotBefore:                 formValues.ValidFrom,
		ContractNotAfter:                  formValues.ValidUntil,
		PeerRegistrationGrants:            mapPeerRegistrationGrants(formValues.PeerRegistrationGrants),
		ServicePublicationGrants:          mapServicePublicationGrants(formValues.ServicePublicationGrants),
		ServiceConnectionGrants:           mapServiceConnectionGrants(formValues.ServiceConnectionGrants),
		DelegatedServiceConnectionGrants:  mapDelegatedServiceConnectionGrants(formValues.DelegatedServiceConnectionGrants),
		DelegatedServicePublicationGrants: mapDelegatedServicePublicationGrants(formValues.DelegatedServicePublicationGrants),
	})
	if err != nil {
		page.renderWithError(w, err)

		s.logger.Error("create contract", err)

		return
	}

	page.renderWithSuccess(w, s.i18n.Translate("The contract was added successfully."))
}

func getDefaultPageSetup(basePage *BasePage, form *AddContractsPageForm) addContractsPage {
	return addContractsPage{
		BasePage: basePage,
		BaseAuthenticatedPage: BaseAuthenticatedPage{
			PrimaryNavigationActivePath: PathContractsPage,
		},
		Groups: []string{basePage.GroupID},
		HashAlgorithms: HashAlgorithms{
			"SHA3-512",
		},
		OutwayCertificateThumbprints: map[string][]string{},
		Form:                         form,
	}
}

func mapPeerRegistrationGrants(input []*AddContractsPageFormPeerRegistrationGrant) []*command.PeerRegistrationGrant {
	result := make([]*command.PeerRegistrationGrant, len(input))

	for i, grant := range input {
		result[i] = &command.PeerRegistrationGrant{
			DirectoryPeerID: grant.DirectoryPeerID,
			PeerID:          grant.PeerID,
			PeerName:        grant.PeerName,
		}
	}

	return result
}

func mapServicePublicationGrants(input []*AddContractsPageFormServicePublicationGrant) []*command.ServicePublicationGrant {
	result := make([]*command.ServicePublicationGrant, len(input))

	for i, grant := range input {
		result[i] = &command.ServicePublicationGrant{
			DirectoryPeerID: grant.DirectoryPeerID,
			ServicePeerID:   grant.ServicePeerID,
			ServiceName:     grant.ServiceName,
		}
	}

	return result
}

func mapServiceConnectionGrants(input []*AddContractsPageFormServiceConnectionGrant) []*command.ServiceConnectionGrant {
	result := make([]*command.ServiceConnectionGrant, len(input))

	for i, grant := range input {
		result[i] = &command.ServiceConnectionGrant{
			ServicePeerID:               grant.ServicePeerID,
			ServiceName:                 grant.ServiceName,
			OutwayPeerID:                grant.OutwayPeerID,
			OutwayCertificateThumbprint: grant.OutwayCertificateThumbprint,
		}
	}

	return result
}

func mapDelegatedServiceConnectionGrants(input []*AddContractsPageFormDelegatedServiceConnectionGrant) []*command.DelegatedServiceConnectionGrant {
	result := make([]*command.DelegatedServiceConnectionGrant, len(input))

	for i, grant := range input {
		result[i] = &command.DelegatedServiceConnectionGrant{
			DelegatorPeerID:             grant.DelegatorPeerID,
			ServicePeerID:               grant.ServicePeerID,
			ServiceName:                 grant.ServiceName,
			OutwayPeerID:                grant.OutwayPeerID,
			OutwayCertificateThumbprint: grant.OutwayCertificateThumbprint,
		}
	}

	return result
}

func mapDelegatedServicePublicationGrants(input []*AddContractsPageFormDelegatedServicePublicationGrant) []*command.DelegatedServicePublicationGrant {
	result := make([]*command.DelegatedServicePublicationGrant, len(input))

	for i, grant := range input {
		result[i] = &command.DelegatedServicePublicationGrant{
			DirectoryPeerID: grant.DirectoryPeerID,
			DelegatorPeerID: grant.DelegatorPeerID,
			ServicePeerID:   grant.ServicePeerID,
			ServiceName:     grant.ServiceName,
		}
	}

	return result
}
