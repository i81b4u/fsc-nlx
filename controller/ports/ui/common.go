// Copyright © VNG Realisatie 2023
// Licensed under the EUPL

package uiport

import (
	"context"
	"net/http"

	"github.com/pkg/errors"

	"go.nlx.io/nlx/common/clock"
	"go.nlx.io/nlx/controller/app/apiapp/command"
	"go.nlx.io/nlx/controller/app/apiapp/query"
)

func mapError(err error) (statusCode int, message string) {
	var e *command.ValidationError
	if errors.As(err, &e) {
		return http.StatusBadRequest, e.Error()
	}

	return http.StatusInternalServerError, "unexpected error"
}

func getContractState(contract *query.Contract, clk clock.Clock) contractState {
	if len(contract.RejectSignatures) > 0 {
		return rejected
	}

	if len(contract.RevokeSignatures) > 0 {
		return revoked
	}

	if len(contract.Peers) == len(contract.AcceptSignatures) {
		if clk.Now().After(contract.ValidUntil) {
			return expired
		}

		return valid
	}

	if clk.Now().After(contract.ValidUntil) {
		return rejected
	}

	return proposed
}

func (s *Server) getPeerInfo(ctx context.Context) *PeerInfo {
	if s.peerInfo != nil {
		return s.peerInfo
	}

	p, err := s.apiApp.Queries.GetPeerInfo.Handle(ctx)
	if err != nil {
		// this info is not important enough to error on
		return &PeerInfo{
			ID:   "",
			Name: "",
		}
	}

	peerInfo := &PeerInfo{
		ID:   p.ID,
		Name: p.Name,
	}

	s.peerInfo = peerInfo

	return peerInfo
}
