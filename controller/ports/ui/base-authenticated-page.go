// Copyright © VNG Realisatie 2023
// Licensed under the EUPL

package uiport

type BaseAuthenticatedPage struct {
	PrimaryNavigationActivePath string
	Title                       string
	Description                 string
	Peer                        *PeerInfo
	Username                    string
}

type PeerInfo struct {
	ID   string
	Name string
}
