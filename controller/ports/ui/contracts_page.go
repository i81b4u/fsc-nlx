// Copyright © VNG Realisatie 2023
// Licensed under the EUPL

package uiport

import (
	"net/http"

	"go.nlx.io/nlx/controller/app/apiapp/query"
)

type contractsPage struct {
	*BasePage
	BaseAuthenticatedPage

	GrantTypeFilters map[query.GrantType]string
	GrantType        string
	Contracts        []*contractsPageContract
}

type contractsPageContract struct {
	ID                                string
	IsActive                          bool
	Hash                              string
	CreatedAt                         string
	PeerRegistrationGrant             *PeerRegistrationGrant
	ServicePublicationGrants          []*ServicePublicationGrant
	ServiceConnectionGrants           []*ServiceConnectionGrant
	DelegatedServicePublicationGrants []*DelegatedServicePublicationGrant
	DelegatedServiceConnectionGrants  []*DelegatedServiceConnectionGrant
}

func (p *contractsPage) render(w http.ResponseWriter) error {
	baseTemplate := p.TemplateWithHelpers()

	t, err := baseTemplate.
		ParseFS(
			tplFolder,
			"templates/base-authenticated.html",
			"templates/contracts.html",
		)
	if err != nil {
		return err
	}

	err = t.ExecuteTemplate(w, "base-authenticated.html", p)
	if err != nil {
		return err
	}

	return nil
}
