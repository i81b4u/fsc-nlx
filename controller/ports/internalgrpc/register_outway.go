// Copyright © VNG Realisatie 2023
// Licensed under the EUPL

package internalgrpc

import (
	"context"

	"go.nlx.io/nlx/controller/app/internalapp/commands"
	"go.nlx.io/nlx/controller/ports/internalgrpc/api"
)

func (s *Server) RegisterOutway(ctx context.Context, req *api.RegisterOutwayRequest) (*api.RegisterOutwayResponse, error) {
	s.logger.Info("rpc request RegisterOutway")

	err := s.app.Commands.RegisterOutway.Handle(ctx, &commands.RegisterOutwayArgs{
		Name:                  req.Name,
		CertificateThumbprint: req.CertificateThumbprint,
	})
	if err != nil {
		s.logger.Error("error executing register outway command", err)
		return nil, ResponseFromError(err)
	}

	return &api.RegisterOutwayResponse{}, nil
}
