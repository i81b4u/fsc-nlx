// Copyright © VNG Realisatie 2023
// Licensed under the EUPL

package internalgrpc

import (
	"context"

	"go.nlx.io/nlx/controller/app/internalapp/commands"
	"go.nlx.io/nlx/controller/ports/internalgrpc/api"
)

func (s *Server) RegisterInway(ctx context.Context, req *api.RegisterInwayRequest) (*api.RegisterInwayResponse, error) {
	s.logger.Info("rpc request RegisterInway")

	err := s.app.Commands.RegisterInway.Handle(ctx, &commands.RegisterInwayArgs{
		Name:    req.Name,
		Address: req.Address,
	})
	if err != nil {
		s.logger.Error("error executing register inway command", err)
		return nil, ResponseFromError(err)
	}

	return &api.RegisterInwayResponse{}, nil
}
