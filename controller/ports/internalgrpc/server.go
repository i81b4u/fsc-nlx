// Copyright © VNG Realisatie 2021
// Licensed under the EUPL

package internalgrpc

import (
	"context"
	"errors"
	"fmt"
	"net"
	"net/http"
	"runtime/debug"

	grpc_recovery "github.com/grpc-ecosystem/go-grpc-middleware/recovery"
	grpc_ctxtags "github.com/grpc-ecosystem/go-grpc-middleware/tags"
	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials"

	common_tls "go.nlx.io/nlx/common/tls"
	"go.nlx.io/nlx/controller/adapters/logger"
	"go.nlx.io/nlx/controller/app/internalapp"
	"go.nlx.io/nlx/controller/ports/internalgrpc/api"
)

type Server struct {
	api.UnimplementedControllerServiceServer
	app     *internalapp.Application
	logger  logger.Logger
	service *grpc.Server
	cert    *common_tls.CertificateBundle
}

func New(lgr logger.Logger, a *internalapp.Application, cert *common_tls.CertificateBundle) (*Server, error) {
	if lgr == nil {
		return nil, fmt.Errorf("logger cannot be nil")
	}

	if a == nil {
		return nil, fmt.Errorf("app cannot be nil")
	}

	if cert == nil {
		return nil, fmt.Errorf("cert cannot be nil")
	}

	grpcServer := newGRPCServer(lgr, cert)

	server := &Server{
		logger:  lgr,
		app:     a,
		service: grpcServer,
		cert:    cert,
	}

	api.RegisterControllerServiceServer(grpcServer, server)

	return server, nil
}

func newGRPCServer(lgr logger.Logger, cert *common_tls.CertificateBundle) *grpc.Server {
	tlsConfig := cert.TLSConfig(cert.WithTLSClientAuth())
	transportCredentials := credentials.NewTLS(tlsConfig)

	recoveryOptions := []grpc_recovery.Option{
		grpc_recovery.WithRecoveryHandler(func(p interface{}) error {
			lgr.Warn("recovered from a panic in a grpc request handler", errors.New(string(debug.Stack())))
			return ResponseFromError(fmt.Errorf("%s", p))
		}),
	}

	opts := []grpc.ServerOption{
		grpc.Creds(transportCredentials),
		grpc.ChainStreamInterceptor(
			grpc_ctxtags.StreamServerInterceptor(),
			grpc_recovery.StreamServerInterceptor(recoveryOptions...),
		),
		grpc.ChainUnaryInterceptor(
			grpc_ctxtags.UnaryServerInterceptor(),
			grpc_recovery.UnaryServerInterceptor(recoveryOptions...),
		),
	}

	return grpc.NewServer(opts...)
}

func (s *Server) ListenAndServe(address string) error {
	errorChannel := make(chan error)

	listen, err := net.Listen("tcp", address)
	if err != nil {
		return err
	}

	go func() {
		errGrpc := s.service.Serve(listen)
		if errGrpc != nil {
			errorChannel <- fmt.Errorf("error listening on grpc server: %w", errGrpc)
		}
	}()

	err = <-errorChannel

	if err == http.ErrServerClosed {
		return nil
	}

	return fmt.Errorf("error listening on server: %w", err)
}

func (s *Server) Shutdown(context.Context) error {
	shutdownGrpcServer(context.Background(), s.service)

	return nil
}

func shutdownGrpcServer(ctx context.Context, s *grpc.Server) {
	stopped := make(chan struct{})

	go func() {
		s.GracefulStop()
		close(stopped)
	}()

	select {
	case <-ctx.Done():
		s.Stop()
	case <-stopped:
		return
	}
}
