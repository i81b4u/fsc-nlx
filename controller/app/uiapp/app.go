// Copyright © VNG Realisatie 2023
// Licensed under the EUPL

package uiapp

import (
	"context"
	"github.com/pkg/errors"

	"go.nlx.io/nlx/controller/adapters/storage"
	"go.nlx.io/nlx/controller/app/uiapp/query"
)

type Application struct {
	Queries Queries
}

type Queries struct {
	ListInwayAddresses    *query.ListInwayAddressesHandler
	ListOutwayThumbprints *query.ListOutwayThumbprintsHandler
}

type NewApplicationArgs struct {
	Context context.Context
	Storage storage.Storage
	GroupID string
}

func NewApplication(args *NewApplicationArgs) (*Application, error) {
	listInwayAddresses, err := query.NewListInwayAddressesHandler(args.GroupID, args.Storage)
	if err != nil {
		return nil, errors.Wrap(err, "could not create new ListInwayAddresses handler")
	}

	listOutwayThumbprints, err := query.NewListOutwayThumbprintsHandler(args.GroupID, args.Storage)
	if err != nil {
		return nil, errors.Wrap(err, "could not create new ListOutwayThumbprints handler")
	}

	application := &Application{
		Queries: Queries{
			ListInwayAddresses:    listInwayAddresses,
			ListOutwayThumbprints: listOutwayThumbprints,
		},
	}

	return application, nil
}
