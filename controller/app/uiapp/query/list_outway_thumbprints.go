// Copyright © VNG Realisatie 2023
// Licensed under the EUPL

package query

import (
	"context"
	"fmt"

	"go.nlx.io/nlx/controller/adapters/storage"
)

type ListOutwayThumbprintsHandler struct {
	storage storage.Storage
	groupID string
}

func NewListOutwayThumbprintsHandler(groupID string, s storage.Storage) (*ListOutwayThumbprintsHandler, error) {
	if s == nil {
		return nil, fmt.Errorf("storage is required")
	}

	if groupID == "" {
		return nil, fmt.Errorf("group ID is required")
	}

	return &ListOutwayThumbprintsHandler{
		groupID: groupID,
		storage: s,
	}, nil
}

func (h *ListOutwayThumbprintsHandler) Handle(ctx context.Context) (map[string][]string, error) {
	result, err := h.storage.ListOutwayThumbprints(ctx, h.groupID)
	if err != nil {
		return nil, fmt.Errorf("%w: could not get outway thumbprints from storage: %s", ErrInternalError, err)
	}

	return result, nil
}
