// Copyright © VNG Realisatie 2023
// Licensed under the EUPL
package query_test

import (
	"context"
	"errors"
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"

	"go.nlx.io/nlx/controller/app/uiapp/query"
)

// nolint:funlen // these tests should not fit 100 lines
func TestListInwayAddresses(t *testing.T) {
	t.Parallel()

	groupID := "directory1.com"

	tests := map[string]struct {
		setup   func(m *mocks) context.Context
		want    []string
		wantErr error
	}{
		"when_storage_errors": {
			setup: func(m *mocks) context.Context {
				ctx := context.Background()

				m.storage.
					EXPECT().
					ListInwayAddresses(ctx, groupID).
					Return(nil, errors.New("unexpected error"))

				return ctx
			},
			wantErr: query.ErrInternalError,
		},
		"happy_flow": {
			setup: func(m *mocks) context.Context {
				ctx := context.Background()

				m.storage.EXPECT().
					ListInwayAddresses(ctx, groupID).
					Return([]string{"inway1.local.nlx:443", "inway2.local.nlx:443"}, nil)

				return ctx
			},
			want: []string{"inway1.local.nlx:443", "inway2.local.nlx:443"},
		},
	}

	for name, tt := range tests {
		tt := tt

		t.Run(name, func(t *testing.T) {
			t.Parallel()

			mocks := newMocks(t)

			h, err := query.NewListInwayAddressesHandler(groupID, mocks.storage)
			require.NoError(t, err)

			ctx := tt.setup(mocks)

			actual, err := h.Handle(ctx)

			assert.ErrorIs(t, err, tt.wantErr)
			assert.Equal(t, tt.want, actual)
		})
	}
}
