// Copyright © VNG Realisatie 2023
// Licensed under the EUPL

package query

import "errors"

var ErrInternalError = errors.New("internal error")
var ErrNotFound = errors.New("not found")
