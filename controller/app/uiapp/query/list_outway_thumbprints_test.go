// Copyright © VNG Realisatie 2023
// Licensed under the EUPL
package query_test

import (
	"context"
	"errors"
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"

	"go.nlx.io/nlx/controller/app/uiapp/query"
)

// nolint:funlen // these tests should not fit 100 lines
func TestListOutwayThumbprints(t *testing.T) {
	t.Parallel()

	groupID := "directory1.com"

	tests := map[string]struct {
		setup   func(context.Context, *mocks)
		want    map[string][]string
		wantErr error
	}{
		"when_storage_errors": {
			setup: func(ctx context.Context, m *mocks) {
				m.storage.
					EXPECT().
					ListOutwayThumbprints(ctx, groupID).
					Return(nil, errors.New("unexpected error"))
			},
			wantErr: query.ErrInternalError,
		},
		"happy_flow": {
			setup: func(ctx context.Context, m *mocks) {
				m.storage.EXPECT().
					ListOutwayThumbprints(ctx, groupID).
					Return(map[string][]string{"thumbprint-1": {"outway-1"}, "thumbprint-2": {"outway-2", "outway-3"}}, nil)
			},
			want: map[string][]string{"thumbprint-1": {"outway-1"}, "thumbprint-2": {"outway-2", "outway-3"}},
		},
	}

	for name, tt := range tests {
		tt := tt

		t.Run(name, func(t *testing.T) {
			t.Parallel()

			mocks := newMocks(t)

			h, err := query.NewListOutwayThumbprintsHandler(groupID, mocks.storage)
			require.NoError(t, err)

			ctx := context.Background()

			tt.setup(ctx, mocks)

			actual, err := h.Handle(ctx)

			assert.ErrorIs(t, err, tt.wantErr)
			assert.Equal(t, tt.want, actual)
		})
	}
}
