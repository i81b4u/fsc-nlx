// Copyright © VNG Realisatie 2023
// Licensed under the EUPL

package query

import (
	"context"
	"fmt"

	"go.nlx.io/nlx/controller/adapters/storage"
)

type ListInwayAddressesHandler struct {
	storage storage.Storage
	groupID string
}

func NewListInwayAddressesHandler(groupID string, s storage.Storage) (*ListInwayAddressesHandler, error) {
	if s == nil {
		return nil, fmt.Errorf("storage is required")
	}

	if groupID == "" {
		return nil, fmt.Errorf("group ID is required")
	}

	return &ListInwayAddressesHandler{
		groupID: groupID,
		storage: s,
	}, nil
}

func (h *ListInwayAddressesHandler) Handle(ctx context.Context) ([]string, error) {
	result, err := h.storage.ListInwayAddresses(ctx, h.groupID)
	if err != nil {
		return nil, fmt.Errorf("%w: could not get inway addresses from storage: %s", ErrInternalError, err)
	}

	return result, nil
}
