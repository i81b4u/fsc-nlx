// Copyright © VNG Realisatie 2023
// Licensed under the EUPL
package query_test

import (
	"context"
	"errors"
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"

	"go.nlx.io/nlx/controller/adapters/manager"
	"go.nlx.io/nlx/controller/app/apiapp/query"
	"go.nlx.io/nlx/txlog-api/domain/record"
)

// nolint:funlen // these tests should not fit 100 lines
func TestListTXLogRecords(t *testing.T) {
	t.Parallel()

	tests := map[string]struct {
		setup   func(context.Context, *mocks)
		want    query.TXLogRecords
		wantErr error
	}{
		"when_storage_errors": {
			setup: func(ctx context.Context, m *mocks) {
				m.manager.EXPECT().
					ListTXLogRecords(ctx, &manager.ListTXLogRecordsRequest{Pagination: &manager.Pagination{}, Filters: []*manager.TXLogFilter{}}).
					Return(nil, errors.New("unexpected error"))
			},
			wantErr: &query.InternalError{},
		},
		"happy_flow_one": {
			setup: func(ctx context.Context, m *mocks) {
				m.manager.EXPECT().
					ListTXLogRecords(ctx, &manager.ListTXLogRecordsRequest{Pagination: &manager.Pagination{}, Filters: []*manager.TXLogFilter{}}).
					Return([]*manager.TXLogRecord{
						{
							TransactionID: "id-1",
							GrantHash:     "hash-1",
							ServiceName:   "service-1",
							Direction:     record.DirectionIn,
							Source: &manager.TXLogRecordSource{
								OutwayPeerID: "1",
							},
							Destination: &manager.TXLogRecordDestination{
								ServicePeerID: "2",
							},
							CreatedAt: time.Unix(1684938019, 0),
						},
					}, nil)
			},
			want: query.TXLogRecords{
				{
					TransactionID: "id-1",
					GrantHash:     "hash-1",
					ServiceName:   "service-1",
					Direction:     record.DirectionIn,
					Source: &query.TXLogRecordSource{
						OutwayPeerID: "1",
					},
					Destination: &query.TXLogRecordDestination{
						ServicePeerID: "2",
					},
					CreatedAt: time.Unix(1684938019, 0),
				},
			},
		},
		"happy_flow_multiple": {
			setup: func(ctx context.Context, m *mocks) {
				m.manager.EXPECT().
					ListTXLogRecords(ctx, &manager.ListTXLogRecordsRequest{Pagination: &manager.Pagination{}, Filters: []*manager.TXLogFilter{}}).
					Return([]*manager.TXLogRecord{
						{
							TransactionID: "id-1",
							GrantHash:     "hash-1",
							ServiceName:   "service-1",
							Direction:     record.DirectionIn,
							Source: &manager.TXLogRecordSource{
								OutwayPeerID: "1",
							},
							Destination: &manager.TXLogRecordDestination{
								ServicePeerID: "2",
							},
							CreatedAt: time.Unix(1684938019, 0),
						},
						{
							TransactionID: "id-2",
							GrantHash:     "hash-2",
							ServiceName:   "service-2",
							Direction:     record.DirectionOut,
							Source: &manager.TXLogRecordDelegatedSource{
								OutwayPeerID:    "1",
								DelegatorPeerID: "2",
							},
							Destination: &manager.TXLogRecordDestination{
								ServicePeerID: "3",
							},
							CreatedAt: time.Unix(1684938020, 0),
						},
						{
							TransactionID: "id-3",
							GrantHash:     "hash-3",
							ServiceName:   "service-3",
							Direction:     record.DirectionIn,
							Source: &manager.TXLogRecordSource{
								OutwayPeerID: "1",
							},
							Destination: &manager.TXLogRecordDelegatedDestination{
								ServicePeerID:   "2",
								DelegatorPeerID: "3",
							},
							CreatedAt: time.Unix(1684938021, 0),
						},
						{
							TransactionID: "id-4",
							GrantHash:     "hash-4",
							ServiceName:   "service-4",
							Direction:     record.DirectionOut,
							Source: &manager.TXLogRecordDelegatedSource{
								OutwayPeerID:    "1",
								DelegatorPeerID: "2",
							},
							Destination: &manager.TXLogRecordDelegatedDestination{
								ServicePeerID:   "3",
								DelegatorPeerID: "4",
							},
							CreatedAt: time.Unix(1684938022, 0),
						},
					}, nil)
			},
			want: query.TXLogRecords{
				{
					TransactionID: "id-1",
					GrantHash:     "hash-1",
					ServiceName:   "service-1",
					Direction:     record.DirectionIn,
					Source: &query.TXLogRecordSource{
						OutwayPeerID: "1",
					},
					Destination: &query.TXLogRecordDestination{
						ServicePeerID: "2",
					},
					CreatedAt: time.Unix(1684938019, 0),
				},
				{
					TransactionID: "id-2",
					GrantHash:     "hash-2",
					ServiceName:   "service-2",
					Direction:     record.DirectionOut,
					Source: &query.TXLogRecordDelegatedSource{
						OutwayPeerID:    "1",
						DelegatorPeerID: "2",
					},
					Destination: &query.TXLogRecordDestination{
						ServicePeerID: "3",
					},
					CreatedAt: time.Unix(1684938020, 0),
				},
				{
					TransactionID: "id-3",
					GrantHash:     "hash-3",
					ServiceName:   "service-3",
					Direction:     record.DirectionIn,
					Source: &query.TXLogRecordSource{
						OutwayPeerID: "1",
					},
					Destination: &query.TXLogRecordDelegatedDestination{
						ServicePeerID:   "2",
						DelegatorPeerID: "3",
					},
					CreatedAt: time.Unix(1684938021, 0),
				},
				{
					TransactionID: "id-4",
					GrantHash:     "hash-4",
					ServiceName:   "service-4",
					Direction:     record.DirectionOut,
					Source: &query.TXLogRecordDelegatedSource{
						OutwayPeerID:    "1",
						DelegatorPeerID: "2",
					},
					Destination: &query.TXLogRecordDelegatedDestination{
						ServicePeerID:   "3",
						DelegatorPeerID: "4",
					},
					CreatedAt: time.Unix(1684938022, 0),
				},
			},
		},
	}

	for name, tt := range tests {
		tt := tt

		t.Run(name, func(t *testing.T) {
			t.Parallel()

			mocks := newMocks(t)

			h, err := query.NewListTransactionLogRecordsHandler(mocks.manager)
			require.NoError(t, err)

			ctx := context.Background()

			tt.setup(ctx, mocks)

			actual, err := h.Handle(ctx, &query.ListTXLogRecordsHandlerArgs{Pagination: &query.Pagination{}})

			assert.Equal(t, tt.want, actual)

			if tt.wantErr == nil {
				assert.NoError(t, err)
			} else {
				assert.ErrorAs(t, err, &tt.wantErr)
			}
		})
	}
}
