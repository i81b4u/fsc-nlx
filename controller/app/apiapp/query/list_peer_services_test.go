// Copyright © VNG Realisatie 2023
// Licensed under the EUPL
package query_test

import (
	"context"
	"errors"
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"

	"go.nlx.io/nlx/controller/adapters/manager"
	"go.nlx.io/nlx/controller/app/apiapp/query"
)

// nolint:funlen // these tests should not fit 100 lines
func TestListPeerServices(t *testing.T) {
	t.Parallel()

	tests := map[string]struct {
		setup   func(context.Context, *mocks)
		want    query.PeerServices
		wantErr error
	}{
		"when_storage_errors": {
			setup: func(ctx context.Context, m *mocks) {
				m.manager.EXPECT().
					ListServices(ctx).
					Return(nil, errors.New("unexpected error"))
			},
			wantErr: &query.InternalError{},
		},
		"happy_flow_one": {
			setup: func(ctx context.Context, m *mocks) {
				m.manager.EXPECT().
					ListServices(ctx).
					Return(manager.Services{
						&manager.Service{
							PeerID:   "1",
							PeerName: "peer-1",
							Name:     "service-1",
						},
					}, nil)
			},
			want: query.PeerServices{
				&query.PeerService{
					PeerID:   "1",
					PeerName: "peer-1",
					Name:     "service-1",
				},
			},
		},
		"happy_flow_multiple": {
			setup: func(ctx context.Context, m *mocks) {
				m.manager.EXPECT().
					ListServices(ctx).
					Return(manager.Services{
						&manager.Service{
							PeerID:   "1",
							PeerName: "peer-1",
							Name:     "service-1",
						},
						&manager.Service{
							PeerID:   "2",
							PeerName: "peer-2",
							Name:     "service-2",
						},
						&manager.Service{
							PeerID:   "3",
							PeerName: "peer-3",
							Name:     "service-3",
						},
					}, nil)
			},
			want: query.PeerServices{
				&query.PeerService{
					PeerID:   "1",
					PeerName: "peer-1",
					Name:     "service-1",
				},
				&query.PeerService{
					PeerID:   "2",
					PeerName: "peer-2",
					Name:     "service-2",
				},
				&query.PeerService{
					PeerID:   "3",
					PeerName: "peer-3",
					Name:     "service-3",
				},
			},
		},
	}

	for name, tt := range tests {
		tt := tt

		t.Run(name, func(t *testing.T) {
			t.Parallel()

			mocks := newMocks(t)

			h, err := query.NewListPeerServicesHandler(mocks.manager, mocks.logger)
			require.NoError(t, err)

			ctx := context.Background()

			tt.setup(ctx, mocks)

			actual, err := h.Handle(ctx)

			if tt.wantErr == nil {
				assert.NoError(t, err)
				assert.Equal(t, tt.want, actual)
			} else {
				assert.ErrorAs(t, err, &tt.wantErr)
			}
		})
	}
}
