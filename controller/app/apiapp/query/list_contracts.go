// Copyright © VNG Realisatie 2023
// Licensed under the EUPL

package query

import (
	"context"
	"fmt"

	"go.nlx.io/nlx/controller/adapters/manager"
)

type ListContractsHandler struct {
	manager manager.Manager
}

type ListContractsFilter struct {
	ContentHash string
	GrantType   GrantType
}

type GrantType string

const (
	GrantTypePeerRegistration            GrantType = "peer_registration"
	GrantTypeServicePublication          GrantType = "service_publication"
	GrantTypeServiceConnection           GrantType = "service_connection"
	GrantTypeDelegatedServicePublication GrantType = "delegated_service_publication"
	GrantTypeDelegatedServiceConnection  GrantType = "delegated_service_connection"
)

type Contracts []*Contract

func NewListContractsHandler(m manager.Manager) (*ListContractsHandler, error) {
	if m == nil {
		return nil, fmt.Errorf("manager is required")
	}

	return &ListContractsHandler{
		manager: m,
	}, nil
}

func (h *ListContractsHandler) Handle(ctx context.Context, filters []*ListContractsFilter) (Contracts, error) {
	contentHashes := make([]string, 0)
	grantTypes := make([]manager.GrantType, 0)

	for _, filter := range filters {
		if filter.ContentHash != "" {
			contentHashes = append(contentHashes, filter.ContentHash)
		}

		if filter.GrantType != "" {
			grantTypes = append(grantTypes, convertGrantType(filter.GrantType))
		}
	}

	contracts, err := h.manager.ListContracts(ctx, contentHashes, grantTypes)
	if err != nil {
		return nil, fmt.Errorf("%s: %w", newInternalError("could not get contracts from manager"), err)
	}

	resp := make(Contracts, len(contracts))

	for i, c := range contracts {
		resp[i] = convertContract(c)
	}

	return resp, nil
}

func convertGrantType(input GrantType) manager.GrantType {
	switch input {
	case GrantTypePeerRegistration:
		return manager.GrantTypePeerRegistration
	case GrantTypeServicePublication:
		return manager.GrantTypeServicePublication
	case GrantTypeServiceConnection:
		return manager.GrantTypeServiceConnection
	case GrantTypeDelegatedServicePublication:
		return manager.GrantTypeDelegatedServicePublication
	case GrantTypeDelegatedServiceConnection:
		return manager.GrantTypeDelegatedServiceConnection
	default:
		return manager.GrantTypePeerUnspecified
	}
}
