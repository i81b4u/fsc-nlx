// Copyright © VNG Realisatie 2023
// Licensed under the EUPL
package query_test

import (
	"context"
	"errors"
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"

	"go.nlx.io/nlx/controller/adapters/manager"
	"go.nlx.io/nlx/controller/app/apiapp/query"
)

// nolint:funlen // these tests do not fit in 100 lines
func TestListContracts(t *testing.T) {
	t.Parallel()

	var tests = map[string]struct {
		setup   func(context.Context, *mocks)
		want    query.Contracts
		wantErr error
	}{
		"when_manager_errors": {
			setup: func(ctx context.Context, m *mocks) {
				m.manager.EXPECT().
					ListContracts(ctx, []string{}, []manager.GrantType{}).
					Return(nil, errors.New("unexpected error"))
			},
			wantErr: &query.InternalError{},
		},
		"happy_flow_one": {
			setup: func(ctx context.Context, m *mocks) {
				m.manager.EXPECT().
					ListContracts(ctx, []string{}, []manager.GrantType{}).
					Return(manager.Contracts{
						{
							ID:            []byte("ce754ff9-3d1d-4dac-9f76-2267ff36cb25"),
							GroupID:       "mock-group",
							Hash:          "hash-1",
							HashAlgorithm: manager.HashAlgSHA3_512,
							CreatedAt:     time.Unix(1684938019, 0),
							ValidFrom:     time.Unix(1684938019, 0),
							ValidUntil:    time.Unix(1684939000, 0),
							Peers: map[string]*manager.Peer{
								"12345678901234567890": {
									ID:   "12345678901234567890",
									Name: "Peer A",
								},
								"12345678901234567899": {
									ID:   "12345678901234567899",
									Name: "Peer B",
								},
							},
							RevokeSignatures: map[string]manager.Signature{},
							AcceptSignatures: map[string]manager.Signature{
								"12345678901234567890": {
									SignedAt: time.Unix(1684938020, 0),
								},
							},
							RejectSignatures: map[string]manager.Signature{},
							PeerRegistrationGrant: &manager.PeerRegistrationGrant{
								DirectoryPeerID: "12345678901234567899",
								PeerID:          "12345678901234567890",
								PeerName:        "Peer A",
							},
						},
					}, nil)
			},
			want: query.Contracts{
				{
					ID:            []byte("ce754ff9-3d1d-4dac-9f76-2267ff36cb25"),
					GroupID:       "mock-group",
					Hash:          "hash-1",
					HashAlgorithm: query.HashAlgSHA3_512,
					CreatedAt:     time.Unix(1684938019, 0),
					ValidFrom:     time.Unix(1684938019, 0),
					ValidUntil:    time.Unix(1684939000, 0),
					Peers: map[string]*query.Peer{
						"12345678901234567890": {
							ID:   "12345678901234567890",
							Name: "Peer A",
						},
						"12345678901234567899": {
							ID:   "12345678901234567899",
							Name: "Peer B",
						},
					},
					RejectSignatures: map[string]query.Signature{},
					AcceptSignatures: map[string]query.Signature{
						"12345678901234567890": {
							SignedAt: time.Unix(1684938020, 0),
						},
					},
					RevokeSignatures: map[string]query.Signature{},
					PeerRegistrationGrant: &query.PeerRegistrationGrant{
						DirectoryPeerID: "12345678901234567899",
						PeerID:          "12345678901234567890",
						PeerName:        "Peer A",
					},
					ServicePublicationGrants:          []*query.ServicePublicationGrant{},
					ServiceConnectionGrants:           []*query.ServiceConnectionGrant{},
					DelegatedServicePublicationGrants: []*query.DelegatedServicePublicationGrant{},
					DelegatedServiceConnectionGrants:  []*query.DelegatedServiceConnectionGrant{},
				},
			},
		},
		"happy_flow": {
			setup: func(ctx context.Context, m *mocks) {
				m.manager.EXPECT().
					ListContracts(ctx, []string{}, []manager.GrantType{}).
					Return(
						manager.Contracts{
							{
								ID:            []byte("ce754ff9-3d1d-4dac-9f76-2267ff36cb25"),
								GroupID:       "mock-group",
								Hash:          "hash-1",
								HashAlgorithm: manager.HashAlgSHA3_512,
								CreatedAt:     time.Unix(1684938019, 0),
								ValidFrom:     time.Unix(1684938019, 0),
								ValidUntil:    time.Unix(1684939000, 0),
								Peers: map[string]*manager.Peer{
									"12345678901234567890": {
										ID:   "12345678901234567890",
										Name: "Peer A",
									},
									"12345678901234567899": {
										ID:   "12345678901234567890",
										Name: "Peer B",
									},
								},
								AcceptSignatures: map[string]manager.Signature{
									"12345678901234567890": {
										SignedAt: time.Unix(1684938020, 0),
									},
								},
								RejectSignatures:      map[string]manager.Signature{},
								RevokeSignatures:      map[string]manager.Signature{},
								HasRejected:           false,
								HasAccepted:           false,
								HasRevoked:            false,
								PeerRegistrationGrant: nil,
								ServicePublicationGrants: []*manager.ServicePublicationGrant{
									{
										DirectoryPeerID: "12345678901234567899",
										ServicePeerID:   "12345678901234567890",
										ServiceName:     "mock-service",
									},
								},
								ServiceConnectionGrants:           nil,
								DelegatedServicePublicationGrants: nil,
								DelegatedServiceConnectionGrants:  nil,
							},
							{
								ID:            []byte("ce754ff9-3d1d-4dac-9f76-2267ff36cb26"),
								GroupID:       "mock-group",
								Hash:          "hash-2",
								HashAlgorithm: manager.HashAlgSHA3_512,
								Peers: map[string]*manager.Peer{
									"12345678901234567890": {
										ID:   "12345678901234567890",
										Name: "Peer A",
									},
									"12345678901234567891": {
										ID:   "12345678901234567891",
										Name: "Peer B",
									},
								},
								CreatedAt:        time.Unix(1684938010, 0),
								ValidFrom:        time.Unix(1684938019, 0),
								ValidUntil:       time.Unix(1684949000, 0),
								RevokeSignatures: map[string]manager.Signature{},
								AcceptSignatures: map[string]manager.Signature{},
								RejectSignatures: map[string]manager.Signature{
									"12345678901234567891": {
										SignedAt: time.Unix(1684938020, 0),
									},
								},
								ServiceConnectionGrants: []*manager.ServiceConnectionGrant{
									{
										ServicePeerID:               "12345678901234567891",
										ServiceName:                 "mock-service",
										OutwayPeerID:                "12345678901234567890",
										OutwayCertificateThumbprint: "_0yCZurQcAtocDFSdbWcwhFJNzQHoZw1paeFr7D2G-c",
									},
								},
							},
							{
								ID:            []byte("ce754ff9-3d1d-4dac-9f76-2267ff36cb27"),
								GroupID:       "mock-group",
								Hash:          "hash-3",
								HashAlgorithm: manager.HashAlgSHA3_512,
								Peers: map[string]*manager.Peer{
									"12345678901234567890": {
										ID:   "12345678901234567890",
										Name: "Peer A",
									},
									"12345678901234567899": {
										ID:   "12345678901234567899",
										Name: "Peer B",
									},
									"12345678901234567891": {
										ID:   "12345678901234567891",
										Name: "Peer C",
									},
								},
								CreatedAt:  time.Unix(1684938021, 0),
								ValidFrom:  time.Unix(1684938000, 0),
								ValidUntil: time.Unix(1684949000, 0),
								AcceptSignatures: map[string]manager.Signature{
									"12345678901234567890": {
										SignedAt: time.Unix(1684938020, 0),
									},
									"12345678901234567891": {
										SignedAt: time.Unix(1684938025, 0),
									},
								},
								RejectSignatures: map[string]manager.Signature{},
								RevokeSignatures: map[string]manager.Signature{
									"12345678901234567890": {
										SignedAt: time.Unix(1684938030, 0),
									},
								},
								DelegatedServicePublicationGrants: []*manager.DelegatedServicePublicationGrant{
									{
										DirectoryPeerID: "12345678901234567899",
										DelegatorPeerID: "12345678901234567891",
										ServicePeerID:   "12345678901234567890",
										ServiceName:     "mock-service",
									},
								},
							},
							{
								ID:            []byte("ce754ff9-3d1d-4dac-9f76-2267ff36cb28"),
								GroupID:       "mock-group",
								Hash:          "hash-4",
								HashAlgorithm: manager.HashAlgSHA3_512,
								Peers: map[string]*manager.Peer{
									"12345678901234567890": {
										ID:   "12345678901234567890",
										Name: "Peer A",
									},
									"12345678901234567891": {
										ID:   "12345678901234567891",
										Name: "Peer B",
									},
									"12345678901234567892": {
										ID:   "12345678901234567892",
										Name: "Peer C",
									},
								},
								CreatedAt:  time.Unix(1684938021, 0),
								ValidFrom:  time.Unix(1684938000, 0),
								ValidUntil: time.Unix(1684949000, 0),
								AcceptSignatures: map[string]manager.Signature{
									"12345678901234567890": {
										SignedAt: time.Unix(1684938020, 0),
									},
									"12345678901234567891": {
										SignedAt: time.Unix(1684938025, 0),
									},
								},
								RejectSignatures: map[string]manager.Signature{},
								RevokeSignatures: map[string]manager.Signature{
									"12345678901234567890": {
										SignedAt: time.Unix(1684938030, 0),
									},
								},
								DelegatedServiceConnectionGrants: []*manager.DelegatedServiceConnectionGrant{
									{
										DelegatorPeerID:             "12345678901234567892",
										ServicePeerID:               "12345678901234567891",
										ServiceName:                 "mock-service",
										OutwayPeerID:                "12345678901234567890",
										OutwayCertificateThumbprint: "_0yCZurQcAtocDFSdbWcwhFJNzQHoZw1paeFr7D2G-c",
									},
								},
							},
						},
						nil,
					)
			},
			want: query.Contracts{
				{
					ID:            []byte("ce754ff9-3d1d-4dac-9f76-2267ff36cb25"),
					GroupID:       "mock-group",
					Hash:          "hash-1",
					HashAlgorithm: query.HashAlgSHA3_512,
					CreatedAt:     time.Unix(1684938019, 0),
					ValidFrom:     time.Unix(1684938019, 0),
					ValidUntil:    time.Unix(1684939000, 0),
					Peers: map[string]*query.Peer{
						"12345678901234567890": {
							ID:   "12345678901234567890",
							Name: "Peer A",
						},
						"12345678901234567899": {
							ID:   "12345678901234567899",
							Name: "Peer B",
						},
					},
					RejectSignatures: map[string]query.Signature{},
					AcceptSignatures: map[string]query.Signature{
						"12345678901234567890": {
							SignedAt: time.Unix(1684938020, 0),
						},
					},
					RevokeSignatures: map[string]query.Signature{},
					ServicePublicationGrants: []*query.ServicePublicationGrant{{
						DirectoryPeerID: "12345678901234567899",
						ServicePeerID:   "12345678901234567890",
						ServiceName:     "mock-service",
					}},
					ServiceConnectionGrants:           []*query.ServiceConnectionGrant{},
					DelegatedServicePublicationGrants: []*query.DelegatedServicePublicationGrant{},
					DelegatedServiceConnectionGrants:  []*query.DelegatedServiceConnectionGrant{},
				},
				{
					ID:            []byte("ce754ff9-3d1d-4dac-9f76-2267ff36cb26"),
					GroupID:       "mock-group",
					Hash:          "hash-2",
					HashAlgorithm: query.HashAlgSHA3_512,
					CreatedAt:     time.Unix(1684938010, 0),
					ValidFrom:     time.Unix(1684938019, 0),
					ValidUntil:    time.Unix(1684949000, 0),
					Peers: map[string]*query.Peer{
						"12345678901234567890": {
							ID:   "12345678901234567890",
							Name: "Peer A",
						},
						"12345678901234567891": {
							ID:   "12345678901234567891",
							Name: "Peer B",
						},
					},
					RejectSignatures: map[string]query.Signature{"12345678901234567891": {
						SignedAt: time.Unix(1684938020, 0),
					}},
					AcceptSignatures:         map[string]query.Signature{},
					RevokeSignatures:         map[string]query.Signature{},
					ServicePublicationGrants: []*query.ServicePublicationGrant{},
					ServiceConnectionGrants: []*query.ServiceConnectionGrant{
						{
							ServicePeerID:               "12345678901234567891",
							ServiceName:                 "mock-service",
							OutwayPeerID:                "12345678901234567890",
							OutwayCertificateThumbprint: "_0yCZurQcAtocDFSdbWcwhFJNzQHoZw1paeFr7D2G-c",
						},
					},
					DelegatedServicePublicationGrants: []*query.DelegatedServicePublicationGrant{},
					DelegatedServiceConnectionGrants:  []*query.DelegatedServiceConnectionGrant{},
				},
				{
					ID:            []byte("ce754ff9-3d1d-4dac-9f76-2267ff36cb27"),
					GroupID:       "mock-group",
					Hash:          "hash-3",
					HashAlgorithm: query.HashAlgSHA3_512,
					CreatedAt:     time.Unix(1684938021, 0),
					ValidFrom:     time.Unix(1684938000, 0),
					ValidUntil:    time.Unix(1684949000, 0),
					Peers: map[string]*query.Peer{
						"12345678901234567890": {
							ID:   "12345678901234567890",
							Name: "Peer A",
						},
						"12345678901234567899": {
							ID:   "12345678901234567899",
							Name: "Peer B",
						},
						"12345678901234567891": {
							ID:   "12345678901234567891",
							Name: "Peer C",
						},
					},
					RejectSignatures: map[string]query.Signature{},
					AcceptSignatures: map[string]query.Signature{
						"12345678901234567890": {
							SignedAt: time.Unix(1684938020, 0),
						},
						"12345678901234567891": {
							SignedAt: time.Unix(1684938025, 0),
						},
					},
					RevokeSignatures: map[string]query.Signature{
						"12345678901234567890": {
							SignedAt: time.Unix(1684938030, 0),
						},
					},
					ServicePublicationGrants: []*query.ServicePublicationGrant{},
					ServiceConnectionGrants:  []*query.ServiceConnectionGrant{},
					DelegatedServicePublicationGrants: []*query.DelegatedServicePublicationGrant{
						{
							DirectoryPeerID: "12345678901234567899",
							DelegatorPeerID: "12345678901234567891",
							ServicePeerID:   "12345678901234567890",
							ServiceName:     "mock-service",
						},
					},
					DelegatedServiceConnectionGrants: []*query.DelegatedServiceConnectionGrant{},
				},
				{
					ID:            []byte("ce754ff9-3d1d-4dac-9f76-2267ff36cb28"),
					GroupID:       "mock-group",
					Hash:          "hash-4",
					HashAlgorithm: query.HashAlgSHA3_512,
					CreatedAt:     time.Unix(1684938021, 0),
					ValidFrom:     time.Unix(1684938000, 0),
					ValidUntil:    time.Unix(1684949000, 0),
					Peers: map[string]*query.Peer{
						"12345678901234567890": {
							ID:   "12345678901234567890",
							Name: "Peer A",
						},
						"12345678901234567891": {
							ID:   "12345678901234567891",
							Name: "Peer B",
						},
						"12345678901234567892": {
							ID:   "12345678901234567892",
							Name: "Peer C",
						},
					},
					RejectSignatures: map[string]query.Signature{},
					AcceptSignatures: map[string]query.Signature{
						"12345678901234567890": {
							SignedAt: time.Unix(1684938020, 0),
						},
						"12345678901234567891": {
							SignedAt: time.Unix(1684938025, 0),
						},
					},
					RevokeSignatures: map[string]query.Signature{
						"12345678901234567890": {
							SignedAt: time.Unix(1684938030, 0),
						},
					},
					ServicePublicationGrants:          []*query.ServicePublicationGrant{},
					ServiceConnectionGrants:           []*query.ServiceConnectionGrant{},
					DelegatedServicePublicationGrants: []*query.DelegatedServicePublicationGrant{},
					DelegatedServiceConnectionGrants: []*query.DelegatedServiceConnectionGrant{
						{
							DelegatorPeerID:             "12345678901234567892",
							OutwayPeerID:                "12345678901234567890",
							OutwayCertificateThumbprint: "_0yCZurQcAtocDFSdbWcwhFJNzQHoZw1paeFr7D2G-c",
							ServicePeerID:               "12345678901234567891",
							ServiceName:                 "mock-service",
						},
					},
				},
			},
		},
	}

	for name, tt := range tests {
		tt := tt

		t.Run(name, func(t *testing.T) {
			t.Parallel()

			mocks := newMocks(t)

			h, err := query.NewListContractsHandler(mocks.manager)
			require.NoError(t, err)

			ctx := context.Background()

			if tt.setup != nil {
				tt.setup(ctx, mocks)
			}

			actual, err := h.Handle(ctx, []*query.ListContractsFilter{})

			if tt.wantErr == nil {
				assert.NoError(t, err)
				assert.Equal(t, tt.want, actual)
			} else {
				assert.ErrorAs(t, err, &tt.wantErr)
			}
		})
	}
}
