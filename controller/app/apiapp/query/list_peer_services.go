// Copyright © VNG Realisatie 2023
// Licensed under the EUPL

package query

import (
	"context"
	"fmt"
	"go.nlx.io/nlx/controller/adapters/logger"
	"go.nlx.io/nlx/controller/adapters/manager"
)

type ListPeerServicesHandler struct {
	m manager.Manager
	l logger.Logger
}

type PeerServices []interface{}

type PeerService struct {
	Name     string
	PeerID   string
	PeerName string
}

type DelegatedPeerService struct {
	Name          string
	PeerID        string
	PeerName      string
	DelegatorID   string
	DelegatorName string
}

func NewListPeerServicesHandler(m manager.Manager, l logger.Logger) (*ListPeerServicesHandler, error) {
	if m == nil {
		return nil, fmt.Errorf("manager is required")
	}

	if l == nil {
		return nil, fmt.Errorf("logger is required")
	}

	return &ListPeerServicesHandler{
		m: m,
		l: l,
	}, nil
}

func (h *ListPeerServicesHandler) Handle(ctx context.Context) (PeerServices, error) {
	services, err := h.m.ListServices(ctx)
	if err != nil {
		return nil, fmt.Errorf("%s: %w", newInternalError("could not get services from manager"), err)
	}

	resp := make(PeerServices, len(services))

	for i, s := range services {
		switch service := s.(type) {
		case *manager.Service:
			resp[i] = &PeerService{
				Name:     service.Name,
				PeerID:   service.PeerID,
				PeerName: service.PeerName,
			}

		case *manager.DelegatedService:
			resp[i] = &DelegatedPeerService{
				Name:          service.Name,
				PeerID:        service.ProviderPeerID,
				PeerName:      service.ProviderPeerName,
				DelegatorID:   service.DelegateeID,
				DelegatorName: service.DelegateeName,
			}

		default:
			h.l.Info(fmt.Sprintf("unknown service type %v", service))
		}
	}

	return resp, nil
}
