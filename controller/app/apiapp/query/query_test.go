// Copyright © VNG Realisatie 2023
// Licensed under the EUPL

package query_test

import (
	"net/url"
	"testing"

	"github.com/stretchr/testify/require"

	mock_clock "go.nlx.io/nlx/common/clock/mock"
	"go.nlx.io/nlx/controller/adapters/logger"
	mock_manager "go.nlx.io/nlx/controller/adapters/manager/mock"
	mock_storage "go.nlx.io/nlx/controller/adapters/storage/mock"
	discardlogger "go.nlx.io/nlx/txlog-api/adapters/logger/discard"
)

type mocks struct {
	manager *mock_manager.MockManager
	storage *mock_storage.MockStorage
	clock   *mock_clock.MockClock
	logger  logger.Logger
}

func newMocks(t *testing.T) *mocks {
	return &mocks{
		manager: mock_manager.NewMockManager(t),
		storage: mock_storage.NewMockStorage(t),
		clock:   mock_clock.NewMockClock(t),
		logger:  discardlogger.New(),
	}
}

func newURL(t *testing.T, u string) *url.URL {
	ur, err := url.Parse(u)
	require.NoError(t, err)

	return ur
}
