// Copyright © VNG Realisatie 2023
// Licensed under the EUPL

package query

import (
	"context"
	"fmt"
	"time"

	"go.nlx.io/nlx/controller/adapters/manager"
)

type GetContractHandler struct {
	manager manager.Manager
}

type Contract struct {
	ID                                []byte
	Hash                              string
	HashAlgorithm                     HashAlg
	GroupID                           string
	CreatedAt                         time.Time
	ValidFrom                         time.Time
	ValidUntil                        time.Time
	Peers                             map[string]*Peer
	AcceptSignatures                  map[string]Signature
	RejectSignatures                  map[string]Signature
	RevokeSignatures                  map[string]Signature
	HasRejected                       bool
	HasAccepted                       bool
	HasRevoked                        bool
	PeerRegistrationGrant             *PeerRegistrationGrant
	ServicePublicationGrants          []*ServicePublicationGrant
	ServiceConnectionGrants           []*ServiceConnectionGrant
	DelegatedServicePublicationGrants []*DelegatedServicePublicationGrant
	DelegatedServiceConnectionGrants  []*DelegatedServiceConnectionGrant
}

type HashAlg int32

const (
	HashAlgUnspecified HashAlg = iota
	HashAlgSHA3_512
)

type PeerRegistrationGrant struct {
	Hash            string
	DirectoryPeerID string
	PeerID          string
	PeerName        string
}

type ServicePublicationGrant struct {
	Hash            string
	DirectoryPeerID string
	ServicePeerID   string
	ServiceName     string
}

type ServiceConnectionGrant struct {
	Hash                        string
	ServicePeerID               string
	ServiceName                 string
	OutwayPeerID                string
	OutwayCertificateThumbprint string
}

type DelegatedServiceConnectionGrant struct {
	Hash                        string
	DelegatorPeerID             string
	OutwayPeerID                string
	OutwayCertificateThumbprint string
	ServicePeerID               string
	ServiceName                 string
}

type DelegatedServicePublicationGrant struct {
	Hash            string
	DirectoryPeerID string
	DelegatorPeerID string
	ServicePeerID   string
	ServiceName     string
}

type Peer struct {
	ID   string
	Name string
}

type Signature struct {
	SignedAt time.Time
}

type ListContractFilter struct {
	ContentHash string
}

func NewGetContractHandler(m manager.Manager) (*GetContractHandler, error) {
	if m == nil {
		return nil, fmt.Errorf("manager is required")
	}

	return &GetContractHandler{
		manager: m,
	}, nil
}

func (h *GetContractHandler) Handle(ctx context.Context, contentHash string) (*Contract, error) {
	contracts, err := h.manager.ListContracts(ctx, []string{contentHash}, []manager.GrantType{})
	if err != nil {
		return nil, fmt.Errorf("%s: %w", newInternalError("could not get contracts from manager"), err)
	}

	if len(contracts) != 1 {
		return nil, newInternalError("contract not returned by manager")
	}

	c := contracts[0]

	contract := convertContract(c)

	return contract, nil
}

func convertContract(contract *manager.Contract) *Contract {
	var peerRegistrationGrant *PeerRegistrationGrant

	if contract.PeerRegistrationGrant != nil {
		grant := contract.PeerRegistrationGrant

		peerRegistrationGrant = &PeerRegistrationGrant{
			Hash:            grant.Hash,
			DirectoryPeerID: grant.DirectoryPeerID,
			PeerID:          grant.PeerID,
			PeerName:        grant.PeerName,
		}
	}

	servicePublicationGrants := make([]*ServicePublicationGrant, len(contract.ServicePublicationGrants))

	for i, grant := range contract.ServicePublicationGrants {
		servicePublicationGrants[i] = &ServicePublicationGrant{
			Hash:            grant.Hash,
			DirectoryPeerID: grant.DirectoryPeerID,
			ServicePeerID:   grant.ServicePeerID,
			ServiceName:     grant.ServiceName,
		}
	}

	serviceConnectionGrants := make([]*ServiceConnectionGrant, len(contract.ServiceConnectionGrants))

	for i, grant := range contract.ServiceConnectionGrants {
		serviceConnectionGrants[i] = &ServiceConnectionGrant{
			Hash:                        grant.Hash,
			ServicePeerID:               grant.ServicePeerID,
			ServiceName:                 grant.ServiceName,
			OutwayPeerID:                grant.OutwayPeerID,
			OutwayCertificateThumbprint: grant.OutwayCertificateThumbprint,
		}
	}

	delegatedServiceConnectionGrants := make([]*DelegatedServiceConnectionGrant, len(contract.DelegatedServiceConnectionGrants))

	for i, grant := range contract.DelegatedServiceConnectionGrants {
		delegatedServiceConnectionGrants[i] = &DelegatedServiceConnectionGrant{
			Hash:                        grant.Hash,
			DelegatorPeerID:             grant.DelegatorPeerID,
			OutwayPeerID:                grant.OutwayPeerID,
			OutwayCertificateThumbprint: grant.OutwayCertificateThumbprint,
			ServicePeerID:               grant.ServicePeerID,
			ServiceName:                 grant.ServiceName,
		}
	}

	delegatedServicePublicationGrants := make([]*DelegatedServicePublicationGrant, len(contract.DelegatedServicePublicationGrants))

	for i, grant := range contract.DelegatedServicePublicationGrants {
		delegatedServicePublicationGrants[i] = &DelegatedServicePublicationGrant{
			Hash:            grant.Hash,
			DirectoryPeerID: grant.DirectoryPeerID,
			DelegatorPeerID: grant.DelegatorPeerID,
			ServicePeerID:   grant.ServicePeerID,
			ServiceName:     grant.ServiceName,
		}
	}

	convertedContract := &Contract{
		ID:                                contract.ID,
		Hash:                              contract.Hash,
		HashAlgorithm:                     HashAlg(contract.HashAlgorithm),
		GroupID:                           contract.GroupID,
		CreatedAt:                         contract.CreatedAt,
		ValidFrom:                         contract.ValidFrom,
		ValidUntil:                        contract.ValidUntil,
		Peers:                             make(map[string]*Peer),
		AcceptSignatures:                  convertSignatures(contract.AcceptSignatures),
		RejectSignatures:                  convertSignatures(contract.RejectSignatures),
		RevokeSignatures:                  convertSignatures(contract.RevokeSignatures),
		HasRejected:                       contract.HasRejected,
		HasAccepted:                       contract.HasAccepted,
		HasRevoked:                        contract.HasRevoked,
		PeerRegistrationGrant:             peerRegistrationGrant,
		ServicePublicationGrants:          servicePublicationGrants,
		ServiceConnectionGrants:           serviceConnectionGrants,
		DelegatedServicePublicationGrants: delegatedServicePublicationGrants,
		DelegatedServiceConnectionGrants:  delegatedServiceConnectionGrants,
	}

	for peerID, p := range contract.Peers {
		peer := &Peer{
			ID:   peerID,
			Name: p.Name,
		}
		convertedContract.Peers[peerID] = peer
	}

	return convertedContract
}

func convertSignatures(signatures map[string]manager.Signature) map[string]Signature {
	convertedSignatures := make(map[string]Signature)
	for peerID, signature := range signatures {
		convertedSignatures[peerID] = Signature{SignedAt: signature.SignedAt}
	}

	return convertedSignatures
}
