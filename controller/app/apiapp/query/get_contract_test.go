// Copyright © VNG Realisatie 2023
// Licensed under the EUPL
package query_test

import (
	"context"
	"testing"
	"time"

	"github.com/pkg/errors"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"

	"go.nlx.io/nlx/controller/adapters/manager"
	"go.nlx.io/nlx/controller/app/apiapp/query"
)

// nolint:funlen // these tests do not fit in 100 lines
func TestGetContract(t *testing.T) {
	t.Parallel()

	tests := map[string]struct {
		setup   func(context.Context, *mocks)
		args    string
		want    *query.Contract
		wantErr error
	}{
		"when_manager_errors": {
			args: "hash-1",
			setup: func(ctx context.Context, m *mocks) {
				m.manager.EXPECT().
					ListContracts(ctx, []string{"hash-1"}, []manager.GrantType{}).
					Return(nil, errors.New("unexpected error"))
			},
			wantErr: &query.InternalError{},
		},
		"when_contract_does_not_exist": {
			args: "hash-1",
			setup: func(ctx context.Context, m *mocks) {
				m.manager.EXPECT().
					ListContracts(ctx, []string{"hash-1"}, []manager.GrantType{}).
					Return(manager.Contracts{}, nil)
			},
			wantErr: &query.InternalError{},
		},
		"happy_flow": {
			args: "hash-1",
			setup: func(ctx context.Context, m *mocks) {
				m.manager.EXPECT().
					ListContracts(ctx, []string{"hash-1"}, []manager.GrantType{}).
					Return(manager.Contracts{
						{
							ID:            []byte("ce754ff9-3d1d-4dac-9f76-2267ff36cb25"),
							GroupID:       "mock-group",
							Hash:          "hash-1",
							HashAlgorithm: manager.HashAlgSHA3_512,
							CreatedAt:     time.Unix(1684938019, 0),
							ValidFrom:     time.Unix(1684938019, 0),
							ValidUntil:    time.Unix(1684939000, 0),
							Peers: map[string]*manager.Peer{
								"12345678901234567890": {
									ID:   "12345678901234567890",
									Name: "Peer A",
								},
								"12345678901234567891": {
									ID:   "12345678901234567891",
									Name: "Peer B",
								},
							},
							RejectSignatures: map[string]manager.Signature{
								"12345678901234567890": {SignedAt: time.Unix(1684938020, 0)},
							},
							ServiceConnectionGrants: []*manager.ServiceConnectionGrant{{
								ServicePeerID:               "12345678901234567891",
								ServiceName:                 "mock-service",
								OutwayPeerID:                "12345678901234567890",
								OutwayCertificateThumbprint: "_0yCZurQcAtocDFSdbWcwhFJNzQHoZw1paeFr7D2G-c",
							}},
						},
					}, nil)
			},
			want: &query.Contract{
				ID:            []byte("ce754ff9-3d1d-4dac-9f76-2267ff36cb25"),
				GroupID:       "mock-group",
				Hash:          "hash-1",
				HashAlgorithm: query.HashAlgSHA3_512,
				CreatedAt:     time.Unix(1684938019, 0),
				ValidFrom:     time.Unix(1684938019, 0),
				ValidUntil:    time.Unix(1684939000, 0),
				Peers: map[string]*query.Peer{
					"12345678901234567890": {
						ID:   "12345678901234567890",
						Name: "Peer A",
					},
					"12345678901234567891": {
						ID:   "12345678901234567891",
						Name: "Peer B",
					},
				},
				RejectSignatures: map[string]query.Signature{
					"12345678901234567890": {SignedAt: time.Unix(1684938020, 0)},
				},
				AcceptSignatures:         map[string]query.Signature{},
				RevokeSignatures:         map[string]query.Signature{},
				ServicePublicationGrants: []*query.ServicePublicationGrant{},
				ServiceConnectionGrants: []*query.ServiceConnectionGrant{{
					ServicePeerID:               "12345678901234567891",
					ServiceName:                 "mock-service",
					OutwayPeerID:                "12345678901234567890",
					OutwayCertificateThumbprint: "_0yCZurQcAtocDFSdbWcwhFJNzQHoZw1paeFr7D2G-c",
				}},
				DelegatedServicePublicationGrants: []*query.DelegatedServicePublicationGrant{},
				DelegatedServiceConnectionGrants:  []*query.DelegatedServiceConnectionGrant{},
			},
		},
	}

	for name, tt := range tests {
		tt := tt

		t.Run(name, func(t *testing.T) {
			t.Parallel()

			mocks := newMocks(t)

			h, err := query.NewGetContractHandler(mocks.manager)
			require.NoError(t, err)

			ctx := context.Background()

			if tt.setup != nil {
				tt.setup(ctx, mocks)
			}

			actual, err := h.Handle(ctx, tt.args)

			if tt.wantErr == nil {
				assert.NoError(t, err)
				assert.Equal(t, tt.want, actual)
			} else {
				assert.ErrorAs(t, err, &tt.wantErr)
			}
		})
	}
}
