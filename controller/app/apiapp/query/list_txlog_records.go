// Copyright © VNG Realisatie 2022
// Licensed under the EUPL

package query

import (
	"context"
	"fmt"
	"time"

	"github.com/pkg/errors"

	"go.nlx.io/nlx/controller/adapters/manager"
	"go.nlx.io/nlx/txlog-api/domain/record"
)

type ListTXLogRecordsHandler struct {
	manager manager.Manager
}

func NewListTransactionLogRecordsHandler(m manager.Manager) (*ListTXLogRecordsHandler, error) {
	if m == nil {
		return nil, fmt.Errorf("manager is required")
	}

	return &ListTXLogRecordsHandler{
		manager: m,
	}, nil
}

type TXLogFilter struct {
	Period        *Period
	TransactionID string
	ServiceName   string
	GrantHash     string
	PeerID        string
}

type ListTXLogRecordsHandlerArgs struct {
	DataSourcePeerID string
	Pagination       *Pagination
	Filters          []*TXLogFilter
}

type TXLogRecords []*TXLogRecord

type TXLogRecord struct {
	TransactionID string
	GrantHash     string
	ServiceName   string
	Direction     record.Direction
	Source        interface{}
	Destination   interface{}
	CreatedAt     time.Time
}

type TXLogRecordSource struct {
	OutwayPeerID string
}

type TXLogRecordDelegatedSource struct {
	OutwayPeerID    string
	DelegatorPeerID string
}

type TXLogRecordDestination struct {
	ServicePeerID string
}

type TXLogRecordDelegatedDestination struct {
	ServicePeerID   string
	DelegatorPeerID string
}

func (h *ListTXLogRecordsHandler) Handle(ctx context.Context, args *ListTXLogRecordsHandlerArgs) (TXLogRecords, error) {
	req, err := reqToRepo(args)
	if err != nil {
		return nil, errors.Wrap(err, "invalid request in transaction log records handler")
	}

	records, err := h.manager.ListTXLogRecords(ctx, req)
	if err != nil {
		return nil, mapError(err, "could not get tx log records from manager")
	}

	return respToRecords(records)
}

func respToRecords(recs []*manager.TXLogRecord) (TXLogRecords, error) {
	records := make(TXLogRecords, len(recs))

	for i, r := range recs {
		var src interface{}
		switch s := r.Source.(type) {
		case *manager.TXLogRecordSource:
			src = &TXLogRecordSource{
				OutwayPeerID: s.OutwayPeerID,
			}
		case *manager.TXLogRecordDelegatedSource:
			src = &TXLogRecordDelegatedSource{
				OutwayPeerID:    s.OutwayPeerID,
				DelegatorPeerID: s.DelegatorPeerID,
			}
		default:
			return nil, fmt.Errorf("unknown source type: %T", s)
		}

		var dest interface{}
		switch d := r.Destination.(type) {
		case *manager.TXLogRecordDestination:
			dest = &TXLogRecordDestination{
				ServicePeerID: d.ServicePeerID,
			}
		case *manager.TXLogRecordDelegatedDestination:
			dest = &TXLogRecordDelegatedDestination{
				ServicePeerID:   d.ServicePeerID,
				DelegatorPeerID: d.DelegatorPeerID,
			}
		default:
			return nil, fmt.Errorf("unknown destination type: %T", d)
		}

		records[i] = &TXLogRecord{
			TransactionID: r.TransactionID,
			GrantHash:     r.GrantHash,
			Direction:     r.Direction,
			ServiceName:   r.ServiceName,
			Source:        src,
			Destination:   dest,
			CreatedAt:     r.CreatedAt,
		}
	}

	return records, nil
}

func reqToRepo(req *ListTXLogRecordsHandlerArgs) (*manager.ListTXLogRecordsRequest, error) {
	filters := make([]*manager.TXLogFilter, len(req.Filters))

	for i, f := range req.Filters {
		id, err := record.NewTransactionIDFromString(f.TransactionID)
		if err != nil {
			return nil, errors.Wrap(err, "invalid transaction ID in filter")
		}

		filter := &manager.TXLogFilter{
			TransactionID: id,
			ServiceName:   f.ServiceName,
			GrantHash:     f.GrantHash,
			PeerID:        f.PeerID,
		}

		if f.Period != nil {
			filter.Period = &manager.Period{
				Start: f.Period.Start,
				End:   f.Period.End,
			}
		}

		filters[i] = filter
	}

	return &manager.ListTXLogRecordsRequest{
		DataSourcePeerID: req.DataSourcePeerID,
		Pagination: &manager.Pagination{
			StartID:   req.Pagination.StartID,
			Limit:     req.Pagination.Limit,
			SortOrder: sortOrderToProto(req.Pagination.SortOrder),
		},
		Filters: filters,
	}, nil
}

func sortOrderToProto(o SortOrder) manager.SortOrder {
	switch o {
	case SortOrderAscending:
		return manager.SortOrderAscending
	case SortOrderDescending:
		return manager.SortOrderDescending
	default:
		return manager.SortOrderUnspecified
	}
}
