// Copyright © VNG Realisatie 2023
// Licensed under the EUPL

package query

import (
	"fmt"
	"time"
)

type InternalError struct {
	m string
}

func newInternalError(message string) *InternalError {
	return &InternalError{
		m: message,
	}
}

func (v *InternalError) Error() string {
	return "internal error: " + v.m
}

func mapError(err error, message string) error {
	return fmt.Errorf("%s: %w", newInternalError(message), err)
}

type SortOrder string

const (
	SortOrderAscending  SortOrder = "asc"
	SortOrderDescending SortOrder = "desc"
)

type Pagination struct {
	StartID   uint64
	Limit     uint32
	SortOrder SortOrder
}

type Period struct {
	Start time.Time
	End   time.Time
}
