// Copyright © VNG Realisatie 2023
// Licensed under the EUPL

package query

import (
	"context"
	"fmt"

	"go.nlx.io/nlx/controller/adapters/manager"
)

type GetPeerInfoHandler struct {
	manager manager.Manager
}

func NewGetPeerInfoHandler(ctx context.Context, m manager.Manager) (*GetPeerInfoHandler, error) {
	if m == nil {
		return nil, fmt.Errorf("manager is required")
	}

	return &GetPeerInfoHandler{
		manager: m,
	}, nil
}

func (h *GetPeerInfoHandler) Handle(ctx context.Context) (*Peer, error) {
	p, err := h.manager.GetPeerInfo(ctx)
	if err != nil {
		return nil, fmt.Errorf("%s: %w", newInternalError("could not get peer info from manager"), err)
	}

	return &Peer{
		ID:   p.ID,
		Name: p.Name,
	}, nil
}
