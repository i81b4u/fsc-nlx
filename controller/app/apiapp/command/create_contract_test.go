// Copyright © VNG Realisatie 2023
// Licensed under the EUPL
package command_test

import (
	"context"
	"errors"
	"fmt"
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	"golang.org/x/exp/maps"

	"go.nlx.io/nlx/controller/adapters/manager"
	"go.nlx.io/nlx/controller/app/apiapp/command"
)

// nolint:funlen // these tests should not fit 100 lines
func TestCreateContract(t *testing.T) {
	t.Parallel()

	type tests map[string]struct {
		setup   func(context.Context, *mocks)
		args    *command.CreateContractArgs
		wantErr error
	}

	validArgs := &command.CreateContractArgs{
		HashAlgorithm:     command.HashAlgSHA3_512,
		GroupID:           "group-id",
		ContractNotBefore: "2023-05-23",
		ContractNotAfter:  "2023-05-24",
		PeerRegistrationGrants: []*command.PeerRegistrationGrant{
			{
				DirectoryPeerID: "12345678901234567899",
				PeerID:          "12345678901234567890",
				PeerName:        "Gemeente Stijns",
			},
		},
		ServicePublicationGrants: []*command.ServicePublicationGrant{
			{
				DirectoryPeerID: "12345678901234567899",
				ServicePeerID:   "12345678901234567890",
				ServiceName:     "petstore",
			},
		},
		ServiceConnectionGrants: []*command.ServiceConnectionGrant{
			{
				ServicePeerID:               "12345678901234567891",
				ServiceName:                 "petstore",
				OutwayPeerID:                "12345678901234567890",
				OutwayCertificateThumbprint: "1JfTc4daaf_6RRIrhY0hva7o2Drz21tmIMWN4eGgGoE=",
			},
		},
		DelegatedServiceConnectionGrants: []*command.DelegatedServiceConnectionGrant{
			{
				DelegatorPeerID:             "12345678901234567892",
				ServicePeerID:               "12345678901234567891",
				ServiceName:                 "petstore",
				OutwayPeerID:                "12345678901234567890",
				OutwayCertificateThumbprint: "1JfTc4daaf_6RRIrhY0hva7o2Drz21tmIMWN4eGgGoE=",
			},
		},
		DelegatedServicePublicationGrants: []*command.DelegatedServicePublicationGrant{
			{
				DirectoryPeerID: "12345678901234567899",
				DelegatorPeerID: "12345678901234567891",
				ServicePeerID:   "12345678901234567890",
				ServiceName:     "petstore",
			},
		},
	}

	flowTests := tests{
		"when_id_generator_errors": {
			setup: func(ctx context.Context, m *mocks) {
				m.id.EXPECT().
					New().
					Return(nil, fmt.Errorf("arbitrary"))
			},
			args:    validArgs,
			wantErr: &command.InternalError{},
		},
		"when_manager_errors": {
			args: validArgs,
			setup: func(ctx context.Context, m *mocks) {
				m.id.EXPECT().
					New().
					Return([]byte("id"), nil)

				now := time.Now()

				m.clock.EXPECT().
					Now().
					Return(now)

				m.manager.EXPECT().
					CreateContract(ctx, &manager.CreateContractArgs{
						ID:                []byte("id"),
						HashAlgorithm:     manager.HashAlgSHA3_512,
						GroupID:           "group-id",
						ContractNotBefore: time.Date(2023, 5, 23, 0, 0, 0, 0, time.UTC),
						ContractNotAfter:  time.Date(2023, 5, 24, 0, 0, 0, 0, time.UTC),
						CreatedAt:         now,
						PeerRegistrationGrants: []*manager.PeerRegistrationGrant{
							{
								DirectoryPeerID: "12345678901234567899",
								PeerID:          "12345678901234567890",
								PeerName:        "Gemeente Stijns",
							},
						},
						ServicePublicationGrants: []*manager.ServicePublicationGrant{
							{
								DirectoryPeerID: "12345678901234567899",
								ServicePeerID:   "12345678901234567890",
								ServiceName:     "petstore",
							},
						},
						ServiceConnectionGrants: []*manager.ServiceConnectionGrant{
							{
								ServicePeerID:               "12345678901234567891",
								ServiceName:                 "petstore",
								OutwayPeerID:                "12345678901234567890",
								OutwayCertificateThumbprint: "1JfTc4daaf_6RRIrhY0hva7o2Drz21tmIMWN4eGgGoE=",
							},
						},
						DelegatedServiceConnectionGrants: []*manager.DelegatedServiceConnectionGrant{
							{
								DelegatorPeerID:             "12345678901234567892",
								ServicePeerID:               "12345678901234567891",
								ServiceName:                 "petstore",
								OutwayPeerID:                "12345678901234567890",
								OutwayCertificateThumbprint: "1JfTc4daaf_6RRIrhY0hva7o2Drz21tmIMWN4eGgGoE=",
							},
						},
						DelegatedServicePublicationGrants: []*manager.DelegatedServicePublicationGrant{
							{
								DirectoryPeerID: "12345678901234567899",
								DelegatorPeerID: "12345678901234567891",
								ServicePeerID:   "12345678901234567890",
								ServiceName:     "petstore",
							},
						},
					}).
					Return(errors.New("arbitrary"))
			},
			wantErr: &command.InternalError{},
		},
		"happy_flow": {
			args: validArgs,
			setup: func(ctx context.Context, m *mocks) {
				m.id.EXPECT().
					New().
					Return([]byte("id"), nil)

				now := time.Now()

				m.clock.EXPECT().
					Now().
					Return(now)

				m.manager.EXPECT().
					CreateContract(ctx, &manager.CreateContractArgs{
						ID:                []byte("id"),
						HashAlgorithm:     manager.HashAlgSHA3_512,
						GroupID:           "group-id",
						ContractNotBefore: time.Date(2023, 5, 23, 0, 0, 0, 0, time.UTC),
						ContractNotAfter:  time.Date(2023, 5, 24, 0, 0, 0, 0, time.UTC),
						CreatedAt:         now,
						PeerRegistrationGrants: []*manager.PeerRegistrationGrant{
							{
								DirectoryPeerID: "12345678901234567899",
								PeerID:          "12345678901234567890",
								PeerName:        "Gemeente Stijns",
							},
						},
						ServicePublicationGrants: []*manager.ServicePublicationGrant{
							{
								DirectoryPeerID: "12345678901234567899",
								ServicePeerID:   "12345678901234567890",
								ServiceName:     "petstore",
							},
						},
						ServiceConnectionGrants: []*manager.ServiceConnectionGrant{
							{
								ServicePeerID:               "12345678901234567891",
								ServiceName:                 "petstore",
								OutwayPeerID:                "12345678901234567890",
								OutwayCertificateThumbprint: "1JfTc4daaf_6RRIrhY0hva7o2Drz21tmIMWN4eGgGoE=",
							},
						},
						DelegatedServiceConnectionGrants: []*manager.DelegatedServiceConnectionGrant{
							{
								DelegatorPeerID:             "12345678901234567892",
								ServicePeerID:               "12345678901234567891",
								ServiceName:                 "petstore",
								OutwayPeerID:                "12345678901234567890",
								OutwayCertificateThumbprint: "1JfTc4daaf_6RRIrhY0hva7o2Drz21tmIMWN4eGgGoE=",
							},
						},
						DelegatedServicePublicationGrants: []*manager.DelegatedServicePublicationGrant{
							{
								DirectoryPeerID: "12345678901234567899",
								DelegatorPeerID: "12345678901234567891",
								ServicePeerID:   "12345678901234567890",
								ServiceName:     "petstore",
							},
						},
					}).
					Return(nil)
			},
		},
	}

	validationTests := tests{
		"invalid_not_before_format": {
			args: &command.CreateContractArgs{
				HashAlgorithm:     command.HashAlgSHA3_512,
				GroupID:           "group-id",
				ContractNotBefore: "2023-05-23-invalid-format",
				ContractNotAfter:  "2023-05-24",
				PeerRegistrationGrants: []*command.PeerRegistrationGrant{
					{
						DirectoryPeerID: "12345678901234567899",
						PeerID:          "12345678901234567890",
						PeerName:        "Gemeente Stijns",
					},
				},
				ServicePublicationGrants: nil,
				ServiceConnectionGrants:  nil,
			},
			wantErr: &command.ValidationError{},
		},
		"invalid_not_after_format": {
			args: &command.CreateContractArgs{
				HashAlgorithm:     command.HashAlgSHA3_512,
				GroupID:           "group-id",
				ContractNotBefore: "2023-05-23",
				ContractNotAfter:  "2023-05-24-invalid-format",
				PeerRegistrationGrants: []*command.PeerRegistrationGrant{
					{
						DirectoryPeerID: "12345678901234567899",
						PeerID:          "12345678901234567890",
						PeerName:        "Gemeente Stijns",
					},
				},
				ServicePublicationGrants: nil,
				ServiceConnectionGrants:  nil,
			},
			wantErr: &command.ValidationError{},
		},
	}

	allTests := tests{}
	maps.Copy(allTests, flowTests)
	maps.Copy(allTests, validationTests)

	for name, tt := range allTests {
		tt := tt

		t.Run(name, func(t *testing.T) {
			t.Parallel()

			mocks := newMocks(t)

			h, err := command.NewCreateContractHandler(mocks.manager, mocks.clock, mocks.id, mocks.logger)
			require.NoError(t, err)

			ctx := context.Background()

			if tt.setup != nil {
				tt.setup(ctx, mocks)
			}

			err = h.Handle(ctx, tt.args)

			if tt.wantErr == nil {
				assert.NoError(t, err)
			} else {
				assert.ErrorAs(t, err, &tt.wantErr)
			}
		})
	}
}
