// Copyright © VNG Realisatie 2023
// Licensed under the EUPL

//nolint:dupl // looks like accept contract but is different
package command

import (
	"context"
	"fmt"

	"go.nlx.io/nlx/controller/adapters/manager"
	"go.nlx.io/nlx/manager/adapters/logger"
)

type RevokeContractHandler struct {
	manager manager.Manager
	lgr     logger.Logger
}

func NewRevokeContractHandler(m manager.Manager, l logger.Logger) (*RevokeContractHandler, error) {
	if m == nil {
		return nil, fmt.Errorf("manager is required")
	}

	if l == nil {
		return nil, fmt.Errorf("logger is required")
	}

	return &RevokeContractHandler{
		manager: m,
		lgr:     l,
	}, nil
}

func (h *RevokeContractHandler) Handle(ctx context.Context, contentHash string) error {
	if contentHash == "" {
		return newValidationError("contentHash cannot be empty")
	}

	err := h.manager.RevokeContract(ctx, contentHash)
	if err != nil {
		h.lgr.Error("failed to revoke contract", err)

		return mapError(err, "could not revoke contract in manager")
	}

	return nil
}
