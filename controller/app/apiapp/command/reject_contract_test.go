// Copyright © VNG Realisatie 2023
// Licensed under the EUPL

//nolint:dupl // looks like accept contract but is different
package command_test

import (
	"context"
	"errors"
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	"golang.org/x/exp/maps"

	"go.nlx.io/nlx/controller/app/apiapp/command"
)

func TestRejectContract(t *testing.T) {
	t.Parallel()

	type tests map[string]struct {
		setup       func(context.Context, *mocks)
		contentHash string
		wantErr     error
	}

	contentHash := "$1$1$mTfBbWw2A6UQU5oU7RR2JbEtTB9GnT2syfopGwICT5Mo9qDgFb1L_o0kFi0-TG13LX2czdAL60KaY7NbB-uMAQ=="

	flowTests := tests{
		"when_manager_errors": {
			contentHash: contentHash,
			setup: func(ctx context.Context, m *mocks) {
				m.manager.EXPECT().
					RejectContract(ctx, contentHash).
					Return(errors.New("arbitrary"))
			},
			wantErr: &command.InternalError{},
		},
		"happy_flow": {
			contentHash: contentHash,
			setup: func(ctx context.Context, m *mocks) {
				m.manager.EXPECT().
					RejectContract(ctx, contentHash).
					Return(nil)
			},
			wantErr: nil,
		},
	}

	validationTests := tests{
		"empty_content_hash": {
			contentHash: "",
			wantErr:     &command.ValidationError{},
		},
	}

	allTests := tests{}
	maps.Copy(allTests, flowTests)
	maps.Copy(allTests, validationTests)

	for name, tt := range allTests {
		tt := tt

		t.Run(name, func(t *testing.T) {
			t.Parallel()

			mocks := newMocks(t)

			h, err := command.NewRejectContractHandler(mocks.manager, mocks.logger)
			require.NoError(t, err)

			ctx := context.Background()

			if tt.setup != nil {
				tt.setup(ctx, mocks)
			}

			err = h.Handle(ctx, tt.contentHash)

			if tt.wantErr == nil {
				assert.NoError(t, err)
			} else {
				assert.ErrorAs(t, err, &tt.wantErr)
			}
		})
	}
}
