// Copyright © VNG Realisatie 2023
// Licensed under the EUPL

package command

import (
	"context"
	"fmt"
	"time"

	"go.nlx.io/nlx/common/clock"
	"go.nlx.io/nlx/controller/adapters/idgenerator"
	"go.nlx.io/nlx/controller/adapters/manager"
	"go.nlx.io/nlx/manager/adapters/logger"
)

type CreateContractHandler struct {
	manager manager.Manager
	clock   clock.Clock
	id      idgenerator.IDGenerator
	lgr     logger.Logger
}

func NewCreateContractHandler(m manager.Manager, c clock.Clock, i idgenerator.IDGenerator, l logger.Logger) (*CreateContractHandler, error) {
	if m == nil {
		return nil, fmt.Errorf("manager is required")
	}

	if c == nil {
		return nil, fmt.Errorf("clock is required")
	}

	if i == nil {
		return nil, fmt.Errorf("id generator is required")
	}

	if l == nil {
		return nil, fmt.Errorf("logger is required")
	}

	return &CreateContractHandler{
		manager: m,
		clock:   c,
		id:      i,
		lgr:     l,
	}, nil
}

type CreateContractArgs struct {
	HashAlgorithm                     string
	GroupID                           string
	ContractNotBefore                 string
	ContractNotAfter                  string
	PeerRegistrationGrants            []*PeerRegistrationGrant
	ServicePublicationGrants          []*ServicePublicationGrant
	ServiceConnectionGrants           []*ServiceConnectionGrant
	DelegatedServiceConnectionGrants  []*DelegatedServiceConnectionGrant
	DelegatedServicePublicationGrants []*DelegatedServicePublicationGrant
}

type PeerRegistrationGrant struct {
	DirectoryPeerID string
	PeerID          string
	PeerName        string
}

type ServicePublicationGrant struct {
	DirectoryPeerID string
	ServicePeerID   string
	ServiceName     string
}

type ServiceConnectionGrant struct {
	ServicePeerID               string
	ServiceName                 string
	OutwayPeerID                string
	OutwayCertificateThumbprint string
}

type DelegatedServiceConnectionGrant struct {
	DelegatorPeerID             string
	ServicePeerID               string
	ServiceName                 string
	OutwayPeerID                string
	OutwayCertificateThumbprint string
}

type DelegatedServicePublicationGrant struct {
	DelegatorPeerID string
	DirectoryPeerID string
	ServicePeerID   string
	ServiceName     string
}

const HashAlgSHA3_512 = "SHA3-512"

var mapHashAlgorithm = map[string]manager.HashAlg{
	HashAlgSHA3_512: manager.HashAlgSHA3_512,
}

func (h *CreateContractHandler) Handle(ctx context.Context, args *CreateContractArgs) error {
	notBefore, err := time.Parse("2006-01-02", args.ContractNotBefore)
	if err != nil {
		return newValidationError("the not before date should be formatted as 2006-01-02")
	}

	notAfter, err := time.Parse("2006-01-02", args.ContractNotAfter)
	if err != nil {
		return newValidationError("the not after date should be formatted as 2006-01-02")
	}

	peerRegistrationGrants := mapPeerRegistrationGrants(args.PeerRegistrationGrants)
	servicePublicationGrants := mapServicePublicationGrants(args.ServicePublicationGrants)
	serviceConnectionGrants := mapServiceConnectionGrants(args.ServiceConnectionGrants)
	delegatedServiceConnectionGrants := mapDelegatedServiceConnectionGrants(args.DelegatedServiceConnectionGrants)
	delegatedServicePublicationGrants := mapDelegatedServicePublicationGrants(args.DelegatedServicePublicationGrants)

	id, err := h.id.New()
	if err != nil {
		return mapError(err, "failed to generate id")
	}

	hashAlgorithm, ok := mapHashAlgorithm[args.HashAlgorithm]
	if !ok {
		return newValidationError("invalid hash algorithm")
	}

	createContractArgs := &manager.CreateContractArgs{
		ID:                                id,
		HashAlgorithm:                     hashAlgorithm,
		GroupID:                           args.GroupID,
		ContractNotBefore:                 notBefore,
		ContractNotAfter:                  notAfter,
		PeerRegistrationGrants:            peerRegistrationGrants,
		ServicePublicationGrants:          servicePublicationGrants,
		ServiceConnectionGrants:           serviceConnectionGrants,
		DelegatedServiceConnectionGrants:  delegatedServiceConnectionGrants,
		DelegatedServicePublicationGrants: delegatedServicePublicationGrants,

		CreatedAt: h.clock.Now(),
	}

	err = h.manager.CreateContract(ctx, createContractArgs)
	if err != nil {
		h.lgr.Error("failed to create contract", err)
		return mapError(err, "could not create contract in manager")
	}

	return nil
}

func mapPeerRegistrationGrants(grants []*PeerRegistrationGrant) []*manager.PeerRegistrationGrant {
	result := make([]*manager.PeerRegistrationGrant, len(grants))

	for i, grant := range grants {
		result[i] = &manager.PeerRegistrationGrant{
			DirectoryPeerID: grant.DirectoryPeerID,
			PeerID:          grant.PeerID,
			PeerName:        grant.PeerName,
		}
	}

	return result
}

func mapServicePublicationGrants(grants []*ServicePublicationGrant) []*manager.ServicePublicationGrant {
	result := make([]*manager.ServicePublicationGrant, len(grants))

	for i, grant := range grants {
		result[i] = &manager.ServicePublicationGrant{
			DirectoryPeerID: grant.DirectoryPeerID,
			ServicePeerID:   grant.ServicePeerID,
			ServiceName:     grant.ServiceName,
		}
	}

	return result
}

func mapServiceConnectionGrants(grants []*ServiceConnectionGrant) []*manager.ServiceConnectionGrant {
	result := make([]*manager.ServiceConnectionGrant, len(grants))

	for i, grant := range grants {
		result[i] = &manager.ServiceConnectionGrant{
			ServicePeerID:               grant.ServicePeerID,
			ServiceName:                 grant.ServiceName,
			OutwayPeerID:                grant.OutwayPeerID,
			OutwayCertificateThumbprint: grant.OutwayCertificateThumbprint,
		}
	}

	return result
}

func mapDelegatedServiceConnectionGrants(grants []*DelegatedServiceConnectionGrant) []*manager.DelegatedServiceConnectionGrant {
	result := make([]*manager.DelegatedServiceConnectionGrant, len(grants))

	for i, grant := range grants {
		result[i] = &manager.DelegatedServiceConnectionGrant{
			DelegatorPeerID:             grant.DelegatorPeerID,
			ServicePeerID:               grant.ServicePeerID,
			ServiceName:                 grant.ServiceName,
			OutwayPeerID:                grant.OutwayPeerID,
			OutwayCertificateThumbprint: grant.OutwayCertificateThumbprint,
		}
	}

	return result
}

func mapDelegatedServicePublicationGrants(grants []*DelegatedServicePublicationGrant) []*manager.DelegatedServicePublicationGrant {
	result := make([]*manager.DelegatedServicePublicationGrant, len(grants))

	for i, grant := range grants {
		result[i] = &manager.DelegatedServicePublicationGrant{
			DirectoryPeerID: grant.DirectoryPeerID,
			DelegatorPeerID: grant.DelegatorPeerID,
			ServicePeerID:   grant.ServicePeerID,
			ServiceName:     grant.ServiceName,
		}
	}

	return result
}
