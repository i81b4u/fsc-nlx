// Copyright © VNG Realisatie 2023
// Licensed under the EUPL

package command

import (
	"context"
	"fmt"

	"go.nlx.io/nlx/controller/adapters/storage"
	"go.nlx.io/nlx/manager/adapters/logger"
)

type CreateServiceHandler struct {
	groupID string
	lgr     logger.Logger
	s       storage.Storage
}

func NewCreateServiceHandler(groupID string, s storage.Storage, l logger.Logger) (*CreateServiceHandler, error) {
	if groupID == "" {
		return nil, fmt.Errorf("group id is required")
	}

	if l == nil {
		return nil, fmt.Errorf("logger is required")
	}

	if s == nil {
		return nil, fmt.Errorf("storage is required")
	}

	return &CreateServiceHandler{
		groupID: groupID,
		lgr:     l,
		s:       s,
	}, nil
}

type CreateServiceArgs struct {
	Name         string
	EndpointURL  string
	InwayAddress string
}

func (h *CreateServiceHandler) Handle(ctx context.Context, args *CreateServiceArgs) error {
	if args.Name == "" {
		return newValidationError("service name cannot be blank")
	}

	if args.EndpointURL == "" {
		return newValidationError("endpoint URL cannot be blank")
	}

	err := h.s.CreateService(ctx, &storage.CreateServiceArgs{
		GroupID:      h.groupID,
		Name:         args.Name,
		EndpointURL:  args.EndpointURL,
		InwayAddress: args.InwayAddress,
	})
	if err != nil {
		h.lgr.Error("failed to create service", err)

		return mapError(err, "could not create service in storage")
	}

	return nil
}
