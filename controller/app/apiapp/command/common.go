// Copyright © VNG Realisatie 2023
// Licensed under the EUPL

package command

import (
	"errors"
	"fmt"

	"go.nlx.io/nlx/controller/adapters/manager"
)

type ValidationError struct {
	m string
}

func newValidationError(message string) *ValidationError {
	return &ValidationError{
		m: message,
	}
}

func (v *ValidationError) Error() string {
	return "validation error: " + v.m
}

type InternalError struct {
	m string
}

func newInternalError(message string) *InternalError {
	return &InternalError{
		m: message,
	}
}

func (v *InternalError) Error() string {
	return "internal error: " + v.m
}

func mapError(err error, message string) error {
	var e *manager.ValidationError
	if errors.As(err, &e) {
		return fmt.Errorf("%s: %s: %w", message, newValidationError(e.Error()), err)
	}

	return fmt.Errorf("%s: %w", newInternalError(message), err)
}
