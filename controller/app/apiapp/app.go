// Copyright © VNG Realisatie 2023
// Licensed under the EUPL

package apiapp

import (
	"context"

	"github.com/pkg/errors"

	"go.nlx.io/nlx/common/clock"
	"go.nlx.io/nlx/controller/adapters/idgenerator"
	"go.nlx.io/nlx/controller/adapters/logger"
	"go.nlx.io/nlx/controller/adapters/manager"
	"go.nlx.io/nlx/controller/adapters/storage"
	"go.nlx.io/nlx/controller/app/apiapp/command"
	"go.nlx.io/nlx/controller/app/apiapp/query"
)

type Application struct {
	Commands Commands
	Queries  Queries
}

type Commands struct {
	AcceptContract *command.AcceptContractHandler
	RevokeContract *command.RevokeContractHandler
	RejectContract *command.RejectContractHandler
	CreateContract *command.CreateContractHandler
	CreateService  *command.CreateServiceHandler
}

type Queries struct {
	GetContract      *query.GetContractHandler
	ListServices     *query.ListServicesHandler
	ListContracts    *query.ListContractsHandler
	ListPeerServices *query.ListPeerServicesHandler
	ListTXLogRecords *query.ListTXLogRecordsHandler
	GetPeerInfo      *query.GetPeerInfoHandler
}

type NewApplicationArgs struct {
	Manager     manager.Manager
	Storage     storage.Storage
	Clock       clock.Clock
	IDGenerator idgenerator.IDGenerator
	Logger      logger.Logger
	GroupID     string
}

//nolint:gocyclo // new function
func NewApplication(ctx context.Context, args *NewApplicationArgs) (*Application, error) {
	listServices, err := query.NewListServicesHandler(args.GroupID, args.Storage)
	if err != nil {
		return nil, errors.Wrap(err, "could not create new ListServices handler")
	}

	listContracts, err := query.NewListContractsHandler(args.Manager)
	if err != nil {
		return nil, errors.Wrap(err, "could not create new ListContracts handler")
	}

	getContract, err := query.NewGetContractHandler(args.Manager)
	if err != nil {
		return nil, errors.Wrap(err, "could not create new GetContract handler")
	}

	createContract, err := command.NewCreateContractHandler(args.Manager, args.Clock, args.IDGenerator, args.Logger)
	if err != nil {
		return nil, errors.Wrap(err, "could not create new CreateContract handler")
	}

	acceptContract, err := command.NewAcceptContractHandler(args.Manager, args.Logger)
	if err != nil {
		return nil, errors.Wrap(err, "could not create new AcceptContract handler")
	}

	revokeContract, err := command.NewRevokeContractHandler(args.Manager, args.Logger)
	if err != nil {
		return nil, errors.Wrap(err, "could not create new RevokeContract handler")
	}

	rejectContract, err := command.NewRejectContractHandler(args.Manager, args.Logger)
	if err != nil {
		return nil, errors.Wrap(err, "could not create new RejectContract handler")
	}

	createService, err := command.NewCreateServiceHandler(args.GroupID, args.Storage, args.Logger)
	if err != nil {
		return nil, errors.Wrap(err, "could not create new CreateService handler")
	}

	listPeerServices, err := query.NewListPeerServicesHandler(args.Manager, args.Logger)
	if err != nil {
		return nil, errors.Wrap(err, "could not create new ListPeerServices handler")
	}

	listTXLogRecords, err := query.NewListTransactionLogRecordsHandler(args.Manager)
	if err != nil {
		return nil, errors.Wrap(err, "could not create new ListTXLogRecords handler")
	}

	getPeerInfo, err := query.NewGetPeerInfoHandler(ctx, args.Manager)
	if err != nil {
		return nil, errors.Wrap(err, "could not create new GetPeerInfo handler")
	}

	application := &Application{
		Commands: Commands{
			AcceptContract: acceptContract,
			RevokeContract: revokeContract,
			RejectContract: rejectContract,
			CreateContract: createContract,
			CreateService:  createService,
		},
		Queries: Queries{
			GetContract:      getContract,
			ListServices:     listServices,
			ListContracts:    listContracts,
			ListPeerServices: listPeerServices,
			ListTXLogRecords: listTXLogRecords,
			GetPeerInfo:      getPeerInfo,
		},
	}

	return application, nil
}
