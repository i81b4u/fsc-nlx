// Copyright © VNG Realisatie 2023
// Licensed under the EUPL
package query_test

import (
	"context"
	"errors"
	"net/url"
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"

	"go.nlx.io/nlx/controller/app/internalapp/query"
)

func TestGetServiceEndpointURL(t *testing.T) {
	t.Parallel()

	urlServiceEndpoint, err := url.Parse("api1.com")
	assert.NoError(t, err)

	tests := map[string]struct {
		setup   func(*mocks) context.Context
		want    *url.URL
		wantErr error
	}{
		"when_storage_errors": {
			setup: func(m *mocks) context.Context {
				ctx := context.Background()

				m.storage.EXPECT().
					GetServiceEndpointURL(ctx, "directory1.com", "service-1", "inway1.com").
					Return(nil, errors.New("unexpected error"))

				return ctx
			},
			wantErr: query.ErrInternalError,
		},
		"happy_flow": {
			setup: func(m *mocks) context.Context {
				ctx := context.Background()

				m.storage.EXPECT().
					GetServiceEndpointURL(ctx, "directory1.com", "service-1", "inway1.com").
					Return(urlServiceEndpoint, nil)

				return ctx
			},
			want: urlServiceEndpoint,
		},
	}

	for name, tt := range tests {
		tt := tt

		t.Run(name, func(t *testing.T) {
			t.Parallel()

			mocks := newMocks(t)

			h, err := query.NewGetServiceEndpointURLHandler("directory1.com", mocks.storage)
			require.NoError(t, err)

			ctx := tt.setup(mocks)

			actual, err := h.Handle(ctx, "service-1", "inway1.com")

			assert.ErrorIs(t, err, tt.wantErr)
			assert.Equal(t, tt.want, actual)
		})
	}
}
