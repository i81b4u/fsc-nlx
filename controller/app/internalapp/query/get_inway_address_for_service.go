// Copyright © VNG Realisatie 2023
// Licensed under the EUPL

package query

import (
	"context"
	"fmt"
	"net/url"

	"go.nlx.io/nlx/controller/adapters/storage"
)

type GetInwayAddressForServiceHandler struct {
	storage storage.Storage
}

func NewGetInwayAddressForServiceHandler(s storage.Storage) (*GetInwayAddressForServiceHandler, error) {
	if s == nil {
		return nil, fmt.Errorf("storage is required")
	}

	return &GetInwayAddressForServiceHandler{
		storage: s,
	}, nil
}

func (h *GetInwayAddressForServiceHandler) Handle(ctx context.Context, groupID, serviceName string) (*url.URL, error) {
	address, err := h.storage.GetInwayAddressForService(ctx, groupID, serviceName)
	if err != nil {
		return nil, fmt.Errorf("%w: could not inway address for service from storage: %s", ErrInternalError, err)
	}

	return address, nil
}
