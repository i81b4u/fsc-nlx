// Copyright © VNG Realisatie 2023
// Licensed under the EUPL

package query

import (
	"context"
	"fmt"
	"net/url"

	"go.nlx.io/nlx/controller/adapters/storage"
)

type GetServiceEndpointURLHandler struct {
	groupID string
	storage storage.Storage
}

func NewGetServiceEndpointURLHandler(groupID string, s storage.Storage) (*GetServiceEndpointURLHandler, error) {
	if groupID == "" {
		return nil, fmt.Errorf("groupID is required")
	}

	if s == nil {
		return nil, fmt.Errorf("storage is required")
	}

	return &GetServiceEndpointURLHandler{
		groupID: groupID,
		storage: s,
	}, nil
}

func (h *GetServiceEndpointURLHandler) Handle(ctx context.Context, serviceName, inwayAddress string) (*url.URL, error) {
	address, err := h.storage.GetServiceEndpointURL(ctx, h.groupID, serviceName, inwayAddress)
	if err != nil {
		return nil, fmt.Errorf("%w: could not get service endpoint URL for inway address from storage: %s", ErrInternalError, err)
	}

	return address, nil
}
