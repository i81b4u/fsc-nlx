// Copyright © VNG Realisatie 2023
// Licensed under the EUPL

package internalapp

import (
	"context"

	"github.com/pkg/errors"

	"go.nlx.io/nlx/controller/adapters/storage"
	"go.nlx.io/nlx/controller/app/internalapp/commands"
	"go.nlx.io/nlx/controller/app/internalapp/query"
)

type Application struct {
	Commands Commands
	Queries  Queries
}

type Queries struct {
	GetInwayAddressForService *query.GetInwayAddressForServiceHandler
	GetServiceEndpointURL     *query.GetServiceEndpointURLHandler
}

type Commands struct {
	RegisterInway  *commands.RegisterInwayHandler
	RegisterOutway *commands.RegisterOutwayHandler
}

type NewApplicationArgs struct {
	Context context.Context
	Storage storage.Storage
	GroupID string
}

func NewApplication(args *NewApplicationArgs) (*Application, error) {
	registerInway, err := commands.NewRegisterInwayHandler(args.GroupID, args.Storage)
	if err != nil {
		return nil, errors.Wrap(err, "could not create new RegisterInway handler")
	}

	registerOutway, err := commands.NewRegisterOutwayHandler(args.GroupID, args.Storage)
	if err != nil {
		return nil, errors.Wrap(err, "could not create new RegisterOutway handler")
	}

	getInwayAddressForService, err := query.NewGetInwayAddressForServiceHandler(args.Storage)
	if err != nil {
		return nil, errors.Wrap(err, "could not create new GetInwayAddressForService handler")
	}

	getServiceEndpointURL, err := query.NewGetServiceEndpointURLHandler(args.GroupID, args.Storage)
	if err != nil {
		return nil, errors.Wrap(err, "could not create new GetServiceEndpointURL handler")
	}

	application := &Application{
		Commands: Commands{
			RegisterInway:  registerInway,
			RegisterOutway: registerOutway,
		},
		Queries: Queries{
			GetInwayAddressForService: getInwayAddressForService,
			GetServiceEndpointURL:     getServiceEndpointURL,
		},
	}

	return application, nil
}
