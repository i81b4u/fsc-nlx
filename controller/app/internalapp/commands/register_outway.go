// Copyright © VNG Realisatie 2023
// Licensed under the EUPL

package commands

import (
	"context"
	"fmt"

	"go.nlx.io/nlx/controller/adapters/storage"
)

type RegisterOutwayHandler struct {
	groupID string
	storage storage.Storage
}

func NewRegisterOutwayHandler(groupID string, s storage.Storage) (*RegisterOutwayHandler, error) {
	if groupID == "" {
		return nil, fmt.Errorf("groupID is required")
	}

	if s == nil {
		return nil, fmt.Errorf("storage is required")
	}

	return &RegisterOutwayHandler{
		storage: s,
		groupID: groupID,
	}, nil
}

type RegisterOutwayArgs struct {
	Name                  string
	CertificateThumbprint string
}

func (h *RegisterOutwayHandler) Handle(ctx context.Context, args *RegisterOutwayArgs) error {
	err := args.Validate()
	if err != nil {
		return fmt.Errorf("%w: invalid register outway args: %s", ErrValidationError, err)
	}

	err = h.storage.RegisterOutway(ctx, h.groupID, args.Name, args.CertificateThumbprint)
	if err != nil {
		return fmt.Errorf("%w: could not register outway in storage: %s", ErrInternalError, err)
	}

	return nil
}

func (a *RegisterOutwayArgs) Validate() error {
	if a.Name == "" {
		return fmt.Errorf("%w: Name is required", ErrValidationError)
	}

	if a.CertificateThumbprint == "" {
		return fmt.Errorf("%w: CertificateThumbprint is required", ErrValidationError)
	}

	return nil
}
