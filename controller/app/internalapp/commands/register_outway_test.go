// Copyright © VNG Realisatie 2023
// Licensed under the EUPL
package commands_test

import (
	"context"
	"errors"
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	"golang.org/x/exp/maps"

	mock_storage "go.nlx.io/nlx/controller/adapters/storage/mock"
	"go.nlx.io/nlx/controller/app/internalapp/commands"
)

func TestRegisterOutway(t *testing.T) {
	t.Parallel()

	groupID := "group-1"

	type tests map[string]struct {
		setup   func(*mock_storage.MockStorage) context.Context
		args    *commands.RegisterOutwayArgs
		wantErr error
	}

	validArgs := &commands.RegisterOutwayArgs{
		Name:                  "outway-1",
		CertificateThumbprint: "thumbprint-1",
	}

	flowTests := tests{
		"when_storage_errors": {
			setup: func(s *mock_storage.MockStorage) context.Context {
				ctx := context.Background()

				s.EXPECT().
					RegisterOutway(ctx, groupID, validArgs.Name, validArgs.CertificateThumbprint).
					Return(errors.New("unexpected error"))

				return ctx
			},
			args:    validArgs,
			wantErr: commands.ErrInternalError,
		},
		"happy_flow": {
			setup: func(s *mock_storage.MockStorage) context.Context {
				ctx := context.Background()

				s.EXPECT().
					RegisterOutway(ctx, groupID, validArgs.Name, validArgs.CertificateThumbprint).
					Return(nil)

				return ctx
			},
			args: validArgs,
		},
	}

	validationTests := tests{
		"missing_name": {
			args: func(a commands.RegisterOutwayArgs) *commands.RegisterOutwayArgs {
				a.Name = ""
				return &a
			}(*validArgs),
			wantErr: commands.ErrValidationError,
		},
		"missing_cert_thumbprint": {
			args: func(a commands.RegisterOutwayArgs) *commands.RegisterOutwayArgs {
				a.CertificateThumbprint = ""
				return &a
			}(*validArgs),
			wantErr: commands.ErrValidationError,
		},
	}

	allTests := tests{}
	maps.Copy(allTests, flowTests)
	maps.Copy(allTests, validationTests)

	for name, tt := range allTests {
		tt := tt

		t.Run(name, func(t *testing.T) {
			t.Parallel()

			mocks := newMocks(t)

			h, err := commands.NewRegisterOutwayHandler(groupID, mocks.storage)
			require.NoError(t, err)

			var ctx context.Context

			if tt.setup == nil {
				tt.setup = func(s *mock_storage.MockStorage) context.Context {
					return context.Background()
				}
			}

			ctx = tt.setup(mocks.storage)

			err = h.Handle(ctx, tt.args)

			assert.ErrorIs(t, err, tt.wantErr)
		})
	}
}
