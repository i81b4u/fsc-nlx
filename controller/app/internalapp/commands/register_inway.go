// Copyright © VNG Realisatie 2023
// Licensed under the EUPL

package commands

import (
	"context"
	"fmt"
	"net/url"

	"go.nlx.io/nlx/controller/adapters/storage"
)

type RegisterInwayHandler struct {
	groupID string
	storage storage.Storage
}

func NewRegisterInwayHandler(groupID string, s storage.Storage) (*RegisterInwayHandler, error) {
	if groupID == "" {
		return nil, fmt.Errorf("groupID is required")
	}

	if s == nil {
		return nil, fmt.Errorf("storage is required")
	}

	return &RegisterInwayHandler{
		groupID: groupID,
		storage: s,
	}, nil
}

type RegisterInwayArgs struct {
	Name    string
	Address string
}

func (h *RegisterInwayHandler) Handle(ctx context.Context, args *RegisterInwayArgs) error {
	err := args.Validate()
	if err != nil {
		return fmt.Errorf("%w: invalid register inway args: %s", ErrValidationError, err)
	}

	u, err := url.Parse(args.Address)
	if err != nil {
		return fmt.Errorf("invalid inway address: %w", err)
	}

	err = h.storage.RegisterInway(ctx, h.groupID, args.Name, u)
	if err != nil {
		return fmt.Errorf("%w: could not register inway in storage: %s", ErrInternalError, err)
	}

	return nil
}

func (a *RegisterInwayArgs) Validate() error {
	if a.Name == "" {
		return fmt.Errorf("%w: Name is required", ErrValidationError)
	}

	if a.Address == "" {
		return fmt.Errorf("%w: Address is required", ErrValidationError)
	}

	return nil
}
