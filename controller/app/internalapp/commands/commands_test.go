// Copyright © VNG Realisatie 2023
// Licensed under the EUPL

package commands_test

import (
	"testing"

	mock_storage "go.nlx.io/nlx/controller/adapters/storage/mock"
)

type mocks struct {
	storage *mock_storage.MockStorage
}

func newMocks(t *testing.T) *mocks {
	return &mocks{
		storage: mock_storage.NewMockStorage(t),
	}
}
