// Copyright © VNG Realisatie 2023
// Licensed under the EUPL

package cmd

import (
	"context"
	"fmt"
	"log"
	"os"
	"path/filepath"
	"time"

	"github.com/spf13/cobra"

	"go.nlx.io/nlx/common/clock"
	"go.nlx.io/nlx/common/cmd"
	"go.nlx.io/nlx/common/logoptions"
	"go.nlx.io/nlx/common/process"
	common_tls "go.nlx.io/nlx/common/tls"
	"go.nlx.io/nlx/common/version"
	uuidgenerator "go.nlx.io/nlx/controller/adapters/idgenerator/uuid"
	zaplogger "go.nlx.io/nlx/controller/adapters/logger/zap"
	grpcmanager "go.nlx.io/nlx/controller/adapters/manager/grpc"
	postgresstorage "go.nlx.io/nlx/controller/adapters/storage/postgres"
	"go.nlx.io/nlx/controller/app/apiapp"
	"go.nlx.io/nlx/controller/app/internalapp"
	"go.nlx.io/nlx/controller/app/uiapp"
	managerclient "go.nlx.io/nlx/controller/pkg/manager"
	"go.nlx.io/nlx/controller/ports/internalgrpc"
	"go.nlx.io/nlx/controller/ports/rest"
	uiport "go.nlx.io/nlx/controller/ports/ui"
)

var serveOpts struct {
	ListenAddressUI       string
	ListenAddressInternal string
	ListenAddressAPI      string
	DirectoryAddress      string
	ManagerAddress        string
	Environment           string
	StaticPath            string

	StoragePostgresDSN string

	logoptions.LogOptions
	cmd.TLSOptions
}

//nolint:gochecknoinits,funlen,gocyclo // this is the recommended way to use cobra, also a lot of flags..
func init() {
	serveCommand.Flags().StringVarP(&serveOpts.ListenAddressUI, "listen-address-ui", "", "127.0.0.1:3001", "Address for the UI to listen on. Read https://golang.org/pkg/net/#Dial for possible tcp address specs.")
	serveCommand.Flags().StringVarP(&serveOpts.ListenAddressInternal, "listen-address-internal", "", "127.0.0.1:443", "Address for the internal api to listen on. Read https://golang.org/pkg/net/#Dial for possible tcp address specs.")
	serveCommand.Flags().StringVarP(&serveOpts.ListenAddressAPI, "listen-address-api", "", "127.0.0.1:444", "Address for the api to listen on. Read https://golang.org/pkg/net/#Dial for possible tcp address specs.")
	serveCommand.Flags().StringVarP(&serveOpts.DirectoryAddress, "directory-address", "", "https://directory.shared.nlx.local:8443", "URL to the Directory API")
	serveCommand.Flags().StringVarP(&serveOpts.ManagerAddress, "manager-address", "", "manager.shared.nlx.local:443", "URL to the Manager internal API")
	serveCommand.Flags().StringVarP(&serveOpts.StoragePostgresDSN, "storage-postgres-dsn", "", "", "Postgres Connection URL")
	serveCommand.Flags().StringVarP(&serveOpts.StaticPath, "static-path", "", "public", "Path to the static web files")
	serveCommand.Flags().StringVarP(&serveOpts.LogOptions.LogType, "log-type", "", "live", "Set the logging config. See NewProduction and NewDevelopment at https://godoc.org/go.uber.org/zap#Logger.")
	serveCommand.Flags().StringVarP(&serveOpts.LogOptions.LogLevel, "log-level", "", "", "Set loglevel")
	serveCommand.Flags().StringVarP(&serveOpts.TLSOptions.RootCertFile, "tls-root-cert", "", "", "Absolute or relative path to the root cert .pem")
	serveCommand.Flags().StringVarP(&serveOpts.TLSOptions.CertFile, "tls-cert", "", "", "Absolute or relative path to the cert .pem")
	serveCommand.Flags().StringVarP(&serveOpts.TLSOptions.KeyFile, "tls-key", "", "", "Absolute or relative path to the key .pem")
}

var serveCommand = &cobra.Command{
	Use:   "serve",
	Short: "Start the UI",
	Run: func(cmd *cobra.Command, args []string) {
		p := process.NewProcess()

		logger, err := zaplogger.New(serveOpts.LogOptions.LogLevel, serveOpts.LogOptions.LogType)
		if err != nil {
			log.Fatalf("failed to create logger: %v", err)
		}

		logger.Info(fmt.Sprintf("version info: version: %s source-hash: %s", version.BuildVersion, version.BuildSourceHash))

		if errValidate := common_tls.VerifyPrivateKeyPermissions(serveOpts.KeyFile); errValidate != nil {
			logger.Warn("invalid key permissions", err)
		}

		cert, err := common_tls.NewBundleFromFiles(serveOpts.CertFile, serveOpts.KeyFile, serveOpts.RootCertFile)
		if err != nil {
			logger.Fatal("loading internal TLS files", err)
		}

		ctx := context.Background()

		managerClient, err := managerclient.NewClient(ctx, serveOpts.ManagerAddress, cert)
		if err != nil {
			logger.Fatal("could not create manager client", err)
		}

		mgr, err := grpcmanager.New(managerClient, logger)
		if err != nil {
			logger.Fatal("could not create grpc manager", err)
		}

		db, err := postgresstorage.NewConnection(serveOpts.StoragePostgresDSN)
		if err != nil {
			logger.Fatal("could not create db connection", err)
		}

		storage, err := postgresstorage.New(db)
		if err != nil {
			logger.Fatal("could not create postgres storage", err)
		}

		idGenerator := uuidgenerator.New()

		app, err := apiapp.NewApplication(ctx, &apiapp.NewApplicationArgs{
			Manager:     mgr,
			Storage:     storage,
			Clock:       clock.New(),
			IDGenerator: idGenerator,
			Logger:      logger,
			GroupID:     serveOpts.DirectoryAddress,
		})
		if err != nil {
			logger.Fatal("could not create application", err)
		}

		workDir, err := os.Getwd()
		if err != nil {
			logger.Fatal("failed to get work dir", err)
		}

		staticFilesPath := filepath.Join(workDir, serveOpts.StaticPath)

		uiApp, err := uiapp.NewApplication(&uiapp.NewApplicationArgs{
			Context: ctx,
			Storage: storage,
			GroupID: serveOpts.DirectoryAddress,
		})

		if err != nil {
			logger.Fatal("failed to create ui app", err)
		}

		serverArgs := &uiport.NewServerArgs{
			Locale:     "nl",
			StaticPath: staticFilesPath,
			GroupID:    serveOpts.DirectoryAddress,
			Logger:     logger,
			APIApp:     app,
			UIApp:      uiApp,
			Clock:      clock.New(),
		}

		uiServer, err := uiport.New(ctx, serverArgs)

		go func() {
			err = uiServer.ListenAndServe(serveOpts.ListenAddressUI)
			if err != nil {
				logger.Fatal("could not listen and serve", err)
			}
		}()

		internalApp, err := internalapp.NewApplication(&internalapp.NewApplicationArgs{
			Context: ctx,
			GroupID: serveOpts.DirectoryAddress,
			Storage: storage,
		})
		if err != nil {
			logger.Fatal("could not create application", err)
		}

		grpcServerInternal, err := internalgrpc.New(logger, internalApp, cert)
		if err != nil {
			logger.Fatal("could not create internal grpc server", err)
		}

		go func() {
			err = grpcServerInternal.ListenAndServe(serveOpts.ListenAddressInternal)
			if err != nil {
				logger.Fatal("could not listen and serve internal grpc server", err)
			}
		}()

		restAPI, err := rest.New(&rest.NewArgs{
			Logger:        logger,
			App:           app,
			Cert:          cert,
			ListenAddress: serveOpts.ListenAddressAPI,
		})

		if err != nil {
			logger.Fatal("could not create rest api", err)
		}

		go func() {
			err = restAPI.ListenAndServeTLS(serveOpts.TLSOptions.CertFile, serveOpts.TLSOptions.KeyFile)
			if err != nil {
				logger.Fatal("could not listen and serve API server", err)
			}
		}()

		p.Wait()

		logger.Info("starting graceful shutdown")

		gracefulCtx, cancel := context.WithTimeout(context.Background(), time.Minute)
		defer cancel()

		err = uiServer.Shutdown(gracefulCtx)
		if err != nil {
			logger.Error("could not shutdown server", err)
		}
	},
}
