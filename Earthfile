VERSION 0.5

FROM registry.gitlab.com/commonground/nlx/earthly-base-image:latest

WORKDIR /src

all:
    BUILD +proto
    BUILD +mocks
    BUILD +sqlc
    BUILD +oas

proto:
    BUILD +proto-txlog-api
    BUILD +proto-grpc-errors
    BUILD +proto-manager
    BUILD +proto-controller

oas:
    BUILD +oas-manager-external
    BUILD +oas-controller

mocks:
    BUILD +mocks-mockery

sqlc:
    BUILD +sqlc-txlog-api
    BUILD +sqlc-manager
    BUILD +sqlc-controller

enums:
    BUILD +enums-permissions

deps:
    COPY go.mod go.sum /src/

proto-txlog-api:
    FROM +deps
    COPY ./txlog-api/api/*.proto /src/

    RUN mkdir -p /dist || true && \
        protoc \
            -I. \
            -I/protobuf/include \
            -I/protobuf/googleapis \
            --go_out=/dist --go_opt=paths=source_relative \
            --go-grpc_out=/dist --go-grpc_opt=paths=source_relative \
            --grpc-gateway_out=/dist --grpc-gateway_opt=paths=source_relative \
            --openapiv2_out=/dist \
            --openapiv2_opt json_names_for_fields=false \
            ./txlog.proto
    RUN goimports -w -local "go.nlx.io" /dist/

    SAVE ARTIFACT /dist/*.* AS LOCAL ./txlog-api/api/

proto-grpc-errors:
    FROM +deps
    COPY ./common/grpcerrors/errors/*.proto /src/

    RUN mkdir -p /dist/external || true && \
        protoc \
            -I. \
            -I/protobuf/include \
            -I/protobuf/googleapis \
            --go_out=/dist --go_opt=paths=source_relative \
            --go-grpc_out=/dist --go-grpc_opt=paths=source_relative \
            --grpc-gateway_out=/dist --grpc-gateway_opt=paths=source_relative \
            --openapiv2_out=/dist \
            --openapiv2_opt json_names_for_fields=false \
            ./*.proto

    RUN goimports -w -local "go.nlx.io" /dist/

    SAVE ARTIFACT /dist/*.* AS LOCAL ./common/grpcerrors/errors/

proto-manager-internal:
    FROM +deps

    COPY ./manager/ports/int/grpc/api/*.proto /src/int/api/

    RUN cd /src/int/api/ && \
        mkdir -p /dist || true && \
        protoc \
            -I. \
            -I/protobuf/include \
            -I/protobuf/googleapis \
            --go_out=/dist --go_opt=paths=source_relative \
            --go-grpc_out=/dist --go-grpc_opt=paths=source_relative \
            ./manager.proto

    RUN goimports -w -local "go.nlx.io" /dist/

    SAVE ARTIFACT /dist/*.* AS LOCAL ./manager/ports/int/grpc/api/

proto-controller:
    FROM +deps

    COPY ./controller/ports/internalgrpc/api/*.proto /src/internalgrpc/api/

    RUN cd /src/internalgrpc/api/ && \
        mkdir -p /dist || true && \
        protoc \
            -I. \
            -I/protobuf/include \
            -I/protobuf/googleapis \
            --go_out=/dist --go_opt=paths=source_relative \
            --go-grpc_out=/dist --go-grpc_opt=paths=source_relative \
            ./controller.proto

    RUN goimports -w -local "go.nlx.io" /dist/

    SAVE ARTIFACT /dist/*.* AS LOCAL ./controller/ports/internalgrpc/api/

oas-manager-external:
    FROM +deps
    COPY ./manager/ports/ext/rest/api/*.yaml /src/

    RUN npx @redocly/cli join core.yaml logging.yaml --prefix-components-with-info-prop title -o fsc.yaml

    RUN mkdir models
    RUN oapi-codegen --config model.cfg.yaml fsc.yaml
    RUN oapi-codegen --config server.cfg.yaml fsc.yaml

    SAVE ARTIFACT /src/*.go AS LOCAL ./manager/ports/ext/rest/api/
    SAVE ARTIFACT /src/models/*.go AS LOCAL ./manager/ports/ext/rest/api/models/

oas-controller:
    FROM +deps
    COPY ./controller/ports/rest/api/*.yaml /src/

    RUN mkdir models
    RUN oapi-codegen --config model.cfg.yaml api.yaml
    RUN oapi-codegen --config server.cfg.yaml api.yaml

    SAVE ARTIFACT /src/*.go AS LOCAL ./controller/ports/rest/api/
    SAVE ARTIFACT /src/models/*.go AS LOCAL ./controller/ports/rest/api/models/

proto-manager:
    BUILD +proto-manager-internal

mocks-mockery:
    FROM +deps

    COPY ./ /src/

    RUN mkdir -p /dist || true
    WORKDIR /src

    RUN mockery

    RUN goimports -w -local "go.nlx.io" /src/

    # common
    SAVE ARTIFACT /src/common/transactionlog/mock/mock_transaction_logger.go AS LOCAL ./common/transactionlog/mock/mock_transaction_logger.go
    SAVE ARTIFACT /src/common/clock/mock/mock_clock.go AS LOCAL ./common/clock/mock/mock_clock.go

    # ca-certportal
    SAVE ARTIFACT /src/ca-certportal/mock/mock_signer.go AS LOCAL ./ca-certportal/mock/mock_signer.go

    # manager
    SAVE ARTIFACT /src/manager/domain/contract/mock/mock_repository.go AS LOCAL ./manager/domain/contract/mock/mock_repository.go

    # controller
    SAVE ARTIFACT /src/controller/adapters/idgenerator/mock/mock_id_generator.go AS LOCAL ./controller/adapters/idgenerator/mock/mock_id_generator.go
    SAVE ARTIFACT /src/controller/adapters/manager/mock/mock_manager.go AS LOCAL ./controller/adapters/manager/mock/mock_manager.go
    SAVE ARTIFACT /src/controller/adapters/storage/mock/mock_storage.go AS LOCAL ./controller/adapters/storage/mock/mock_storage.go

    # outway
    SAVE ARTIFACT /src/outway/domain/config/mock/mock_repository.go AS LOCAL ./outway/domain/config/mock/mock_repository.go

    # txlog
    SAVE ARTIFACT /src/txlog-api/domain/record/mock/mock_repository.go AS LOCAL ./txlog-api/domain/record/mock/mock_repository.go

    # directory-ui
    SAVE ARTIFACT /src/directory-ui/adapters/directory/mock/mock_repository.go AS LOCAL ./directory-ui/adapters/directory/mock/mock_repository.go


sqlc-txlog-api:
    FROM +deps
    COPY ./txlog-api/adapters/storage/postgres/queries /src/txlog-api/adapters/storage/postgres/queries
    COPY ./txlog-api/adapters/storage/postgres/migrations/sql /src/txlog-api/adapters/storage/postgres/migrations/sql

    WORKDIR /src/txlog-api/adapters/storage/postgres/queries

    RUN /usr/bin/sqlc generate

    RUN goimports -w -local "go.nlx.io" /src/

    SAVE ARTIFACT /src/txlog-api/adapters/storage/postgres/queries/* AS LOCAL ./txlog-api/adapters/storage/postgres/queries/

sqlc-manager:
    FROM +deps
    COPY ./manager/adapters/storage/postgres/queries /src/manager/adapters/storage/postgres/queries
    COPY ./manager/adapters/storage/postgres/migrations/sql /src/manager/adapters/storage/postgres/migrations/sql

    WORKDIR /src/manager/adapters/storage/postgres/queries

    RUN /usr/bin/sqlc generate

    RUN goimports -w -local "go.nlx.io" /src/

    SAVE ARTIFACT /src/manager/adapters/storage/postgres/queries/* AS LOCAL ./manager/adapters/storage/postgres/queries/

sqlc-controller:
    FROM +deps
    COPY ./controller/adapters/storage/postgres/queries /src/controller/adapters/storage/postgres/queries
    COPY ./controller/adapters/storage/postgres/migrations/sql /src/controller/adapters/storage/postgres/migrations/sql

    WORKDIR /src/controller/adapters/storage/postgres/queries

    RUN /usr/bin/sqlc generate

    RUN goimports -w -local "go.nlx.io" /src/

    SAVE ARTIFACT /src/controller/adapters/storage/postgres/queries/* AS LOCAL ./controller/adapters/storage/postgres/queries/
