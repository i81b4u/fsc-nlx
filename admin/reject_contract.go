// Copyright © VNG Realisatie 2023
// Licensed under the EUPL

package main

import (
	"context"
	"encoding/json"
	"fmt"
	"net/http"

	contractmanagerapi "go.nlx.io/nlx/manager/ports/int/grpc/api"
)

func parseRejectContractBody(r *http.Request) RejectContractFields {
	decoder := json.NewDecoder(r.Body)

	var t RejectContractFields
	err := decoder.Decode(&t)

	if err != nil {
		panic(err)
	}

	return t
}

func rejectContract(w http.ResponseWriter, r *http.Request) {
	requestBody := parseRejectContractBody(r)
	client := setupInternalManagerClient(requestBody.ManagerAddress)

	response, err := client.RejectContract(context.TODO(), &contractmanagerapi.RejectContractRequest{
		ContentHash: requestBody.ContentHash,
	})
	if err != nil {
		fmt.Fprintf(w, "failed to reject contract error: %s", err)
		return
	}

	fmt.Fprintf(w, "reject contract: %v", response)
}
