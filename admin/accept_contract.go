// Copyright © VNG Realisatie 2023
// Licensed under the EUPL

package main

import (
	"context"
	"encoding/json"
	"fmt"
	"net/http"

	managerapi "go.nlx.io/nlx/manager/ports/int/grpc/api"
)

func parseAcceptContractBody(r *http.Request) AcceptContractFields {
	decoder := json.NewDecoder(r.Body)

	var t AcceptContractFields

	err := decoder.Decode(&t)
	if err != nil {
		panic(err)
	}

	return t
}

func acceptContract(w http.ResponseWriter, r *http.Request) {
	requestBody := parseAcceptContractBody(r)
	client := setupInternalManagerClient(requestBody.ManagerAddress)

	response, err := client.AcceptContract(context.TODO(), &managerapi.AcceptContractRequest{
		ContentHash: requestBody.ContentHash,
	})
	if err != nil {
		fmt.Fprintf(w, "failed to accept contract error: %s", err)
		return
	}

	fmt.Fprintf(w, "accepted contract: %v", response)
}
