// Copyright © VNG Realisatie 2023
// Licensed under the EUPL

package main

import (
	"context"
	"errors"
	"fmt"
	"log"
	"net/http"

	"go.nlx.io/nlx/admin/pkg/manager"
	common_tls "go.nlx.io/nlx/common/tls"
)

type CreateServiceFields struct {
	ManagerAddress string
	ServicePeerID  string `json:"servicePeerId"`
	ServiceName    string
}

type RequestAccessToServiceFields struct {
	ManagerAddress              string
	OutwayPeerID                string `json:"outwayPeerId"`
	OutwayCertificateThumbprint string
	ServicePeerID               string `json:"servicePeerId"`
	ServiceName                 string
}

type AcceptContractFields struct {
	ManagerAddress string
	ContentHash    string
}

type RevokeContractFields struct {
	ManagerAddress string
	ContentHash    string
}

type RejectContractFields struct {
	ManagerAddress string
	ContentHash    string
}

type DelegateServiceConnectionFields struct {
	ManagerAddress              string
	OutwayPeerID                string `json:"outwayPeerId"`
	OutwayCertificateThumbprint string
	ServicePeerID               string `json:"servicePeerId"`
	ServiceName                 string
	Delegator                   string
}

type DelegateServicePublicationFields struct {
	ManagerAddress string
	ServicePeerID  string `json:"servicePeerId"`
	ServiceName    string
	Delegator      string
}

func main() {
	fmt.Printf("Starting server at port 8080\n")

	http.HandleFunc("/create-service", createService)
	http.HandleFunc("/request-access-to-service", requestAccessToService)
	http.HandleFunc("/accept-contract", acceptContract)
	http.HandleFunc("/revoke-contract", revokeContract)
	http.HandleFunc("/reject-contract", rejectContract)
	http.HandleFunc("/delegate-service-connection", delegateServiceConnection)
	http.HandleFunc("/delegate-service-publication", delegateServicePublication)

	// nolint:gosec // simple server since this is a tool for development only
	if err := http.ListenAndServe(":8080", nil); err != nil {
		log.Fatal(err)
	}
}

func getInternalCertOrgA() (*common_tls.CertificateBundle, error) {
	return common_tls.NewBundleFromFiles(
		"pki/internal/organization-a/certs/manager/cert.pem",
		"pki/internal/organization-a/certs/manager/key.pem",
		"pki/internal/organization-a/ca/root.pem",
	)
}

func getInternalCertOrgB() (*common_tls.CertificateBundle, error) {
	return common_tls.NewBundleFromFiles(
		"pki/internal/organization-b/certs/manager/cert.pem",
		"pki/internal/organization-b/certs/manager/key.pem",
		"pki/internal/organization-b/ca/root.pem",
	)
}

func getInternalCertOrgC() (*common_tls.CertificateBundle, error) {
	return common_tls.NewBundleFromFiles(
		"pki/internal/organization-c/certs/manager/cert.pem",
		"pki/internal/organization-c/certs/manager/key.pem",
		"pki/internal/organization-c/ca/root.pem",
	)
}

func setupInternalManagerClient(managerAddress string) manager.Client {
	var (
		internalCert *common_tls.CertificateBundle
		err          error
	)

	switch managerAddress {
	case "manager.organization-a.nlx.local:443":
		internalCert, err = getInternalCertOrgA()
	case "manager.organization-b.nlx.local:443":
		internalCert, err = getInternalCertOrgB()
	case "manager.organization-c.nlx.local:443":
		internalCert, err = getInternalCertOrgC()
	default:
		err = errors.New("unknown manager")
	}

	if err != nil {
		log.Fatalf("loading internal cert for org A: %v", err)
	}

	client, err := manager.NewClient(context.TODO(), managerAddress, internalCert)
	if err != nil {
		log.Fatalf("failed to setup manager client: %v", err)
	}

	return client
}
