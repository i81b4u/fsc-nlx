// Copyright © VNG Realisatie 2023
// Licensed under the EUPL

package main

import (
	"context"
	"encoding/json"
	"fmt"
	"net/http"
	"time"

	"github.com/google/uuid"

	managerapi "go.nlx.io/nlx/manager/ports/int/grpc/api"
)

func parseServiceBody(r *http.Request) CreateServiceFields {
	decoder := json.NewDecoder(r.Body)

	var t CreateServiceFields

	err := decoder.Decode(&t)
	if err != nil {
		panic(err)
	}

	return t
}

func createService(w http.ResponseWriter, r *http.Request) {
	requestBody := parseServiceBody(r)
	client := setupInternalManagerClient(requestBody.ManagerAddress)

	id := uuid.New()

	response, err := client.CreateContract(context.TODO(), &managerapi.CreateContractRequest{
		ContractContent: &managerapi.ContractContent{
			Id:      id[:],
			GroupId: "https://directory.shared.nlx.local:8443",
			Validity: &managerapi.Validity{
				NotBefore: time.Now().Unix(),
				NotAfter:  time.Now().Add(oneYear).Unix(),
			},
			Grants: []*managerapi.Grant{
				{
					Data: &managerapi.Grant_ServicePublication{
						ServicePublication: &managerapi.GrantServicePublication{
							Directory: &managerapi.GrantServicePublication_Directory{
								PeerId: "12345678901234567899",
							},
							Service: &managerapi.GrantServicePublication_Service{
								PeerId: requestBody.ServicePeerID,
								Name:   requestBody.ServiceName,
							},
						},
					},
				},
			},
			HashAlgorithm: managerapi.HashAlgorithm_HASH_ALGORITHM_SHA3_512,
			CreatedAt:     time.Now().Unix(),
		},
	})
	if err != nil {
		fmt.Fprintf(w, "failed to create contract error: %s", err)
		return
	}

	fmt.Fprintf(w, "created contract with id %s: %v", id, response)
}
