// Copyright © VNG Realisatie 2023
// Licensed under the EUPL

package main

import (
	"context"
	"encoding/json"
	"fmt"
	"net/http"

	contractmanagerapi "go.nlx.io/nlx/manager/ports/int/grpc/api"
)

func parseRevokeContractBody(r *http.Request) RevokeContractFields {
	decoder := json.NewDecoder(r.Body)

	var t RevokeContractFields
	err := decoder.Decode(&t)

	if err != nil {
		panic(err)
	}

	return t
}

func revokeContract(w http.ResponseWriter, r *http.Request) {
	requestBody := parseRevokeContractBody(r)
	client := setupInternalManagerClient(requestBody.ManagerAddress)

	response, err := client.RevokeContract(context.TODO(), &contractmanagerapi.RevokeContractRequest{
		ContentHash: requestBody.ContentHash,
	})
	if err != nil {
		fmt.Fprintf(w, "failed to revoke contract error: %s", err)
		return
	}

	fmt.Fprintf(w, "revoke contract: %v", response)
}
