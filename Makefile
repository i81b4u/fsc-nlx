# Copyright © VNG Realisatie 2022
# Licensed under the EUPL

.PHONY: fix-copyright-headers
fix-copyright-headers:
	docker run -it -v ${PWD}:/src ghcr.io/google/addlicense -ignore "**/node_modules/**" -ignore "helm/**/*.yaml" -f license-header.tmpl .

.PHONY: fix-newlines
fix-newlines:
	./scripts/eol-at-eof-linter.sh -f

.PHONY: fix-proto-formatting
fix-proto-formatting:
	docker run --rm \
		-v ${PWD}/controller/ports/internalgrpc/api:/src/controller/ports/internalgrpc/api \
		-v ${PWD}/txlog-api/api:/src/txlog-api/api \
		-v ${PWD}/manager/ports/int/grpc/api:/src/manager/ports/int/grpc/api \
 		--workdir /src bufbuild/buf:1.26.1 format -w
