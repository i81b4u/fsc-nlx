/**
 * Copyright (c) 2017-present, Facebook, Inc.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

module.exports = {
  docs: [
    'introduction/introduction',
    {
      type: 'category',
      label: 'Try NLX',
      collapsible: true,
      collapsed: true,
      items: [
        {
          type: 'category',
          label: 'Docker Compose',
          collapsible: true,
          collapsed: true,
          items: [
            'try-nlx/docker/introduction',
            'try-nlx/docker/setup-your-environment',
            'try-nlx/docker/retrieve-a-demo-certificate',
            'try-nlx/docker/getting-up-and-running',
            'try-nlx/docker/provide-an-api',
          ],
        },
        {
          type: 'category',
          label: 'Helm',
          collapsible: true,
          collapsed: true,
          items: [
            'try-nlx/helm/introduction',
            'try-nlx/helm/preparation',
            'try-nlx/helm/create-namespace',
            'try-nlx/helm/create-certificate',
            'try-nlx/helm/postgresql',
            'try-nlx/helm/transaction-log',
            'try-nlx/helm/nlx-controller',
            'try-nlx/helm/nlx-manager',
            'try-nlx/helm/nlx-inway',
            'try-nlx/helm/nlx-outway',
            'try-nlx/helm/sample-api',
            'try-nlx/helm/access-api',
            'try-nlx/helm/finish',
          ],
        },
      ],
    },
  ],
};
