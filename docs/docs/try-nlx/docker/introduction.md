---
id: introduction
title: Introduction
---

> Note that this tutorial is not suited for production environments.
> Its only purpose is to enable you to setup a local test environment.

In this guide you will learn how to set up a local test environment, provide and consume APIs on the NLX network.
We will use the NLX Controller, a web interface for managing your NLX components.

The target audience is **system operators**.

## Components

The following components are part of the NLX setup :

**Controller**

The Controller is at the heart of NLX. It provides a web interface to manage NLX.
Your Inways and Outways use the Controller to retrieve their configuration.

**Manager**

The Manager is the contact point for your organization on the NLX network. It negotiates Contracts.

**Inway**

The Inway functions as a gateway to the Services you offer to the NLX network.

**Outway**

The Outway is used by your organization to access services on the NLX network.

**Transaction Log**

The Transaction Log is a log of transactions made from and to your organization.

**PostgreSQL Database**

The configuration of your Inways, the Services you provide and Contracts are stored in a PostgreSQL database.

## In sum

You've learned about all the components used by NLX.
Next up, lets [setup our local environment](./setup-your-environment).
