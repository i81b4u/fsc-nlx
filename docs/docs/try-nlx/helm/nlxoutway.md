---
id: nlx-outway
title: Install NLX Outway
---

# 8. Install NLX Outway

## Install internal certificate

We are going to use the cert-manager we installed earlier to create an internal certificate.

Run the following command to install the certificate on the Kubernetes cluster:

```shell
kubectl -n nlx apply -f outway-internal-tls.yaml
```

Check if the certificate has been created

```shell
kubectl -n nlx get secrets | grep outway-internal-tls
```

The output should look similar to:

```
outway-internal-tls             kubernetes.io/tls    3      35s
```

## Install Outway Chart

Now we create the values file with the settings for Helm. Open the file `nlx-outway-values.yaml` in an editor, edit the values below and save the file:

-   The value `<file: ca.crt>` must be replaced by the contents of the ca.crt file. This file is in your working directory.
    -   Copy the contents of the files **excluding** the '-----BEGIN CERTIFICATE-----' and '-----END CERTIFICATE-----' lines.
    -   Paste the content between the start and end lines and make sure the alignment is the same as the start and end lines
    -   Save the modified file

Then we install the Outway by running:

```shell
helm -n nlx upgrade --install outway -f nlx-outway-values.yaml commonground/fsc-nlx-outway
```

Check if the Outway pod is healthy:

```shell
kubectl -n nlx get pods
```

A similar line should now show up:

```
NAME                                             READY   STATUS    RESTARTS   AGE
controller-fsc-nlx-controller-7b5589c697-mxrrp   1/1     Running   0          8m9s
inway-fsc-nlx-inway-565d45fd48-r8ckt             1/1     Running   0          7m56s
manager-fsc-nlx-manager-5857d8f878-h2484         1/1     Running   0          8m7s
outway-fsc-nlx-outway-675f7fd5bb-4wstg           1/1     Running   0          28s
postgresql-0                                     1/1     Running   0          58m
txlog-api-fsc-nlx-txlog-api-69f9487bf5-w2xqj     1/1     Running   0          8m6s
```
