---
id: nlx-manager
title: Install NLX Manager
---

# 6. Install NLX Manager

We are now going to install NLX Manager. The NLX Manager negotiates Contracts between Peers. The NLX Manager is exposed externally.

If you have **not** installed Postgres via the Bitnami chart as described in this guide then you need to make sure that a database called `nlx_manager` exists.

## Install internal certificate

Run the following command to install the internal NLX Manager certificate on the Kubernetes cluster

```
kubectl -n nlx apply -f manager-internal-tls.yaml
```

Check if the certificate has been created

```
kubectl -n nlx get secrets | grep manager-internal-tls
```

The output should look similar to:

```
manager-internal-tls             kubernetes.io/tls    3      35s
```

## NLX Manager chart

We are now going to create a configuration file for the NLX Manager installation.
Open the file `nlx-manager-values.yaml` in a text editor and edit the values below:

-   `<managerAddress>` replace this with the external address of the Manager, this address should contain the same domain name as the certificate you created for the Manager in [step 2 of this guide](createcertificate.mdx). Make sure to include the scheme and port number. E.g. https://my-manager.my-organisation.com:8443
-   `<postgres-password>` replace this with the Postgres password you saved earlier.
-   The values `<file: ca.crt>` must be replaced by the contents of the file `ca.crt`. You have this file in your working directory.
    -   Copy the contents of the files **excluding** the '-----BEGIN XXXXXXXXX-----' and '-----END XXXXXXXXX-----' lines.
    -   Paste the content between the start and end lines and make sure the alignment is the same as the start and end lines
    -   Save the modified file

Make sure to save the file. Next, let's install NLX Manager:

```
helm -n nlx upgrade --install manager -f nlx-manager-values.yaml commonground/fsc-nlx-manager
```

Check if the Manager is running:

```
kubectl -n nlx get pods
```

The output should look similar to:

```
NAME                                             READY   STATUS    RESTARTS   AGE
controller-fsc-nlx-controller-7fb775cc75-hjg76   1/1     Running   0          4m51s
manager-fsc-nlx-manager-5956594bb7-dkt7j         1/1     Running   0          14s
postgresql-0                                     1/1     Running   0          19m
txlog-api-fsc-nlx-txlog-api-69f9487bf5-fqj2w     1/1     Running   0          11m
```

Now that the Manager is running we can continue open the Contracts page of the Controller.

Open your browser and navigate to the path `/contracts` of the Controller UI. The address if the Controller is determined by the value you used for `<hostname nlx-controller>` in [step 4](./nlxcontroller.md). E.g. https://controller-ui.example.com/contracts

You should see an empty overview of your Contracts. This means both your Manager and Controller are running.
