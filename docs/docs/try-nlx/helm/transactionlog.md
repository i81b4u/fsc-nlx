---
id: transaction-log
title: Setup the Transaction Log
---

# 4. Setup the Transaction Log

The Inway and the Outway are able to log metadata of the requests they process, these log records can be viewed using
the NLX Controler. The Transaction log is mandatory when deploying NLX. In this step you will learn how to setup
the Transaction Log.

## Certificate

Run the following command to install a certificate for the transaction-log API on the Kubernetes cluster:

```shell
kubectl -n nlx apply -f txlog-api-internal-tls.yaml
```

Check if the certificate has been created

```shell
kubectl -n nlx get secrets | grep txlog-api-internal-tls
```

The output should look similar to:

```
txlog-api-internal-tls             kubernetes.io/tls    3      35s
```

## Install the Transaction Log API

The transaction logs can be viewed in NLX Controller. NLX Controller communicates with the Transaction Log API to
retrieve the logs from the database. Now let's install the Transaction Log API on the Kubernetes cluster.

First open the `txlog-api-values.yaml`, edit the values below and save the file:

-   `<postgres-password>` replace this with the Postgres password you saved earlier.
-   The values `<file: ca.crt>` must be replaced by the contents of the file `ca.crt`. You have this file in your working directory.
    -   Copy the contents of the files **excluding** the '-----BEGIN XXXXXXXXX-----' and '-----END XXXXXXXXX-----' lines.
    -   Paste the content between the start and end lines and make sure the alignment is the same as the start and end lines
    -   Save the modified file

Run the following commands to install the Transaction Log API on the cluster:

```shell
helm -n nlx upgrade --install txlog-api -f txlog-api-values.yaml commonground/fsc-nlx-txlog-api
```

Check if the Transaction Log API is running:

```shell
kubectl -n nlx get pods
```

A similar line should now show up:

```
txlog-api-fsc-nlx-txlog-api-69f9487bf5-fqj2w   1/1     Running     1          1m30s
```

You are now ready to setup your Controller
