---
id: nlx-inway
title: Install NLX Inway
---

# 7. Install NLX Inway

## Install internal certificate

We are going to use the cert-manager we installed earlier to create an internal certificate.

Run the following command to install the certificate on the Kubernetes cluster:

```shell
kubectl -n nlx apply -f inway-internal-tls.yaml
```

Check if the certificate has been created

```shell
kubectl -n nlx get secrets | grep inway-internal-tls
```

The output should look similar to:

```
inway-internal-tls             kubernetes.io/tls    3      35s
```

## Install Inway chart

Now we create the values file with the settings for Helm. Open the file `nlx-inway-values.yaml` in an editor, edit the values below and save the file:

-   `<inway-name>` the name of your inway. The name must contain only alphanumeric characters and dashes. eg: `my-inway`
-   `<self-address>` the address at which your inway can be reached, including the port on which the inway is available (443 by default). This address must match the address (the Common Name) of your Inway certificate created in [step 2](./createcertificate.mdx) . E.g.: `https://inway.example.com:443`
-   The value `<file: ca.crt>` must be replaced by the contents of the ca.crt file. This file is in your working directory.
    -   Copy the contents of the files **excluding** the '-----BEGIN CERTIFICATE-----' and '-----END CERTIFICATE-----' lines.
    -   Paste the content between the start and end lines and make sure the alignment is the same as the start and end lines
    -   Save the modified file

Then we install the Inway by running:

```shell
helm -n nlx upgrade --install inway -f nlx-inway-values.yaml commonground/fsc-nlx-inway
```

Check if the Inway pod is healthy.

```shell
kubectl -n nlx get pods
```

You should now see something like this:

```
NAME                                             READY   STATUS    RESTARTS   AGE
controller-fsc-nlx-controller-7b5589c697-mxrrp   1/1     Running   0          79s
inway-fsc-nlx-inway-565d45fd48-r8ckt             1/1     Running   0          66s
manager-fsc-nlx-manager-5857d8f878-h2484         1/1     Running   0          77s
postgresql-0                                     1/1     Running   0          51m
txlog-api-fsc-nlx-txlog-api-69f9487bf5-w2xqj     1/1     Running   0          76s
```

To verify the status of the Inway, go to the NLX Controller interface and see if your Inway (with the name you chose) is in your list of Inways when you try to add a new Service.

## Link domain to your Inway

Run the following command:

```shell
kubectl get -n nlx svc
```

In the result, you will see a service for the Inway called `nlx-inway`. Copy the value stated at `EXTERNAL-IP` and link this IP address (I.E. create a DNS record) to your Inway domain.

After linking your domain, run the following command:

```shell
nslookup <the domain address of your Inway>
```

In the result with the address equal to the `EXTERNAL-IP` of the service `nlx-inway`

_Note: it may take some time before the domain is linked. You can continue with this guide, but eventually, the domain must be linked before your service can be accessed via the Inway._
