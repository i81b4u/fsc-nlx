---
id: nlx-controller
title: Install NLX Controller
---

# 5. Install NLX Controller

We are now going to install NLX Controller which exposes a RESTful API and web interface.
The web interface enables you to easily manage your NLX installation using the browser.

The web interface of the Controller can be exposed through an ingress.
In this tutorial we expect you to have a hostname for the Controller. Eg. 'controller-ui.example.com'.

If you have **not** installed Postgres via the Bitnami chart as described in this guide then you need to make sure that a database called `nlx_controller` exists.

## Install internal certificate

Run the following command to install the internal NLX Controller certificate on the Kubernetes cluster

```
kubectl -n nlx apply -f controller-internal-tls.yaml
```

Check if the certificate has been created:

```
kubectl -n nlx get secrets | grep controller-internal-tls
```

The output should look similar to:

```
controller-internal-tls             kubernetes.io/tls    3      35s
```

## NLX Controller chart

We are now going to create a configuration file for the NLX Controller installation.
Open the file `nlx-controller-values.yaml` in a text editor and edit the values below:

-   `<hostname nlx-controller>` replace this with the hostname of the Controller UI. E.g. controller-ui.example.com
-   `<postgres-password>` replace this with the Postgres password you saved earlier.
-   The values `<file: ca.crt>` must be replaced by the contents of the file `ca.crt`. You have this file in your working directory.
    -   Copy the contents of the files **excluding** the '-----BEGIN XXXXXXXXX-----' and '-----END XXXXXXXXX-----' lines.
    -   Paste the content between the start and end lines and make sure the alignment is the same as the start and end lines
    -   Save the modified file

Make sure to save the file. Next, let's install NLX Controller:

```
helm -n nlx upgrade --install controller -f nlx-controller-values.yaml commonground/fsc-nlx-controller
```

Check if the Controller is running:

```
kubectl -n nlx get pods
```

The output should look similar to:

```
NAME                                             READY   STATUS            RESTARTS   AGE
controller-fsc-nlx-controller-7fb775cc75-hjg76   1/1     Running           0          9s
postgresql-0                                     1/1     Running           0          14m
txlog-api-fsc-nlx-txlog-api-69f9487bf5-fqj2w     1/1     Running           0          6m32s
```

Now that the Controller is running we can continue with the Manager.
