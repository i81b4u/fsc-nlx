---
id: access-api
title: Access API through a client
---

# 10. Access API through a client

## Set port forwarding

You have installed the [NLX Outway](nlxoutway.md) and the example Service `basisregister-fictieve-kentekens.`
The Outway is currently only available from within the cluster.
In order to make a request to our API via the Outway,
the Outway must be available for your local machine. For this, you need to create a port forward which will forward traffic from our machine to the Outway running in the Kubernetes cluster.

You can create the port forward by running the following command:

```
kubectl -n nlx port-forward service/outway-fsc-nlx-outway 8081:80
```

Check if the port forward is working

```
curl localhost:8081
```

You should get the following response:

```
{"message":"unknown grant hash: \"\" in Fsc-Grant-Hash header","source":"outway","location":"C1","code":"UNKNOWN_GRANT_HASH_IN_HEADER","metadata":{"services":[]}}%
```

The Outway produced an error because it needs a reference to the Contract containing the authorization to use our Service. This reference is called a Grant hash, and we can obtain one by creating a Contract.

## Create a Contract which will grant us access to the Service

Requesting access via NLX Controller interface:

1. Navigate to the 'Contracts overview' in NLX Controller.
2. Click the yellow `Add Contract` button
3. Click on the `Add` button below ServiceConnectionGrant
4. Enter your PeerID into the Service Peer ID field. Your Peer ID is visible in the top right corner of the Contracts overview page
5. Enter `basisregister-fictieve-kentekens` into the Service Naam field.
6. Once again enter your PeerID into the Outway Peer ID field. Your Peer ID is visible in the top right corner of the Contracts overview page
7. Confirm that field Outway Certificate Thumbprint is prefilled with a thumbprint
8. Click the `Add contract` button.

Navigate to the Contracts overview page. A Contract with the Grant type `Service Connection` for the Service `basisregister-fictieve-kentekens` should be visible.
Normally the Peer hosting the Service must also sign the Contract but since we are both providing and consuming the Service this is not necessary.

## Consuming the Service

Let's call our Outway again.

```
curl localhost:8081
```

The response should look similar to:

```
{"message":"unknown grant hash: \"\" in Fsc-Grant-Hash header","source":"outway","location":"C1","code":"UNKNOWN_GRANT_HASH_IN_HEADER","metadata":{"services":[{"GrantHash":"$1$4$hdxN9_CV2iFeSA7O_eOn4rgLmiIcZimspK16-VYHGC_ogCTuO6xAC_hv-dA09QS0qKfKEMsFLXjs10LJHosZUQ==","Name":"basisregister-fictieve-kentekens","PeerID":"01691582300541934913"}]}}%
```

The Outway still produces an error telling us it did not receive a Grant hash but because we created the Contract, the Grant hash is in the response. In the above example `$1$4$hdxN9_CV2iFeSA7O_eOn4rgLmiIcZimspK16-VYHGC_ogCTuO6xAC_hv-dA09QS0qKfKEMsFLXjs10LJHosZUQ==`

Let's call our Outway again with the Grant hash. We can do so by providing the Grant hash using the HTTP header `Fsc-Grant-Hash`. Make sure to replace `<grant-hash>` with the Grant hash outputted by the previous command.

```
curl -H 'Fsc-Grant-Hash: <grant-hash>' localhost:8081/voertuigen
```

After running the command you should see the following result:

```json
{
    "aantal": 6,
    "resultaten": [
        {
            "burgerservicenummer": "663678651",
            "datum_tenaamstelling": "29-01-2018",
            "eerste_kleur": "GRIJS",
            "europese_voertuigcategorie": "M1",
            "handelsbenaming": "MAZDA 3",
            "kenteken": "RT774D",
            "merk": "MAZDA",
            "voertuigsoort": "Personenauto"
        },
        {
            "burgerservicenummer": "425749708",
            "datum_tenaamstelling": "16-11-2016",
            "eerste_kleur": "GRIJS",
            "europese_voertuigcategorie": "M1",
            "handelsbenaming": "TOYOTA YARIS HYBRID",
            "kenteken": "KN958B",
            "merk": "TOYOTA",
            "voertuigsoort": "Personenauto"
        },
        {
            "burgerservicenummer": "248496086",
            "datum_tenaamstelling": "17-04-2009",
            "eerste_kleur": "GRIJS",
            "europese_voertuigcategorie": "M1",
            "handelsbenaming": "CORSA-C",
            "kenteken": "81HZFB",
            "merk": "OPEL",
            "voertuigsoort": "Personenauto"
        },
        {
            "burgerservicenummer": "187788248",
            "datum_tenaamstelling": "15-06-2015",
            "eerste_kleur": "GRIJS",
            "europese_voertuigcategorie": "M1",
            "handelsbenaming": "HEARSE",
            "kenteken": "GJ713R",
            "merk": "CADILLAC",
            "voertuigsoort": "Personenauto"
        },
        {
            "burgerservicenummer": "581536630",
            "datum_tenaamstelling": "13-07-2017",
            "eerste_kleur": "BLAUW",
            "europese_voertuigcategorie": "M1",
            "handelsbenaming": "AGILA",
            "kenteken": "50HSZS",
            "merk": "OPEL",
            "voertuigsoort": "Personenauto"
        },
        {
            "burgerservicenummer": "750461986",
            "datum_tenaamstelling": "06-11-2000",
            "eerste_kleur": "BLAUW",
            "europese_voertuigcategorie": "M1",
            "handelsbenaming": "SOVEREIGN HE",
            "kenteken": "KS98DN",
            "merk": "JAGUAR",
            "voertuigsoort": "Personenauto"
        }
    ]
}
```
