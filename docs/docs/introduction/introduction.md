---
id: introduction
title: Introduction
---

**FSC NLX** is an open source peer-to-peer system facilitating federated authentication, secure connecting and protocolling in a large-scale, dynamic API ecosystem with many organizations.

FSC NLX is based on the [Federated Service Connectivity (FSC) standard](https://commonground.gitlab.io/standards/fsc/). This standard describes how (governmental) organizations should interact when exchanging data in a uniform and automated manner. The standard is developed in the context of the [Common Ground vision](https://github.com/VNG-Realisatie/common-ground) and the functionality is located on the integration layer of the Common Ground five-layer model, which supports the separation of  applications and data. This standard fulfills some of the required functions that together lead to technical interoperability. The standard therefore also applies to various national programs such as 'Data bij de Bron', 'Federatief Data Stelsel', 'Regie op gegevens' and the 'Inter-Bestuurlijke Data Strategie'. The standard is about standardizing the exchange of data, not about standardizing the data itself.
Together with the description of the standard, a test suite and a working reference implementation are also available. This reference implementation is FSC NLX.

### Quick history lesson
Before September 2022 the NLX initiative had the same objectives as FSC, but was based on the vision that the technical interoperability was best supported by mandatory software. The idea was that this software (NLX) would be optimized for its purpose and therefor more efficient and effective than existing generic gateways available. Being open source software owned by and maintained via the government, it would be easier to manage the continuesly improvement and releasing of the software since there would be only one codebase. This vision was not shared by all stakeholders. Some just wanted to use the gateway software already in place. Then in September 2022 Gartner published the 'objectiveringsonderzoek NLX', which was an independent study based on literature and stakeholder interviews. The most relevant conclusion in this context was that there is a need for a standard opposed to mandatory software. From then on forward the FSC standard, a test suite and a reference implementation has been written, developed and tested together with a few suppliers and municipalities. Both on municipality level as on national level standardisation processes have been started.

### FSC NLX Component overview
Below an overview of the FSC NLX components is displayed.

![FSC NLX Component overview](component-overview-FSCNLX.drawio.png)

When deploying FSC NLX you will deploy the transaction logging, the Outway, the Manager and the Controller (not described in the FSC standard - its a UI to manage everything for your convinience).

The component names correspond to the names used in the FSC standard.
