// Copyright © VNG Realisatie 2022
// Licensed under the EUPL

package command

import (
	"context"
	"errors"
	"fmt"
	"time"

	app_errors "go.nlx.io/nlx/txlog-api/app/errors"
	"go.nlx.io/nlx/txlog-api/domain/record"
	"go.nlx.io/nlx/txlog-api/ports/logger"
)

type Clock interface {
	Now() time.Time
}

type CreateRecordsHandler struct {
	clock      Clock
	repository record.Repository
	logger     logger.Logger
}

func NewCreateRecordsHandler(repository record.Repository, clock Clock, lgr logger.Logger) (*CreateRecordsHandler, error) {
	if repository == nil {
		return nil, errors.New("repository is required")
	}

	if clock == nil {
		return nil, errors.New("repository is required")
	}

	if lgr == nil {
		return nil, errors.New("logger is required")
	}

	return &CreateRecordsHandler{
		repository: repository,
		clock:      clock,
		logger:     lgr,
	}, nil
}

type NewRecordArgs struct {
	TransactionID string
	GrantHash     string
	ServiceName   string
	Direction     record.Direction
	Source        interface{}
	Destination   interface{}
	CreatedAt     time.Time
}

type NewRecordSourceArgs struct {
	OutwayPeerID string
}

type NewRecordDelegatedSourceArgs struct {
	OutwayPeerID    string
	DelegatorPeerID string
}

type NewRecordDestinationArgs struct {
	ServicePeerID string
}

type NewRecordDelegatedDestinationArgs struct {
	ServicePeerID   string
	DelegatorPeerID string
}

func (h *CreateRecordsHandler) Handle(ctx context.Context, records []*NewRecordArgs) error {
	newRecords := make([]*record.Record, len(records))

	for i, r := range records {
		var src interface{}
		switch s := r.Source.(type) {
		case *NewRecordSourceArgs:
			src = &record.NewRecordSourceArgs{
				OutwayPeerID: s.OutwayPeerID,
			}
		case *NewRecordDelegatedSourceArgs:
			src = &record.NewRecordDelegatedSourceArgs{
				OutwayPeerID:    s.OutwayPeerID,
				DelegatorPeerID: s.DelegatorPeerID,
			}
		}

		var dest interface{}
		switch s := r.Destination.(type) {
		case *NewRecordDestinationArgs:
			dest = &record.NewRecordDestinationArgs{
				ServicePeerID: s.ServicePeerID,
			}
		case *NewRecordDelegatedDestinationArgs:
			dest = &record.NewRecordDelegatedDestinationArgs{
				ServicePeerID:   s.ServicePeerID,
				DelegatorPeerID: s.DelegatorPeerID,
			}
		}

		var err error

		newRecords[i], err = record.NewRecord(&record.NewRecordArgs{
			TransactionID: r.TransactionID,
			GrantHash:     r.GrantHash,
			ServiceName:   r.ServiceName,
			Direction:     r.Direction,
			Source:        src,
			Destination:   dest,
			CreatedAt:     r.CreatedAt,
		})
		if err != nil {
			return app_errors.NewIncorrectInputError(fmt.Sprintf("invalid input: %s", err))
		}
	}

	err := h.repository.CreateRecords(ctx, newRecords)
	if err != nil {
		h.logger.Error("create records", err)
		return err
	}

	return nil
}
