// Copyright © VNG Realisatie 2022
// Licensed under the EUPL

package query

import (
	"context"
	"fmt"
	"time"

	"github.com/pkg/errors"

	"go.nlx.io/nlx/txlog-api/domain/record"
)

const defaultLimit = 100

type ListRecordsHandler struct {
	recordsRepository record.Repository
}

type SortOrder string

const (
	SortOrderAscending  SortOrder = "asc"
	SortOrderDescending SortOrder = "desc"
)

type Period struct {
	Start time.Time
	End   time.Time
}

type Filter struct {
	Period        *Period
	TransactionID string
	ServiceName   string
	GrantHash     string
	PeerID        string
}

type Pagination struct {
	StartID   uint64
	Limit     uint32
	SortOrder SortOrder
}

type ListTransactionLogRecordsHandlerArgs struct {
	Pagination *Pagination
	Filters    []*Filter
}

func NewListRecordsHandler(repository record.Repository) *ListRecordsHandler {
	return &ListRecordsHandler{
		recordsRepository: repository,
	}
}

func (l *ListRecordsHandler) Handle(ctx context.Context, args *ListTransactionLogRecordsHandlerArgs) ([]*Record, error) {
	req, err := reqToRepo(args)
	if err != nil {
		return nil, errors.Wrap(err, "invalid list records handler args")
	}

	records, err := l.recordsRepository.ListRecords(ctx, req)
	if err != nil {
		return nil, err
	}

	result := make([]*Record, len(records))

	for i, model := range records {
		var src interface{}
		switch s := model.Source().(type) {
		case *record.Source:
			src = &RecordSource{
				OutwayPeerID: s.OutwayPeerID(),
			}
		case *record.DelegatedSource:
			src = &RecordDelegatedSource{
				OutwayPeerID:    s.OutwayPeerID(),
				DelegatorPeerID: s.DelegatorPeerID(),
			}
		}

		var dest interface{}
		switch s := model.Destination().(type) {
		case *record.Destination:
			dest = &RecordDestination{
				ServicePeerID: s.ServicePeerID(),
			}
		case *record.DelegatedDestination:
			dest = &RecordDelegatedDestination{
				ServicePeerID:   s.ServicePeerID(),
				DelegatorPeerID: s.DelegatorPeerID(),
			}
		}

		result[i] = &Record{
			ID:            model.ID(),
			TransactionID: model.TransactionID().String(),
			GrantHash:     model.GrantHash(),
			Direction:     model.Direction(),
			ServiceName:   model.ServiceName(),
			Source:        src,
			Destination:   dest,
			CreatedAt:     model.CreatedAt(),
		}
	}

	return result, nil
}

func reqToRepo(req *ListTransactionLogRecordsHandlerArgs) (*record.ListRecordsRequest, error) {
	filters := make([]*record.Filter, len(req.Filters))

	for i, f := range req.Filters {
		var id *record.TransactionID

		if f.TransactionID != "" {
			var err error

			id, err = record.NewTransactionIDFromString(f.TransactionID)
			if err != nil {
				return nil, fmt.Errorf("invalid transaction ID: %q in filter: %w", f.TransactionID, err)
			}
		}

		filter := &record.Filter{
			TransactionID: id,
			ServiceName:   f.ServiceName,
			GrantHash:     f.GrantHash,
			PeerID:        f.PeerID,
		}

		if f.Period != nil {
			filter.Period = &record.Period{
				Start: f.Period.Start,
				End:   f.Period.End,
			}
		}

		filters[i] = filter
	}

	if req.Pagination.Limit == 0 {
		req.Pagination.Limit = defaultLimit
	}

	return &record.ListRecordsRequest{
		Pagination: &record.Pagination{
			StartID:   req.Pagination.StartID,
			Limit:     req.Pagination.Limit,
			SortOrder: sortOrderToProto(req.Pagination.SortOrder),
		},
		Filters: filters,
	}, nil
}

func sortOrderToProto(o SortOrder) record.SortOrder {
	switch o {
	case SortOrderAscending:
		return record.SortOrderAscending
	case SortOrderDescending:
		return record.SortOrderDescending
	default:
		return record.SortOrderUnspecified
	}
}
