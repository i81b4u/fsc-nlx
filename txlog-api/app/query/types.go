// Copyright © VNG Realisatie 2022
// Licensed under the EUPL

package query

import (
	"time"

	"go.nlx.io/nlx/txlog-api/domain/record"
)

type Record struct {
	ID            uint64
	TransactionID string
	GrantHash     string
	ServiceName   string
	Direction     record.Direction
	Source        interface{}
	Destination   interface{}
	CreatedAt     time.Time
}

type RecordSource struct {
	OutwayPeerID string
}

type RecordDelegatedSource struct {
	OutwayPeerID    string
	DelegatorPeerID string
}

type RecordDestination struct {
	ServicePeerID string
}

type RecordDelegatedDestination struct {
	ServicePeerID   string
	DelegatorPeerID string
}
