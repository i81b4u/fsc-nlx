// Copyright © VNG Realisatie 2022
// Licensed under the EUPL

package postgresadapter

import (
	"context"
	"database/sql"
	"fmt"

	"github.com/gofrs/uuid"
	"github.com/pkg/errors"

	"go.nlx.io/nlx/txlog-api/adapters/storage/postgres/queries"
	"go.nlx.io/nlx/txlog-api/domain/record"
)

type sortOrder string

const (
	sortOrderAsc  sortOrder = "asc"
	sortOrderDesc sortOrder = "desc"
)

func (r *PostgreSQLRepository) CreateRecords(ctx context.Context, records []*record.Record) error {
	tx, err := r.db.Begin()
	if err != nil {
		return err
	}

	defer func() {
		err = tx.Rollback()
		if err != nil {
			if errors.Is(err, sql.ErrTxDone) {
				return
			}

			fmt.Printf("cannot rollback database transaction while creating record: %e", err)
		}
	}()

	qtx := r.queries.WithTx(tx)

	for _, r := range records {
		var rec *queries.CreateRecordParams

		rec, err = recordModelToDB(r)
		if err != nil {
			return errors.Wrap(err, "could not create record params from model")
		}

		err = qtx.CreateRecord(ctx, rec)
		if err != nil {
			return errors.Wrap(err, "could not create log record in database")
		}
	}

	err = tx.Commit()
	if err != nil {
		return err
	}

	return nil
}

func recordModelToDB(r *record.Record) (*queries.CreateRecordParams, error) {
	rec := &queries.CreateRecordParams{
		TransactionID:                r.TransactionID().UUID(),
		GrantHash:                    r.GrantHash(),
		Direction:                    directionModelToDB(r.Direction()),
		ServiceName:                  r.ServiceName(),
		CreatedAt:                    r.CreatedAt(),
		SrcOutwayPeerID:              sql.NullString{},
		DelegatedSrcOutwayPeerID:     sql.NullString{},
		DelegatedSrcDelegatorPeerID:  sql.NullString{},
		DestServicePeerID:            sql.NullString{},
		DelegatedDestServicePeerID:   sql.NullString{},
		DelegatedDestDelegatorPeerID: sql.NullString{},
	}

	src := r.Source()

	switch d := src.(type) {
	case *record.Source:
		rec.SrcOutwayPeerID = sql.NullString{Valid: true, String: d.OutwayPeerID()}
	case *record.DelegatedSource:
		rec.DelegatedSrcOutwayPeerID = sql.NullString{Valid: true, String: d.OutwayPeerID()}
		rec.DelegatedSrcDelegatorPeerID = sql.NullString{Valid: true, String: d.DelegatorPeerID()}
	default:
		return nil, fmt.Errorf("unknown record source type: %T", d)
	}

	dest := r.Destination()

	switch d := dest.(type) {
	case *record.Destination:
		rec.DestServicePeerID = sql.NullString{Valid: true, String: d.ServicePeerID()}
	case *record.DelegatedDestination:
		rec.DelegatedDestServicePeerID = sql.NullString{Valid: true, String: d.ServicePeerID()}
		rec.DelegatedDestDelegatorPeerID = sql.NullString{Valid: true, String: d.DelegatorPeerID()}
	default:
		return nil, fmt.Errorf("unknown record destination type: %T", d)
	}

	return rec, nil
}

func directionModelToDB(d record.Direction) queries.TransactionlogDirection {
	switch d {
	case record.DirectionIn:
		return queries.TransactionlogDirectionIn
	case record.DirectionOut:
		return queries.TransactionlogDirectionOut
	default:
		return queries.TransactionlogDirection("")
	}
}

func directionDBToModel(d queries.TransactionlogDirection) record.Direction {
	switch d {
	case queries.TransactionlogDirectionIn:
		return record.DirectionIn
	case queries.TransactionlogDirectionOut:
		return record.DirectionOut
	default:
		return record.DirectionUnspecified
	}
}

func (r *PostgreSQLRepository) ListRecords(ctx context.Context, req *record.ListRecordsRequest) ([]*record.Record, error) {
	dbRecords, err := r.queries.ListRecords(ctx, listRecordsReqToDB(req))
	if err != nil {
		return nil, err
	}

	records := make([]*record.Record, len(dbRecords))

	for i, r := range dbRecords {
		var src interface{}
		if r.SrcOutwayPeerID.Valid {
			src = &record.NewRecordSourceArgs{
				OutwayPeerID: r.SrcOutwayPeerID.String,
			}
		} else {
			src = &record.NewRecordDelegatedSourceArgs{
				OutwayPeerID:    r.DelegatedSrcOutwayPeerID.String,
				DelegatorPeerID: r.DelegatedSrcDelegatorPeerID.String,
			}
		}

		var dest interface{}
		if r.DestServicePeerID.Valid {
			dest = &record.NewRecordDestinationArgs{
				ServicePeerID: r.DestServicePeerID.String,
			}
		} else {
			dest = &record.NewRecordDelegatedDestinationArgs{
				ServicePeerID:   r.DelegatedDestServicePeerID.String,
				DelegatorPeerID: r.DelegatedDestDelegatorPeerID.String,
			}
		}

		records[i], err = record.NewRecord(&record.NewRecordArgs{
			ID:            uint64(r.ID),
			TransactionID: r.TransactionID.String(),
			GrantHash:     r.GrantHash,
			Direction:     directionDBToModel(r.Direction),
			ServiceName:   r.ServiceName,
			Source:        src,
			Destination:   dest,
			CreatedAt:     r.CreatedAt,
		})
		if err != nil {
			return nil, err
		}
	}

	return records, nil
}

func listRecordsReqToDB(req *record.ListRecordsRequest) *queries.ListRecordsParams {
	serviceNames := make([]string, 0)
	transactionIDs := make([]uuid.UUID, 0)
	grantHashes := make([]string, 0)
	peerIDs := make([]string, 0)

	var (
		periodStart sql.NullTime
		periodEnd   sql.NullTime
	)

	for _, f := range req.Filters {
		if f.ServiceName != "" {
			serviceNames = append(serviceNames, f.ServiceName)
		}

		if f.TransactionID != nil {
			transactionIDs = append(transactionIDs, f.TransactionID.UUID())
		}

		if f.GrantHash != "" {
			grantHashes = append(grantHashes, f.GrantHash)
		}

		if f.PeerID != "" {
			peerIDs = append(peerIDs, f.PeerID)
		}

		if !f.Period.Start.IsZero() {
			periodStart = sql.NullTime{
				Time:  f.Period.Start,
				Valid: true,
			}
		}

		if !f.Period.End.IsZero() {
			periodEnd = sql.NullTime{
				Time:  f.Period.End,
				Valid: true,
			}
		}
	}

	return &queries.ListRecordsParams{
		StartID:        req.Pagination.StartID,
		Limit:          int32(req.Pagination.Limit),
		OrderDirection: string(sortOrderToDB(req.Pagination.SortOrder)),
		ServiceNames:   serviceNames,
		TransactionIds: transactionIDs,
		GrantHashes:    grantHashes,
		PeerIds:        peerIDs,
		PeriodStart:    periodStart,
		PeriodEnd:      periodEnd,
	}
}

func sortOrderToDB(d record.SortOrder) sortOrder {
	switch d {
	case record.SortOrderAscending:
		return sortOrderAsc
	case record.SortOrderDescending:
		return sortOrderDesc
	default:
		return sortOrderAsc
	}
}
