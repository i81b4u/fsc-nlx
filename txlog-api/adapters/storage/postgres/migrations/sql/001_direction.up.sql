-- Copyright © VNG Realisatie 2022
-- Licensed under the EUPL

BEGIN transaction;

CREATE SCHEMA transactionlog;

CREATE TYPE transactionlog.direction AS ENUM (
    'in',
    'out'
);

CREATE SEQUENCE transactionlog.records_id_seq
    INCREMENT BY 1
    MINVALUE 1
    MAXVALUE 9223372036854775807
    START WITH 1
    CACHE 1
    NO CYCLE
    OWNED BY NONE;

CREATE TABLE transactionlog.records (
    id bigint NOT NULL DEFAULT nextval('transactionlog.records_id_seq'::regclass),
    transaction_id uuid NOT NULL, -- UUID v7
    grant_hash text NOT NULL,
    direction transactionlog.direction NOT NULL,
    service_name text NOT NULL,
    src_outway_peer_id VARCHAR(20),
    delegated_src_outway_peer_id VARCHAR(20),
    delegated_src_delegator_peer_id VARCHAR(20),
    dest_service_peer_id VARCHAR(20),
    delegated_dest_service_peer_id VARCHAR(20),
    delegated_dest_delegator_peer_id VARCHAR(20),
    created_at timestamp with time zone NOT NULL,
    CONSTRAINT records_pk PRIMARY KEY (id)
);

COMMIT;
