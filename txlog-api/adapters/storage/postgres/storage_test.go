// Copyright © VNG Realisatie 2021
// Licensed under the EUPL

//go:build integration

package postgresadapter_test

import (
	"context"
	"os"
	"sync"
	"testing"
	"time"

	"github.com/DATA-DOG/go-txdb"
	_ "github.com/golang-migrate/migrate/v4/source/file"
	"github.com/jmoiron/sqlx"
	"github.com/stretchr/testify/require"

	"go.nlx.io/nlx/common/strings"
	"go.nlx.io/nlx/testing/testingutils"
	postgresadapter "go.nlx.io/nlx/txlog-api/adapters/storage/postgres"
	"go.nlx.io/nlx/txlog-api/domain/record"
)

const dbName = "test_txlog"
const dbDriver = "txdb"

var setupOnce sync.Once

func new(t *testing.T, enableFixtures bool) (record.Repository, func() error) {
	setupOnce.Do(func() {
		setupDatabase(t)
	})

	db, err := sqlx.Open(dbDriver, t.Name())
	require.NoError(t, err)

	db.MapperFunc(strings.ToSnakeCase)

	repo, err := postgresadapter.New(db)
	require.NoError(t, err)

	if enableFixtures {
		loadFixtures(t, repo)
	}

	return repo, db.Close
}

func setupDatabase(t *testing.T) {
	dsnBase := os.Getenv("POSTGRES_DSN")
	dsn, err := testingutils.CreateTestDatabase(dsnBase, dbName)
	require.NoError(t, err)

	dsnForMigrations := testingutils.AddQueryParamToAddress(dsn, "x-migrations-table", dbName)
	err = postgresadapter.PerformMigrations(dsnForMigrations)
	require.NoError(t, err)

	txdb.Register(dbDriver, "postgres", dsn)

	// This is necessary because the default BindVars for txdb isn't correct
	sqlx.BindDriver(dbDriver, sqlx.DOLLAR)

}

func loadFixtures(t *testing.T, repo record.Repository) error {
	newRecordsArgs := []*record.NewRecordArgs{
		{
			TransactionID: "01899c62-eba5-7b58-b68d-000000000001",
			GrantHash:     "$1$4$testhash",
			Direction:     record.DirectionIn,
			ServiceName:   "test-service",
			Source: &record.NewRecordSourceArgs{
				OutwayPeerID: "1",
			},
			Destination: &record.NewRecordDelegatedDestinationArgs{
				ServicePeerID:   "3",
				DelegatorPeerID: "4",
			},
			CreatedAt: time.Date(2021, 1, 2, 1, 2, 3, 0, time.UTC),
		},
		{
			TransactionID: "01899c62-eba5-7b58-b68d-000000000002",
			GrantHash:     "$1$4$testhash",
			Direction:     record.DirectionIn,
			ServiceName:   "test-service",
			Source: &record.NewRecordDelegatedSourceArgs{
				OutwayPeerID:    "1",
				DelegatorPeerID: "2",
			},
			Destination: &record.NewRecordDestinationArgs{
				ServicePeerID: "3",
			},
			CreatedAt: time.Date(2021, 1, 2, 1, 2, 3, 0, time.UTC),
		},
	}

	recs := make([]*record.Record, len(newRecordsArgs))
	for i, args := range newRecordsArgs {
		var err error
		recs[i], err = record.NewRecord(args)
		require.NoError(t, err)
	}

	err := repo.CreateRecords(context.Background(), recs)
	require.NoError(t, err)

	return nil
}
