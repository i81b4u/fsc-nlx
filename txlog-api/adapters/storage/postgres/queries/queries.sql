-- Copyright © VNG Realisatie 2022
-- Licensed under the EUPL

-- name: ListRecords :many
SELECT r.id,
       r.transaction_id,
    r.grant_hash,
    r.direction,
    r.service_name,
    r.src_outway_peer_id,
    r.delegated_src_outway_peer_id,
    r.delegated_src_delegator_peer_id,
    r.dest_service_peer_id,
    r.delegated_dest_service_peer_id,
    r.delegated_dest_delegator_peer_id,
    r.created_at
FROM transactionlog.records as r
WHERE
    (cardinality(@service_names::text[]) = 0 OR r.service_name = ANY(@service_names::text[]))
    AND (cardinality(@transaction_ids::uuid[]) = 0 OR r.transaction_id = ANY(@transaction_ids::uuid[]))
    AND (cardinality(@grant_hashes::text[]) = 0 OR r.grant_hash = ANY(@grant_hashes::text[]))
    AND (cardinality(@peer_ids::text[]) = 0
        OR r.src_outway_peer_id = ANY(@peer_ids::text[])
        OR r.delegated_src_outway_peer_id = ANY(@peer_ids::text[])
        OR r.delegated_src_delegator_peer_id = ANY(@peer_ids::text[])
        OR r.dest_service_peer_id = ANY(@peer_ids::text[])
        OR r.delegated_dest_service_peer_id = ANY(@peer_ids::text[])
        OR r.delegated_dest_delegator_peer_id = ANY(@peer_ids::text[])
    )
    AND (sqlc.narg('period_start')::timestamp IS NULL OR r.created_at >= sqlc.narg('period_start')::timestamp)
    AND (sqlc.narg('period_end')::timestamp IS NULL OR r.created_at <= sqlc.narg('period_end')::timestamp)
    AND (@start_id = 0
        OR (@order_direction::text = 'asc' AND r.id > @start_id)
        OR (@order_direction::text = 'desc' AND r.id < @start_id)
    )
ORDER BY
    CASE
        WHEN @order_direction::text = 'asc' THEN r.id END ASC,
    CASE
        WHEN @order_direction::text = 'desc' THEN r.id END DESC
LIMIT $1;

-- name: CreateRecord :exec
INSERT INTO transactionlog.records (
    transaction_id,
    grant_hash,
    direction,
    service_name,
    src_outway_peer_id,
    delegated_src_outway_peer_id,
    delegated_src_delegator_peer_id,
    dest_service_peer_id,
    delegated_dest_service_peer_id,
    delegated_dest_delegator_peer_id,
    created_at
)
VALUES ($1, $2, $3, $4, $5, $6, $7, $8, $9, $10, $11);
