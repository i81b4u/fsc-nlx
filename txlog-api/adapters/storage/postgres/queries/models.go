// Code generated by sqlc. DO NOT EDIT.
// versions:
//   sqlc v1.19.1

package queries

import (
	"database/sql"
	"database/sql/driver"
	"fmt"
	"time"

	"github.com/gofrs/uuid"
)

type TransactionlogDirection string

const (
	TransactionlogDirectionIn  TransactionlogDirection = "in"
	TransactionlogDirectionOut TransactionlogDirection = "out"
)

func (e *TransactionlogDirection) Scan(src interface{}) error {
	switch s := src.(type) {
	case []byte:
		*e = TransactionlogDirection(s)
	case string:
		*e = TransactionlogDirection(s)
	default:
		return fmt.Errorf("unsupported scan type for TransactionlogDirection: %T", src)
	}
	return nil
}

type NullTransactionlogDirection struct {
	TransactionlogDirection TransactionlogDirection
	Valid                   bool // Valid is true if TransactionlogDirection is not NULL
}

// Scan implements the Scanner interface.
func (ns *NullTransactionlogDirection) Scan(value interface{}) error {
	if value == nil {
		ns.TransactionlogDirection, ns.Valid = "", false
		return nil
	}
	ns.Valid = true
	return ns.TransactionlogDirection.Scan(value)
}

// Value implements the driver Valuer interface.
func (ns NullTransactionlogDirection) Value() (driver.Value, error) {
	if !ns.Valid {
		return nil, nil
	}
	return string(ns.TransactionlogDirection), nil
}

type TransactionlogRecord struct {
	ID                           int64
	TransactionID                uuid.UUID
	GrantHash                    string
	Direction                    TransactionlogDirection
	ServiceName                  string
	SrcOutwayPeerID              sql.NullString
	DelegatedSrcOutwayPeerID     sql.NullString
	DelegatedSrcDelegatorPeerID  sql.NullString
	DestServicePeerID            sql.NullString
	DelegatedDestServicePeerID   sql.NullString
	DelegatedDestDelegatorPeerID sql.NullString
	CreatedAt                    time.Time
}
