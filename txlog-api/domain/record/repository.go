// Copyright © VNG Realisatie 2021
// Licensed under the EUPL

package record

import (
	"context"
	"time"
)

type Repository interface {
	CreateRecords(context.Context, []*Record) error
	ListRecords(ctx context.Context, req *ListRecordsRequest) ([]*Record, error)

	Shutdown() error
}

type SortOrder uint32

const (
	SortOrderUnspecified SortOrder = iota
	SortOrderAscending
	SortOrderDescending
)

type Pagination struct {
	StartID   uint64
	Limit     uint32
	SortOrder SortOrder
}

type Period struct {
	Start time.Time
	End   time.Time
}

type Filter struct {
	Period        *Period
	TransactionID *TransactionID
	ServiceName   string
	GrantHash     string
	PeerID        string
}

type ListRecordsRequest struct {
	Pagination *Pagination
	Filters    []*Filter
}
