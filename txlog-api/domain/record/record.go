// Copyright © VNG Realisatie 2021
// Licensed under the EUPL

package record

import (
	"fmt"
	"time"

	"github.com/gofrs/uuid"
	"github.com/pkg/errors"

	"go.nlx.io/nlx/manager/domain/contract"
)

type TransactionID uuid.UUID

type Record struct {
	id            uint64
	transactionID *TransactionID
	grantHash     string
	direction     Direction
	serviceName   string
	source        interface{}
	destination   interface{}
	createdAt     time.Time
}

type Source struct {
	outwayPeerID string
}

type DelegatedSource struct {
	outwayPeerID    string
	delegatorPeerID string
}

type Destination struct {
	servicePeerID string
}

type DelegatedDestination struct {
	servicePeerID   string
	delegatorPeerID string
}

type Direction int32

const (
	DirectionUnspecified Direction = iota
	DirectionIn
	DirectionOut
)

func NewRecord(args *NewRecordArgs) (*Record, error) {
	if args.Direction == DirectionUnspecified {
		return nil, errors.New("Direction: cannot be unspecified")
	}

	if args.Direction != DirectionIn && args.Direction != DirectionOut {
		return nil, errors.New("Direction: must be IN or OUT")
	}

	if args.ServiceName == "" {
		return nil, errors.New("ServiceName: cannot be blank")
	}

	if args.CreatedAt.IsZero() {
		return nil, errors.New("CreatedAt: cannot be blank")
	}

	if args.CreatedAt.After(time.Now()) {
		return nil, errors.New("CreatedAt: cannot be in the future")
	}

	transactionID, err := NewTransactionIDFromString(args.TransactionID)
	if err != nil {
		return nil, errors.Wrap(err, "invalid transaction id in args")
	}

	src, err := newSource(args.Source)
	if err != nil {
		return nil, errors.Wrap(err, "invalid source in new record args")
	}

	dest, err := newDestination(args.Destination)
	if err != nil {
		return nil, errors.Wrap(err, "invalid destination in new record args")
	}

	hash, err := contract.DecodeHashFromString(args.GrantHash)
	if err != nil {
		return nil, errors.Wrap(err, "could not decode grant hash")
	}

	t := hash.Type()
	if t != contract.HashTypeGrantServiceConnection && t != contract.HashTypeGrantDelegatedServiceConnection {
		return nil, fmt.Errorf("invalid grant hash type, only service connection or delegated service connection grant hash types are allowed")
	}

	return &Record{
		id:            args.ID,
		transactionID: transactionID,
		grantHash:     hash.String(),
		direction:     args.Direction,
		serviceName:   args.ServiceName,
		source:        src,
		destination:   dest,
		createdAt:     args.CreatedAt,
	}, nil
}

func (t *TransactionID) String() string {
	return uuid.UUID(*t).String()
}

func (t *TransactionID) Bytes() []byte {
	return uuid.UUID(*t).Bytes()
}

func (t *TransactionID) UUID() uuid.UUID {
	return uuid.UUID(*t)
}

func (r *Record) ID() uint64 {
	return r.id
}

func (r *Record) TransactionID() *TransactionID {
	return r.transactionID
}

func (r *Record) GrantHash() string {
	return r.grantHash
}

func (r *Record) Direction() Direction {
	return r.direction
}

func (r *Record) ServiceName() string {
	return r.serviceName
}

func (r *Record) Source() interface{} {
	return r.source
}

func (s *Source) OutwayPeerID() string {
	return s.outwayPeerID
}

func (s *DelegatedSource) OutwayPeerID() string {
	return s.outwayPeerID
}

func (s *DelegatedSource) DelegatorPeerID() string {
	return s.delegatorPeerID
}

func (r *Record) Destination() interface{} {
	return r.destination
}

func (d *Destination) ServicePeerID() string {
	return d.servicePeerID
}

func (d *DelegatedDestination) ServicePeerID() string {
	return d.servicePeerID
}

func (d *DelegatedDestination) DelegatorPeerID() string {
	return d.delegatorPeerID
}

func (r *Record) CreatedAt() time.Time {
	return r.createdAt
}

func newSource(src interface{}) (interface{}, error) {
	switch s := src.(type) {
	case *NewRecordSourceArgs:
		return newRecordSource(s)
	case *NewRecordDelegatedSourceArgs:
		return newRecordDelegatedSource(s)
	default:
		return nil, fmt.Errorf("invalid source type: %T", s)
	}
}

func newRecordSource(src *NewRecordSourceArgs) (*Source, error) {
	if src.OutwayPeerID == "" {
		return nil, fmt.Errorf("outway peer ID cannot be empty")
	}

	return &Source{
		outwayPeerID: src.OutwayPeerID,
	}, nil
}

func newRecordDelegatedSource(src *NewRecordDelegatedSourceArgs) (*DelegatedSource, error) {
	if src.OutwayPeerID == "" {
		return nil, fmt.Errorf("outway peer ID cannot be empty")
	}

	if src.DelegatorPeerID == "" {
		return nil, fmt.Errorf("delegator peer ID cannot be empty")
	}

	return &DelegatedSource{
		outwayPeerID:    src.OutwayPeerID,
		delegatorPeerID: src.DelegatorPeerID,
	}, nil
}

func newDestination(dest interface{}) (interface{}, error) {
	switch d := dest.(type) {
	case *NewRecordDestinationArgs:
		return newRecordDestination(d)
	case *NewRecordDelegatedDestinationArgs:
		return newRecordDelegatedDestination(d)
	default:
		return nil, fmt.Errorf("invalid destination type: %T", d)
	}
}

func newRecordDestination(src *NewRecordDestinationArgs) (*Destination, error) {
	if src.ServicePeerID == "" {
		return nil, fmt.Errorf("service peer ID cannot be empty")
	}

	return &Destination{
		servicePeerID: src.ServicePeerID,
	}, nil
}

func newRecordDelegatedDestination(src *NewRecordDelegatedDestinationArgs) (*DelegatedDestination, error) {
	if src.ServicePeerID == "" {
		return nil, fmt.Errorf("service peer ID cannot be empty")
	}

	if src.DelegatorPeerID == "" {
		return nil, fmt.Errorf("delegator peer ID cannot be empty")
	}

	return &DelegatedDestination{
		servicePeerID:   src.ServicePeerID,
		delegatorPeerID: src.DelegatorPeerID,
	}, nil
}

func NewTransactionIDFromString(t string) (*TransactionID, error) {
	u, err := uuid.FromString(t)
	if err != nil {
		return nil, errors.Wrap(err, "could not create new transaction ID from string")
	}

	if u.IsNil() {
		return nil, fmt.Errorf("invalid uuid, must not be nil UUID")
	}

	if u.Version() != uuid.V7 {
		return nil, fmt.Errorf("invalid uuid version, must be v7, got v%d", u.Version())
	}

	l := TransactionID(u)

	return &l, nil
}
