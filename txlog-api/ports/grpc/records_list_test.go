// Copyright © VNG Realisatie 2022
// Licensed under the EUPL

package grpc_test

import (
	"context"
	"errors"
	"testing"
	"time"

	"github.com/gofrs/uuid"
	"github.com/stretchr/testify/assert"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"

	"go.nlx.io/nlx/txlog-api/api"
	"go.nlx.io/nlx/txlog-api/domain/record"
	txlog_mock "go.nlx.io/nlx/txlog-api/domain/record/mock"
)

func TestListRecords(t *testing.T) {
	now := time.Now()

	id := uuid.Must(uuid.FromString("01899c62-eba5-7b58-b68d-000000000001"))

	tests := map[string]struct {
		setup   func(context.Context, *txlog_mock.MockRepository)
		want    *api.ListRecordsResponse
		wantErr error
	}{
		"database_error": {
			setup: func(ctx context.Context, mocks *txlog_mock.MockRepository) {
				mocks.
					EXPECT().
					ListRecords(ctx, &record.ListRecordsRequest{
						Pagination: &record.Pagination{
							Limit:     100,
							SortOrder: record.SortOrderDescending,
						},
						Filters: []*record.Filter{},
					}).
					Return(nil, errors.New("arbitrary error"))
			},
			want:    nil,
			wantErr: status.Error(codes.Internal, "storage error"),
		},
		"happy_flow": {
			setup: func(ctx context.Context, mocks *txlog_mock.MockRepository) {
				records := mustNewRecords(t, []*record.NewRecordArgs{
					{
						TransactionID: id.String(),
						GrantHash:     "$1$4$testhash",
						Direction:     record.DirectionIn,
						ServiceName:   "test-service",
						Source: &record.NewRecordSourceArgs{
							OutwayPeerID: "1",
						},
						Destination: &record.NewRecordDelegatedDestinationArgs{
							ServicePeerID:   "3",
							DelegatorPeerID: "4",
						},
						CreatedAt: now,
					},
				})

				mocks.
					EXPECT().
					ListRecords(ctx, &record.ListRecordsRequest{
						Pagination: &record.Pagination{
							Limit:     100,
							SortOrder: record.SortOrderDescending,
						},
						Filters: []*record.Filter{},
					}).Return(records, nil)
			},
			want: &api.ListRecordsResponse{
				Records: []*api.TransactionLogRecord{
					{
						TransactionId: id.String(),
						Direction:     api.TransactionLogRecord_DIRECTION_IN,
						GrantHash:     "$1$4$testhash",
						ServiceName:   "test-service",
						Source: &api.TransactionLogRecordSource{
							Data: &api.TransactionLogRecordSource_Source_{
								Source: &api.TransactionLogRecordSource_Source{
									OutwayPeerId: "1",
								},
							},
						},
						Destination: &api.TransactionLogRecordDestination{
							Data: &api.TransactionLogRecordDestination_DelegatedDestination_{
								DelegatedDestination: &api.TransactionLogRecordDestination_DelegatedDestination{
									ServicePeerId:   "3",
									DelegatorPeerId: "4",
								},
							},
						},
						CreatedAt: now.Unix(),
					},
				},
			},
			wantErr: nil,
		},
	}

	for name, tt := range tests {
		tt := tt

		t.Run(name, func(t *testing.T) {
			service, mocks := newService(t)

			ctx := context.Background()

			tt.setup(ctx, mocks)

			got, err := service.ListRecords(ctx, &api.ListRecordsRequest{
				Pagination: &api.Pagination{
					Limit: 100,
					Order: api.SortOrder_SORT_ORDER_DESCENDING,
				},
			})

			assert.Equal(t, tt.wantErr, err)
			assert.Equal(t, tt.want, got)
		})
	}
}

func mustNewRecords(t *testing.T, args []*record.NewRecordArgs) []*record.Record {
	records := make([]*record.Record, len(args))

	for i, r := range args {
		var err error
		records[i], err = record.NewRecord(r)
		assert.NoError(t, err)
	}

	return records
}
