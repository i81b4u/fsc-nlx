// Copyright © VNG Realisatie 2022
// Licensed under the EUPL

package grpc

import (
	"context"
	"fmt"

	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"

	"go.nlx.io/nlx/txlog-api/api"
	"go.nlx.io/nlx/txlog-api/app/query"
	"go.nlx.io/nlx/txlog-api/domain/record"
)

func (s *Server) ListRecords(ctx context.Context, req *api.ListRecordsRequest) (*api.ListRecordsResponse, error) {
	s.logger.Info("rpc request ListRecords")

	sortOrder := query.SortOrderAscending

	if req.Pagination != nil && req.Pagination.Order == api.SortOrder_SORT_ORDER_DESCENDING {
		sortOrder = query.SortOrderDescending
	}

	records, err := s.app.Queries.ListRecords.Handle(ctx, &query.ListTransactionLogRecordsHandlerArgs{
		Pagination: &query.Pagination{
			Limit:     req.Pagination.Limit,
			StartID:   req.Pagination.StartId,
			SortOrder: sortOrder,
		},
	})
	if err != nil {
		s.logger.Error("error getting record list from storage", err)
		return nil, status.Error(codes.Internal, "storage error")
	}

	response, err := dataModelToResponse(records)
	if err != nil {
		s.logger.Error("unable to convert query model to api model", err)
		return nil, status.Error(codes.Internal, "internal")
	}

	return response, nil
}

func dataModelToResponse(records []*query.Record) (*api.ListRecordsResponse, error) {
	response := &api.ListRecordsResponse{}
	response.Records = make([]*api.TransactionLogRecord, len(records))

	for i, r := range records {
		recordResponse := &api.TransactionLogRecord{
			Id:            r.ID,
			TransactionId: r.TransactionID,
			Direction:     directionToProto(r.Direction),
			GrantHash:     r.GrantHash,
			ServiceName:   r.ServiceName,
			Source:        &api.TransactionLogRecordSource{},
			Destination:   &api.TransactionLogRecordDestination{},
			CreatedAt:     r.CreatedAt.Unix(),
		}

		switch s := r.Source.(type) {
		case *query.RecordSource:
			recordResponse.Source.Data = &api.TransactionLogRecordSource_Source_{
				Source: &api.TransactionLogRecordSource_Source{
					OutwayPeerId: s.OutwayPeerID,
				},
			}
		case *query.RecordDelegatedSource:
			recordResponse.Source.Data = &api.TransactionLogRecordSource_DelegatedSource_{
				DelegatedSource: &api.TransactionLogRecordSource_DelegatedSource{
					OutwayPeerId:    s.OutwayPeerID,
					DelegatorPeerId: s.DelegatorPeerID,
				},
			}
		default:
			return nil, fmt.Errorf("unknown source type: %T", s)
		}

		switch d := r.Destination.(type) {
		case *query.RecordDestination:
			recordResponse.Destination.Data = &api.TransactionLogRecordDestination_Destination_{
				Destination: &api.TransactionLogRecordDestination_Destination{
					ServicePeerId: d.ServicePeerID,
				},
			}
		case *query.RecordDelegatedDestination:
			recordResponse.Destination.Data = &api.TransactionLogRecordDestination_DelegatedDestination_{
				DelegatedDestination: &api.TransactionLogRecordDestination_DelegatedDestination{
					ServicePeerId:   d.ServicePeerID,
					DelegatorPeerId: d.DelegatorPeerID,
				},
			}
		default:
			return nil, fmt.Errorf("unknown destination type: %T", d)
		}

		response.Records[i] = recordResponse
	}

	return response, nil
}

func directionToProto(d record.Direction) api.TransactionLogRecord_Direction {
	switch d {
	case record.DirectionIn:
		return api.TransactionLogRecord_DIRECTION_IN
	case record.DirectionOut:
		return api.TransactionLogRecord_DIRECTION_OUT
	default:
		return api.TransactionLogRecord_DIRECTION_UNSPECIFIED
	}
}
