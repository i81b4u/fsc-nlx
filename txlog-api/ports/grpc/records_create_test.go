// Copyright © VNG Realisatie 2022
// Licensed under the EUPL

package grpc_test

import (
	"context"
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"

	"go.nlx.io/nlx/txlog-api/api"
	"go.nlx.io/nlx/txlog-api/domain/record"
	txlog_mock "go.nlx.io/nlx/txlog-api/domain/record/mock"
)

//nolint:funlen // this is a test
func TestCreateRecord(t *testing.T) {
	tests := map[string]struct {
		setup   func(context.Context, *txlog_mock.MockRepository)
		req     *api.CreateRecordsRequest
		want    *api.CreateRecordsResponse
		wantErr error
	}{
		"happy_flow": {
			setup: func(ctx context.Context, mocks *txlog_mock.MockRepository) {
				model, err := record.NewRecord(&record.NewRecordArgs{
					TransactionID: "01899c62-eba5-7b58-b68d-000000000001",
					GrantHash:     "$1$4$testhash",
					Direction:     record.DirectionIn,
					ServiceName:   "test-service",
					Source: &record.NewRecordDelegatedSourceArgs{
						OutwayPeerID:    "1",
						DelegatorPeerID: "2",
					},
					Destination: &record.NewRecordDelegatedDestinationArgs{
						ServicePeerID:   "3",
						DelegatorPeerID: "4",
					},
					CreatedAt: fixedTestClockTime,
				})
				require.NoError(t, err)

				mocks.
					EXPECT().
					CreateRecords(ctx, []*record.Record{model}).
					Return(nil)
			},
			req: &api.CreateRecordsRequest{
				Records: []*api.TransactionLogRecord{
					{
						TransactionId: "01899c62-eba5-7b58-b68d-000000000001",
						GrantHash:     "$1$4$testhash",
						Direction:     api.TransactionLogRecord_DIRECTION_IN,
						ServiceName:   "test-service",
						Source: &api.TransactionLogRecordSource{
							Data: &api.TransactionLogRecordSource_DelegatedSource_{
								DelegatedSource: &api.TransactionLogRecordSource_DelegatedSource{
									OutwayPeerId:    "1",
									DelegatorPeerId: "2",
								},
							},
						},
						Destination: &api.TransactionLogRecordDestination{
							Data: &api.TransactionLogRecordDestination_DelegatedDestination_{
								DelegatedDestination: &api.TransactionLogRecordDestination_DelegatedDestination{
									ServicePeerId:   "3",
									DelegatorPeerId: "4",
								},
							},
						},
						CreatedAt: fixedTestClockTime.Unix(),
					},
				},
			},
			want:    &api.CreateRecordsResponse{},
			wantErr: nil,
		},
	}

	for name, tt := range tests {
		tt := tt

		t.Run(name, func(t *testing.T) {
			ctx := context.Background()
			service, mocks := newService(t)
			tt.setup(ctx, mocks)

			got, err := service.CreateRecords(ctx, tt.req)

			if err != nil {
				t.Log(err)
			}
			assert.Equal(t, tt.wantErr, err)
			assert.Equal(t, tt.want, got)
		})
	}
}
