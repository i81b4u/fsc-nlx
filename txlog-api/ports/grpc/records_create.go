// Copyright © VNG Realisatie 2022
// Licensed under the EUPL

package grpc

import (
	"context"
	"time"

	"go.nlx.io/nlx/txlog-api/api"
	"go.nlx.io/nlx/txlog-api/app/command"
	"go.nlx.io/nlx/txlog-api/domain/record"
)

func (s *Server) CreateRecords(ctx context.Context, req *api.CreateRecordsRequest) (*api.CreateRecordsResponse, error) {
	s.logger.Info("rpc request CreateRecord")

	records := make([]*command.NewRecordArgs, len(req.Records))

	for i, r := range req.Records {
		var src interface{}
		switch s := r.Source.Data.(type) {
		case *api.TransactionLogRecordSource_Source_:
			src = &command.NewRecordSourceArgs{
				OutwayPeerID: s.Source.OutwayPeerId,
			}
		case *api.TransactionLogRecordSource_DelegatedSource_:
			src = &command.NewRecordDelegatedSourceArgs{
				OutwayPeerID:    s.DelegatedSource.OutwayPeerId,
				DelegatorPeerID: s.DelegatedSource.DelegatorPeerId,
			}
		}

		var dest interface{}
		switch s := r.Destination.Data.(type) {
		case *api.TransactionLogRecordDestination_Destination_:
			dest = &command.NewRecordDestinationArgs{
				ServicePeerID: s.Destination.ServicePeerId,
			}
		case *api.TransactionLogRecordDestination_DelegatedDestination_:
			dest = &command.NewRecordDelegatedDestinationArgs{
				ServicePeerID:   s.DelegatedDestination.ServicePeerId,
				DelegatorPeerID: s.DelegatedDestination.DelegatorPeerId,
			}
		}

		records[i] = &command.NewRecordArgs{
			TransactionID: r.TransactionId,
			GrantHash:     r.GrantHash,
			Direction:     directionProtoToModel(r.Direction),
			ServiceName:   r.ServiceName,
			Source:        src,
			Destination:   dest,
			CreatedAt:     time.Unix(int64(r.CreatedAt), 0),
		}
	}

	err := s.app.Commands.CreateRecords.Handle(ctx, records)
	if err != nil {
		return nil, ResponseFromError(err)
	}

	return &api.CreateRecordsResponse{}, nil
}

func directionProtoToModel(d api.TransactionLogRecord_Direction) record.Direction {
	switch d {
	case api.TransactionLogRecord_DIRECTION_IN:
		return record.DirectionIn
	case api.TransactionLogRecord_DIRECTION_OUT:
		return record.DirectionOut
	default:
		return record.DirectionUnspecified
	}
}
