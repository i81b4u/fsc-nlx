// Copyright © VNG Realisatie 2018
// Licensed under the EUPL

package main

import (
	"context"
	"log"
	"net/url"
	"strings"
	"time"

	"github.com/jessevdk/go-flags"
	_ "github.com/lib/pq"
	"go.uber.org/zap"

	"go.nlx.io/nlx/common/clock"
	"go.nlx.io/nlx/common/cmd"
	"go.nlx.io/nlx/common/logoptions"
	"go.nlx.io/nlx/common/process"
	common_tls "go.nlx.io/nlx/common/tls"
	"go.nlx.io/nlx/common/transactionlog"
	"go.nlx.io/nlx/common/version"
	"go.nlx.io/nlx/inway"
	grpcconfig "go.nlx.io/nlx/inway/adapters/config/grpc"
	grpccontroller "go.nlx.io/nlx/inway/adapters/controller/grpc"
	"go.nlx.io/nlx/inway/domain/config"
	"go.nlx.io/nlx/inway/pkg/controller"
	"go.nlx.io/nlx/inway/pkg/manager"
)

var options struct {
	ListenAddress               string `long:"listen-address" env:"LISTEN_ADDRESS" default:"127.0.0.1:8443" description:"Address for the inway to listen on."`
	Address                     string `long:"self-address" env:"SELF_ADDRESS" description:"The address that outways can use to reach me" required:"true"`
	ControllerAPIAddress        string `long:"controller-api-address" env:"CONTROLLER_API_ADDRESS" description:"The Controller API address." required:"true"`
	MonitoringAddress           string `long:"monitoring-address" env:"MONITORING_ADDRESS" default:"127.0.0.1:8081" description:"Address for the inway monitoring endpoints to listen on. Read https://golang.org/pkg/net/#Dial for possible tcp address specs."`
	TxLogAPIAddress             string `long:"tx-log-api-address" env:"TX_LOG_API_ADDRESS" description:"The address of the transaction log API" required:"true"`
	Name                        string `long:"name" env:"INWAY_NAME" description:"Name of the inway. Every inway should have a unique name within the organization." required:"true"`
	AuthorizationServiceAddress string `long:"authorization-service-address" env:"AUTHORIZATION_SERVICE_ADDRESS" description:"Address of the authorization service. If set calls will go through the authorization service before being send to the service"`
	AuthorizationCA             string `long:"authorization-root-ca" env:"AUTHORIZATION_ROOT_CA" description:"absolute path to root CA used to verify auth service certificate"`
	ManagerAddress              string `long:"manager-internal-address" env:"MANAGER_INTERNAL_ADDRESS" description:"Internal Manager address to communicate with for dealing with contracts." required:"true"`
	ConfigExpiresAfter          string `long:"config-expires-after" env:"CONFIG_EXPIRES_AFTER" default:"5s" description:"Time after which the config is invalided. See https://pkg.go.dev/time#ParseDuration" required:"false"`

	logoptions.LogOptions
	cmd.TLSGroupOptions
	cmd.TLSOptions
}

func main() {
	parseOptions()

	p := process.NewProcess()

	logger := setupLogger()

	if errValidate := common_tls.VerifyPrivateKeyPermissions(options.GroupKeyFile); errValidate != nil {
		logger.Warn("invalid organization key permissions", zap.Error(errValidate), zap.String("file-path", options.GroupKeyFile))
	}

	orgCert, err := common_tls.NewBundleFromFiles(options.GroupCertFile, options.GroupKeyFile, options.GroupRootCert)
	if err != nil {
		logger.Fatal("loading TLS files", zap.Error(err))
	}

	if errValidate := common_tls.VerifyPrivateKeyPermissions(options.KeyFile); errValidate != nil {
		logger.Warn("invalid internal PKI key permissions", zap.Error(errValidate), zap.String("file-path", options.KeyFile))
	}

	cert, err := common_tls.NewBundleFromFiles(options.CertFile, options.KeyFile, options.RootCertFile)
	if err != nil {
		logger.Fatal("loading TLS files", zap.Error(err))
	}

	txLogger, err := transactionlog.NewAPITransactionLogger(&transactionlog.NewAPITransactionLoggerArgs{
		Logger:       logger,
		APIAddress:   options.TxLogAPIAddress,
		InternalCert: cert,
	})
	if err != nil {
		logger.Fatal("unable to setup the transaction logger", zap.Error(err))
	}

	configExpiresAfter, err := time.ParseDuration(options.ConfigExpiresAfter)
	if err != nil {
		logger.Fatal("invalid config expiration duration", zap.Error(err))
	}

	managerClient, err := manager.NewClient(context.Background(), options.ManagerAddress, cert)
	if err != nil {
		logger.Fatal("unable to setup manager client")
	}

	controllerClient, err := controller.NewClient(context.Background(), options.ControllerAPIAddress, cert)
	if err != nil {
		logger.Fatal("could not create controller client", zap.Error(err))
	}

	mgr, err := grpccontroller.New(controllerClient)
	if err != nil {
		logger.Fatal("could not create grpc controller", zap.Error(err))
	}

	grpcConfigRepository, err := grpcconfig.New(&grpcconfig.NewGRPCConfigArgs{
		SelfAddress:      options.Address,
		ManagerClient:    managerClient,
		ControllerClient: controllerClient,
		TrustedRootCert:  orgCert.RootCAs(),
	})
	if err != nil {
		logger.Fatal("failed to setup grpc config repository", zap.Error(err))
	}

	if !strings.HasPrefix(options.Address, "https://") {
		logger.Fatal("invalid self-address: scheme is missing", zap.String("self-address", options.Address))
	}

	selfAddress, err := url.Parse(options.Address)
	if err != nil {
		logger.Fatal("failed to parse self address as a URL", zap.Error(err))
	}

	iw, err := inway.NewInway(&inway.Params{
		Clock:                        clock.New(),
		Context:                      context.Background(),
		Logger:                       logger,
		Txlogger:                     txLogger,
		Controller:                   mgr,
		Name:                         options.Name,
		Address:                      selfAddress,
		MonitoringAddress:            options.MonitoringAddress,
		OrgCertBundle:                orgCert,
		AuthServiceURL:               options.AuthorizationServiceAddress,
		AuthCAPath:                   options.AuthorizationCA,
		ConfigRepository:             grpcConfigRepository,
		ServiceEndpointCacheDuration: configExpiresAfter,
	})
	if err != nil {
		logger.Fatal("cannot setup inway", zap.Error(err))
	}

	go func() {
		err = iw.Run(context.Background(), options.ListenAddress)
		if err != nil {
			logger.Fatal("failed to run server", zap.Error(err))
		}
	}()

	p.Wait()

	shutdown(logger, iw, txLogger, grpcConfigRepository)
}

func shutdown(logger *zap.Logger, iw *inway.Inway, transactionLogger transactionlog.TransactionLogger, configRepository config.Repository) {
	logger.Info("starting graceful shutdown")

	gracefulCtx, cancel := context.WithTimeout(context.Background(), time.Minute)
	defer cancel()

	err := iw.Shutdown(gracefulCtx)
	if err != nil {
		logger.Error("failed to shutdown", zap.Error(err))
	}

	err = transactionLogger.Close()
	if err != nil {
		logger.Error("failed to close transactionLogger", zap.Error(err))
	}

	err = configRepository.Close()
	if err != nil {
		logger.Error("failed to close config repository", zap.Error(err))
	}
}

func setupLogger() *zap.Logger {
	zapConfig := options.LogOptions.ZapConfig()

	logger, err := zapConfig.Build()
	if err != nil {
		log.Fatalf("failed to create new zap logger: %v", err)
	}

	logger.Info("version info", zap.String("version", version.BuildVersion), zap.String("source-hash", version.BuildSourceHash))
	logger = logger.With(zap.String("version", version.BuildVersion))

	logger.Info("starting inway")

	return logger
}

func parseOptions() {
	args, err := flags.Parse(&options)
	if err != nil {
		if et, ok := err.(*flags.Error); ok {
			if et.Type == flags.ErrHelp {
				return
			}
		}

		log.Fatalf("error parsing flags: %v", err)
	}

	if len(args) > 0 {
		log.Fatalf("unexpected arguments: %v", args)
	}
}
