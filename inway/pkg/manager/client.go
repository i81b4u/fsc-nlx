// Copyright © VNG Realisatie 2023
// Licensed under the EUPL

package manager

import (
	"context"
	"time"

	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials"

	"go.nlx.io/nlx/common/nlxversion"
	common_tls "go.nlx.io/nlx/common/tls"
	"go.nlx.io/nlx/common/version"
	managerapi "go.nlx.io/nlx/manager/ports/int/grpc/api"
)

const component = "inway"

var (
	userAgent = component + "/" + version.BuildVersion
)

type Client interface {
	managerapi.ManagerServiceClient
	Close() error
}

type client struct {
	managerapi.ManagerServiceClient
	conn             *grpc.ClientConn
	cancelTimeoutCtx context.CancelFunc
}

func NewClient(ctx context.Context, managerAddress string, cert *common_tls.CertificateBundle) (Client, error) {
	dialCredentials := credentials.NewTLS(cert.TLSConfig())
	dialOptions := []grpc.DialOption{
		grpc.WithTransportCredentials(dialCredentials),
		grpc.WithUserAgent(userAgent),
	}

	var grpcTimeout = 10 * time.Second

	timeoutCtx, cancel := context.WithTimeout(ctx, grpcTimeout)

	grpcCtx := nlxversion.NewGRPCContext(timeoutCtx, component)

	managerConn, err := grpc.DialContext(grpcCtx, managerAddress, dialOptions...)
	if err != nil {
		cancel()
		return nil, err
	}

	c := &client{
		ManagerServiceClient: managerapi.NewManagerServiceClient(managerConn),
		conn:                 managerConn,
		cancelTimeoutCtx:     cancel,
	}

	return c, nil
}

func (c *client) Close() error {
	c.cancelTimeoutCtx()

	return c.conn.Close()
}
