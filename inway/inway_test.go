// Copyright © VNG Realisatie 2018
// Licensed under the EUPL

package inway_test

import (
	"context"
	"net/url"
	"path/filepath"
	"testing"

	"github.com/stretchr/testify/assert"

	common_tls "go.nlx.io/nlx/common/tls"
	"go.nlx.io/nlx/inway"
	common_testing "go.nlx.io/nlx/testing/testingutils"
)

var pkiDir = filepath.Join("..", "testing", "pki")

func Test_NewInway(t *testing.T) {
	orgCertWithoutName, orgCert := getCertBundles()

	tests := map[string]struct {
		params               *inway.Params
		expectedErrorMessage string
	}{
		"missing_context": {
			params: &inway.Params{
				Context: nil,
				Address: func() *url.URL {
					u, err := url.Parse("https://inway.test")
					assert.NoError(t, err)

					return u
				}(),
				OrgCertBundle:     orgCert,
				MonitoringAddress: "localhost:8080",
				Name:              "my-inway",
			},
			expectedErrorMessage: "context is nil. needed to close gracefully",
		},
		"certificates_without_an_organization_name": {
			params: &inway.Params{
				Context: context.Background(),
				Address: func() *url.URL {
					u, err := url.Parse("https://inway.test")
					assert.NoError(t, err)

					return u
				}(),
				OrgCertBundle:     orgCertWithoutName,
				MonitoringAddress: "localhost:8080",
				Name:              "my-inway",
			},
			expectedErrorMessage: "cannot obtain organization name from self cert",
		},
		"self_address_not_in_certicate": {
			params: &inway.Params{
				Context: context.Background(),
				Address: func() *url.URL {
					u, err := url.Parse("https://test.com")
					assert.NoError(t, err)

					return u
				}(),
				OrgCertBundle:     orgCert,
				MonitoringAddress: "localhost:8080",
				Name:              "my-inway",
			},
			expectedErrorMessage: "'test.com' is not in the list of DNS names of the certificate, [localhost inway.test]",
		},
		"missing_name": {
			params: &inway.Params{
				Context: context.Background(),
				Address: func() *url.URL {
					u, err := url.Parse("https://inway.test")
					assert.NoError(t, err)

					return u
				}(),
				OrgCertBundle:     orgCert,
				MonitoringAddress: "localhost:8080",
				Name:              "",
			},
			expectedErrorMessage: "a valid name is required (alphanumeric & dashes, max. 100 characters)",
		},
		"invalid_name": {
			params: &inway.Params{
				Context: context.Background(),
				Address: func() *url.URL {
					u, err := url.Parse("https://inway.test")
					assert.NoError(t, err)

					return u
				}(),
				OrgCertBundle:     orgCert,
				MonitoringAddress: "localhost:8080",
				Name:              "#",
			},
			expectedErrorMessage: "a valid name is required (alphanumeric & dashes, max. 100 characters)",
		},
	}

	for name, tt := range tests {
		tt := tt

		t.Run(name, func(t *testing.T) {
			_, err := inway.NewInway(tt.params)
			assert.EqualError(t, err, tt.expectedErrorMessage)
		})
	}
}

func getCertBundles() (orgCertWithoutName, orgCert *common_tls.CertificateBundle) {
	orgCertWithoutName, _ = common_testing.GetCertificateBundle(pkiDir, common_testing.OrgWithoutName)

	orgCert, _ = common_testing.GetCertificateBundle(pkiDir, common_testing.OrgNLXTest)

	return orgCertWithoutName, orgCert
}
