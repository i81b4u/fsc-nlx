// Copyright © VNG Realisatie 2023
// Licensed under the EUPL

package config

import (
	"context"

	"github.com/pkg/errors"

	"go.nlx.io/nlx/manager/domain/contract"
)

func (c *Config) GetCertificate(ctx context.Context, thumbPrint contract.CertificateThumbprint) (*contract.PeerCertificate, error) {
	c.certificates.mut.RLock()
	cert, ok := c.certificates.certificates[thumbPrint]
	c.certificates.mut.RUnlock()

	if ok {
		return cert, nil
	}

	certificate, err, _ := c.certificates.getCertificate.Do(thumbPrint.Value(), func() (interface{}, error) {
		peerCert, errCerts := c.repo.GetCertificate(ctx, thumbPrint.Value())
		if errCerts != nil {
			return nil, errors.Wrap(errCerts, "unable to get certificates from repository")
		}

		c.certificates.mut.Lock()

		c.certificates.certificates[peerCert.CertificateThumbprint()] = peerCert

		c.certificates.mut.Unlock()

		return peerCert, nil
	})
	if err != nil {
		return nil, err
	}

	return certificate.(*contract.PeerCertificate), nil
}
