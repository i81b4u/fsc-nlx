// Copyright © VNG Realisatie 2023
// Licensed under the EUPL

package config

import (
	"context"
	"net/url"

	"go.nlx.io/nlx/manager/domain/contract"
)

type Repository interface {
	GetServiceEndpointURL(ctx context.Context, serviceName string) (*url.URL, error)
	GetCertificate(ctx context.Context, thumbprint string) (*contract.PeerCertificate, error)
	Close() error
}
