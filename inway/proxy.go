// Copyright © VNG Realisatie 2018
// Licensed under the EUPL

package inway

import (
	"fmt"
	"net/http"
	"net/http/httputil"

	"go.uber.org/zap"

	"go.nlx.io/nlx/common/accesstoken"
	"go.nlx.io/nlx/common/httperrors"
	inway_http "go.nlx.io/nlx/inway/http"
	"go.nlx.io/nlx/inway/plugins"
)

const FscAuthorizationHeader = "Fsc-Authorization"

func (i *Inway) handleProxyRequest(w http.ResponseWriter, r *http.Request) {
	ctx := r.Context()

	now := i.clock.Now()

	logger := i.logger.With(
		zap.String("request-path", r.URL.Path),
		zap.String("request-remote-address", r.RemoteAddr),
	)

	encodedToken := r.Header.Get(FscAuthorizationHeader)
	if encodedToken == "" {
		logger.Warn(fmt.Sprintf("received request without %s header", FscAuthorizationHeader))
		inway_http.WriteError(w, httperrors.O1, httperrors.MissingAuthHeader())

		return
	}

	certificateThumbprint, err := accesstoken.GetCertificateThumbprintFromToken(encodedToken)
	if err != nil {
		logger.Warn("could not get certificate thumbprint from token", zap.Error(err))
		inway_http.WriteError(w, httperrors.O1, httperrors.ErrorWhileAuthorizingRequest())

		return
	}

	cert, err := i.config.GetCertificate(ctx, *certificateThumbprint)
	if err != nil {
		logger.Warn("could not get certificate from config repository", zap.Error(err))
		inway_http.WriteError(w, httperrors.O1, httperrors.ErrorWhileAuthorizingRequest())

		return
	}

	token, err := accesstoken.DecodeFromString(i.clock, cert, encodedToken)
	if err != nil {
		logger.Warn("received request with invalid authorization token", zap.Error(err))
		inway_http.WriteError(w, httperrors.O1, httperrors.InvalidAuthorizationToken(err.Error()))

		return
	}

	endpointURL, err := i.config.GetEndpointURL(ctx, token.ServiceName)
	if err != nil {
		logger.Warn("could not get endpoint URL", zap.Error(err))
		// TODO: rename to httperrors.ErrorGettingEndpointURL
		inway_http.WriteError(w, httperrors.O1, httperrors.ServiceDoesNotExist(token.ServiceName))

		return
	}

	context := &plugins.Context{
		Logger:            logger,
		Response:          w,
		Request:           r,
		ConnectionInfo:    &plugins.ConnectionInfo{},
		Token:             token,
		RequestReceivedAt: now,
		EndpointURL:       endpointURL,
	}

	chain := plugins.BuildChain(func(context *plugins.Context) error {
		e := context.EndpointURL

		r.Host = e.Host
		proxy := &httputil.ReverseProxy{
			// Use custom director because the default Director appends a trailing slash when the request path is empty. The trailing slash made requests to a SOAP API fail.
			// See: https://github.com/golang/go/issues/50337
			Director: func(req *http.Request) {
				req.URL.Scheme = e.Scheme
				req.URL.Host = e.Host
				req.URL.Path = e.Path + context.Request.URL.Path

				if e.RawQuery == "" || req.URL.RawQuery == "" {
					req.URL.RawQuery = e.RawQuery + req.URL.RawQuery
				} else {
					req.URL.RawQuery = e.RawQuery + "&" + req.URL.RawQuery
				}

				if _, ok := req.Header["User-Agent"]; !ok {
					// explicitly disable User-Agent so it's not set to default value
					req.Header.Set("User-Agent", "")
				}
			},
		}

		proxy.Transport = newRoundTripHTTPTransport()
		proxy.ErrorHandler = i.LogAPIErrors
		proxy.ServeHTTP(w, r)

		return nil
	}, i.plugins...)

	if err := chain(context); err != nil {
		logger.Error("error executing plugin chain", zap.Error(err))

		inway_http.WriteError(w, httperrors.O1, httperrors.ErrorExecutingPluginChain())
	}
}

func (i *Inway) LogAPIErrors(w http.ResponseWriter, r *http.Request, err error) {
	i.logger.Error(fmt.Sprintf("failed internal API request to %s try again later. service api down/unreachable. check A1 error at https://docs.nlx.io/support/common-errors/", r.URL.String()), zap.Error(err))

	inway_http.WriteError(w, httperrors.A1, httperrors.ServiceUnreachable(r.URL.String()))
}
