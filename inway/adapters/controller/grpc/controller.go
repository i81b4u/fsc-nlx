// Copyright © VNG Realisatie 2023
// Licensed under the EUPL

package grpccontroller

import (
	"context"
	"fmt"

	"github.com/pkg/errors"

	"go.nlx.io/nlx/controller/ports/internalgrpc/api"
	"go.nlx.io/nlx/inway/adapters/controller"
	controllerclient "go.nlx.io/nlx/inway/pkg/controller"
)

type grpcController struct {
	controller controllerclient.Client
}

func New(client controllerclient.Client) (*grpcController, error) {
	if client == nil {
		return nil, fmt.Errorf("client is required")
	}

	return &grpcController{
		controller: client,
	}, nil
}

func (m *grpcController) RegisterInway(ctx context.Context, args *controller.RegisterInwayArgs) error {
	_, err := m.controller.RegisterInway(ctx, &api.RegisterInwayRequest{
		Name:    args.Name,
		Address: args.Address,
	})
	if err != nil {
		return errors.Wrap(err, "could not register inway in grpc controller")
	}

	return nil
}
