// Copyright © VNG Realisatie 2023
// Licensed under the EUPL

package grpcconfig

import (
	"context"

	"github.com/pkg/errors"

	"go.nlx.io/nlx/manager/domain/contract"
	"go.nlx.io/nlx/manager/ports/int/grpc/api"
)

func (r *Repository) GetCertificate(ctx context.Context, thumbprint string) (*contract.PeerCertificate, error) {
	response, err := r.managerClient.GetCertificate(ctx, &api.GetCertificateRequest{Thumbprint: thumbprint})
	if err != nil {
		return nil, errors.Wrap(err, "could not get certificate from manager")
	}

	cert, err := contract.NewPeerCertFromCertificate(r.trustedRootCerts, response.CertificateWithChain)
	if err != nil {
		return nil, errors.Wrap(err, "")
	}

	return cert, nil
}
