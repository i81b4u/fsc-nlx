// Copyright © VNG Realisatie 2023
// Licensed under the EUPL

package grpcconfig

import (
	"context"
	"net/url"

	"github.com/pkg/errors"

	controllerapi "go.nlx.io/nlx/controller/ports/internalgrpc/api"
)

func (r *Repository) GetServiceEndpointURL(ctx context.Context, serviceName string) (*url.URL, error) {
	resp, err := r.controllerClient.GetServiceEndpointURL(ctx, &controllerapi.GetServiceEndpointURLRequest{
		InwayAddress: r.selfAddress,
		ServiceName:  serviceName,
	})

	if err != nil {
		return nil, errors.Wrap(err, "unable to get service endpoint url from management api")
	}

	parsedURL, err := url.Parse(resp.EndpointUrl)
	if err != nil {
		return nil, errors.Wrapf(err, "failed to parse url returned by the management api. url: %s ", parsedURL)
	}

	return parsedURL, nil
}
