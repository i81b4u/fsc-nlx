// Copyright © VNG Realisatie 2018
// Licensed under the EUPL

package delegation

import (
	"github.com/golang-jwt/jwt/v4"
)

type AccessProof struct {
	ServiceName              string `json:"service_name"`
	OrganizationSerialNumber string `json:"organization_serial_number"`
	CertificateThumbprint    string `json:"certificate_thumbprint"`
}

type JWTClaims struct {
	jwt.RegisteredClaims
	Delegatee                      string       `json:"delegatee"`
	DelegateeCertificateThumbprint string       `json:"delegatee_certificate_thumbprint"`
	OrderReference                 string       `json:"orderReference"`
	AccessProof                    *AccessProof `json:"accessProof"`
}

func (j *JWTClaims) IsValidFor(serviceName, organizationSerialNumber, certificateThumbprint string) bool {
	if j.AccessProof.ServiceName == serviceName && j.AccessProof.OrganizationSerialNumber == organizationSerialNumber && certificateThumbprint == j.AccessProof.CertificateThumbprint {
		return true
	}

	return false
}
