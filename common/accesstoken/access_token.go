// Copyright © VNG Realisatie 2023
// Licensed under the EUPL

package accesstoken

import (
	"crypto"
	"encoding/json"
	"fmt"
	"time"

	"github.com/pkg/errors"
	"gopkg.in/square/go-jose.v2"

	"go.nlx.io/nlx/common/clock"
	"go.nlx.io/nlx/manager/domain/contract"
)

type jsonToken struct {
	GrantHash             string            `json:"gth,omitempty"`
	OutwayPeerID          string            `json:"sub,omitempty"`
	OutwayDelegatorPeerID string            `json:"cdi,omitempty"`
	ServiceName           string            `json:"svc,omitempty"`
	ServiceInwayAddress   string            `json:"aud,omitempty"`
	ServicePeerID         string            `json:"iss,omitempty"`
	ServiceDelegatorID    string            `json:"pdi,omitempty"`
	ExpiryDate            int64             `json:"exp,omitempty"`
	NotBefore             int64             `json:"nbf,omitempty"`
	ConfirmationMethod    map[string]string `json:"cnf,omitempty"`
}

// DecodedToken is a valid decoded token
// and checked if signature is valid
type DecodedToken token

// UnsafeDecodedToken is an unsafely decoded token
// and the signature is NOT validated
// can be used to easily decode token from string in e.g. the Outway
type UnsafeDecodedToken token

type token struct {
	GrantHash                   string
	OutwayPeerID                string
	OutwayCertificateThumbprint string
	OutwayDelegatorPeerID       string
	ServiceName                 string
	ServiceInwayAddress         string
	ServicePeerID               string
	ServiceDelegatorPeerID      string
	ExpiryDate                  time.Time
}

type NewTokenArgs struct {
	GrantHash                   string
	OutwayPeerID                string
	OutwayDelegatorPeerID       string
	OutwayCertificateThumbprint string
	ServiceName                 string
	ServiceInwayAddress         string
	ServicePeerID               string
	ServiceDelegatorPeerID      string
	ExpiryDate                  time.Time
	NotBefore                   time.Time
	SignWith                    *SignWith
}

type SignWith struct {
	CertificateThumbprint string
	PrivateKey            crypto.PrivateKey
}

type signedToken string

const X5THeader = "x5t#S256"

func New(args *NewTokenArgs) (signedToken, error) {
	options := &jose.SignerOptions{}
	options.WithHeader(X5THeader, args.SignWith.CertificateThumbprint)

	signer, err := jose.NewSigner(jose.SigningKey{Algorithm: jose.RS512, Key: args.SignWith.PrivateKey}, options)
	if err != nil {
		return "", errors.Wrap(err, "error creating new signer")
	}

	payloadData, err := json.Marshal(&jsonToken{
		GrantHash:             args.GrantHash,
		OutwayPeerID:          args.OutwayPeerID,
		OutwayDelegatorPeerID: args.OutwayDelegatorPeerID,
		ServiceName:           args.ServiceName,
		ServicePeerID:         args.ServicePeerID,
		ServiceInwayAddress:   args.ServiceInwayAddress,
		ServiceDelegatorID:    args.ServiceDelegatorPeerID,
		ExpiryDate:            args.ExpiryDate.Unix(),
		NotBefore:             args.NotBefore.Unix(),
		ConfirmationMethod: map[string]string{
			X5THeader: args.OutwayCertificateThumbprint,
		},
	})
	if err != nil {
		return "", errors.Wrap(err, "error while marshaling token payload")
	}

	jws, err := signer.Sign(payloadData)
	if err != nil {
		return "", errors.Wrap(err, "error while signing token payload")
	}

	serializedToken, err := jws.CompactSerialize()
	if err != nil {
		return "", errors.Wrap(err, "cannot marshal token")
	}

	return signedToken(serializedToken), nil
}

func GetCertificateThumbprintFromToken(token string) (*contract.CertificateThumbprint, error) {
	_, p, err := decodeToken(token)
	if err != nil {
		return nil, errors.Wrap(err, "could not get certificate thumbprint from token")
	}

	return p, nil
}

func DecodeUnsafeFromString(token string) (*UnsafeDecodedToken, error) {
	jws, err := jose.ParseSigned(token)
	if err != nil {
		return nil, errors.Wrap(err, "could not parse token")
	}

	payloadJSON := &jsonToken{}

	err = json.Unmarshal(jws.UnsafePayloadWithoutVerification(), payloadJSON)
	if err != nil {
		return nil, errors.Wrap(err, "could not parse jws signature")
	}

	return &UnsafeDecodedToken{
		GrantHash:              payloadJSON.GrantHash,
		OutwayPeerID:           payloadJSON.OutwayPeerID,
		OutwayDelegatorPeerID:  payloadJSON.OutwayDelegatorPeerID,
		ServiceName:            payloadJSON.ServiceName,
		ServiceInwayAddress:    payloadJSON.ServiceInwayAddress,
		ServicePeerID:          payloadJSON.ServicePeerID,
		ServiceDelegatorPeerID: payloadJSON.ServiceDelegatorID,
		ExpiryDate:             time.Unix(payloadJSON.ExpiryDate, 0),
	}, nil
}

func DecodeFromString(clck clock.Clock, peerCertificate *contract.PeerCertificate, token string) (*DecodedToken, error) {
	jws, certificateThumbprint, err := decodeToken(token)
	if err != nil {
		return nil, errors.Wrap(err, "could not decode token")
	}

	certificateCertificateThumbprint := peerCertificate.CertificateThumbprint()
	if !certificateCertificateThumbprint.IsEqual(*certificateThumbprint) {
		return nil, fmt.Errorf("certificate thumbprint does not match with certificate thumbprint in token")
	}

	jsonPayload, err := jws.Verify(peerCertificate.PublicKey())
	if err != nil {
		return nil, errors.Wrap(err, "could not verify jws token in signature")
	}

	payload := &jsonToken{}

	err = json.Unmarshal(jsonPayload, payload)
	if err != nil {
		return nil, errors.Wrap(err, "could not parse jws signature")
	}

	if payload.NotBefore == 0 {
		return nil, fmt.Errorf("invalid token, not before cannot be zero")
	}

	if payload.ExpiryDate == 0 {
		return nil, fmt.Errorf("invalid token, expiry date cannot be zero")
	}

	notBefore := time.Unix(payload.NotBefore, 0)
	expiresAt := time.Unix(payload.ExpiryDate, 0)

	if notBefore.After(clck.Now()) {
		return nil, fmt.Errorf("invalid token, becomes valid on: %s", notBefore)
	}

	if clck.Now().After(expiresAt) {
		return nil, fmt.Errorf("invalid token, expired on: %s", expiresAt)
	}

	owCertThumbprint, ok := payload.ConfirmationMethod[X5THeader]
	if !ok || owCertThumbprint == "" {
		return nil, fmt.Errorf("invalid token, missing outway certificate thumbprint in confirmation method: %s", X5THeader)
	}

	if payload.GrantHash == "" {
		return nil, fmt.Errorf("invalid token, grant hash cannot be empty")
	}

	if payload.ServiceName == "" {
		return nil, fmt.Errorf("invalid token, service name cannot be empty")
	}

	if payload.ServicePeerID == "" {
		return nil, fmt.Errorf("invalid token, service peer ID cannot be empty")
	}

	return &DecodedToken{
		GrantHash:                   payload.GrantHash,
		OutwayPeerID:                payload.OutwayPeerID,
		OutwayDelegatorPeerID:       payload.OutwayDelegatorPeerID,
		OutwayCertificateThumbprint: owCertThumbprint,
		ServiceName:                 payload.ServiceName,
		ServicePeerID:               payload.ServicePeerID,
		ServiceDelegatorPeerID:      payload.ServiceDelegatorID,
		ServiceInwayAddress:         payload.ServiceInwayAddress,
		ExpiryDate:                  expiresAt,
	}, nil
}

func (s signedToken) Value() string {
	return string(s)
}

func decodeToken(token string) (*jose.JSONWebSignature, *contract.CertificateThumbprint, error) {
	jws, err := jose.ParseSigned(token)
	if err != nil {
		return nil, nil, errors.Wrap(err, "could not parse token")
	}

	if len(jws.Signatures) != 1 {
		return nil, nil, fmt.Errorf("exactly one signature is required, found either zero or multiple signatures in jws token")
	}

	header, ok := jws.Signatures[0].Header.ExtraHeaders[X5THeader]
	if !ok {
		return nil, nil, fmt.Errorf("could not find required header in jws signature: %s", X5THeader)
	}

	pubKey, ok := header.(string)
	if !ok {
		return nil, nil, fmt.Errorf("certificate thumbprint must be of type string")
	}

	certificateThumbprint, err := contract.NewCertificateThumbprint(pubKey)
	if err != nil {
		return nil, nil, fmt.Errorf("invalid certificate thumbprint in jws header: %v", err)
	}

	return jws, &certificateThumbprint, nil
}
