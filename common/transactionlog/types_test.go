// Copyright © VNG Realisatie 2023
// Licensed under the EUPL

package transactionlog

import "testing"

var RecordIDResult string

func BenchmarkLogRecordID(b *testing.B) {
	var r string

	for i := 0; i < b.N; i++ {
		l, _ := NewTransactionID()
		r = l.String()
	}

	RecordIDResult = r
}
