// Copyright © VNG Realisatie 2022
// Licensed under the EUPL

package transactionlog

import (
	"context"
	"fmt"
	"time"

	"go.uber.org/zap"
	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials"

	common_tls "go.nlx.io/nlx/common/tls"
	"go.nlx.io/nlx/txlog-api/api"
	"go.nlx.io/nlx/txlog-api/domain/record"
)

type APITransactionLogger struct {
	logger     *zap.Logger
	client     api.TXLogServiceClient
	connection *grpc.ClientConn
	cancelFunc context.CancelFunc
}

type NewAPITransactionLoggerArgs struct {
	Logger       *zap.Logger
	APIAddress   string
	InternalCert *common_tls.CertificateBundle
}

func NewAPITransactionLogger(args *NewAPITransactionLoggerArgs) (TransactionLogger, error) {
	if args.Logger == nil {
		return nil, fmt.Errorf("logger is required")
	}

	if args.APIAddress == "" {
		return nil, fmt.Errorf("API address is required")
	}

	if args.InternalCert == nil {
		return nil, fmt.Errorf("internal cert is required")
	}

	dialCredentials := credentials.NewTLS(args.InternalCert.TLSConfig())
	dialOptions := []grpc.DialOption{
		grpc.WithTransportCredentials(dialCredentials),
	}

	var grpcTimeout = 5 * time.Second

	grpcCtx, cancel := context.WithTimeout(context.Background(), grpcTimeout)

	txlogConn, err := grpc.DialContext(grpcCtx, args.APIAddress, dialOptions...)
	if err != nil {
		cancel()
		return nil, err
	}

	txlogClient := api.NewTXLogServiceClient(txlogConn)

	result := &APITransactionLogger{
		logger:     args.Logger,
		client:     txlogClient,
		connection: txlogConn,
		cancelFunc: cancel,
	}

	return result, nil
}

func (txl *APITransactionLogger) AddRecords(ctx context.Context, recs []*Record) error {
	records, err := recordsToProto(recs)
	if err != nil {
		return err
	}

	_, err = txl.client.CreateRecords(ctx, &api.CreateRecordsRequest{
		Records: records,
	})
	if err != nil {
		return err
	}

	return nil
}

func (txl *APITransactionLogger) Close() error {
	txl.cancelFunc()
	return txl.connection.Close()
}

func recordsToProto(recs []*Record) ([]*api.TransactionLogRecord, error) {
	records := make([]*api.TransactionLogRecord, len(recs))

	for i, r := range recs {
		recordResponse := &api.TransactionLogRecord{
			TransactionId: r.TransactionID.String(),
			Direction:     directionToProto(r.Direction),
			GrantHash:     r.GrantHash,
			ServiceName:   r.ServiceName,
			Source:        &api.TransactionLogRecordSource{},
			Destination:   &api.TransactionLogRecordDestination{},
			CreatedAt:     r.CreatedAt.Unix(),
		}

		switch s := r.Source.(type) {
		case *RecordSource:
			recordResponse.Source.Data = &api.TransactionLogRecordSource_Source_{
				Source: &api.TransactionLogRecordSource_Source{
					OutwayPeerId: s.OutwayPeerID,
				},
			}
		case *RecordDelegatedSource:
			recordResponse.Source.Data = &api.TransactionLogRecordSource_DelegatedSource_{
				DelegatedSource: &api.TransactionLogRecordSource_DelegatedSource{
					OutwayPeerId:    s.OutwayPeerID,
					DelegatorPeerId: s.DelegatorPeerID,
				},
			}
		default:
			return nil, fmt.Errorf("unknown source type: %T", s)
		}

		switch d := r.Destination.(type) {
		case *RecordDestination:
			recordResponse.Destination.Data = &api.TransactionLogRecordDestination_Destination_{
				Destination: &api.TransactionLogRecordDestination_Destination{
					ServicePeerId: d.ServicePeerID,
				},
			}
		case *RecordDelegatedDestination:
			recordResponse.Destination.Data = &api.TransactionLogRecordDestination_DelegatedDestination_{
				DelegatedDestination: &api.TransactionLogRecordDestination_DelegatedDestination{
					ServicePeerId:   d.ServicePeerID,
					DelegatorPeerId: d.DelegatorPeerID,
				},
			}
		default:
			return nil, fmt.Errorf("unknown destination type: %T", d)
		}

		records[i] = recordResponse
	}

	return records, nil
}

func directionToProto(d record.Direction) api.TransactionLogRecord_Direction {
	switch d {
	case record.DirectionIn:
		return api.TransactionLogRecord_DIRECTION_IN
	case record.DirectionOut:
		return api.TransactionLogRecord_DIRECTION_OUT
	default:
		return api.TransactionLogRecord_DIRECTION_UNSPECIFIED
	}
}
