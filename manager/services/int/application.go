// Copyright © VNG Realisatie 2022
// Licensed under the EUPL

package internalservice

import (
	"context"
	"crypto/tls"
	"crypto/x509"

	"github.com/pkg/errors"

	"go.nlx.io/nlx/manager/adapters/controller"
	"go.nlx.io/nlx/manager/adapters/logger"
	"go.nlx.io/nlx/manager/adapters/manager"
	"go.nlx.io/nlx/manager/adapters/txlog"
	app "go.nlx.io/nlx/manager/apps/int"
	"go.nlx.io/nlx/manager/apps/int/command"
	"go.nlx.io/nlx/manager/apps/int/query"
	"go.nlx.io/nlx/manager/domain/contract"
	"go.nlx.io/nlx/manager/internal/peers"
)

type NewApplicationArgs struct {
	Context                     context.Context
	Clock                       command.Clock
	Logger                      logger.Logger
	Repository                  contract.Repository
	Peers                       *peers.Peers
	TXLog                       txlog.TXLog
	SelfPeerID                  contract.PeerID
	SelfPeerName                string
	TrustedExternalRootCAs      *x509.CertPool
	SignatureCertificate        *tls.Certificate
	ControllerRepository        controller.Controller
	ManagerFactory              manager.Factory
	DirectoryPeerManagerAddress string
}

func NewApplication(args *NewApplicationArgs) (*app.Application, error) {
	registerPeer, err := command.NewAnnouncePeerHandler(args.TrustedExternalRootCAs, args.Peers, args.Logger)
	if err != nil {
		return nil, err
	}

	createContract, err := command.NewCreateContractHandler(&command.NewCreateContractHandlerArgs{
		TrustedExternalRootCAs: args.TrustedExternalRootCAs,
		Repository:             args.Repository,
		Peers:                  args.Peers,
		Clock:                  args.Clock,
		Logger:                 args.Logger,
		SignatureCert:          args.SignatureCertificate,
		SelfPeerID:             &args.SelfPeerID,
	})
	if err != nil {
		return nil, err
	}

	approveContract, err := command.NewAcceptContractHandler(&command.NewAcceptContractHandlerArgs{
		TrustedExternalRootCAs: args.TrustedExternalRootCAs,
		Repository:             args.Repository,
		Peers:                  args.Peers,
		Clock:                  args.Clock,
		Logger:                 args.Logger,
		SignatureCert:          args.SignatureCertificate,
	})
	if err != nil {
		return nil, err
	}

	rejectContract, err := command.NewRejectContractHandler(&command.NewRejectContractHandlerArgs{
		TrustedExternalRootCAs: args.TrustedExternalRootCAs,
		Repository:             args.Repository,
		Peers:                  args.Peers,
		Clock:                  args.Clock,
		Logger:                 args.Logger,
		SignatureCert:          args.SignatureCertificate,
	})
	if err != nil {
		return nil, err
	}

	revokeContract, err := command.NewRevokeContractHandler(&command.NewRevokeContractHandlerArgs{
		TrustedExternalRootCAs: args.TrustedExternalRootCAs,
		Repository:             args.Repository,
		Peers:                  args.Peers,
		Clock:                  args.Clock,
		Logger:                 args.Logger,
		SignatureCert:          args.SignatureCertificate,
	})
	if err != nil {
		return nil, err
	}

	getToken, err := query.NewGetTokenHandler(&query.NewGetTokenHandlerArgs{
		Repository: args.Repository,
		Peers:      args.Peers,
		Clock:      args.Clock,
	})
	if err != nil {
		return nil, err
	}

	getServiceEndpointURL, err := query.NewGetServiceEndpointURLHandler(args.ControllerRepository)
	if err != nil {
		return nil, err
	}

	getServicesForOutway, err := query.NewGetServicesForOutwayHandler(args.Repository)
	if err != nil {
		return nil, err
	}

	getCertificates, err := query.NewGetCertificateHandler(&query.NewGetCertificateHandlerArg{
		Repository: args.Repository,
		SelfPeerID: args.SelfPeerID,
	})
	if err != nil {
		return nil, err
	}

	getContracts, err := query.NewListContractsHandler(args.Repository, args.SelfPeerID)
	if err != nil {
		return nil, errors.Wrap(err, "could not create get contracts handler")
	}

	getServices, err := query.NewGetServicesHandler(args.ManagerFactory, args.DirectoryPeerManagerAddress)
	if err != nil {
		return nil, errors.Wrap(err, "could not create get services handler")
	}

	createCertificate, err := command.NewCreateCertificateHandler(args.TrustedExternalRootCAs, args.Repository)
	if err != nil {
		return nil, err
	}

	getTXLogRecords, err := query.NewListTransactionLogRecordsHandler(args.TXLog, args.Peers)
	if err != nil {
		return nil, errors.Wrap(err, "could not create list txlog records handler")
	}

	getPeerInfo, err := query.NewGetPeerInfoHandler(args.SelfPeerID.Value(), args.SelfPeerName)
	if err != nil {
		return nil, errors.Wrap(err, "could not create get peer info handler")
	}

	application := &app.Application{
		Queries: app.Queries{
			GetToken:              getToken,
			GetServiceEndpointURL: getServiceEndpointURL,
			GetServicesForOutway:  getServicesForOutway,
			GetCertificate:        getCertificates,
			GetContracts:          getContracts,
			GetServices:           getServices,
			GetTXLogRecords:       getTXLogRecords,
			GetPeerInfo:           getPeerInfo,
		},
		Commands: app.Commands{
			AnnouncePeer:      registerPeer,
			CreateContract:    createContract,
			AcceptContract:    approveContract,
			RejectContract:    rejectContract,
			RevokeContract:    revokeContract,
			CreateCertificate: createCertificate,
		},
	}

	return application, nil
}
