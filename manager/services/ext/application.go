// Copyright © VNG Realisatie 2022
// Licensed under the EUPL

package externalservice

import (
	"context"
	"crypto/tls"
	"crypto/x509"
	"time"

	"go.nlx.io/nlx/common/clock"
	"go.nlx.io/nlx/manager/adapters/controller"
	"go.nlx.io/nlx/manager/adapters/logger"
	"go.nlx.io/nlx/manager/adapters/txlog"
	app "go.nlx.io/nlx/manager/apps/ext"
	"go.nlx.io/nlx/manager/apps/ext/command"
	"go.nlx.io/nlx/manager/apps/ext/query"
	"go.nlx.io/nlx/manager/domain/contract"
	"go.nlx.io/nlx/manager/internal/peers"
)

type NewApplicationArgs struct {
	Context              context.Context
	Clock                clock.Clock
	Logger               logger.Logger
	Repository           contract.Repository
	ControllerRepository controller.Controller
	TXLogRepository      txlog.TXLog
	Peers                *peers.Peers
	AutoSignGrants       []string
	SelfPeerID           contract.PeerID
	TrustedRootCAs       *x509.CertPool
	AutoSignCertificate  *tls.Certificate
	TokenSignCertificate *tls.Certificate
	TokenTTL             time.Duration
}

func NewApplication(args *NewApplicationArgs) (*app.Application, error) {
	commands, err := setupCommands(args)
	if err != nil {
		return nil, err
	}

	queries, err := setupQueries(args)
	if err != nil {
		return nil, err
	}

	application := &app.Application{
		Queries:  *queries,
		Commands: *commands,
	}

	return application, nil
}

func setupCommands(args *NewApplicationArgs) (*app.Commands, error) {
	submitContract, err := command.NewSubmitContractHandler(&command.NewSubmitContractHandlerArgs{
		SelfPeerID:          args.SelfPeerID,
		TrustedRootCAs:      args.TrustedRootCAs,
		Repository:          args.Repository,
		Peers:               args.Peers,
		Logger:              args.Logger,
		Clock:               args.Clock,
		AutoSignGrants:      args.AutoSignGrants,
		AutoSignCertificate: args.AutoSignCertificate,
	})
	if err != nil {
		return nil, err
	}

	acceptContract, err := command.NewAcceptContractHandler(&command.NewAcceptContractHandlerArgs{
		SelfPeerID:     args.SelfPeerID,
		TrustedRootCAs: args.TrustedRootCAs,
		Contracts:      args.Repository,
		Peers:          args.Peers,
		Logger:         args.Logger,
		Clock:          args.Clock,
	})
	if err != nil {
		return nil, err
	}

	rejectContract, err := command.NewRejectContractHandler(&command.NewRejectContractHandlerArgs{
		SelfPeerID:     args.SelfPeerID,
		TrustedRootCAs: args.TrustedRootCAs,
		Repository:     args.Repository,
		Peers:          args.Peers,
		Logger:         args.Logger,
		Clock:          args.Clock,
	})
	if err != nil {
		return nil, err
	}

	revokeContract, err := command.NewRevokeContractHandler(&command.NewRevokeContractHandlerArgs{
		SelfPeerID:     args.SelfPeerID,
		TrustedRootCAs: args.TrustedRootCAs,
		Repository:     args.Repository,
		Peers:          args.Peers,
		Logger:         args.Logger,
		Clock:          args.Clock,
	})
	if err != nil {
		return nil, err
	}

	announce, err := command.NewAnnounceHandler(&command.NewAnnounceHandlerArgs{Repository: args.Repository, Logger: args.Logger})
	if err != nil {
		return nil, err
	}

	return &app.Commands{
		SubmitContract: submitContract,
		AcceptContract: acceptContract,
		RejectContract: rejectContract,
		RevokeContract: revokeContract,
		Announce:       announce,
	}, nil
}

func setupQueries(args *NewApplicationArgs) (*app.Queries, error) {
	listContractsHandler, err := query.NewListContractsHandler(args.Repository)
	if err != nil {
		return nil, err
	}

	getKeySetHandler, err := query.NewGetKeySetHandler(args.Repository, args.SelfPeerID)
	if err != nil {
		return nil, err
	}

	listPeersHandler, err := query.NewListPeersHandler(args.Repository)
	if err != nil {
		return nil, err
	}

	listServicesHandler, err := query.NewListServicesHandler(args.Repository)
	if err != nil {
		return nil, err
	}

	getPeerInfoHandler, err := query.NewGetPeerInfoHandler()
	if err != nil {
		return nil, err
	}

	listTransactionLogRecords, err := query.NewListTransactionLogRecordsHandler(args.TXLogRepository)
	if err != nil {
		return nil, err
	}

	getTokenHandler, err := query.NewGetTokenHandler(&query.NewGetTokenHandlerArgs{
		Clock:            args.Clock,
		Repo:             args.Repository,
		SignTokenCert:    args.TokenSignCertificate,
		TrustedRootCAs:   args.TrustedRootCAs,
		TokenTTL:         args.TokenTTL,
		ManagementClient: args.ControllerRepository,
	})
	if err != nil {
		return nil, err
	}

	return &app.Queries{
		ListContracts:             listContractsHandler,
		GetKeySet:                 getKeySetHandler,
		ListPeers:                 listPeersHandler,
		ListServices:              listServicesHandler,
		GetPeerInfo:               getPeerInfoHandler,
		ListTransactionLogRecords: listTransactionLogRecords,
		GetToken:                  getTokenHandler,
	}, nil
}
