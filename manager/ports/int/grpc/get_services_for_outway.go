// Copyright © VNG Realisatie 2023
// Licensed under the EUPL

package internalgrpc

import (
	"context"

	"go.nlx.io/nlx/manager/ports/int/grpc/api"
)

func (s *Server) GetServicesForOutway(ctx context.Context, req *api.GetServicesForOutwayRequest) (*api.GetServicesForOutwayResponse, error) {
	s.logger.Info("rpc request GetServicesForOutway")

	res, err := s.app.Queries.GetServicesForOutway.Handle(ctx, req.OutwayCertificateThumbprint)
	if err != nil {
		return nil, ResponseFromError(err)
	}

	services := make(map[string]*api.GetServicesForOutwayResponse_Service)
	for grantHash, s := range res {
		services[grantHash] = &api.GetServicesForOutwayResponse_Service{
			PeerId: s.PeerID,
			Name:   s.Name,
		}
	}

	return &api.GetServicesForOutwayResponse{Services: services}, nil
}
