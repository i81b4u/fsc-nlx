// Copyright © VNG Realisatie 2023
// Licensed under the EUPL

package internalgrpc

import (
	"context"
	"fmt"

	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"

	"go.nlx.io/nlx/manager/apps/int/query"
	"go.nlx.io/nlx/manager/ports/int/grpc/api"
	"go.nlx.io/nlx/txlog-api/domain/record"
)

func (s *Server) ListTXLogRecords(ctx context.Context, req *api.ListTXLogRecordsRequest) (*api.ListTXLogRecordsResponse, error) {
	s.logger.Info("rpc request ListRecords")

	sortOrder := query.SortOrderAscending

	if req.Pagination != nil && req.Pagination.Order == api.SortOrder_SORT_ORDER_DESCENDING {
		sortOrder = query.SortOrderDescending
	}

	records, err := s.app.Queries.GetTXLogRecords.Handle(ctx, &query.ListTXLogRecordsHandlerArgs{
		Pagination: &query.Pagination{
			StartID:   req.Pagination.StartId,
			Limit:     req.Pagination.Limit,
			SortOrder: sortOrder,
		},
		DataSourcePeerID: req.DataSourcePeerId,
	})
	if err != nil {
		s.logger.Error("error getting tx log records from query", err)
		return nil, status.Error(codes.Internal, "storage error")
	}

	response, err := dataModelToResponse(records)
	if err != nil {
		s.logger.Error("unable to convert query model to api model", err)
		return nil, status.Error(codes.Internal, "internal")
	}

	return response, nil
}

func dataModelToResponse(records []*query.TXLogRecord) (*api.ListTXLogRecordsResponse, error) {
	response := &api.ListTXLogRecordsResponse{}
	response.Records = make([]*api.TransactionLogRecord, len(records))

	for i, r := range records {
		recordResponse := &api.TransactionLogRecord{
			Id:            r.ID,
			TransactionId: r.TransactionID,
			Direction:     directionToProto(r.Direction),
			GrantHash:     r.GrantHash,
			ServiceName:   r.ServiceName,
			Source:        &api.TransactionLogRecord_Source{},
			Destination:   &api.TransactionLogRecord_Destination{},
			CreatedAt:     r.CreatedAt.Unix(),
		}

		switch s := r.Source.(type) {
		case *query.TXLogRecordSource:
			recordResponse.Source.Data = &api.TransactionLogRecord_Source_Source_{
				Source: &api.TransactionLogRecord_Source_Source{
					OutwayPeerId: s.OutwayPeerID,
				},
			}
		case *query.TXLogRecordDelegatedSource:
			recordResponse.Source.Data = &api.TransactionLogRecord_Source_DelegatedSource_{
				DelegatedSource: &api.TransactionLogRecord_Source_DelegatedSource{
					OutwayPeerId:    s.OutwayPeerID,
					DelegatorPeerId: s.DelegatorPeerID,
				},
			}
		default:
			return nil, fmt.Errorf("unknown source type: %T", s)
		}

		switch d := r.Destination.(type) {
		case *query.TXLogRecordDestination:
			recordResponse.Destination.Data = &api.TransactionLogRecord_Destination_Destination_{
				Destination: &api.TransactionLogRecord_Destination_Destination{
					ServicePeerId: d.ServicePeerID,
				},
			}
		case *query.TXLogRecordDelegatedDestination:
			recordResponse.Destination.Data = &api.TransactionLogRecord_Destination_DelegatedDestination_{
				DelegatedDestination: &api.TransactionLogRecord_Destination_DelegatedDestination{
					ServicePeerId:   d.ServicePeerID,
					DelegatorPeerId: d.DelegatorPeerID,
				},
			}
		default:
			return nil, fmt.Errorf("unknown destination type: %T", d)
		}

		response.Records[i] = recordResponse
	}

	return response, nil
}

func directionToProto(d record.Direction) api.TransactionLogRecord_Direction {
	switch d {
	case record.DirectionIn:
		return api.TransactionLogRecord_DIRECTION_IN
	case record.DirectionOut:
		return api.TransactionLogRecord_DIRECTION_OUT
	default:
		return api.TransactionLogRecord_DIRECTION_UNSPECIFIED
	}
}
