// Copyright © VNG Realisatie 2023
// Licensed under the EUPL

package internalgrpc

import (
	"context"

	"go.nlx.io/nlx/manager/ports/int/grpc/api"
)

func (s *Server) AcceptContract(ctx context.Context, req *api.AcceptContractRequest) (*api.AcceptContractResponse, error) {
	s.logger.Info("rpc request AcceptContract")

	err := s.app.Commands.AcceptContract.Handle(ctx, req.ContentHash)
	if err != nil {
		s.logger.Error("error executing accept contract command", err)
		return nil, ResponseFromError(err)
	}

	return &api.AcceptContractResponse{}, nil
}
