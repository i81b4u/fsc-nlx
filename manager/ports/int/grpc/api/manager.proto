// Copyright © VNG Realisatie 2022
// Licensed under the EUPL

syntax = "proto3";

package nlx.manager_internal;

option go_package = "go.nlx.io/nlx/manager/ports/int/grpc/api";

service ManagerService {
  rpc CreateContract(CreateContractRequest) returns (CreateContractResponse);
  rpc AcceptContract(AcceptContractRequest) returns (AcceptContractResponse);
  rpc RejectContract(RejectContractRequest) returns (RejectContractResponse);
  rpc RevokeContract(RevokeContractRequest) returns (RevokeContractResponse);

  rpc GetServiceEndpointURL(GetServiceEndpointURLRequest) returns (GetServiceEndpointURLResponse);
  rpc GetServicesForOutway(GetServicesForOutwayRequest) returns (GetServicesForOutwayResponse);
  rpc GetToken(GetTokenRequest) returns (GetTokenResponse);
  rpc GetCertificate(GetCertificateRequest) returns (GetCertificateResponse);
  rpc GetPeerInfo(GetPeerInfoRequest) returns (GetPeerInfoResponse);

  rpc ListContracts(ListContractsRequest) returns (ListContractsResponse);
  rpc ListServices(ListServicesRequest) returns (ListServicesResponse);
  rpc ListTXLogRecords(ListTXLogRecordsRequest) returns (ListTXLogRecordsResponse) {}
}

enum HashAlgorithm {
  HASH_ALGORITHM_UNSPECIFIED = 0;
  HASH_ALGORITHM_SHA3_512 = 1;
}

message ContractContent {
  bytes id = 1;
  string group_id = 2;
  Validity validity = 3;
  repeated Grant grants = 4;
  HashAlgorithm hash_algorithm = 5;
  int64 created_at = 6;
}

message Validity {
  int64 not_before = 1;
  int64 not_after = 2;
}

message Signatures {
  map<string, string> accept = 1;
  map<string, string> reject = 2;
  map<string, string> revoke = 3;
}

enum GrantType {
  GRANT_TYPE_UNSPECIFIED = 0;
  GRANT_TYPE_PEER_REGISTRATION = 1;
  GRANT_TYPE_SERVICE_PUBLICATION = 2;
  GRANT_TYPE_SERVICE_CONNECTION = 3;
  GRANT_TYPE_DELEGATED_SERVICE_CONNECTION = 4;
  GRANT_TYPE_DELEGATED_SERVICE_PUBLICATION = 5;
}

message Grant {
  GrantType type = 1;
  oneof data {
    GrantPeerRegistration peer_registration = 2;
    GrantServicePublication service_publication = 3;
    GrantServiceConnection service_connection = 4;
    GrantDelegatedServiceConnection delegated_service_connection = 5;
    GrantDelegatedServicePublication delegated_service_publication = 6;
  }
}

message GrantPeerRegistration {
  message Directory {
    string peer_id = 1;
  }

  message Peer {
    string id = 1;
    string name = 2;
  }

  string hash = 1;
  Directory directory = 2;
  Peer peer = 3;
}

message GrantServicePublication {
  message Service {
    string peer_id = 1;
    string name = 2;
  }

  message Directory {
    string peer_id = 1;
  }

  string hash = 1;
  Directory directory = 2;
  Service service = 3;
}

message GrantServiceConnection {
  message Service {
    string peer_id = 1;
    string name = 2;
  }

  message Outway {
    string peer_id = 1;
    string certificate_thumbprint = 2;
  }

  string hash = 1;
  Outway outway = 2;
  Service service = 3;
}

message GrantDelegatedServiceConnection {
  message Service {
    string peer_id = 1;
    string name = 2;
  }

  message Outway {
    string peer_id = 1;
    string certificate_thumbprint = 2;
  }

  message Delegator {
    string peer_id = 1;
  }

  string hash = 1;
  Outway outway = 2;
  Service service = 3;
  Delegator delegator = 4;
}

message GrantDelegatedServicePublication {
  message Service {
    string peer_id = 1;
    string name = 2;
  }

  message Directory {
    string peer_id = 1;
  }

  message Delegator {
    string peer_id = 1;
  }

  string hash = 1;
  Directory directory = 2;
  Service service = 3;
  Delegator delegator = 4;
}

message CreateContractRequest {
  ContractContent contract_content = 1;
}

message CreateContractResponse {
  // TODO: content_hash should be removed, since commands should not return anything.
  // Remove once we start working on the UI and have ListContracts implemented (which contains the hash)
  string content_hash = 1;
}

message AcceptContractRequest {
  string content_hash = 1;
}

message AcceptContractResponse {}

message RejectContractRequest {
  string content_hash = 1;
}

message RejectContractResponse {}

message RevokeContractRequest {
  string content_hash = 1;
}

message RevokeContractResponse {}

message GetOutwayConfigRequest {
  string certificate_thumbprint = 1;
}

message GetOutwayConfigResponse {
  message Service {
    string name = 1;
    string inway_address = 2;
    string delegator_peer_id = 3;
    string service_provider_peer_id = 4;
    string service_delegator_peer_id = 5;
  }

  map<string, Service> services = 1;
}

message GetInwayConfigRequest {
  string inway_address = 1;
}

message GetInwayConfigResponse {
  message Service {
    message Outway {
      string peer_id = 1;
      string certificate_thumbprint = 2;
      string delegator_peer_id = 3;
    }

    string endpoint_url = 1;
    repeated Outway authorized_outways = 2;
    string delegator_peer_id = 3;
  }

  map<string, Service> services = 1;
}

message GetServiceEndpointURLRequest {
  string inway_address = 1;
  string service_name = 2;
}

message GetServiceEndpointURLResponse {
  string service_endpoint_url = 1;
}

message GetServicesForOutwayRequest {
  string outway_certificate_thumbprint = 1;
}

message GetServicesForOutwayResponse {
  message Service {
    string peer_id = 1;
    string name = 2;
  }

  map<string, Service> services = 1;
}

message GetTokenRequest {
  string grant_hash = 1;
  string outway_certificate_thumbprint = 2;
}

message GetTokenResponse {
  message TokenInfo {
    string grant_hash = 1;
    string outway_peer_id = 2;
    string outway_certificate_thumbprint = 3;
    string outway_delegator_peer_id = 4;
    string service_name = 5;
    string service_inway_address = 6;
    string service_peer_id = 7;
    string service_delegator_peer_id = 8;
    int64 expiry_date = 9;
  }

  string token = 1;
  TokenInfo token_info = 2;
}

message GetCertificateRequest {
  string thumbprint = 1;
}

message GetCertificateResponse {
  repeated bytes certificate_with_chain = 1; // Raw certificate bytes including chain
}

message ListContractsRequest {
  message Filter {
    string content_hash = 1;
    GrantType grant_type = 2;
  }

  repeated Filter filters = 1;
}

message ListContractsResponse {
  message Peer {
    string id = 1;
    string name = 2;
  }

  message Signature {
    Peer peer = 1;
    int64 signed_at = 2;
  }

  message Signatures {
    map<string, Signature> accept = 1;
    map<string, Signature> revoke = 2;
    map<string, Signature> reject = 3;
  }

  message Contract {
    ContractContent content = 1;
    string hash = 2;
    map<string, Peer> peers = 3;
    bool has_rejected = 4;
    bool has_accepted = 5;
    bool has_revoked = 6;
    Signatures signatures = 7;
  }

  repeated Contract contracts = 1;
}

message ListServicesRequest {}

message ListServicesResponse {
  message Peer {
    string id = 1;
    string name = 2;
  }

  message Service {
    message Service {
      string name = 1;
      Peer provider = 2;
    }

    message DelegatedService {
      string name = 1;
      Peer provider = 2;
      Peer delegator = 3;
    }

    oneof data {
      Service service = 1;
      DelegatedService delegated_service = 2;
    }
  }

  repeated Service services = 1;
}

enum SortOrder {
  SORT_ORDER_UNSPECIFIED = 0;
  SORT_ORDER_ASCENDING = 1;
  SORT_ORDER_DESCENDING = 2;
}

message Pagination {
  uint64 start_id = 1;
  uint32 limit = 2;
  SortOrder order = 3;
}

message ListTXLogRecordsRequest {
  message Period {
    uint64 start = 1;
    uint64 end = 2;
  }

  message Filter {
    Period period = 1;
    string transaction_id = 2;
    string service_name = 3;
    string grant_hash = 4;
    string peer_id = 5;
  }

  Pagination pagination = 1;
  repeated Filter filters = 2;
  string data_source_peer_id = 3;
}

message ListTXLogRecordsResponse {
  repeated TransactionLogRecord records = 1;
}

message TransactionLogRecord {
  enum Direction {
    DIRECTION_UNSPECIFIED = 0;
    DIRECTION_IN = 1;
    DIRECTION_OUT = 2;
  }

  message Source {
    message Source {
      string outway_peer_id = 1;
    }

    message DelegatedSource {
      string outway_peer_id = 1;
      string delegator_peer_id = 2;
    }

    oneof data {
      Source source = 1;
      DelegatedSource delegated_source = 2;
    }
  }

  message Destination {
    message Destination {
      string service_peer_id = 1;
    }

    message DelegatedDestination {
      string service_peer_id = 1;
      string delegator_peer_id = 2;
    }

    oneof data {
      Destination destination = 1;
      DelegatedDestination delegated_destination = 2;
    }
  }

  uint64 id = 1;
  string transaction_id = 2;
  Direction direction = 3;
  string grant_hash = 4;
  string service_name = 5;
  Source source = 6;
  Destination destination = 7;
  int64 created_at = 8;
}

message GetPeerInfoRequest {}

message GetPeerInfoResponse {
  message Peer {
    string id = 1;
    string name = 2;
  }

  Peer peer = 1;
}
