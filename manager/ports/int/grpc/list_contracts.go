// Copyright © VNG Realisatie 2023
// Licensed under the EUPL

package internalgrpc

import (
	"context"

	"go.nlx.io/nlx/manager/apps/int/query"
	"go.nlx.io/nlx/manager/ports/int/grpc/api"
)

func (s *Server) ListContracts(ctx context.Context, req *api.ListContractsRequest) (*api.ListContractsResponse, error) {
	s.logger.Info("rpc request ListContracts")

	filters := make([]*query.ListContractsFilter, len(req.Filters))

	for i, filter := range req.Filters {
		filters[i] = &query.ListContractsFilter{
			ContentHash: filter.ContentHash,
			GrantType:   convertGrantType(filter.GrantType),
		}
	}

	res, err := s.app.Queries.GetContracts.Handle(ctx, filters)
	if err != nil {
		s.logger.Error("get contracts query", err)
		return nil, ResponseFromError(err)
	}

	contracts := make([]*api.ListContractsResponse_Contract, len(res))

	for i, c := range res {
		peers := make(map[string]*api.ListContractsResponse_Peer)
		for _, peer := range c.Peers {
			peers[peer.ID] = &api.ListContractsResponse_Peer{
				Id:   peer.ID,
				Name: peer.Name,
			}
		}

		grants := make([]*api.Grant, 0)

		if c.PeerRegistrationGrant != nil {
			grant := convertPeerRegistrationGrant(c.PeerRegistrationGrant)
			grants = append(grants, grant)
		}

		for _, grant := range c.ServicePublicationGrants {
			convertedGrant := convertServicePublicationGrant(grant)
			grants = append(grants, convertedGrant)
		}

		for _, grant := range c.ServiceConnectionGrants {
			convertedGrant := convertServiceConnectionGrant(grant)
			grants = append(grants, convertedGrant)
		}

		for _, grant := range c.DelegatedServiceConnectionGrants {
			convertedGrant := convertDelegatedServiceConnectionGrant(grant)
			grants = append(grants, convertedGrant)
		}

		for _, grant := range c.DelegatedServicePublicationGrants {
			convertedGrant := convertDelegatedServicePublicationGrant(grant)
			grants = append(grants, convertedGrant)
		}

		contracts[i] = &api.ListContractsResponse_Contract{
			Content: &api.ContractContent{
				Id:      c.ID,
				GroupId: c.GroupID,
				Validity: &api.Validity{
					NotBefore: c.NotBefore.Unix(),
					NotAfter:  c.NotAfter.Unix(),
				},
				HashAlgorithm: convertHashAlgorithm(c.HashAlgorithm),
				CreatedAt:     c.CreatedAt.Unix(),
				Grants:        grants,
			},
			Hash:        c.Hash,
			Peers:       peers,
			HasRejected: c.HasRejected,
			HasAccepted: c.HasAccepted,
			HasRevoked:  c.HasRevoked,
			Signatures: &api.ListContractsResponse_Signatures{
				Accept: convertToSignatures(c.AcceptSignatures),
				Revoke: convertToSignatures(c.RevokeSignatures),
				Reject: convertToSignatures(c.RejectSignatures),
			},
		}
	}

	return &api.ListContractsResponse{
		Contracts: contracts,
	}, nil
}

func convertToSignatures(signatures map[string]query.Signature) map[string]*api.ListContractsResponse_Signature {
	convertedSignatures := map[string]*api.ListContractsResponse_Signature{}

	for p, s := range signatures {
		convertedSignatures[p] = &api.ListContractsResponse_Signature{
			Peer: &api.ListContractsResponse_Peer{
				Id:   s.Peer.ID,
				Name: s.Peer.Name,
			},
			SignedAt: s.SignedAt.Unix(),
		}
	}

	return convertedSignatures
}

func convertGrantType(grantType api.GrantType) query.GrantType {
	switch grantType {
	case api.GrantType_GRANT_TYPE_PEER_REGISTRATION:
		return query.GrantTypePeerRegistration
	case api.GrantType_GRANT_TYPE_SERVICE_PUBLICATION:
		return query.GrantTypeServicePublication
	case api.GrantType_GRANT_TYPE_SERVICE_CONNECTION:
		return query.GrantTypeServiceConnection
	case api.GrantType_GRANT_TYPE_DELEGATED_SERVICE_CONNECTION:
		return query.GrantTypeDelegatedServiceConnection
	case api.GrantType_GRANT_TYPE_DELEGATED_SERVICE_PUBLICATION:
		return query.GrantTypeDelegatedServicePublication
	default:
		return query.GrantTypeUnspecified
	}
}

func convertPeerRegistrationGrant(grant *query.PeerRegistrationGrant) *api.Grant {
	return &api.Grant{
		Type: api.GrantType_GRANT_TYPE_PEER_REGISTRATION,
		Data: &api.Grant_PeerRegistration{
			PeerRegistration: &api.GrantPeerRegistration{
				Hash: grant.Hash,
				Directory: &api.GrantPeerRegistration_Directory{
					PeerId: grant.DirectoryPeerID,
				},
				Peer: &api.GrantPeerRegistration_Peer{
					Id:   grant.PeerID,
					Name: grant.PeerName,
				},
			},
		},
	}
}

func convertServicePublicationGrant(grant *query.ServicePublicationGrant) *api.Grant {
	return &api.Grant{
		Type: api.GrantType_GRANT_TYPE_SERVICE_PUBLICATION,
		Data: &api.Grant_ServicePublication{
			ServicePublication: &api.GrantServicePublication{
				Hash: grant.Hash,
				Directory: &api.GrantServicePublication_Directory{
					PeerId: grant.DirectoryPeerID,
				},
				Service: &api.GrantServicePublication_Service{
					PeerId: grant.ServicePeerID,
					Name:   grant.ServiceName,
				},
			},
		},
	}
}

func convertServiceConnectionGrant(grant *query.ServiceConnectionGrant) *api.Grant {
	return &api.Grant{
		Type: api.GrantType_GRANT_TYPE_SERVICE_CONNECTION,
		Data: &api.Grant_ServiceConnection{
			ServiceConnection: &api.GrantServiceConnection{
				Hash: grant.Hash,
				Outway: &api.GrantServiceConnection_Outway{
					PeerId:                grant.OutwayPeerID,
					CertificateThumbprint: grant.OutwayCertificateThumbprint,
				},
				Service: &api.GrantServiceConnection_Service{
					PeerId: grant.ServicePeerID,
					Name:   grant.ServiceName,
				},
			},
		},
	}
}

func convertDelegatedServiceConnectionGrant(grant *query.DelegatedServiceConnectionGrant) *api.Grant {
	return &api.Grant{
		Type: api.GrantType_GRANT_TYPE_DELEGATED_SERVICE_CONNECTION,
		Data: &api.Grant_DelegatedServiceConnection{
			DelegatedServiceConnection: &api.GrantDelegatedServiceConnection{
				Hash: grant.Hash,
				Outway: &api.GrantDelegatedServiceConnection_Outway{
					PeerId:                grant.OutwayPeerID,
					CertificateThumbprint: grant.OutwayCertificateThumbprint,
				},
				Service: &api.GrantDelegatedServiceConnection_Service{
					PeerId: grant.ServicePeerID,
					Name:   grant.ServiceName,
				},
				Delegator: &api.GrantDelegatedServiceConnection_Delegator{
					PeerId: grant.DelegatorPeerID,
				},
			},
		},
	}
}

func convertDelegatedServicePublicationGrant(grant *query.DelegatedServicePublicationGrant) *api.Grant {
	return &api.Grant{
		Type: api.GrantType_GRANT_TYPE_DELEGATED_SERVICE_PUBLICATION,
		Data: &api.Grant_DelegatedServicePublication{
			DelegatedServicePublication: &api.GrantDelegatedServicePublication{
				Hash: grant.Hash,
				Directory: &api.GrantDelegatedServicePublication_Directory{
					PeerId: grant.DirectoryPeerID,
				},
				Service: &api.GrantDelegatedServicePublication_Service{
					PeerId: grant.ServicePeerID,
					Name:   grant.ServiceName,
				},
				Delegator: &api.GrantDelegatedServicePublication_Delegator{
					PeerId: grant.DelegatorPeerID,
				},
			},
		},
	}
}

func convertHashAlgorithm(algorithm query.HashAlg) api.HashAlgorithm {
	switch algorithm {
	case query.HashAlgSHA3_512:
		return api.HashAlgorithm_HASH_ALGORITHM_SHA3_512
	default:
		return api.HashAlgorithm_HASH_ALGORITHM_UNSPECIFIED
	}
}
