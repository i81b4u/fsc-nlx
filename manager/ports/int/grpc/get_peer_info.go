// Copyright © VNG Realisatie 2023
// Licensed under the EUPL

package internalgrpc

import (
	"context"

	"go.nlx.io/nlx/manager/ports/int/grpc/api"
)

func (s *Server) GetPeerInfo(ctx context.Context, _ *api.GetPeerInfoRequest) (*api.GetPeerInfoResponse, error) {
	s.logger.Info("rpc request GetPeerInfo")

	peer := s.app.Queries.GetPeerInfo.Handle(ctx)

	return &api.GetPeerInfoResponse{
		Peer: &api.GetPeerInfoResponse_Peer{
			Id:   peer.ID,
			Name: peer.Name,
		},
	}, nil
}
