// Copyright © VNG Realisatie 2023
// Licensed under the EUPL

package internalgrpc

import (
	"context"

	"go.nlx.io/nlx/manager/ports/int/grpc/api"
)

func (s *Server) GetCertificate(ctx context.Context, req *api.GetCertificateRequest) (*api.GetCertificateResponse, error) {
	s.logger.Info("rpc request GetCertificate")

	cert, err := s.app.Queries.GetCertificate.Handle(ctx, req.Thumbprint)
	if err != nil {
		return nil, ResponseFromError(err)
	}

	return &api.GetCertificateResponse{CertificateWithChain: cert.RawDERs}, nil
}
