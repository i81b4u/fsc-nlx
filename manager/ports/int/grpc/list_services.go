// Copyright © VNG Realisatie 2023
// Licensed under the EUPL

package internalgrpc

import (
	"context"
	"fmt"

	"go.nlx.io/nlx/manager/apps/int/query"
	"go.nlx.io/nlx/manager/ports/int/grpc/api"
)

func (s *Server) ListServices(ctx context.Context, req *api.ListServicesRequest) (*api.ListServicesResponse, error) {
	s.logger.Info("rpc request ListServices")

	res, err := s.app.Queries.GetServices.Handle(ctx)
	if err != nil {
		return nil, ResponseFromError(err)
	}

	services := make([]*api.ListServicesResponse_Service, len(res))

	for i, srvc := range res {
		switch service := srvc.(type) {
		case *query.DelegatedService:
			services[i] = &api.ListServicesResponse_Service{
				Data: &api.ListServicesResponse_Service_DelegatedService_{
					DelegatedService: &api.ListServicesResponse_Service_DelegatedService{
						Name: service.Name,
						Provider: &api.ListServicesResponse_Peer{
							Id:   service.PeerID,
							Name: service.PeerName,
						},
						Delegator: &api.ListServicesResponse_Peer{
							Id:   service.DelegatorID,
							Name: service.DelegatorName,
						},
					},
				},
			}
		case *query.Service:
			services[i] = &api.ListServicesResponse_Service{
				Data: &api.ListServicesResponse_Service_Service_{
					Service: &api.ListServicesResponse_Service_Service{
						Name: service.Name,
						Provider: &api.ListServicesResponse_Peer{
							Id:   service.PeerID,
							Name: service.PeerName,
						},
					},
				},
			}
		default:
			s.logger.Info(fmt.Sprintf("unknown service type %v", services))
		}
	}

	return &api.ListServicesResponse{
		Services: services,
	}, nil
}
