// Copyright © VNG Realisatie 2023
// Licensed under the EUPL

package internalgrpc

import (
	"context"

	"go.nlx.io/nlx/manager/apps/int/query"
	"go.nlx.io/nlx/manager/ports/int/grpc/api"
)

func (s *Server) GetServiceEndpointURL(ctx context.Context, req *api.GetServiceEndpointURLRequest) (*api.GetServiceEndpointURLResponse, error) {
	s.logger.Info("rpc request GetServiceEndpointURLRequest")

	endpointURL, err := s.app.Queries.GetServiceEndpointURL.Handle(ctx, &query.GetServiceEndpointURLHandlerArgs{
		InwayAddress: req.InwayAddress,
		ServiceName:  req.ServiceName,
	})
	if err != nil {
		return nil, ResponseFromError(err)
	}

	return &api.GetServiceEndpointURLResponse{ServiceEndpointUrl: endpointURL}, nil
}
