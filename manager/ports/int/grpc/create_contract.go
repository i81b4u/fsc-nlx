// Copyright © VNG Realisatie 2023
// Licensed under the EUPL

package internalgrpc

import (
	"context"
	"time"

	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"

	"go.nlx.io/nlx/manager/apps/int/command"
	"go.nlx.io/nlx/manager/ports/int/grpc/api"
)

func (s *Server) CreateContract(ctx context.Context, req *api.CreateContractRequest) (*api.CreateContractResponse, error) {
	s.logger.Info("rpc request CreateContract")

	grants := make([]interface{}, 0)

	for _, grant := range req.ContractContent.Grants {
		switch data := grant.Data.(type) {
		case *api.Grant_PeerRegistration:
			grants = append(grants, &command.GrantPeerRegistrationArgs{
				DirectoryPeerID: data.PeerRegistration.Directory.PeerId,
				PeerID:          data.PeerRegistration.Peer.Id,
				PeerName:        data.PeerRegistration.Peer.Name,
			})
		case *api.Grant_ServicePublication:
			grants = append(grants, &command.GrantPublicationArgs{
				DirectoryPeerID: data.ServicePublication.Directory.PeerId,
				ServicePeerID:   data.ServicePublication.Service.PeerId,
				ServiceName:     data.ServicePublication.Service.Name,
			})
		case *api.Grant_ServiceConnection:
			grants = append(grants, &command.GrantConnectionArgs{
				CertificateThumbprint: data.ServiceConnection.Outway.CertificateThumbprint,
				OutwayPeerID:          data.ServiceConnection.Outway.PeerId,
				ServicePeerID:         data.ServiceConnection.Service.PeerId,
				ServiceName:           data.ServiceConnection.Service.Name,
			})
		case *api.Grant_DelegatedServiceConnection:
			grants = append(grants, &command.GrantDelegatedServiceConnectionArgs{
				OutwayCertificateThumbprint: data.DelegatedServiceConnection.Outway.CertificateThumbprint,
				OutwayPeerID:                data.DelegatedServiceConnection.Outway.PeerId,
				ServicePeerID:               data.DelegatedServiceConnection.Service.PeerId,
				ServiceName:                 data.DelegatedServiceConnection.Service.Name,
				DelegatorPeerID:             data.DelegatedServiceConnection.Delegator.PeerId,
			})
		case *api.Grant_DelegatedServicePublication:
			grants = append(grants, &command.GrantDelegatedServicePublicationArgs{
				DelegatorPeerID: data.DelegatedServicePublication.Delegator.PeerId,
				DirectoryPeerID: data.DelegatedServicePublication.Directory.PeerId,
				ServicePeerID:   data.DelegatedServicePublication.Service.PeerId,
				ServiceName:     data.DelegatedServicePublication.Service.Name,
			})
		default:
			return nil, status.Errorf(codes.InvalidArgument, "invalid grant type")
		}
	}

	contentHash, err := s.app.Commands.CreateContract.Handle(ctx, &command.CreateContractHandlerArgs{
		HashAlgorithm:     int(req.ContractContent.HashAlgorithm.Number()),
		ID:                req.ContractContent.Id,
		GroupID:           req.ContractContent.GroupId,
		ContractNotBefore: time.Unix(req.ContractContent.Validity.NotBefore, 0),
		ContractNotAfter:  time.Unix(req.ContractContent.Validity.NotAfter, 0),
		CreatedAt:         time.Unix(req.ContractContent.CreatedAt, 0),
		Grants:            grants,
	})
	if err != nil {
		s.logger.Error("error executing create contract command", err)
		return nil, ResponseFromError(err)
	}

	return &api.CreateContractResponse{
		ContentHash: contentHash,
	}, nil
}
