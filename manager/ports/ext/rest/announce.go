// Copyright © VNG Realisatie 2023
// Licensed under the EUPL

package rest

import (
	"context"

	"go.nlx.io/nlx/manager/apps/ext/command"
	"go.nlx.io/nlx/manager/ports/ext/rest/api"
)

func (s *Server) Announce(ctx context.Context, _ api.AnnounceRequestObject) (api.AnnounceResponseObject, error) {
	peer, err := getConnectingPeerFromCtx(ctx)
	if err != nil {
		return nil, err
	}

	peerArgs := &command.PeerArgs{
		ID:             peer.id,
		Name:           peer.name,
		ManagerAddress: peer.managerAddress,
	}

	err = s.app.Commands.Announce.Handle(ctx, peerArgs)
	if err != nil {
		s.logger.Error("could not announce peer", err)
		return nil, err
	}

	return api.Announce200Response{}, nil
}
