// Copyright © VNG Realisatie 2023
// Licensed under the EUPL

package rest

import (
	"context"
	"fmt"

	"go.nlx.io/nlx/manager/apps/ext/query"
	"go.nlx.io/nlx/manager/ports/ext/rest/api"
	"go.nlx.io/nlx/manager/ports/ext/rest/api/models"
)

func (s *Server) GetContracts(ctx context.Context, request api.GetContractsRequestObject) (api.GetContractsResponseObject, error) {
	peer, err := getConnectingPeerFromCtx(ctx)
	if err != nil {
		return nil, err
	}

	var limit uint32

	if request.Params.Limit != nil {
		limit = uint32(*request.Params.Limit)
	}

	var queryGrantType *query.GrantType

	if request.Params.GrantType != nil {
		var gt query.GrantType

		gt, err = convertGrantType(*request.Params.GrantType)
		if err != nil {
			return nil, err
		}

		queryGrantType = &gt
	}

	grantHashes := make([]string, 0)
	if request.Params.GrantHash != nil {
		grantHashes = *request.Params.GrantHash
	}

	var cursor string
	if request.Params.Cursor != nil {
		cursor = *request.Params.Cursor
	}

	contracts, err := s.app.Queries.ListContracts.Handle(ctx, &query.ListContractsHandlerArgs{
		PeerID:            peer.id,
		GrantType:         queryGrantType,
		PaginationLimit:   limit,
		GrantHashes:       grantHashes,
		PaginationStartID: cursor,
	})
	if err != nil {
		return nil, err
	}

	response := api.GetContracts200JSONResponse{
		Contracts: make([]models.FSCCoreContract, len(contracts)),
	}

	for i, contract := range contracts {
		var convertedContract *models.FSCCoreContract

		convertedContract, err = convertContract(contract)
		if err != nil {
			return nil, err
		}

		response.Contracts[i] = *convertedContract
	}

	nextCursor := determineNextCursorForContracts(response.Contracts)

	response.Pagination = models.FSCCorePaginationResult{NextCursor: &nextCursor}

	return response, nil
}

func convertContract(contract *query.Contract) (*models.FSCCoreContract, error) {
	notAfter := contract.ContractNotAfter.Unix()
	notBefore := contract.ContractNotBefore.Unix()
	createdAt := contract.CreatedAt.Unix()

	grants, err := convertGrants(contract)
	if err != nil {
		return nil, err
	}

	content := models.FSCCoreContractContent{
		Id:            contract.ID,
		CreatedAt:     createdAt,
		Grants:        grants,
		GroupId:       contract.GroupID,
		HashAlgorithm: models.HASHALGORITHMSHA3512,
		Validity:      models.FSCCoreValidity{NotAfter: notAfter, NotBefore: notBefore},
	}

	return &models.FSCCoreContract{
		Content: content,
		Signatures: models.FSCCoreSignatures{
			Accept: contract.SignaturesAccepted,
			Reject: contract.SignaturesRejected,
			Revoke: contract.SignaturesRevoked,
		},
	}, nil
}

//nolint:gocyclo // complex function because of the number of grant types
func convertGrants(contract *query.Contract) ([]models.FSCCoreGrant, error) {
	grants := make([]models.FSCCoreGrant, 0)

	for _, g := range contract.ServiceConnectionGrants {
		grant, err := convertServiceConnectionGrant(g)
		if err != nil {
			return nil, err
		}

		grants = append(grants, *grant)
	}

	for _, g := range contract.ServicePublicationGrants {
		grant, err := convertServicePublicationGrant(g)
		if err != nil {
			return nil, err
		}

		grants = append(grants, *grant)
	}

	for _, g := range contract.PeerRegistrationGrants {
		grant, err := convertPeerRegistrationGrant(g)
		if err != nil {
			return nil, err
		}

		grants = append(grants, *grant)
	}

	for _, g := range contract.DelegatedServiceConnectionGrants {
		grant, err := convertDelegatedServiceConnectionGrant(g)
		if err != nil {
			return nil, err
		}

		grants = append(grants, *grant)
	}

	for _, g := range contract.DelegatedServicePublicationGrants {
		grant, err := convertDelegatedServicePublicationGrant(g)
		if err != nil {
			return nil, err
		}

		grants = append(grants, *grant)
	}

	return grants, nil
}

func convertPeerRegistrationGrant(grant *query.PeerRegistrationGrant) (*models.FSCCoreGrant, error) {
	data := models.FSCCoreGrant_Data{}
	err := data.FromFSCCoreGrantPeerRegistration(models.FSCCoreGrantPeerRegistration{
		Directory: models.FSCCoreDirectory{PeerId: grant.DirectoryPeerID},
		Peer: models.FSCCorePeerRegistration{
			Id:   grant.PeerID,
			Name: grant.PeerName,
		},
	})

	if err != nil {
		return nil, err
	}

	return &models.FSCCoreGrant{
		Data: data,
		Type: models.GRANTTYPEPEERREGISTRATION,
	}, nil
}

func convertServicePublicationGrant(grant *query.ServicePublicationGrant) (*models.FSCCoreGrant, error) {
	data := models.FSCCoreGrant_Data{}
	err := data.FromFSCCoreGrantServicePublication(models.FSCCoreGrantServicePublication{
		Directory: models.FSCCoreDirectory{PeerId: grant.DirectoryPeerID},
		Service: models.FSCCoreServicePublication{
			PeerId: grant.ServicePeerID,
			Name:   grant.ServiceName,
		},
	})

	if err != nil {
		return nil, err
	}

	return &models.FSCCoreGrant{
		Data: data,
		Type: models.GRANTTYPESERVICEPUBLICATION,
	}, nil
}

func convertServiceConnectionGrant(grant *query.ServiceConnectionGrant) (*models.FSCCoreGrant, error) {
	data := models.FSCCoreGrant_Data{}
	err := data.FromFSCCoreGrantServiceConnection(models.FSCCoreGrantServiceConnection{
		Outway: models.FSCCoreOutway{
			PeerId:                grant.OutwayPeerID,
			CertificateThumbprint: grant.OutwayCertificateThumbprint,
		},
		Service: models.FSCCoreService{
			PeerId: grant.ServicePeerID,
			Name:   grant.ServiceName,
		},
	})

	if err != nil {
		return nil, err
	}

	return &models.FSCCoreGrant{
		Data: data,
		Type: models.GRANTTYPESERVICECONNECTION,
	}, nil
}

func convertDelegatedServiceConnectionGrant(grant *query.DelegatedServiceConnectionGrant) (*models.FSCCoreGrant, error) {
	data := models.FSCCoreGrant_Data{}
	err := data.FromFSCCoreGrantDelegatedServiceConnection(models.FSCCoreGrantDelegatedServiceConnection{
		Delegator: models.FSCCoreDelegator{PeerId: grant.DelegatorPeerID},
		Service: models.FSCCoreService{
			PeerId: grant.ServicePeerID,
			Name:   grant.ServiceName,
		},
		Outway: models.FSCCoreOutway{
			PeerId:                grant.OutwayPeerID,
			CertificateThumbprint: grant.OutwayCertificateThumbprint,
		},
	})

	if err != nil {
		return nil, err
	}

	return &models.FSCCoreGrant{
		Data: data,
		Type: models.GRANTTYPEDELEGATEDSERVICECONNECTION,
	}, nil
}

func convertDelegatedServicePublicationGrant(grant *query.DelegatedServicePublicationGrant) (*models.FSCCoreGrant, error) {
	data := models.FSCCoreGrant_Data{}
	err := data.FromFSCCoreGrantDelegatedServicePublication(models.FSCCoreGrantDelegatedServicePublication{
		Delegator: models.FSCCoreDelegator{PeerId: grant.DelegatorPeerID},
		Service: models.FSCCoreServicePublication{
			PeerId: grant.ServicePeerID,
			Name:   grant.ServiceName,
		},
		Directory: models.FSCCoreDirectory{
			PeerId: grant.DelegatorPeerID,
		},
	})

	if err != nil {
		return nil, err
	}

	return &models.FSCCoreGrant{
		Data: data,
		Type: models.GRANTTYPEDELEGATEDSERVICEPUBLICATION,
	}, nil
}

func determineNextCursorForContracts(contracts []models.FSCCoreContract) string {
	if len(contracts) == 0 {
		return ""
	}

	return contracts[len(contracts)-1].Content.Id
}

func convertGrantType(grantType models.FSCCoreGrantType) (query.GrantType, error) {
	switch grantType {
	case models.GRANTTYPESERVICECONNECTION:
		return query.GrantTypeServiceConnection, nil
	case models.GRANTTYPESERVICEPUBLICATION:
		return query.GrantTypeServicePublication, nil
	case models.GRANTTYPEPEERREGISTRATION:
		return query.GrantTypePeerRegistration, nil
	case models.GRANTTYPEDELEGATEDSERVICECONNECTION:
		return query.GrantTypeDelegatedServiceConnection, nil
	case models.GRANTTYPEDELEGATEDSERVICEPUBLICATION:
		return query.GrantTypeDelegatedServicePublication, nil
	default:
		return query.GrantTypeUnspecified, fmt.Errorf("unknown grant type. %q", grantType)
	}
}
