// Copyright © VNG Realisatie 2023
// Licensed under the EUPL

package rest

import (
	"context"
	"encoding/base64"

	"go.nlx.io/nlx/manager/apps/ext/query"
	"go.nlx.io/nlx/manager/ports/ext/rest/api"
	"go.nlx.io/nlx/manager/ports/ext/rest/api/models"
)

func (s *Server) GetJSONWebKeySet(ctx context.Context, _ api.GetJSONWebKeySetRequestObject) (api.GetJSONWebKeySetResponseObject, error) {
	certs, err := s.app.Queries.GetKeySet.Handle(ctx)
	if err != nil {
		s.logger.Error("error executing GetJSONWebKeySet query", err)
		return nil, err
	}

	respCerts := make([]models.FSCCoreJsonWebKey, 0)

	for thumbprint, c := range certs {
		thumbprint := thumbprint

		respCerts = append(respCerts, models.FSCCoreJsonWebKey{
			Kid:     thumbprint,
			Kty:     keyTypeToModel(c.KeyType),
			X5tS256: &thumbprint,
			X5c:     rawDERstoBase64(c.RawDERs),
		})
	}

	return api.GetJSONWebKeySet200JSONResponse{Keys: &respCerts}, nil
}

func rawDERstoBase64(rawDERs [][]byte) []string {
	certs := make([]string, len(rawDERs))

	for i, c := range rawDERs {
		certs[i] = base64.URLEncoding.EncodeToString(c)
	}

	return certs
}

func keyTypeToModel(c query.CertKeyType) models.FSCCoreJsonWebKeyKty {
	switch c {
	case query.CertKeyTypeEC:
		return models.EC
	case query.CertKeyTypeRSA:
		return models.RSA
	default:
		return ""
	}
}
