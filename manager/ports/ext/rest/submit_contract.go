// Copyright © VNG Realisatie 2023
// Licensed under the EUPL

package rest

import (
	"context"
	"fmt"
	"time"

	"github.com/google/uuid"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"

	"go.nlx.io/nlx/manager/apps/ext/command"
	"go.nlx.io/nlx/manager/domain/contract"
	"go.nlx.io/nlx/manager/ports/ext/rest/api"
	"go.nlx.io/nlx/manager/ports/ext/rest/api/models"
)

func (s *Server) SubmitContract(ctx context.Context, request api.SubmitContractRequestObject) (api.SubmitContractResponseObject, error) {
	peer, err := getConnectingPeerFromCtx(ctx)
	if err != nil {
		return nil, err
	}

	contractContent, err := contractContentToArgs(&request.Body.ContractContent)
	if err != nil {
		return nil, err
	}

	err = s.app.Commands.SubmitContract.Handle(ctx, &command.HandleSubmitContractArgs{
		ContractContent: contractContent,
		Signature:       request.Body.Signature,
		SubmittedByPeer: &command.PeerArgs{
			ID:             peer.id,
			Name:           peer.name,
			ManagerAddress: peer.managerAddress,
		},
	})
	if err != nil {
		s.logger.Error("could not submit contract", err)
		return nil, err
	}

	return api.SubmitContract201Response{}, nil
}

func contractContentToArgs(content *models.FSCCoreContractContent) (*command.ContractContentArgs, error) {
	grants, err := mapRestToCommandGrants(content.Grants)
	if err != nil {
		return nil, fmt.Errorf("could not map grants: %w", err)
	}

	contractID, err := uuid.Parse(content.Id)
	if err != nil {
		return nil, fmt.Errorf("could not parse contract ID. %s", err)
	}

	uuidBytes, err := contractID.MarshalBinary()
	if err != nil {
		return nil, fmt.Errorf("could not get bytes from uuid. %s", err)
	}

	var hashAlgorithm contract.HashAlg

	switch content.HashAlgorithm {
	case models.HASHALGORITHMSHA3512:
		hashAlgorithm = contract.HashAlgSHA3_512
	default:
		return nil, fmt.Errorf("unknown hash algorithm")
	}

	return &command.ContractContentArgs{
		HashAlgorithm: int(hashAlgorithm),
		ID:            uuidBytes,
		GroupID:       content.GroupId,
		NotBefore:     time.Unix(content.Validity.NotBefore, 0),
		NotAfter:      time.Unix(content.Validity.NotAfter, 0),
		Grants:        grants,
		CreatedAt:     time.Unix(content.CreatedAt, 0),
	}, nil
}

//nolint:gocyclo // difficult to improve complexity without effecting the readability
func mapRestToCommandGrants(modelGrants []models.FSCCoreGrant) ([]interface{}, error) {
	grants := make([]interface{}, 0)

	for _, grant := range modelGrants {
		switch grant.Type {
		case models.GRANTTYPEPEERREGISTRATION:
			data, err := grant.Data.AsFSCCoreGrantPeerRegistration()
			if err != nil {
				return nil, err
			}

			grants = append(grants, &command.GrantPeerRegistrationArgs{
				DirectoryPeerID: data.Directory.PeerId,
				PeerID:          data.Peer.Id,
				PeerName:        data.Peer.Name,
			})
		case models.GRANTTYPESERVICEPUBLICATION:
			data, err := grant.Data.AsFSCCoreGrantServicePublication()
			if err != nil {
				return nil, err
			}

			grants = append(grants, &command.GrantServicePublicationArgs{
				DirectoryPeerID: data.Directory.PeerId,
				ServicePeerID:   data.Service.PeerId,
				ServiceName:     data.Service.Name,
			})
		case models.GRANTTYPESERVICECONNECTION:
			data, err := grant.Data.AsFSCCoreGrantServiceConnection()
			if err != nil {
				return nil, err
			}

			grants = append(grants, &command.GrantServiceConnectionArgs{
				CertificateThumbprint: data.Outway.CertificateThumbprint,
				OutwayPeerID:          data.Outway.PeerId,
				ServicePeerID:         data.Service.PeerId,
				ServiceName:           data.Service.Name,
			})
		case models.GRANTTYPEDELEGATEDSERVICECONNECTION:
			data, err := grant.Data.AsFSCCoreGrantDelegatedServiceConnection()
			if err != nil {
				return nil, err
			}

			grants = append(grants, &command.GrantDelegatedServiceConnectionArgs{
				OutwayCertificateThumbprint: data.Outway.CertificateThumbprint,
				OutwayPeerID:                data.Outway.PeerId,
				ServicePeerID:               data.Service.PeerId,
				ServiceName:                 data.Service.Name,
				DelegatorPeerID:             data.Delegator.PeerId,
			})
		case models.GRANTTYPEDELEGATEDSERVICEPUBLICATION:
			data, err := grant.Data.AsFSCCoreGrantDelegatedServicePublication()
			if err != nil {
				return nil, err
			}

			grants = append(grants, &command.GrantDelegatedServicePublicationArgs{
				DirectoryPeerID: data.Directory.PeerId,
				ServicePeerID:   data.Service.PeerId,
				ServiceName:     data.Service.Name,
				DelegatorPeerID: data.Delegator.PeerId,
			})
		default:
			return nil, status.Errorf(codes.InvalidArgument, "invalid grant type")
		}
	}

	return grants, nil
}
