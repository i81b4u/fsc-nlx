// Copyright © VNG Realisatie 2023
// Licensed under the EUPL

package rest

import (
	"context"

	"github.com/pkg/errors"

	"go.nlx.io/nlx/manager/apps/ext/query"
	"go.nlx.io/nlx/manager/ports/ext/rest/api"
	"go.nlx.io/nlx/manager/ports/ext/rest/api/models"
)

func (s *Server) GetToken(ctx context.Context, req api.GetTokenRequestObject) (api.GetTokenResponseObject, error) {
	peer, err := getConnectingPeerFromCtx(ctx)
	if err != nil {
		return nil, err
	}

	resp, err := s.app.Queries.GetToken.Handle(ctx, &query.GetTokenHandlerArgs{
		ConnectingPeerID: peer.id,
		Scope:            req.Body.Scope,
		OAuthGrantType:   grantTypeToQuery(req.Body.GrantType),
	})
	if err != nil {
		return nil, errors.Wrap(err, "could not get token from query")
	}

	return api.GetToken200JSONResponse{
		AccessToken: resp.Token,
		TokenType:   tokenTypeToModel(resp.TokenType),
	}, nil
}

func grantTypeToQuery(t models.FSCCoreOAuthGrantType) query.OAuthGrantType {
	switch t {
	case models.ClientCredentials:
		return query.OAuthGrantTypeClientCredentials
	default:
		return query.OAuthGrantTypeInvalid
	}
}

func tokenTypeToModel(t query.OAuthTokenType) models.FSCCoreOAuthTokenType {
	switch t {
	case query.OAuthTokenTypeBearer:
		return models.Bearer
	default:
		return ""
	}
}
