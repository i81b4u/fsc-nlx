// Copyright © VNG Realisatie 2023
// Licensed under the EUPL

package rest

import (
	"context"

	"go.nlx.io/nlx/manager/apps/ext/query"
	"go.nlx.io/nlx/manager/ports/ext/rest/api"
	"go.nlx.io/nlx/manager/ports/ext/rest/api/models"
)

func (s *Server) GetPeers(ctx context.Context, request api.GetPeersRequestObject) (api.GetPeersResponseObject, error) {
	sortOrder := query.SortOrderAscending

	if request.Params.SortOrder != nil {
		switch *request.Params.SortOrder {
		case models.FSCCoreSortOrderSORTORDERASCENDING:
			sortOrder = query.SortOrderAscending
		case models.FSCCoreSortOrderSORTORDERDESCENDING:
			sortOrder = query.SortOrderDescending
		}
	}

	var peerIDs []string

	if request.Params.PeerId != nil {
		peerIDs = append(peerIDs, *request.Params.PeerId...)
	}

	var cursor string
	if request.Params.Cursor != nil {
		cursor = *request.Params.Cursor
	}

	var limit uint32

	if request.Params.Limit != nil {
		limit = uint32(*request.Params.Limit)
	}

	records, err := s.app.Queries.ListPeers.Handle(ctx, &query.ListPeersHandlerArgs{
		PaginationLimit:     limit,
		PaginationStartID:   cursor,
		PaginationSortOrder: sortOrder,
		PeerIDs:             peerIDs,
	})

	if err != nil {
		s.logger.Error("error executing list peers query", err)

		return nil, err
	}

	peers := make([]models.FSCCorePeer, len(records))
	for i, p := range records {
		peers[i] = models.FSCCorePeer{
			Id:             p.ID,
			ManagerAddress: p.ManagerAddress,
			Name:           p.Name,
		}
	}

	nextCursor := determineNextCursorForPeers(peers)

	return api.GetPeers200JSONResponse{
		Pagination: models.FSCCorePaginationResult{
			NextCursor: &nextCursor,
		},
		Peers: peers}, nil
}

func determineNextCursorForPeers(peers []models.FSCCorePeer) string {
	if len(peers) == 0 {
		return ""
	}

	return peers[len(peers)-1].Id
}
