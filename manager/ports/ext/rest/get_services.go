// Copyright © VNG Realisatie 2023
// Licensed under the EUPL

package rest

import (
	"context"

	"go.nlx.io/nlx/manager/apps/ext/query"
	"go.nlx.io/nlx/manager/ports/ext/rest/api"
	"go.nlx.io/nlx/manager/ports/ext/rest/api/models"
)

func (s *Server) GetServices(ctx context.Context, request api.GetServicesRequestObject) (api.GetServicesResponseObject, error) {
	sortOrder := query.SortOrderAscending

	if request.Params.SortOrder != nil {
		switch *request.Params.SortOrder {
		case models.FSCCoreSortOrderSORTORDERASCENDING:
			sortOrder = query.SortOrderAscending
		case models.FSCCoreSortOrderSORTORDERDESCENDING:
			sortOrder = query.SortOrderDescending
		}
	}

	var cursor string
	if request.Params.Cursor != nil {
		cursor = *request.Params.Cursor
	}

	var limit uint32

	if request.Params.Limit != nil {
		limit = uint32(*request.Params.Limit)
	}

	var peerIDFilter string

	if request.Params.PeerId != nil {
		peerIDFilter = *request.Params.PeerId
	}

	var serviceNameFilter string

	if request.Params.ServiceName != nil {
		serviceNameFilter = *request.Params.ServiceName
	}

	records, err := s.app.Queries.ListServices.Handle(ctx, &query.ListServicesHandlerArgs{
		PaginationStartID:   cursor,
		PaginationLimit:     limit,
		PaginationSortOrder: sortOrder,
		PeerIDFilter:        peerIDFilter,
		ServiceNameFilter:   serviceNameFilter,
	})
	if err != nil {
		s.logger.Error("error executing list services query", err)
		return nil, err
	}

	services := make([]models.FSCCoreServiceListing, len(records))

	var serviceType models.FSCCoreServiceListingType

	for i, s := range records {
		data := models.FSCCoreServiceListing_Data{}

		if s.DelegatorPeerID == "" {
			err = data.FromFSCCoreServiceListingService(models.FSCCoreServiceListingService{
				Name: s.Name,
				Peer: models.FSCCorePeer{
					Id:             s.PeerID,
					Name:           s.PeerName,
					ManagerAddress: s.PeerManagerAddress,
				},
			})

			serviceType = models.TYPESERVICE
		} else {
			err = data.FromFSCCoreServiceListingDelegatedService(models.FSCCoreServiceListingDelegatedService{
				Name: s.Name,
				Peer: models.FSCCorePeer{
					Id:             s.PeerID,
					Name:           s.PeerName,
					ManagerAddress: s.PeerManagerAddress,
				},
				Delegator: models.FSCCoreDelegatorServiceListing{
					PeerId:   s.DelegatorPeerID,
					PeerName: s.DelegatorPeerName,
				},
			})

			serviceType = models.TYPEDELEGATEDSERVICE
		}

		if err != nil {
			return nil, err
		}

		services[i] = models.FSCCoreServiceListing{
			Type: serviceType,
			Data: data,
		}
	}

	nextCursor := determineNextCursorForServices(records)

	return api.GetServices200JSONResponse{
		Pagination: models.FSCCorePaginationResult{
			NextCursor: &nextCursor,
		},
		Services: services,
	}, nil
}

func determineNextCursorForServices(services []*query.Service) string {
	if len(services) == 0 {
		return ""
	}

	return services[len(services)-1].ContractID
}
