// Copyright © VNG Realisatie 2023
// Licensed under the EUPL

package rest

import (
	"context"

	"go.nlx.io/nlx/manager/apps/ext/command"
	"go.nlx.io/nlx/manager/ports/ext/rest/api"
)

func (s *Server) RevokeContract(ctx context.Context, request api.RevokeContractRequestObject) (api.RevokeContractResponseObject, error) {
	peer, err := getConnectingPeerFromCtx(ctx)
	if err != nil {
		return nil, err
	}

	contractContent, err := contractContentToArgs(&request.Body.ContractContent)
	if err != nil {
		s.logger.Error("could not parse contract content from request ", err)
		return nil, err
	}

	err = s.app.Commands.RevokeContract.Handle(ctx, &command.HandleRevokeContractArgs{
		ContractContent: contractContent,
		Signature:       request.Body.Signature,
		SubmittedByPeer: &command.PeerArgs{
			ID:             peer.id,
			Name:           peer.name,
			ManagerAddress: peer.managerAddress,
		},
	})
	if err != nil {
		s.logger.Error("could not revoke contract", err)
		return nil, err
	}

	return api.RevokeContract201Response{}, nil
}
