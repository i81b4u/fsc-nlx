// Copyright © VNG Realisatie 2022
// Licensed under the EUPL

package query

import (
	"context"
	"fmt"

	"github.com/pkg/errors"

	"go.nlx.io/nlx/manager/domain/contract"
)

type ListServicesHandler struct {
	repository contract.Repository
}

func NewListServicesHandler(repository contract.Repository) (*ListServicesHandler, error) {
	if repository == nil {
		return nil, fmt.Errorf("repository is required")
	}

	return &ListServicesHandler{
		repository: repository,
	}, nil
}

type ListServicesHandlerArgs struct {
	PaginationStartID   string
	PaginationLimit     uint32
	PaginationSortOrder SortOrder
	PeerIDFilter        string
	ServiceNameFilter   string
}

func (h *ListServicesHandler) Handle(ctx context.Context, args *ListServicesHandlerArgs) ([]*Service, error) {
	sortOrder := contract.SortOrderAscending
	if args.PaginationSortOrder == SortOrderDescending {
		sortOrder = contract.SortOrderDescending
	}

	var peerID *contract.PeerID

	if args.PeerIDFilter != "" {
		var err error

		pID, err := contract.NewPeerID(args.PeerIDFilter)
		if err != nil {
			return nil, err
		}

		peerID = &pID
	}

	services, err := h.repository.ListServices(
		ctx,
		peerID,
		args.ServiceNameFilter,
		args.PaginationStartID,
		args.PaginationLimit,
		sortOrder,
	)
	if err != nil {
		return nil, errors.Wrap(err, "could not retrieve services from repository")
	}

	result := make([]*Service, len(services))

	for i, s := range services {
		result[i] = &Service{
			PeerID:             s.PeerID,
			PeerManagerAddress: s.PeerManagerAddress,
			PeerName:           s.PeerName,
			Name:               s.Name,
			DelegatorPeerID:    s.DelegatorPeerID,
			DelegatorPeerName:  s.DelegatorPeerName,
			ContractID:         s.ContractID,
		}
	}

	return result, nil
}
