// Copyright © VNG Realisatie 2022
// Licensed under the EUPL

package query

import (
	"context"
	"crypto/tls"
	"crypto/x509"
	"fmt"
	"time"

	"github.com/pkg/errors"

	"go.nlx.io/nlx/common/accesstoken"
	"go.nlx.io/nlx/common/clock"
	"go.nlx.io/nlx/manager/adapters/controller"
	"go.nlx.io/nlx/manager/domain/contract"
)

// OAuthGrantType The grant_type of an access token request. FSC only uses client_credentials
type OAuthGrantType string

// OAuthTokenType The type of token. FSC only uses Bearer
type OAuthTokenType string

// Defines values for OAuthGrantType.
const (
	OAuthGrantTypeInvalid           OAuthGrantType = ""
	OAuthGrantTypeClientCredentials OAuthGrantType = "client_credentials"
)

// Defines values for OAuthTokenType.
const (
	OAuthTokenTypeInvalid OAuthTokenType = ""
	OAuthTokenTypeBearer  OAuthTokenType = "bearer"
)

type GetTokenHandler struct {
	clock            clock.Clock
	repo             contract.Repository
	signTokenCert    *tls.Certificate
	tokenCert        *contract.PeerCertificate
	tokenTTL         time.Duration
	controllerClient controller.Controller
}

type GetTokenHandlerArgs struct {
	ConnectingPeerID string
	Scope            string
	OAuthGrantType   OAuthGrantType
}

type NewGetTokenHandlerArgs struct {
	Clock            clock.Clock
	Repo             contract.Repository
	SignTokenCert    *tls.Certificate
	TrustedRootCAs   *x509.CertPool
	TokenTTL         time.Duration
	ManagementClient controller.Controller
}

func NewGetTokenHandler(args *NewGetTokenHandlerArgs) (*GetTokenHandler, error) {
	if args.Clock == nil {
		return nil, fmt.Errorf("clock is required")
	}

	if args.Repo == nil {
		return nil, fmt.Errorf("repo is required")
	}

	if args.SignTokenCert == nil {
		return nil, fmt.Errorf("signTokenWith is required")
	}

	if args.TrustedRootCAs == nil {
		return nil, fmt.Errorf("trustedRootCAs is required")
	}

	if args.ManagementClient == nil {
		return nil, fmt.Errorf("management client is required")
	}

	tokenCert, err := contract.NewPeerCertFromCertificate(args.TrustedRootCAs, args.SignTokenCert.Certificate)
	if err != nil {
		return nil, errors.Wrapf(err, "could not create peer cert, invalid certificate chain")
	}

	return &GetTokenHandler{
		clock:            args.Clock,
		repo:             args.Repo,
		signTokenCert:    args.SignTokenCert,
		tokenCert:        tokenCert,
		tokenTTL:         args.TokenTTL,
		controllerClient: args.ManagementClient,
	}, nil
}

func (h *GetTokenHandler) Handle(ctx context.Context, args *GetTokenHandlerArgs) (*GetTokenResponse, error) {
	connPeer, err := contract.NewPeerID(args.ConnectingPeerID)
	if err != nil {
		return nil, errors.Wrap(err, "invalid connecting peer ID in token handler args")
	}

	if args.OAuthGrantType != OAuthGrantTypeClientCredentials {
		return nil, fmt.Errorf("invalid oauth grant type, only %q is supported", OAuthGrantTypeClientCredentials)
	}

	grantHash, err := contract.DecodeHashFromString(args.Scope)
	if err != nil {
		return nil, errors.Wrap(err, "invalid grant hash in scope arg")
	}

	if grantHash.Type() != contract.HashTypeGrantServiceConnection && grantHash.Type() != contract.HashTypeGrantDelegatedServiceConnection {
		return nil, fmt.Errorf("invalid grant hash type, only hashes of type service connection or delegated service connection are allowed")
	}

	tokenInfo, err := h.repo.GetTokenInfo(ctx, connPeer, h.tokenCert.Peer().ID(), grantHash)
	if err != nil {
		return nil, errors.Wrap(err, "could not get token info from repository")
	}

	inwayAddress, err := h.controllerClient.GetInwayAddressForService(ctx, tokenInfo.ServiceName)
	if err != nil {
		return nil, errors.Wrap(err, "could not get Inway address for service from Controller API")
	}

	now := h.clock.Now()

	token, err := accesstoken.New(&accesstoken.NewTokenArgs{
		GrantHash:                   grantHash.String(),
		OutwayPeerID:                args.ConnectingPeerID,
		OutwayDelegatorPeerID:       tokenInfo.OutwayDelegatorPeerID,
		OutwayCertificateThumbprint: tokenInfo.CertificateThumbprint,
		ServicePeerID:               h.tokenCert.Peer().ID().Value(),
		ServiceName:                 tokenInfo.ServiceName,
		ServiceInwayAddress:         inwayAddress,
		ServiceDelegatorPeerID:      tokenInfo.ServiceDelegatorPeerID,
		ExpiryDate:                  now.Add(h.tokenTTL),
		NotBefore:                   now,
		SignWith: &accesstoken.SignWith{
			CertificateThumbprint: h.tokenCert.CertificateThumbprint().Value(),
			PrivateKey:            h.signTokenCert.PrivateKey,
		},
	})
	if err != nil {
		return nil, errors.Wrap(err, "could not sign token")
	}

	return &GetTokenResponse{
		Token:     token.Value(),
		TokenType: OAuthTokenTypeBearer,
	}, nil
}
