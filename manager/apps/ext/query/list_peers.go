// Copyright © VNG Realisatie 2022
// Licensed under the EUPL

package query

import (
	"context"
	"fmt"

	"github.com/pkg/errors"

	"go.nlx.io/nlx/manager/domain/contract"
)

type LimitParameterMissingError struct {
	m string
}

const maxPeerID = "99999999999999999999"

func newLimitParameterMissingError() *LimitParameterMissingError {
	return &LimitParameterMissingError{
		m: "limit parameter is missing",
	}
}

func (e *LimitParameterMissingError) Error() string {
	return e.m
}

type ListPeersHandler struct {
	repository contract.Repository
}

func NewListPeersHandler(repository contract.Repository) (*ListPeersHandler, error) {
	if repository == nil {
		return nil, fmt.Errorf("repository is required")
	}

	return &ListPeersHandler{
		repository: repository,
	}, nil
}

type ListPeersHandlerArgs struct {
	PaginationStartID   string
	PaginationLimit     uint32
	PaginationSortOrder SortOrder
	PeerIDs             []string
}

//nolint:gocyclo // is complex
func (h *ListPeersHandler) Handle(ctx context.Context, args *ListPeersHandlerArgs) ([]*Peer, error) {
	sortOrder := contract.SortOrderAscending

	if args.PaginationSortOrder == SortOrderDescending {
		sortOrder = contract.SortOrderDescending
	}

	var peers []*contract.Peer

	if len(args.PeerIDs) > 0 {
		peerIDs := contract.PeersIDs{}

		for _, pid := range args.PeerIDs {
			peerID, err := contract.NewPeerID(pid)
			if err != nil {
				return nil, errors.Wrap(err, "invalid peer id")
			}

			peerIDs[peerID] = true
		}

		var err error

		peersInDB, err := h.repository.ListPeersByID(ctx, peerIDs)
		if err != nil {
			return nil, err
		}

		for _, peer := range peersInDB {
			peers = append(peers, peer)
		}
	} else {
		var err error

		if args.PaginationLimit == 0 {
			return nil, newLimitParameterMissingError()
		}

		paginationStart := args.PaginationStartID

		if args.PaginationStartID == "" && args.PaginationSortOrder == SortOrderDescending {
			paginationStart = maxPeerID
		}

		peers, err = h.repository.ListPeers(
			ctx,
			paginationStart,
			args.PaginationLimit,
			sortOrder,
		)
		if err != nil {
			return nil, errors.Wrap(err, "could not retrieve peers from repository")
		}
	}

	result := make([]*Peer, len(peers))

	i := 0

	for _, p := range peers {
		result[i] = &Peer{
			ID:             p.ID().Value(),
			Name:           p.Name().Value(),
			ManagerAddress: p.ManagerAddress().Value(),
		}

		i++
	}

	return result, nil
}
