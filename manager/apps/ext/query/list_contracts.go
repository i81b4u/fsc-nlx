// Copyright © VNG Realisatie 2022
// Licensed under the EUPL

package query

import (
	"context"
	"fmt"

	"github.com/pkg/errors"

	app_errors "go.nlx.io/nlx/manager/apps/ext/errors"
	"go.nlx.io/nlx/manager/domain/contract"
)

type ListContractsHandler struct {
	repository contract.Repository
}

func NewListContractsHandler(repository contract.Repository) (*ListContractsHandler, error) {
	if repository == nil {
		return nil, fmt.Errorf("repository is required")
	}

	return &ListContractsHandler{
		repository: repository,
	}, nil
}

type ListContractsHandlerArgs struct {
	PeerID              string
	GrantType           *GrantType
	GrantHashes         []string
	PaginationStartID   string
	PaginationLimit     uint32
	PaginationSortOrder SortOrder
}

//nolint:gocyclo // is a complex function because of the filters
func (h *ListContractsHandler) Handle(ctx context.Context, args *ListContractsHandlerArgs) ([]*Contract, error) {
	peerID, err := contract.NewPeerID(args.PeerID)
	if err != nil {
		return nil, app_errors.NewIncorrectInputError(fmt.Errorf("invalid peer id: '%s'", args.PeerID))
	}

	sortOrder := contract.SortOrderAscending

	if args.PaginationSortOrder == SortOrderDescending {
		sortOrder = contract.SortOrderDescending
	}

	var contracts []*contract.Contract

	switch {
	case len(args.GrantHashes) > 0 && args.GrantType != nil:
		return nil, app_errors.NewIncorrectInputError(fmt.Errorf("cannot combine grant type and grant hash filters"))
	case args.GrantType != nil:
		contracts, err = getContractsOnGrantType(h.repository, ctx, *args.GrantType, peerID, args.PaginationStartID, args.PaginationLimit, sortOrder)
		if err != nil {
			return nil, fmt.Errorf("failed to get contracts for grant type %q: %w", *args.GrantType, err)
		}
	case len(args.GrantHashes) > 0:
		contracts, err = h.repository.ListContractsForPeerByGrantHashes(ctx, peerID, args.GrantHashes)
		if err != nil {
			return nil, fmt.Errorf("failed to get contracts on grant hashes: %w", err)
		}
	default:
		contracts, err = h.repository.ListContractsForPeer(ctx, peerID, args.PaginationStartID, args.PaginationLimit, sortOrder)
		if err != nil {
			return nil, fmt.Errorf("failed to get contracts for peer %q: %w", peerID.Value(), err)
		}
	}

	result, err := convertContracts(contracts)
	if err != nil {
		return nil, err
	}

	return result, nil
}

func convertContracts(contracts []*contract.Contract) ([]*Contract, error) {
	result := make([]*Contract, len(contracts))

	for i, model := range contracts {
		var convertedContract *Contract

		convertedContract, err := convertContract(model)
		if err != nil {
			return nil, fmt.Errorf("failed to convert contract domain model to contract query model: %w", err)
		}

		result[i] = convertedContract
	}

	return result, nil
}

//nolint:gocyclo // complex function because of the number of grant types
func getContractsOnGrantType(repository contract.Repository, ctx context.Context, grantType GrantType, peerID contract.PeerID, startID string, limit uint32, sortOrder contract.SortOrder) ([]*contract.Contract, error) {
	repoGrantType, err := getGrantTypeForRepository(grantType)
	if err != nil {
		return nil, fmt.Errorf("unable to convert grant type: %q, %w", grantType, err)
	}

	var contracts []*contract.Contract

	switch repoGrantType {
	case contract.GrantTypeServiceConnection:
		contracts, err = repository.ListContractsForPeerWithServiceConnectionGrant(ctx, peerID, startID, limit, sortOrder)
		if err != nil {
			return nil, fmt.Errorf("could not retrieve contracts for peer with service connection grant: %w", err)
		}

	case contract.GrantTypePeerRegistration:
		contracts, err = repository.ListContractsForPeerWithPeerRegistrationGrant(ctx, peerID, startID, limit, sortOrder)
		if err != nil {
			return nil, fmt.Errorf("could not retrieve contracts with peer registration grant: %w", err)
		}
	case contract.GrantTypeServicePublication:
		contracts, err = repository.ListContractsForPeerWithServicePublicationGrant(ctx, peerID, startID, limit, sortOrder)
		if err != nil {
			return nil, fmt.Errorf("could not retrieve contracts with service publication grant: %w", err)
		}

	case contract.GrantTypeDelegatedServiceConnection:
		contracts, err = repository.ListContractsForPeerWithDelegatedServiceConnectionGrant(ctx, peerID, startID, limit, sortOrder)
		if err != nil {
			return nil, fmt.Errorf("could not retrieve contracts with delegated service connection grant: %w", err)
		}

	case contract.GrantTypeDelegatedServicePublication:
		contracts, err = repository.ListContractsForPeerWithDelegatedServicePublicationGrant(ctx, peerID, startID, limit, sortOrder)
		if err != nil {
			return nil, fmt.Errorf("could not retrieve contracts with delegated service publication grant: %w", err)
		}

	default:
		return nil, app_errors.NewIncorrectInputError(fmt.Errorf("unable to fetch contracts for grant type '%v'", grantType))
	}

	return contracts, nil
}

func getGrantTypeForRepository(grantType GrantType) (contract.GrantType, error) {
	switch grantType {
	case GrantTypePeerRegistration:
		return contract.GrantTypePeerRegistration, nil

	case GrantTypeServiceConnection:
		return contract.GrantTypeServiceConnection, nil

	case GrantTypeServicePublication:
		return contract.GrantTypeServicePublication, nil

	case GrantTypeDelegatedServiceConnection:
		return contract.GrantTypeDelegatedServiceConnection, nil

	case GrantTypeDelegatedServicePublication:
		return contract.GrantTypeDelegatedServicePublication, nil
	default:
		return contract.GrantTypeUnspecified, errors.New("unknown grant type provided")
	}
}

type grantsByType struct {
	peerRegistration            []*PeerRegistrationGrant
	servicePublication          []*ServicePublicationGrant
	delegatedServicePublication []*DelegatedServicePublicationGrant
	serviceConnection           []*ServiceConnectionGrant
	delegatedServiceConnection  []*DelegatedServiceConnectionGrant
}

func convertContract(c *contract.Contract) (*Contract, error) {
	var grants *grantsByType

	grants, err := extractGrantsFromContract(c)
	if err != nil {
		return nil, err
	}

	acceptedSignatures := make(map[string]string)

	for signaturePeerID, acceptedSignature := range c.SignaturesAccepted() {
		acceptedSignatures[signaturePeerID.Value()] = acceptedSignature.JWS()
	}

	revokedSignatures := make(map[string]string)

	for signaturePeerID, revokedSignature := range c.SignaturesRevoked() {
		revokedSignatures[signaturePeerID.Value()] = revokedSignature.JWS()
	}

	rejectedSignatures := make(map[string]string)

	for signaturePeerID, rejectedSignature := range c.SignaturesRejected() {
		rejectedSignatures[signaturePeerID.Value()] = rejectedSignature.JWS()
	}

	return &Contract{
		ID:                                c.Content().ID().String(),
		HashAlgorithm:                     HashAlg(c.Content().Hash().Algorithm()),
		GroupID:                           c.Content().GroupID(),
		ContractNotBefore:                 c.Content().NotBefore(),
		ContractNotAfter:                  c.Content().NotAfter(),
		CreatedAt:                         c.Content().CreatedAt(),
		ServiceConnectionGrants:           grants.serviceConnection,
		PeerRegistrationGrants:            grants.peerRegistration,
		ServicePublicationGrants:          grants.servicePublication,
		DelegatedServiceConnectionGrants:  grants.delegatedServiceConnection,
		DelegatedServicePublicationGrants: grants.delegatedServicePublication,
		SignaturesAccepted:                acceptedSignatures,
		SignaturesRevoked:                 revokedSignatures,
		SignaturesRejected:                rejectedSignatures,
	}, nil
}

func extractGrantsFromContract(model *contract.Contract) (*grantsByType, error) {
	modelGrants := model.Content().Grants()

	grants := &grantsByType{}

	for _, grant := range modelGrants {
		switch g := grant.(type) {
		case *contract.GrantPeerRegistration:
			grants.peerRegistration = append(grants.peerRegistration, convertPeerRegistrationGrant(g))
		case *contract.GrantServicePublication:
			grants.servicePublication = append(grants.servicePublication, convertServicePublicationGrant(g))
		case *contract.GrantServiceConnection:
			grants.serviceConnection = append(grants.serviceConnection, convertServiceConnectionGrant(g))
		case *contract.GrantDelegatedServiceConnection:
			grants.delegatedServiceConnection = append(grants.delegatedServiceConnection, convertDelegatedServiceConnectionGrant(g))
		case *contract.GrantDelegatedServicePublication:
			grants.delegatedServicePublication = append(grants.delegatedServicePublication, convertDelegatedServicePublicationGrant(g))

		default:
			return nil, fmt.Errorf("unsupported grant type: %T", g)
		}
	}

	return grants, nil
}

func convertPeerRegistrationGrant(g *contract.GrantPeerRegistration) *PeerRegistrationGrant {
	return &PeerRegistrationGrant{
		Hash:            g.Hash().String(),
		DirectoryPeerID: g.Directory().PeerID().Value(),
		PeerID:          g.Directory().PeerID().Value(),
		PeerName:        g.Peer().Name(),
	}
}

func convertServicePublicationGrant(g *contract.GrantServicePublication) *ServicePublicationGrant {
	return &ServicePublicationGrant{
		Hash:            g.Hash().String(),
		DirectoryPeerID: g.Directory().Peer().ID().Value(),
		ServicePeerID:   g.Service().Peer().ID().Value(),
		ServiceName:     g.Service().Name(),
	}
}

func convertServiceConnectionGrant(g *contract.GrantServiceConnection) *ServiceConnectionGrant {
	return &ServiceConnectionGrant{
		Hash:                        g.Hash().String(),
		OutwayPeerID:                g.Outway().Peer().ID().Value(),
		OutwayCertificateThumbprint: g.Outway().CertificateThumbprint().Value(),
		ServiceName:                 g.Service().Name(),
		ServicePeerID:               g.Service().Peer().ID().Value(),
	}
}

func convertDelegatedServiceConnectionGrant(g *contract.GrantDelegatedServiceConnection) *DelegatedServiceConnectionGrant {
	return &DelegatedServiceConnectionGrant{
		Hash:                        g.Hash().String(),
		OutwayPeerID:                g.Outway().Peer().ID().Value(),
		OutwayCertificateThumbprint: g.Outway().CertificateThumbprint().Value(),
		ServiceName:                 g.Service().Name(),
		ServicePeerID:               g.Service().Peer().ID().Value(),
		DelegatorPeerID:             g.Delegator().Peer().ID().Value(),
	}
}

func convertDelegatedServicePublicationGrant(g *contract.GrantDelegatedServicePublication) *DelegatedServicePublicationGrant {
	return &DelegatedServicePublicationGrant{
		Hash:            g.Hash().String(),
		DirectoryPeerID: g.Directory().Peer().ID().Value(),
		ServicePeerID:   g.Service().Peer().ID().Value(),
		ServiceName:     g.Service().Name(),
		DelegatorPeerID: g.Delegator().Peer().ID().Value(),
	}
}
