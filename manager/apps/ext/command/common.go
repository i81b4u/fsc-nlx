// Copyright © VNG Realisatie 2023
// Licensed under the EUPL

package command

import (
	"fmt"
	"time"

	internalapp_errors "go.nlx.io/nlx/manager/apps/int/errors"
	"go.nlx.io/nlx/manager/domain/contract"
)

type GrantServiceConnectionArgs struct {
	CertificateThumbprint string
	OutwayPeerID          string
	ServicePeerID         string
	ServiceName           string
}

type GrantPeerRegistrationArgs struct {
	DirectoryPeerID string
	PeerID          string
	PeerName        string
}

type GrantServicePublicationArgs struct {
	DirectoryPeerID string
	ServicePeerID   string
	ServiceName     string
}

type GrantDelegatedServiceConnectionArgs struct {
	OutwayCertificateThumbprint string
	OutwayPeerID                string
	ServicePeerID               string
	ServiceName                 string
	DelegatorPeerID             string
}

type GrantDelegatedServicePublicationArgs struct {
	DirectoryPeerID string
	ServicePeerID   string
	ServiceName     string
	DelegatorPeerID string
}

type ContractContentArgs struct {
	HashAlgorithm int
	ID            []byte
	GroupID       string
	NotBefore     time.Time
	NotAfter      time.Time
	Grants        []interface{}
	CreatedAt     time.Time
}

type PeerArgs struct {
	ID             string
	Name           string
	ManagerAddress string
}

func contentToModel(c *ContractContentArgs) (*contract.Content, error) {
	contractContentGrants := make([]interface{}, len(c.Grants))

	for i, grant := range c.Grants {
		switch g := grant.(type) {
		case *GrantPeerRegistrationArgs:
			contractContentGrants[i] = &contract.NewGrantPeerRegistrationArgs{
				Directory: &contract.NewGrantPeerRegistrationDirectoryArgs{
					PeerID: g.DirectoryPeerID,
				},
				Peer: &contract.NewGrantPeerRegistrationPeerArgs{
					ID:   g.PeerID,
					Name: g.PeerName,
				},
			}
		case *GrantServicePublicationArgs:
			contractContentGrants[i] = &contract.NewGrantServicePublicationArgs{
				Directory: &contract.NewGrantServicePublicationDirectoryArgs{
					Peer: &contract.NewPeerArgs{
						ID: g.DirectoryPeerID,
					},
				},
				Service: &contract.NewGrantServicePublicationServiceArgs{
					Peer: &contract.NewPeerArgs{
						ID: g.ServicePeerID,
					},
					Name: g.ServiceName,
				},
			}
		case *GrantServiceConnectionArgs:
			contractContentGrants[i] = &contract.NewGrantServiceConnectionArgs{
				Outway: &contract.NewGrantServiceConnectionOutwayArgs{
					Peer: &contract.NewPeerArgs{
						ID: g.OutwayPeerID,
					},
					CertificateThumbprint: g.CertificateThumbprint,
				},
				Service: &contract.NewGrantServiceConnectionServiceArgs{
					Name: g.ServiceName,
					Peer: &contract.NewPeerArgs{
						ID: g.ServicePeerID,
					},
				},
			}
		case *GrantDelegatedServiceConnectionArgs:
			contractContentGrants[i] = &contract.NewGrantDelegatedServiceConnectionArgs{
				Outway: &contract.NewGrantDelegatedServiceConnectionOutwayArgs{
					Peer: &contract.NewPeerArgs{
						ID: g.OutwayPeerID,
					},
					CertificateThumbprint: g.OutwayCertificateThumbprint,
				},
				Service: &contract.NewGrantDelegatedServiceConnectionServiceArgs{
					Name: g.ServiceName,
					Peer: &contract.NewPeerArgs{
						ID: g.ServicePeerID,
					},
				},
				Delegator: &contract.NewGrantDelegatedServiceConnectionDelegatorArgs{
					Peer: &contract.NewPeerArgs{
						ID: g.DelegatorPeerID,
					},
				},
			}
		case *GrantDelegatedServicePublicationArgs:
			contractContentGrants[i] = &contract.NewGrantDelegatedServicePublicationArgs{
				Directory: &contract.NewGrantDelegatedServicePublicationDirectoryArgs{
					Peer: &contract.NewPeerArgs{
						ID: g.DirectoryPeerID,
					},
				},
				Service: &contract.NewGrantDelegatedServicePublicationServiceArgs{
					Name: g.ServiceName,
					Peer: &contract.NewPeerArgs{
						ID: g.ServicePeerID,
					},
				},
				Delegator: &contract.NewGrantDelegatedServicePublicationDelegatorArgs{
					Peer: &contract.NewPeerArgs{
						ID: g.DelegatorPeerID,
					},
				},
			}
		default:
			return nil, fmt.Errorf("unknown grant type %T", g)
		}
	}

	model, err := contract.NewContent(
		&contract.NewContentArgs{
			HashAlgorithm: contract.HashAlg(c.HashAlgorithm),
			ID:            c.ID,
			GroupID:       c.GroupID,
			Validity: &contract.NewValidityArgs{
				NotBefore: c.NotBefore,
				NotAfter:  c.NotAfter,
			},
			Grants:    contractContentGrants,
			CreatedAt: c.CreatedAt,
		},
	)
	if err != nil {
		return nil, internalapp_errors.NewIncorrectInputError(err)
	}

	return model, nil
}

func peerToModel(p *PeerArgs) (*contract.Peer, error) {
	peer, err := contract.NewPeer(&contract.NewPeerArgs{
		ID:             p.ID,
		Name:           p.Name,
		ManagerAddress: p.ManagerAddress,
	})
	if err != nil {
		return nil, internalapp_errors.NewIncorrectInputError(err)
	}

	if peer.ManagerAddress() == "" {
		return nil, internalapp_errors.NewIncorrectInputError(fmt.Errorf("peer manager address cannot be empty"))
	}

	return peer, nil
}
