// Copyright © VNG Realisatie 2022
// Licensed under the EUPL

package command

import (
	"context"
	"crypto/x509"
	"fmt"

	"github.com/pkg/errors"
	"gopkg.in/square/go-jose.v2"

	"go.nlx.io/nlx/common/clock"
	"go.nlx.io/nlx/manager/adapters/logger"
	app_errors "go.nlx.io/nlx/manager/apps/ext/errors"
	"go.nlx.io/nlx/manager/domain/contract"
	"go.nlx.io/nlx/manager/internal/peers"
)

type AcceptContractHandler struct {
	selfPeerID     contract.PeerID
	trustedRootCAs *x509.CertPool
	clock          clock.Clock
	contracts      contract.Repository
	peers          *peers.Peers
	logger         logger.Logger
}

type NewAcceptContractHandlerArgs struct {
	SelfPeerID     contract.PeerID
	TrustedRootCAs *x509.CertPool
	Contracts      contract.Repository
	Peers          *peers.Peers
	Logger         logger.Logger
	Clock          clock.Clock
}

func NewAcceptContractHandler(args *NewAcceptContractHandlerArgs) (*AcceptContractHandler, error) {
	if args.TrustedRootCAs == nil {
		return nil, errors.New("trustedRootCAs is required")
	}

	if args.Contracts == nil {
		return nil, errors.New("repository is required")
	}

	if args.Clock == nil {
		return nil, errors.New("clock is required")
	}

	if args.Logger == nil {
		return nil, errors.New("logger is required")
	}

	return &AcceptContractHandler{
		selfPeerID:     args.SelfPeerID,
		trustedRootCAs: args.TrustedRootCAs,
		contracts:      args.Contracts,
		clock:          args.Clock,
		logger:         args.Logger,
		peers:          args.Peers,
	}, nil
}

type HandleAcceptContractArgs struct {
	ContractContent *ContractContentArgs
	Signature       string
	SubmittedByPeer *PeerArgs
}

func (h *AcceptContractHandler) Handle(ctx context.Context, args *HandleAcceptContractArgs) error {
	submittedByPeer, err := peerToModel(args.SubmittedByPeer)
	if err != nil {
		return app_errors.NewIncorrectInputError(err)
	}

	err = h.contracts.UpsertPeer(ctx, submittedByPeer)
	if err != nil {
		return errors.Wrapf(err, "could not upsert peer: %s into repository", submittedByPeer.ID())
	}

	content, err := contentToModel(args.ContractContent)
	if err != nil {
		return app_errors.NewIncorrectInputError(err)
	}

	if !content.ContainsPeer(submittedByPeer.ID()) {
		return app_errors.NewIncorrectInputError(fmt.Errorf("submitting peer: %s is not found in contract", submittedByPeer.ID()))
	}

	if !content.ContainsPeer(h.selfPeerID) {
		return app_errors.NewIncorrectInputError(fmt.Errorf("self peer: %s is not found in contract", h.selfPeerID))
	}

	CertificateThumbprint, err := getCertificateThumbprintFromSignature(args.Signature)
	if err != nil {
		return app_errors.NewIncorrectInputError(fmt.Errorf("could not get certificate thumbprint from signature: %s.%s", args.Signature, err))
	}

	certsToRequest := make(map[contract.PeerID][]string)
	certsToRequest[submittedByPeer.ID()] = []string{CertificateThumbprint}

	certs, err := h.peers.GetCertificates(ctx, certsToRequest)
	if err != nil {
		h.logger.Error("unable to retrieve certificate from Peer", err)
		return err
	}

	sig, err := contract.NewSignature(
		&contract.NewSignatureArgs{
			SigType:    contract.SignatureTypeAccept,
			Signature:  args.Signature,
			Content:    content,
			PeersCerts: certs,
		})
	if err != nil {
		return app_errors.NewIncorrectInputError(err)
	}

	if !sig.Peer().IsEqual(submittedByPeer) {
		return app_errors.NewIncorrectInputError(fmt.Errorf("signature submitted by peer: %s, but signature is signed by peer: %s", submittedByPeer.ID(), sig.Peer().ID().Value()))
	}

	err = h.contracts.UpsertContent(ctx, content)
	if err != nil {
		h.logger.Error("could not upsert contract", err)
		return err
	}

	err = h.contracts.UpsertSignature(ctx, sig)
	if err != nil {
		h.logger.Error("could not upsert signature", err)
		return err
	}

	return nil
}

func getCertificateThumbprintFromSignature(signature string) (string, error) {
	jws, err := jose.ParseSigned(signature)
	if err != nil {
		return "", fmt.Errorf("could not parse signature: %v", err)
	}

	if len(jws.Signatures) != 1 {
		return "", fmt.Errorf("exactly one signature is required, found either zero or multiple signatures in jws token")
	}

	header, ok := jws.Signatures[0].Header.ExtraHeaders["x5t#S256"]
	if !ok {
		return "", fmt.Errorf("could not find required header in jws signature: x5t#S256")
	}

	pubKeyFingerPrintString, ok := header.(string)
	if !ok {
		return "", fmt.Errorf("certificate thumbprint must be of type string")
	}

	return pubKeyFingerPrintString, nil
}
