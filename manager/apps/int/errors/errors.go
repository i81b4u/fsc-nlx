// Copyright © VNG Realisatie 2022
// Licensed under the EUPL

package errors

type Type struct {
	string
}

var (
	ErrorTypeIncorrectInput = Type{"incorrect-input"}
)

type Error struct {
	message   string
	errorType Type
}

func (s Error) Error() string {
	return s.message
}

func (s Error) ErrorType() Type {
	return s.errorType
}

func NewIncorrectInputError(err error) Error {
	return Error{
		message:   err.Error(),
		errorType: ErrorTypeIncorrectInput,
	}
}
