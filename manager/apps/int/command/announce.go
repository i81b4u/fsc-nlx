// Copyright © VNG Realisatie 2022
// Licensed under the EUPL

package command

import (
	"context"
	"crypto/x509"
	"fmt"

	"github.com/pkg/errors"

	"go.nlx.io/nlx/manager/adapters/logger"
	internalapp_errors "go.nlx.io/nlx/manager/apps/int/errors"
	"go.nlx.io/nlx/manager/domain/contract"
	peers_adapter "go.nlx.io/nlx/manager/internal/peers"
)

type AnnouncePeerHandler struct {
	trustedExternalRootCAs *x509.CertPool
	logger                 logger.Logger
	peers                  *peers_adapter.Peers
}

func NewAnnouncePeerHandler(trustedExternalRootCAs *x509.CertPool, peers *peers_adapter.Peers, lgr logger.Logger) (*AnnouncePeerHandler, error) {
	if trustedExternalRootCAs == nil {
		return nil, errors.New("trustedRootCA's is required")
	}

	if peers == nil {
		return nil, errors.New("peers is required")
	}

	if lgr == nil {
		return nil, errors.New("logger is required")
	}

	return &AnnouncePeerHandler{
		trustedExternalRootCAs: trustedExternalRootCAs,
		peers:                  peers,
		logger:                 lgr,
	}, nil
}

func (h *AnnouncePeerHandler) Handle(ctx context.Context, peerIDs []string) error {
	ids, err := contract.NewPeerIDs(peerIDs)
	if err != nil {
		return internalapp_errors.NewIncorrectInputError(err)
	}

	err = h.peers.Announce(ctx, ids)
	if err != nil {
		return fmt.Errorf("could not announce to peers: %w", err)
	}

	return nil
}
