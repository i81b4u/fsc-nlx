// Copyright © VNG Realisatie 2022
// Licensed under the EUPL

package command

import (
	"context"
	"crypto/tls"
	"crypto/x509"
	"errors"
	"fmt"
	"go.nlx.io/nlx/manager/adapters/logger"
	internalapp_errors "go.nlx.io/nlx/manager/apps/int/errors"
	"go.nlx.io/nlx/manager/domain/contract"
	peers_adapter "go.nlx.io/nlx/manager/internal/peers"
)

type RejectContractHandler struct {
	trustedExternalRootCAs *x509.CertPool
	clock                  Clock
	repository             contract.Repository
	logger                 logger.Logger
	peers                  *peers_adapter.Peers
	signatureCert          *tls.Certificate
}

type NewRejectContractHandlerArgs struct {
	TrustedExternalRootCAs *x509.CertPool
	Repository             contract.Repository
	Peers                  *peers_adapter.Peers
	Clock                  Clock
	Logger                 logger.Logger
	SignatureCert          *tls.Certificate
}

func NewRejectContractHandler(args *NewRejectContractHandlerArgs) (*RejectContractHandler, error) {
	if args.TrustedExternalRootCAs == nil {
		return nil, errors.New("trustedRootCA's is required")
	}

	if args.Repository == nil {
		return nil, errors.New("repository is required")
	}

	if args.Peers == nil {
		return nil, errors.New("peers is required")
	}

	if args.Clock == nil {
		return nil, errors.New("clock is required")
	}

	if args.Logger == nil {
		return nil, errors.New("logger is required")
	}

	if args.SignatureCert == nil {
		return nil, errors.New("signature cert is required")
	}

	return &RejectContractHandler{
		trustedExternalRootCAs: args.TrustedExternalRootCAs,
		repository:             args.Repository,
		peers:                  args.Peers,
		clock:                  args.Clock,
		logger:                 args.Logger,
		signatureCert:          args.SignatureCert,
	}, nil
}

func (h *RejectContractHandler) Handle(ctx context.Context, contentHash string) error {
	hash, err := contract.NewContentHashFromString(contentHash)
	if err != nil {
		return internalapp_errors.NewIncorrectInputError(err)
	}

	model, err := h.repository.GetContentByHash(ctx, hash)
	if err != nil {
		return fmt.Errorf("could not retrieve content by hash: %w", err)
	}

	sig, err := model.Reject(h.trustedExternalRootCAs, h.signatureCert, h.clock.Now())
	if err != nil {
		return fmt.Errorf("unable to place a reject signature on content %w", err)
	}

	err = h.repository.UpsertSignature(ctx, sig)
	if err != nil {
		h.logger.Error("could not upsert signature", err)
		return err
	}

	// send rejected content to all peers
	err = h.peers.RejectContract(ctx, model, sig)
	if err != nil {
		return fmt.Errorf("could not send signed rejected content to peers: %w", err)
	}

	return nil
}
