// Copyright © VNG Realisatie 2022
// Licensed under the EUPL

package command

import (
	"context"
	"crypto/tls"
	"crypto/x509"
	"errors"
	"fmt"
	"go.nlx.io/nlx/manager/adapters/logger"
	internalapp_errors "go.nlx.io/nlx/manager/apps/int/errors"
	"go.nlx.io/nlx/manager/domain/contract"
	peers_adapter "go.nlx.io/nlx/manager/internal/peers"
)

type RevokeContractHandler struct {
	trustedExternalRootCAs *x509.CertPool
	clock                  Clock
	repository             contract.Repository
	logger                 logger.Logger
	peers                  *peers_adapter.Peers
	signatureCert          *tls.Certificate
}

type NewRevokeContractHandlerArgs struct {
	TrustedExternalRootCAs *x509.CertPool
	Repository             contract.Repository
	Peers                  *peers_adapter.Peers
	Clock                  Clock
	Logger                 logger.Logger
	SignatureCert          *tls.Certificate
}

func NewRevokeContractHandler(args *NewRevokeContractHandlerArgs) (*RevokeContractHandler, error) {
	if args.TrustedExternalRootCAs == nil {
		return nil, errors.New("trustedRootCA's is required")
	}

	if args.Repository == nil {
		return nil, errors.New("repository is required")
	}

	if args.Peers == nil {
		return nil, errors.New("peers is required")
	}

	if args.Clock == nil {
		return nil, errors.New("clock is required")
	}

	if args.Logger == nil {
		return nil, errors.New("logger is required")
	}

	if args.SignatureCert == nil {
		return nil, errors.New("signature cert is required")
	}

	return &RevokeContractHandler{
		trustedExternalRootCAs: args.TrustedExternalRootCAs,
		repository:             args.Repository,
		peers:                  args.Peers,
		clock:                  args.Clock,
		logger:                 args.Logger,
		signatureCert:          args.SignatureCert,
	}, nil
}

func (h *RevokeContractHandler) Handle(ctx context.Context, contentHash string) error {
	hash, err := contract.NewContentHashFromString(contentHash)
	if err != nil {
		return internalapp_errors.NewIncorrectInputError(err)
	}

	model, err := h.repository.GetContentByHash(ctx, hash)
	if err != nil {
		return fmt.Errorf("could not retrieve content by hash: %w", err)
	}

	sig, err := model.Revoke(h.trustedExternalRootCAs, h.signatureCert, h.clock.Now())
	if err != nil {
		return fmt.Errorf("unable to place a revoke signature on content %w", err)
	}

	err = h.repository.UpsertSignature(ctx, sig)
	if err != nil {
		h.logger.Error("could not upsert signature", err)
		return err
	}

	// send revoked contract to all peers
	err = h.peers.RevokeContract(ctx, model, sig)
	if err != nil {
		return fmt.Errorf("could not send signed revoked contract to peers: %w", err)
	}

	return nil
}
