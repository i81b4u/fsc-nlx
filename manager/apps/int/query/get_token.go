// Copyright © VNG Realisatie 2023
// Licensed under the EUPL

package query

import (
	"context"
	"fmt"
	"time"

	"go.nlx.io/nlx/common/clock"

	"go.nlx.io/nlx/common/accesstoken"

	"github.com/pkg/errors"

	"go.nlx.io/nlx/manager/domain/contract"
	"go.nlx.io/nlx/manager/internal/peers"
)

type GetTokenHandler struct {
	repository contract.Repository
	peers      *peers.Peers
	clk        clock.Clock
}

type NewGetTokenHandlerArgs struct {
	Repository contract.Repository
	Peers      *peers.Peers
	Clock      clock.Clock
}

func NewGetTokenHandler(args *NewGetTokenHandlerArgs) (*GetTokenHandler, error) {
	if args.Repository == nil {
		return nil, errors.New("repository is required")
	}

	if args.Peers == nil {
		return nil, errors.New("peers is required")
	}

	if args.Clock == nil {
		return nil, errors.New("clock is required")
	}

	return &GetTokenHandler{
		repository: args.Repository,
		peers:      args.Peers,
		clk:        args.Clock,
	}, nil
}

type GetTokenHandlerArgs struct {
	GrantHash                   string
	OutwayCertificateThumbprint string
}

type Token struct {
	GrantHash                   string
	OutwayPeerID                string
	OutwayCertificateThumbprint string
	OutwayDelegatorPeerID       string
	ServiceName                 string
	ServiceInwayAddress         string
	ServicePeerID               string
	ServiceDelegatorPeerID      string
	ExpiryDate                  time.Time
	Token                       string
}

func (h *GetTokenHandler) Handle(ctx context.Context, args *GetTokenHandlerArgs) (*Token, error) {
	gHash, err := contract.DecodeHashFromString(args.GrantHash)
	if err != nil {
		return nil, errors.Wrap(err, "could not decode grant hash in args")
	}

	peer, err := h.repository.GetTokenProviderPeer(ctx, gHash)
	if err != nil {
		return nil, errors.Wrap(err, "could not get peer for getting the token from repository")
	}

	token, err := h.peers.GetToken(ctx, peer, args.GrantHash)
	if err != nil {
		return nil, errors.Wrapf(err, "could not get token from peer: %s for grant hash: %s and outway certificate thumbprint: %s", peer.Value(), args.GrantHash, args.OutwayCertificateThumbprint)
	}

	certificateThumbprint, err := accesstoken.GetCertificateThumbprintFromToken(token)
	if err != nil {
		return nil, errors.Wrap(err, "could not get certificate thumbprint from token")
	}

	cert, err := h.peers.GetCertificate(ctx, peer, certificateThumbprint)
	if err != nil {
		return nil, errors.Wrap(err, "could not get peer certificate for validating token")
	}

	// also the service peer id should be validated against the connection
	decodedToken, err := accesstoken.DecodeFromString(h.clk, cert, token)
	if err != nil {
		return nil, errors.Wrap(err, "unable to decode token")
	}

	if args.OutwayCertificateThumbprint != decodedToken.OutwayCertificateThumbprint {
		return nil, fmt.Errorf("grant contains outway certificate thumbprint %s, not %s", decodedToken.OutwayCertificateThumbprint, args.OutwayCertificateThumbprint)
	}

	return &Token{
		GrantHash:                   args.GrantHash,
		OutwayPeerID:                decodedToken.OutwayPeerID,
		OutwayCertificateThumbprint: decodedToken.OutwayCertificateThumbprint,
		OutwayDelegatorPeerID:       decodedToken.OutwayDelegatorPeerID,
		ServiceName:                 decodedToken.ServiceName,
		ServiceInwayAddress:         decodedToken.ServiceInwayAddress,
		ServicePeerID:               decodedToken.ServicePeerID,
		ServiceDelegatorPeerID:      decodedToken.ServiceDelegatorPeerID,
		ExpiryDate:                  decodedToken.ExpiryDate,
		Token:                       token,
	}, nil
}
