// Copyright © VNG Realisatie 2023
// Licensed under the EUPL
package query_test

import (
	"context"
	"testing"
	"time"

	"github.com/google/uuid"
	"github.com/pkg/errors"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"

	"go.nlx.io/nlx/manager/apps/int/query"
	"go.nlx.io/nlx/manager/domain/contract"
)

// nolint:funlen,dupl // these tests do not fit in 100 lines
func TestListContractsFilters(t *testing.T) {
	t.Parallel()

	now := time.Date(2022, 7, 1, 1, 2, 3, 0, time.UTC)

	id, err := uuid.MustParse("102635ef-7053-4151-8fd1-2537f99bee79").MarshalBinary()
	assert.NoError(t, err)

	tests := map[string]struct {
		setup   func(context.Context, *mocks)
		args    []*query.ListContractsFilter
		want    query.ListContractsContracts
		wantErr error
	}{
		"when_repo_list_contracts_by_content_hash_errors": {
			args: []*query.ListContractsFilter{
				{
					ContentHash: "$1$1$mTfBbWw2A6UQU5oU7RR2JbEtTB9GnT2syfopGwICT5Mo9qDgFb1L_o0kFi0-TG13LX2czdAL60KaY7NbB-uMAQ==",
				},
			},
			setup: func(ctx context.Context, m *mocks) {
				m.repository.EXPECT().
					ListContractsByContentHash(ctx, []string{"$1$1$mTfBbWw2A6UQU5oU7RR2JbEtTB9GnT2syfopGwICT5Mo9qDgFb1L_o0kFi0-TG13LX2czdAL60KaY7NbB-uMAQ=="}).
					Return(nil, errors.New("unexpected error"))
			},
			wantErr: errors.New("unexpected error"),
		},
		"when_filtering_by_content_hash": {
			args: []*query.ListContractsFilter{
				{
					ContentHash: "$1$1$mTfBbWw2A6UQU5oU7RR2JbEtTB9GnT2syfopGwICT5Mo9qDgFb1L_o0kFi0-TG13LX2czdAL60KaY7NbB-uMAQ==",
				},
			},
			setup: func(ctx context.Context, m *mocks) {
				peerRegistrationGrant := &contract.NewGrantPeerRegistrationArgs{
					Directory: &contract.NewGrantPeerRegistrationDirectoryArgs{
						PeerID: "12345678901234567890",
					},
					Peer: &contract.NewGrantPeerRegistrationPeerArgs{
						ID:   "12345678901234567890",
						Name: "Gemeente Stijns",
					},
				}

				c, err := contract.NewContract(&contract.NewContractArgs{
					Content: &contract.NewContentArgs{
						Validity: &contract.NewValidityArgs{
							NotBefore: now,
							NotAfter:  now,
						},
						GroupID:       "https://directory.nlx.com",
						Grants:        []interface{}{peerRegistrationGrant},
						HashAlgorithm: contract.HashAlgSHA3_512,
						ID:            id,
						CreatedAt:     now,
					},
					PeersCerts:         nil,
					SignaturesAccepted: nil,
					SignaturesRejected: nil,
					SignaturesRevoked:  nil,
				})
				assert.NoError(t, err)

				peerGemeenteStijns, err := contract.NewPeer(&contract.NewPeerArgs{
					ID:             "12345678901234567890",
					Name:           "Gemeente Stijns",
					ManagerAddress: "https://manager.organization-a.nlx.local:8443",
				})
				assert.NoError(t, err)

				m.repository.EXPECT().
					ListContractsByContentHash(ctx, []string{"$1$1$mTfBbWw2A6UQU5oU7RR2JbEtTB9GnT2syfopGwICT5Mo9qDgFb1L_o0kFi0-TG13LX2czdAL60KaY7NbB-uMAQ=="}).
					Return([]*contract.Contract{c}, nil)

				m.repository.EXPECT().
					ListPeersByID(ctx, contract.PeersIDs{
						"12345678901234567890": true,
					}).
					Return(contract.Peers{
						"12345678901234567890": peerGemeenteStijns,
					}, nil)
			},
			wantErr: nil,
			want: query.ListContractsContracts{
				&query.ListContractsContract{
					ID:            id,
					Hash:          "$1$1$WkCuFTJzKLXVEZa1HobZpDxd6R8ybHyfv-5VQbrFtf0Lcj3OrpWdem8iz06Zdh6oZNSMbn4_Q2257Fy5KYE3eA==",
					HashAlgorithm: query.HashAlg(contract.HashAlgSHA3_512),
					GroupID:       "https://directory.nlx.com",
					NotBefore:     now,
					NotAfter:      now,
					CreatedAt:     now,
					Peers: []*query.Peer{
						{
							ID:   "12345678901234567890",
							Name: "Gemeente Stijns",
						},
					},
					AcceptSignatures: map[string]query.Signature{},
					RejectSignatures: map[string]query.Signature{},
					RevokeSignatures: map[string]query.Signature{},
					HasRejected:      false,
					HasAccepted:      false,
					HasRevoked:       false,
					PeerRegistrationGrant: &query.PeerRegistrationGrant{
						Hash:            "$1$2$RiSOAOx2J8SEgRC6XKAXdavIFFMSTZ4lQVC8fWyYyqypPE23R0oGjKfZkRo1d-emYA3XLKI3CLhaVMV_vqUcXA==",
						DirectoryPeerID: "12345678901234567890",
						PeerID:          "12345678901234567890",
						PeerName:        "Gemeente Stijns",
					},
					ServicePublicationGrants:          []*query.ServicePublicationGrant{},
					ServiceConnectionGrants:           []*query.ServiceConnectionGrant{},
					DelegatedServicePublicationGrants: []*query.DelegatedServicePublicationGrant{},
					DelegatedServiceConnectionGrants:  []*query.DelegatedServiceConnectionGrant{},
				},
			},
		},
		"when_filtering_by_grant_type": {
			args: []*query.ListContractsFilter{
				{
					GrantType: query.GrantTypePeerRegistration,
				},
			},
			setup: func(ctx context.Context, m *mocks) {
				peerRegistrationGrant := &contract.NewGrantPeerRegistrationArgs{
					Directory: &contract.NewGrantPeerRegistrationDirectoryArgs{
						PeerID: "12345678901234567890",
					},
					Peer: &contract.NewGrantPeerRegistrationPeerArgs{
						ID:   "12345678901234567890",
						Name: "Gemeente Stijns",
					},
				}

				c, err := contract.NewContract(&contract.NewContractArgs{
					Content: &contract.NewContentArgs{
						Validity: &contract.NewValidityArgs{
							NotBefore: now,
							NotAfter:  now,
						},
						GroupID:       "https://directory.nlx.com",
						Grants:        []interface{}{peerRegistrationGrant},
						HashAlgorithm: contract.HashAlgSHA3_512,
						ID:            id,
						CreatedAt:     now,
					},
					PeersCerts:         nil,
					SignaturesAccepted: nil,
					SignaturesRejected: nil,
					SignaturesRevoked:  nil,
				})
				assert.NoError(t, err)

				peerGemeenteStijns, err := contract.NewPeer(&contract.NewPeerArgs{
					ID:             "12345678901234567890",
					Name:           "Gemeente Stijns",
					ManagerAddress: "https://manager.organization-a.nlx.local:8443",
				})
				assert.NoError(t, err)

				m.repository.EXPECT().
					ListContractsWithPeerRegistrationGrant(ctx, "", uint32(100), contract.SortOrderDescending).
					Return([]*contract.Contract{c}, nil)

				m.repository.EXPECT().
					ListPeersByID(ctx, contract.PeersIDs{
						"12345678901234567890": true,
					}).
					Return(contract.Peers{
						"12345678901234567890": peerGemeenteStijns,
					}, nil)
			},
			wantErr: nil,
			want: query.ListContractsContracts{
				&query.ListContractsContract{
					ID:            id,
					Hash:          "$1$1$WkCuFTJzKLXVEZa1HobZpDxd6R8ybHyfv-5VQbrFtf0Lcj3OrpWdem8iz06Zdh6oZNSMbn4_Q2257Fy5KYE3eA==",
					HashAlgorithm: query.HashAlg(contract.HashAlgSHA3_512),
					GroupID:       "https://directory.nlx.com",
					NotBefore:     now,
					NotAfter:      now,
					CreatedAt:     now,
					Peers: []*query.Peer{
						{
							ID:   "12345678901234567890",
							Name: "Gemeente Stijns",
						},
					},
					AcceptSignatures: map[string]query.Signature{},
					RejectSignatures: map[string]query.Signature{},
					RevokeSignatures: map[string]query.Signature{},
					HasRejected:      false,
					HasAccepted:      false,
					HasRevoked:       false,
					PeerRegistrationGrant: &query.PeerRegistrationGrant{
						Hash:            "$1$2$RiSOAOx2J8SEgRC6XKAXdavIFFMSTZ4lQVC8fWyYyqypPE23R0oGjKfZkRo1d-emYA3XLKI3CLhaVMV_vqUcXA==",
						DirectoryPeerID: "12345678901234567890",
						PeerID:          "12345678901234567890",
						PeerName:        "Gemeente Stijns",
					},
					ServicePublicationGrants:          []*query.ServicePublicationGrant{},
					ServiceConnectionGrants:           []*query.ServiceConnectionGrant{},
					DelegatedServicePublicationGrants: []*query.DelegatedServicePublicationGrant{},
					DelegatedServiceConnectionGrants:  []*query.DelegatedServiceConnectionGrant{},
				},
			},
		},
		"when_filtering_by_content_hash_and_grant_type": {
			args: []*query.ListContractsFilter{
				{
					ContentHash: "$1$1$mTfBbWw2A6UQU5oU7RR2JbEtTB9GnT2syfopGwICT5Mo9qDgFb1L_o0kFi0-TG13LX2czdAL60KaY7NbB-uMAQ==",
				},
				{
					GrantType: query.GrantTypePeerRegistration,
				},
			},
			setup: func(ctx context.Context, m *mocks) {
				peerRegistrationGrant := &contract.NewGrantPeerRegistrationArgs{
					Directory: &contract.NewGrantPeerRegistrationDirectoryArgs{
						PeerID: "12345678901234567890",
					},
					Peer: &contract.NewGrantPeerRegistrationPeerArgs{
						ID:   "12345678901234567890",
						Name: "Gemeente Stijns",
					},
				}

				c, err := contract.NewContract(&contract.NewContractArgs{
					Content: &contract.NewContentArgs{
						Validity: &contract.NewValidityArgs{
							NotBefore: now,
							NotAfter:  now,
						},
						GroupID:       "https://directory.nlx.com",
						Grants:        []interface{}{peerRegistrationGrant},
						HashAlgorithm: contract.HashAlgSHA3_512,
						ID:            id,
						CreatedAt:     now,
					},
					PeersCerts:         nil,
					SignaturesAccepted: nil,
					SignaturesRejected: nil,
					SignaturesRevoked:  nil,
				})
				assert.NoError(t, err)

				peerGemeenteStijns, err := contract.NewPeer(&contract.NewPeerArgs{
					ID:             "12345678901234567890",
					Name:           "Gemeente Stijns",
					ManagerAddress: "https://manager.organization-a.nlx.local:8443",
				})
				assert.NoError(t, err)

				m.repository.EXPECT().
					ListContractsWithPeerRegistrationGrant(ctx, "", uint32(100), contract.SortOrderDescending).
					Return([]*contract.Contract{c}, nil)

				m.repository.EXPECT().
					ListPeersByID(ctx, contract.PeersIDs{
						"12345678901234567890": true,
					}).
					Return(contract.Peers{
						"12345678901234567890": peerGemeenteStijns,
					}, nil)
			},
			wantErr: nil,
			want: query.ListContractsContracts{
				&query.ListContractsContract{
					ID:            id,
					Hash:          "$1$1$WkCuFTJzKLXVEZa1HobZpDxd6R8ybHyfv-5VQbrFtf0Lcj3OrpWdem8iz06Zdh6oZNSMbn4_Q2257Fy5KYE3eA==",
					HashAlgorithm: query.HashAlg(contract.HashAlgSHA3_512),
					GroupID:       "https://directory.nlx.com",
					NotBefore:     now,
					NotAfter:      now,
					CreatedAt:     now,
					Peers: []*query.Peer{
						{
							ID:   "12345678901234567890",
							Name: "Gemeente Stijns",
						},
					},
					AcceptSignatures: map[string]query.Signature{},
					RejectSignatures: map[string]query.Signature{},
					RevokeSignatures: map[string]query.Signature{},
					HasRejected:      false,
					HasAccepted:      false,
					HasRevoked:       false,
					PeerRegistrationGrant: &query.PeerRegistrationGrant{
						Hash:            "$1$2$RiSOAOx2J8SEgRC6XKAXdavIFFMSTZ4lQVC8fWyYyqypPE23R0oGjKfZkRo1d-emYA3XLKI3CLhaVMV_vqUcXA==",
						DirectoryPeerID: "12345678901234567890",
						PeerID:          "12345678901234567890",
						PeerName:        "Gemeente Stijns",
					},
					ServicePublicationGrants:          []*query.ServicePublicationGrant{},
					ServiceConnectionGrants:           []*query.ServiceConnectionGrant{},
					DelegatedServicePublicationGrants: []*query.DelegatedServicePublicationGrant{},
					DelegatedServiceConnectionGrants:  []*query.DelegatedServiceConnectionGrant{},
				},
			},
		},
	}

	for name, tt := range tests {
		tt := tt

		t.Run(name, func(t *testing.T) {
			t.Parallel()

			mocks := newMocks(t)

			selfPeerID, err := contract.NewPeerID("12345678901234567890")
			assert.NoError(t, err)

			h, err := query.NewListContractsHandler(mocks.repository, selfPeerID)
			require.NoError(t, err)

			ctx := context.Background()

			if tt.setup != nil {
				tt.setup(ctx, mocks)
			}

			actual, err := h.Handle(ctx, tt.args)

			if tt.wantErr == nil {
				assert.NoError(t, err)
				assert.Equal(t, tt.want, actual)
			} else {
				assert.ErrorAs(t, err, &tt.wantErr)
			}
		})
	}
}
