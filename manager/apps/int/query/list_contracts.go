// Copyright © VNG Realisatie 2023
// Licensed under the EUPL

package query

import (
	"context"
	"fmt"
	"time"

	"github.com/pkg/errors"

	"go.nlx.io/nlx/manager/domain/contract"
)

type ListContractsHandler struct {
	repo       contract.Repository
	selfPeerID contract.PeerID
}

type ListContractsContracts []*ListContractsContract

type ListContractsContract struct {
	ID                                []byte
	Hash                              string
	HashAlgorithm                     HashAlg
	GroupID                           string
	NotBefore                         time.Time
	NotAfter                          time.Time
	CreatedAt                         time.Time
	Peers                             []*Peer
	AcceptSignatures                  map[string]Signature
	RejectSignatures                  map[string]Signature
	RevokeSignatures                  map[string]Signature
	HasRejected                       bool
	HasAccepted                       bool
	HasRevoked                        bool
	PeerRegistrationGrant             *PeerRegistrationGrant
	ServicePublicationGrants          []*ServicePublicationGrant
	ServiceConnectionGrants           []*ServiceConnectionGrant
	DelegatedServicePublicationGrants []*DelegatedServicePublicationGrant
	DelegatedServiceConnectionGrants  []*DelegatedServiceConnectionGrant
}

type PeerRegistrationGrant struct {
	Hash            string
	DirectoryPeerID string
	PeerID          string
	PeerName        string
}

type ServicePublicationGrant struct {
	Hash            string
	DirectoryPeerID string
	ServicePeerID   string
	ServiceName     string
}

type ServiceConnectionGrant struct {
	Hash                        string
	ServicePeerID               string
	ServiceName                 string
	OutwayPeerID                string
	OutwayCertificateThumbprint string
}

type DelegatedServiceConnectionGrant struct {
	Hash                        string
	DelegatorPeerID             string
	OutwayPeerID                string
	OutwayCertificateThumbprint string
	ServicePeerID               string
	ServiceName                 string
}

type DelegatedServicePublicationGrant struct {
	Hash            string
	DirectoryPeerID string
	DelegatorPeerID string
	ServicePeerID   string
	ServiceName     string
}

type HashAlg int32

const (
	HashAlgUnspecified HashAlg = iota
	HashAlgSHA3_512
)

type Peer struct {
	ID   string
	Name string
}

type Signature struct {
	Peer     *Peer
	SignedAt time.Time
}

type GrantType string

const (
	GrantTypeUnspecified                 GrantType = ""
	GrantTypePeerRegistration            GrantType = "peer_registration"
	GrantTypeServicePublication          GrantType = "service_publication"
	GrantTypeServiceConnection           GrantType = "service_connection"
	GrantTypeDelegatedServicePublication GrantType = "delegated_service_publication"
	GrantTypeDelegatedServiceConnection  GrantType = "delegated_service_connection"
)

type ListContractsFilter struct {
	ContentHash string
	GrantType   GrantType
}

func NewListContractsHandler(repository contract.Repository, selfPeerID contract.PeerID) (*ListContractsHandler, error) {
	if repository == nil {
		return nil, errors.New("repository is required")
	}

	return &ListContractsHandler{
		repo:       repository,
		selfPeerID: selfPeerID,
	}, nil
}

const paginationLimit = 100

func (h *ListContractsHandler) Handle(ctx context.Context, filters []*ListContractsFilter) (ListContractsContracts, error) {
	var (
		resp []*contract.Contract
		err  error
	)

	if len(filters) == 0 {
		resp, err = h.repo.ListAllContracts(ctx)
		if err != nil {
			return nil, fmt.Errorf("%s: %w", newInternalError("could not get all contracts from repository"), err)
		}
	} else {
		contractHashFilters, grantTypeFilters := getFiltersFromRequest(filters)

		// when at least one grant type filter is specified, we ignore the content hash filter
		// since filtering by content hash targets individual contracts where filtering by
		// grant type targets multiple contracts
		if len(grantTypeFilters) > 0 {
			for _, grantType := range grantTypeFilters {
				switch grantType {
				case GrantTypePeerRegistration:
					respC, errPeerRegistration := h.repo.ListContractsWithPeerRegistrationGrant(ctx, "", paginationLimit, contract.SortOrderDescending)
					if errPeerRegistration != nil {
						return nil, fmt.Errorf("%s: %w", newInternalError("could not get contracts with peer registration grants from repository"), errPeerRegistration)
					}

					resp = append(resp, respC...)
				case GrantTypeServicePublication:
					respC, errServicePublication := h.repo.ListContractsWithServicePublicationGrant(ctx, "", paginationLimit, contract.SortOrderDescending)
					if errServicePublication != nil {
						return nil, fmt.Errorf("%s: %w", newInternalError("could not get contracts with service publication grants from repository"), errServicePublication)
					}

					resp = append(resp, respC...)
				case GrantTypeServiceConnection:
					respC, errServiceConnection := h.repo.ListContractsWithServiceConnectionGrant(ctx, "", paginationLimit, contract.SortOrderDescending)
					if errServiceConnection != nil {
						return nil, fmt.Errorf("%s: %w", newInternalError("could not get contracts with service connection grants from repository"), errServiceConnection)
					}

					resp = append(resp, respC...)
				case GrantTypeDelegatedServicePublication:
					respC, errDelegatedServicePublication := h.repo.ListContractsWithDelegatedServicePublicationGrant(ctx, "", paginationLimit, contract.SortOrderDescending)
					if errDelegatedServicePublication != nil {
						return nil, fmt.Errorf("%s: %w", newInternalError("could not get contracts with delegated service publication grants from repository"), errDelegatedServicePublication)
					}

					resp = append(resp, respC...)
				case GrantTypeDelegatedServiceConnection:
					respC, errDelegatedServiceConnection := h.repo.ListContractsWithDelegatedServiceConnectionGrant(ctx, "", paginationLimit, contract.SortOrderDescending)
					if errDelegatedServiceConnection != nil {
						return nil, fmt.Errorf("%s: %w", newInternalError("could not get contracts with delegated service connection grants from repository"), errDelegatedServiceConnection)
					}

					resp = append(resp, respC...)
				}
			}
		} else {
			resp, err = h.repo.ListContractsByContentHash(ctx, contractHashFilters)
			if err != nil {
				return nil, fmt.Errorf("%s: %w", newInternalError("could not get contracts by content hash from repository"), err)
			}
		}
	}

	contracts := make(ListContractsContracts, len(resp))

	for i, c := range resp {
		rejectSignatures := make(map[string]Signature)

		for _, s := range c.SignaturesRejected() {
			sig := convertSignature(s)

			rejectSignatures[sig.Peer.ID] = sig
		}

		acceptSignatures := make(map[string]Signature)

		for _, s := range c.SignaturesAccepted() {
			sig := convertSignature(s)

			acceptSignatures[sig.Peer.ID] = sig
		}

		revokeSignatures := make(map[string]Signature)

		for _, s := range c.SignaturesRevoked() {
			sig := convertSignature(s)

			revokeSignatures[sig.Peer.ID] = sig
		}

		peers, errPeers := h.repo.ListPeersByID(ctx, c.Content().PeersIDs())
		if errPeers != nil {
			return nil, fmt.Errorf("%s: %w", newInternalError("could not list peers by id"), err)
		}

		contractPeers := make([]*Peer, 0)
		for _, peer := range peers {
			contractPeers = append(contractPeers, &Peer{
				ID:   peer.ID().Value(),
				Name: peer.Name().Value(),
			})
		}

		var peerRegistrationGrant *PeerRegistrationGrant

		if c.Content().Grants().PeerRegistrationGrant() != nil {
			grant := c.Content().Grants().PeerRegistrationGrant()
			peerRegistrationGrant = &PeerRegistrationGrant{
				Hash:            grant.Hash().String(),
				DirectoryPeerID: grant.Directory().PeerID().Value(),
				PeerID:          grant.Peer().ID().Value(),
				PeerName:        grant.Peer().Name(),
			}
		}

		servicePublicationGrants := make([]*ServicePublicationGrant, len(c.Content().Grants().ServicePublicationGrants()))

		for j, grant := range c.Content().Grants().ServicePublicationGrants() {
			servicePublicationGrants[j] = &ServicePublicationGrant{
				Hash:            grant.Hash().String(),
				DirectoryPeerID: grant.Directory().Peer().ID().Value(),
				ServicePeerID:   grant.Service().Peer().ID().Value(),
				ServiceName:     grant.Service().Name(),
			}
		}

		serviceConnectionGrants := make([]*ServiceConnectionGrant, len(c.Content().Grants().ServiceConnectionGrants()))

		for j, grant := range c.Content().Grants().ServiceConnectionGrants() {
			serviceConnectionGrants[j] = &ServiceConnectionGrant{
				Hash:                        grant.Hash().String(),
				ServicePeerID:               grant.Service().Peer().ID().Value(),
				ServiceName:                 grant.Service().Name(),
				OutwayPeerID:                grant.Outway().Peer().ID().Value(),
				OutwayCertificateThumbprint: grant.Outway().CertificateThumbprint().Value(),
			}
		}

		delegatedServicePublicationGrants := make([]*DelegatedServicePublicationGrant, len(c.Content().Grants().DelegatedServicePublicationGrants()))

		for j, grant := range c.Content().Grants().DelegatedServicePublicationGrants() {
			delegatedServicePublicationGrants[j] = &DelegatedServicePublicationGrant{
				Hash:            grant.Hash().String(),
				DirectoryPeerID: grant.Directory().Peer().ID().Value(),
				DelegatorPeerID: grant.Delegator().Peer().ID().Value(),
				ServicePeerID:   grant.Service().Peer().ID().Value(),
				ServiceName:     grant.Service().Name(),
			}
		}

		delegatedServiceConnectionGrants := make([]*DelegatedServiceConnectionGrant, len(c.Content().Grants().DelegatedServiceConnectionGrants()))

		for j, grant := range c.Content().Grants().DelegatedServiceConnectionGrants() {
			delegatedServiceConnectionGrants[j] = &DelegatedServiceConnectionGrant{
				Hash:                        grant.Hash().String(),
				DelegatorPeerID:             grant.Delegator().Peer().ID().Value(),
				OutwayPeerID:                grant.Outway().Peer().ID().Value(),
				OutwayCertificateThumbprint: grant.Outway().CertificateThumbprint().Value(),
				ServicePeerID:               grant.Service().Peer().ID().Value(),
				ServiceName:                 grant.Service().Name(),
			}
		}

		returnedContract := &ListContractsContract{
			ID:                                c.Content().ID().Bytes(),
			Hash:                              c.Content().Hash().String(),
			HashAlgorithm:                     HashAlg(c.Content().Hash().Algorithm()),
			GroupID:                           c.Content().GroupID(),
			NotBefore:                         c.Content().NotBefore(),
			NotAfter:                          c.Content().NotAfter(),
			CreatedAt:                         c.Content().CreatedAt(),
			Peers:                             contractPeers,
			AcceptSignatures:                  acceptSignatures,
			RejectSignatures:                  rejectSignatures,
			RevokeSignatures:                  revokeSignatures,
			HasRejected:                       c.IsRejectedBy(h.selfPeerID),
			HasAccepted:                       c.IsAcceptedBy(h.selfPeerID),
			HasRevoked:                        c.IsRevokedBy(h.selfPeerID),
			PeerRegistrationGrant:             peerRegistrationGrant,
			ServicePublicationGrants:          servicePublicationGrants,
			ServiceConnectionGrants:           serviceConnectionGrants,
			DelegatedServicePublicationGrants: delegatedServicePublicationGrants,
			DelegatedServiceConnectionGrants:  delegatedServiceConnectionGrants,
		}

		contracts[i] = returnedContract
	}

	return contracts, nil
}

func convertSignature(signature *contract.Signature) Signature {
	return Signature{
		Peer: &Peer{
			ID:   signature.Peer().ID().Value(),
			Name: signature.Peer().Name().Value(),
		},
		SignedAt: signature.SignedAt(),
	}
}

func getFiltersFromRequest(filters []*ListContractsFilter) ([]string, []GrantType) {
	contractHashes := make([]string, 0)
	grantTypes := make([]GrantType, 0)

	for _, filter := range filters {
		if filter.ContentHash != "" {
			contractHashes = append(contractHashes, filter.ContentHash)
		}

		if filter.GrantType != "" {
			grantTypes = append(grantTypes, filter.GrantType)
		}
	}

	return contractHashes, grantTypes
}
