// Copyright © VNG Realisatie 2023
// Licensed under the EUPL
package query_test

import (
	"testing"

	mock_repository "go.nlx.io/nlx/manager/domain/contract/mock"
)

type mocks struct {
	repository *mock_repository.MockRepository
}

func newMocks(t *testing.T) *mocks {
	return &mocks{
		repository: mock_repository.NewMockRepository(t),
	}
}
