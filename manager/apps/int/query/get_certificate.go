// Copyright © VNG Realisatie 2023
// Licensed under the EUPL

package query

import (
	"context"
	"fmt"

	"github.com/pkg/errors"

	"go.nlx.io/nlx/manager/domain/contract"
)

type GetCertificateHandler struct {
	repository contract.Repository
	selfPeerID contract.PeerID
}

type NewGetCertificateHandlerArg struct {
	Repository contract.Repository
	SelfPeerID contract.PeerID
}

func NewGetCertificateHandler(args *NewGetCertificateHandlerArg) (*GetCertificateHandler, error) {
	if args.Repository == nil {
		return nil, fmt.Errorf("repository is required")
	}

	if args.SelfPeerID.Value() == "" {
		return nil, fmt.Errorf("peer id is required")
	}

	return &GetCertificateHandler{
		repository: args.Repository,
		selfPeerID: args.SelfPeerID,
	}, nil
}

func (h *GetCertificateHandler) Handle(ctx context.Context, thumbprint string) (*Certificate, error) {
	certThumbprints, err := contract.NewCertificateThumbprints([]string{thumbprint})
	if err != nil {
		return nil, errors.Wrap(err, "unable to create certificate thumbprint")
	}

	certs, err := h.repository.GetPeerCertsByCertThumbprints(ctx, h.selfPeerID, certThumbprints)
	if err != nil {
		return nil, errors.Wrap(err, "could not retrieve certificates from repository")
	}

	certificate, err := certs.GetCertificate(certThumbprints[0])
	if err != nil {
		return nil, errors.Wrap(err, "could not retrieve certificate from repository")
	}

	return &Certificate{RawDERs: certificate.RawDERs()}, nil
}
