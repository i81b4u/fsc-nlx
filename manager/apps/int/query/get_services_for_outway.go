// Copyright © VNG Realisatie 2023
// Licensed under the EUPL

package query

import (
	"context"

	"github.com/pkg/errors"

	"go.nlx.io/nlx/manager/domain/contract"
)

type GetServicesForOutwayHandler struct {
	repo contract.Repository
}

type OutwayServices map[string]*OutwayService

type OutwayService struct {
	PeerID string
	Name   string
}

func NewGetServicesForOutwayHandler(repository contract.Repository) (*GetServicesForOutwayHandler, error) {
	if repository == nil {
		return nil, errors.New("repository is required")
	}

	return &GetServicesForOutwayHandler{
		repo: repository,
	}, nil
}

func (h *GetServicesForOutwayHandler) Handle(ctx context.Context, outwayCertThumbprint string) (OutwayServices, error) {
	res, err := h.repo.GetServicesForOutway(ctx, outwayCertThumbprint)
	if err != nil {
		return nil, errors.Wrapf(err, "could not get services from repo for outway certificate thumbprint: %s", outwayCertThumbprint)
	}

	services := make(OutwayServices)
	for grantHash, s := range res {
		services[grantHash] = &OutwayService{
			PeerID: s.PeerID,
			Name:   s.Name,
		}
	}

	return services, nil
}
