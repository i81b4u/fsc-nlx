// Copyright © VNG Realisatie 2023
// Licensed under the EUPL

package contract

import (
	"bytes"
	"sort"

	"github.com/pkg/errors"
)

type grantServiceConnectionHash Hash

func newGrantServiceConnectionHash(pID contentID, alg HashAlg, gc *GrantServiceConnection) (*grantServiceConnectionHash, error) {
	h, err := newHash(alg, HashTypeGrantServiceConnection, getSortedGrantConnectionBytes(pID, gc))
	if err != nil {
		return nil, errors.Wrap(err, "could not create grant service connection hash")
	}

	return (*grantServiceConnectionHash)(h), nil
}

func getSortedGrantConnectionBytes(pID contentID, gc *GrantServiceConnection) []byte {
	byteArrays := make([][]byte, 0)

	byteArrays = append(byteArrays,
		pID.Bytes(),
		[]byte(gc.outway.peer.id),
		[]byte(gc.service.peer.id),
		[]byte(gc.service.name),
	)

	byteArrays = append(byteArrays, []byte(gc.outway.certificateThumbprint.Value()))

	sort.Slice(byteArrays, func(i, j int) bool {
		return bytes.Compare(byteArrays[i], byteArrays[j]) < 0
	})

	bytesToHash := make([]byte, 0)
	for _, byteArray := range byteArrays {
		bytesToHash = append(bytesToHash, byteArray...)
	}

	return bytesToHash
}

func (h grantServiceConnectionHash) String() string {
	return Hash(h).String()
}
