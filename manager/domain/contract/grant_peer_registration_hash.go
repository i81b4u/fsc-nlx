// Copyright © VNG Realisatie 2023
// Licensed under the EUPL

package contract

import (
	"bytes"
	"sort"

	"github.com/pkg/errors"
)

type grantPeerRegistrationHash Hash

func newGrantPeerRegistrationHash(pID contentID, alg HashAlg, gc *GrantPeerRegistration) (*grantPeerRegistrationHash, error) {
	h, err := newHash(alg, HashTypeGrantPeerRegistration, getSortedGrantPeerRegistrationBytes(pID, gc))
	if err != nil {
		return nil, errors.Wrap(err, "could not create grant peer registration hash")
	}

	return (*grantPeerRegistrationHash)(h), nil
}

func getSortedGrantPeerRegistrationBytes(pID contentID, gc *GrantPeerRegistration) []byte {
	byteArrays := make([][]byte, 0)

	byteArrays = append(byteArrays,
		pID.Bytes(),
		[]byte(gc.directory.peerID),
		[]byte(gc.peer.id),
		[]byte(gc.peer.name),
	)

	sort.Slice(byteArrays, func(i, j int) bool {
		return bytes.Compare(byteArrays[i], byteArrays[j]) < 0
	})

	bytesToHash := make([]byte, 0)
	for _, byteArray := range byteArrays {
		bytesToHash = append(bytesToHash, byteArray...)
	}

	return bytesToHash
}

func (h grantPeerRegistrationHash) String() string {
	return Hash(h).String()
}
