// Copyright © VNG Realisatie 2023
// Licensed under the EUPL

package contract

import (
	"fmt"
)

const (
	managerAddressMaxLength = 255 // valid DNS hostnames can't be longer than 255 characters: https://www.rfc-editor.org/rfc/rfc3986#section-3.2.2
)

type NewGrantPeerRegistrationArgs struct {
	Directory *NewGrantPeerRegistrationDirectoryArgs
	Peer      *NewGrantPeerRegistrationPeerArgs
}

type NewGrantPeerRegistrationDirectoryArgs struct {
	PeerID string
}

type NewGrantPeerRegistrationPeerArgs struct {
	ID   string
	Name string
}

type GrantPeerRegistration struct {
	hash      *grantPeerRegistrationHash
	directory *grantPeerRegistrationDirectory
	peer      *grantPeerRegistrationPeer
}

type grantPeerRegistrationDirectory struct {
	peerID PeerID
}

type managerAddress string

type grantPeerRegistrationPeer struct {
	id   PeerID
	name peerName
}

func (g *GrantPeerRegistration) Directory() *grantPeerRegistrationDirectory {
	return g.directory
}

func (g *GrantPeerRegistration) Hash() *grantPeerRegistrationHash {
	return g.hash
}

func (g *GrantPeerRegistration) Peer() *grantPeerRegistrationPeer {
	return g.peer
}

func (s *grantPeerRegistrationPeer) Name() string {
	return string(s.name)
}

func (s *grantPeerRegistrationPeer) ID() PeerID {
	return s.id
}

func (c *grantPeerRegistrationDirectory) PeerID() PeerID {
	return c.peerID
}

func newGrantPeerRegistration(pID contentID, alg HashAlg, args *NewGrantPeerRegistrationArgs) (*GrantPeerRegistration, error) {
	if args == nil {
		return nil, fmt.Errorf("new peer registration args cannot be nil")
	}

	directory, err := newGrantPeerRegistrationDirectory(args.Directory)
	if err != nil {
		return nil, err
	}

	peer, err := newGrantPeerRegistrationPeer(args.Peer)
	if err != nil {
		return nil, err
	}

	h, err := newGrantPeerRegistrationHash(pID, alg, &GrantPeerRegistration{
		directory: directory,
		peer:      peer,
	})
	if err != nil {
		return nil, err
	}

	return &GrantPeerRegistration{
		hash:      h,
		directory: directory,
		peer:      peer,
	}, nil
}

func newGrantPeerRegistrationPeer(args *NewGrantPeerRegistrationPeerArgs) (*grantPeerRegistrationPeer, error) {
	if args == nil {
		return nil, fmt.Errorf("new grant peer registration peer args cannot be nil")
	}

	s, err := NewPeerID(args.ID)
	if err != nil {
		return nil, err
	}

	name, err := newPeerName(args.Name)
	if err != nil {
		return nil, err
	}

	return &grantPeerRegistrationPeer{
		id:   s,
		name: name,
	}, nil
}

func newGrantPeerRegistrationDirectory(args *NewGrantPeerRegistrationDirectoryArgs) (*grantPeerRegistrationDirectory, error) {
	if args == nil {
		return nil, fmt.Errorf("new grant peer registration directory args cannot be nil")
	}

	s, err := NewPeerID(args.PeerID)
	if err != nil {
		return nil, err
	}

	return &grantPeerRegistrationDirectory{
		peerID: s,
	}, nil
}

func newManagerAddress(s string) (managerAddress, error) {
	if s == "" {
		return "", fmt.Errorf("manager address cannot be empty")
	}

	if len(s) > managerAddressMaxLength {
		return "", fmt.Errorf("manager address cannot be longer than %d characters", managerAddressMaxLength)
	}

	// TODO check if valid DNS name and valid port (either 8443 or 443)

	return managerAddress(s), nil
}
