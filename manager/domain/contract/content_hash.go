// Copyright © VNG Realisatie 2023
// Licensed under the EUPL

package contract

import (
	"bytes"
	"fmt"
	"sort"

	"github.com/pkg/errors"
)

type ContentHash Hash

func NewContentHashFromString(h string) (*ContentHash, error) {
	hVal, err := DecodeHashFromString(h)
	if err != nil {
		return nil, errors.Wrap(err, "could not decode content hash from string")
	}

	return (*ContentHash)(hVal), nil
}

func newContentHash(c *Content, alg HashAlg) (*ContentHash, error) {
	pBytes, err := getSortedContentBytes(c)
	if err != nil {
		return nil, errors.Wrap(err, "could not get sorted bytes from content")
	}

	h, err := newHash(alg, HashTypeContent, pBytes)
	if err != nil {
		return nil, errors.Wrap(err, "could not create grant peer registration hash")
	}

	return (*ContentHash)(h), nil
}

func getSortedContentBytes(content *Content) ([]byte, error) {
	byteArrays := make([][]byte, 0)

	byteArrays = append(byteArrays,
		content.id.Bytes(),
		[]byte(content.group),
		bytesFromInt64(content.validity.notBefore.Unix()),
		bytesFromInt64(content.validity.notAfter.Unix()),
		bytesFromInt64(content.createdAt.Unix()),
	)

	for _, grant := range content.grants {
		switch g := grant.(type) {
		case *GrantPeerRegistration:
			byteArrays = append(byteArrays, []byte(g.Hash().String()))
		case *GrantServicePublication:
			byteArrays = append(byteArrays, []byte(g.Hash().String()))
		case *GrantServiceConnection:
			byteArrays = append(byteArrays, []byte(g.Hash().String()))
		case *GrantDelegatedServiceConnection:
			byteArrays = append(byteArrays, []byte(g.Hash().String()))
		case *GrantDelegatedServicePublication:
			byteArrays = append(byteArrays, []byte(g.Hash().String()))
		default:
			return nil, fmt.Errorf("unsupported grant type, %T", g)
		}
	}

	sort.Slice(byteArrays, func(i, j int) bool {
		return bytes.Compare(byteArrays[i], byteArrays[j]) < 0
	})

	bytesToHash := make([]byte, 0)
	for _, byteArray := range byteArrays {
		bytesToHash = append(bytesToHash, byteArray...)
	}

	return bytesToHash, nil
}

func (h ContentHash) String() string {
	return Hash(h).String()
}

func (h ContentHash) Algorithm() HashAlg {
	return Hash(h).Algorithm()
}

func (h *ContentHash) isEqual(other *ContentHash) bool {
	return (*Hash)(h).isEqual((*Hash)(other))
}
