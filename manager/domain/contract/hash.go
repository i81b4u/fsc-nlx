// Copyright © VNG Realisatie 2023
// Licensed under the EUPL

package contract

import (
	"encoding/base64"
	"encoding/binary"
	"fmt"
	"strconv"
	"strings"

	"github.com/pkg/errors"
	"golang.org/x/crypto/sha3"
	"golang.org/x/exp/slices"
)

const (
	maxHashLength     = 1 + stringInt32Length + 1 + stringInt32Length + 1 + 88 // $ + int32 + $ + int32 + $ + sha3_512 as base64
	stringInt32Length = 10                                                     // int32 in string format
	hashParts         = 3
)

type HashAlg int32

const (
	HashAlgUnspecified HashAlg = iota
	HashAlgSHA3_512
)

var hashAlgs = []HashAlg{HashAlgSHA3_512}

type HashType int32

const (
	HashTypeUnspecified HashType = iota
	HashTypeContent
	HashTypeGrantPeerRegistration
	HashTypeGrantServicePublication
	HashTypeGrantServiceConnection
	HashTypeGrantDelegatedServiceConnection
	HashTypeGrantDelegatedServicePublication
)

var hashTypes = []HashType{
	HashTypeContent,
	HashTypeGrantPeerRegistration,
	HashTypeGrantServicePublication,
	HashTypeGrantServiceConnection,
	HashTypeGrantDelegatedServiceConnection,
	HashTypeGrantDelegatedServicePublication,
}

type Hash struct {
	hash      []byte
	algorithm HashAlg
	t         HashType
}

func newHash(alg HashAlg, hashType HashType, bytesToHash []byte) (*Hash, error) {
	if !slices.Contains(hashTypes, hashType) {
		return nil, fmt.Errorf("unsupported hash type: %d", hashType)
	}

	if len(bytesToHash) == 0 {
		return nil, fmt.Errorf("bytes to hash cannot be empty")
	}

	var h []byte

	switch alg {
	case HashAlgSHA3_512:
		h = newSHA512Hash(bytesToHash)
	default:
		return nil, fmt.Errorf("unsupported hash algorithm: %d", alg)
	}

	return &Hash{
		hash:      h,
		algorithm: alg,
		t:         hashType,
	}, nil
}

func DecodeHashFromString(h string) (*Hash, error) {
	if len(h) > maxHashLength {
		return nil, fmt.Errorf("hash cannot be longer than %d characters", maxHashLength)
	}

	if h[0] != '$' {
		return nil, fmt.Errorf("invalid hash prefix, must start with '$'")
	}

	parts := strings.Split(h, "$")

	partsLen := len(parts) - 1 // need to skip the first item because that's an empty string
	if partsLen != hashParts {
		return nil, fmt.Errorf("invalid hash, expected %d parts but got %d", hashParts, partsLen)
	}

	stringHashAlg, stringHashType, base64Hash := parts[1], parts[2], parts[3]

	if len(stringHashAlg) > stringInt32Length {
		return nil, fmt.Errorf("invalid hash alg, can't be bigger than %d characters", stringInt32Length)
	}

	if len(stringHashType) > stringInt32Length {
		return nil, fmt.Errorf("invalid hash alg, can't be bigger than %d characters", stringInt32Length)
	}

	intAlg, err := strconv.Atoi(stringHashAlg)
	if err != nil {
		return nil, fmt.Errorf("hash alg is not a valid integer: %s", stringHashAlg)
	}

	alg := HashAlg(intAlg)

	intHashType, err := strconv.Atoi(stringHashType)
	if err != nil {
		return nil, fmt.Errorf("hash type is not a valid integer: %s", stringHashType)
	}

	hashType := HashType(intHashType)

	decodedHash, err := base64.URLEncoding.DecodeString(base64Hash)
	if err != nil {
		return nil, errors.Wrapf(err, "could not decode hash from base64 string: %s", base64Hash)
	}

	if !slices.Contains(hashAlgs, alg) {
		return nil, fmt.Errorf("unsupported hash algorithm: %d", alg)
	}

	if !slices.Contains(hashTypes, hashType) {
		return nil, fmt.Errorf("unsupported hash type: %d", hashType)
	}

	if len(decodedHash) == 0 {
		return nil, fmt.Errorf("hash value cannot be empty")
	}

	return &Hash{
		hash:      decodedHash,
		algorithm: alg,
		t:         hashType,
	}, nil
}

func (h *Hash) isEqual(other *Hash) bool {
	return h.algorithm == other.algorithm &&
		h.t == other.t &&
		slices.Compare(h.hash, other.hash) == 0
}

func newSHA512Hash(data []byte) []byte {
	h := sha3.Sum512(data)
	return h[:]
}

func (h Hash) String() string {
	return fmt.Sprintf("$%d$%d$%s", h.algorithm, h.t, base64.URLEncoding.EncodeToString(h.hash))
}

func (h Hash) Algorithm() HashAlg {
	return h.algorithm
}

func (h Hash) Type() HashType {
	return h.t
}

const int64Bytes = 8

func bytesFromInt64(i int64) []byte {
	b := make([]byte, int64Bytes)
	binary.LittleEndian.PutUint64(b, uint64(i))

	return b
}
