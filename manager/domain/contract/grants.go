// Copyright © VNG Realisatie 2023
// Licensed under the EUPL

package contract

import (
	"fmt"

	"github.com/pkg/errors"
)

type grants []interface{}

// type of single grant
type GrantType int32

const (
	GrantTypeUnspecified GrantType = iota
	GrantTypePeerRegistration
	GrantTypeServicePublication
	GrantTypeServiceConnection
	GrantTypeDelegatedServiceConnection
	GrantTypeDelegatedServicePublication
)

const (
	minAmountOfGrants = 1
	maxAmountOfGrants = 100
)

func NewGrantTypeFromString(g string) (GrantType, error) {
	if g == "" {
		return GrantTypeUnspecified, fmt.Errorf("grant type cannot be empty")
	}

	switch g {
	case "peerRegistration":
		return GrantTypePeerRegistration, nil
	case "servicePublication":
		return GrantTypeServicePublication, nil
	case "serviceConnection":
		return GrantTypeServiceConnection, nil
	case "delegatedServiceConnection":
		return GrantTypeDelegatedServiceConnection, nil
	case "delegatedServicePublication":
		return GrantTypeDelegatedServicePublication, nil
	default:
		return GrantTypeUnspecified, fmt.Errorf("invalid grant type: %s", g)
	}
}

func newGrants(pID contentID, alg HashAlg, g []interface{}) (grants, error) {
	if g == nil {
		return nil, fmt.Errorf("grants cannot be nil")
	}

	if len(g) < minAmountOfGrants {
		return nil, fmt.Errorf("amount of grants must be at least %d", minAmountOfGrants)
	}

	if len(g) > maxAmountOfGrants {
		return nil, fmt.Errorf("amount of grants cannot be greater than %d grants", maxAmountOfGrants)
	}

	modelGrants := make([]interface{}, 0)

	gTypes := make(map[GrantType]uint, 0)

	for _, grant := range g {
		switch v := grant.(type) {
		case *NewGrantPeerRegistrationArgs:
			m, err := newGrantPeerRegistration(pID, alg, v)
			if err != nil {
				return nil, err
			}

			modelGrants = append(modelGrants, m)
			gTypes[GrantTypePeerRegistration] += 1
		case *NewGrantServicePublicationArgs:
			m, err := newGrantServicePublication(pID, alg, v)
			if err != nil {
				return nil, err
			}

			modelGrants = append(modelGrants, m)
			gTypes[GrantTypeServicePublication] += 1
		case *NewGrantServiceConnectionArgs:
			m, err := newGrantServiceConnection(pID, alg, v)
			if err != nil {
				return nil, err
			}

			modelGrants = append(modelGrants, m)
			gTypes[GrantTypeServiceConnection] += 1
		case *NewGrantDelegatedServiceConnectionArgs:
			m, err := newGrantDelegatedServiceConnection(pID, alg, v)
			if err != nil {
				return nil, err
			}

			modelGrants = append(modelGrants, m)
			gTypes[GrantTypeDelegatedServiceConnection] += 1

		case *NewGrantDelegatedServicePublicationArgs:
			m, err := newGrantDelegatedServicePublication(pID, alg, v)
			if err != nil {
				return nil, err
			}

			modelGrants = append(modelGrants, m)
			gTypes[GrantTypeDelegatedServicePublication] += 1
		default:
			return nil, fmt.Errorf("unknown grant type: %T", v)
		}
	}

	err := validateGrantTypeCombinations(gTypes)
	if err != nil {
		return nil, errors.Wrap(err, "invalid grant type combination found")
	}

	return modelGrants, nil
}

func validateGrantTypeCombinations(t map[GrantType]uint) error {
	directoryGrants := t[GrantTypePeerRegistration] > 0 || t[GrantTypeServicePublication] > 0 || t[GrantTypeDelegatedServicePublication] > 0
	peerGrants := t[GrantTypeServiceConnection] > 0 || t[GrantTypeDelegatedServiceConnection] > 0

	if directoryGrants && peerGrants {
		return fmt.Errorf("cannot combine peer grants and directory grants in one contract")
	}

	if peerGrants {
		return nil
	}

	peerRegistrationGrants := t[GrantTypePeerRegistration] > 0
	servicePublicationGrants := t[GrantTypeServicePublication] > 0
	delegatedServicePublicationGrants := t[GrantTypeDelegatedServicePublication] > 0

	if peerRegistrationGrants && (servicePublicationGrants || delegatedServicePublicationGrants) {
		return fmt.Errorf("cannot combine peer registration grants and (delegated) service publication grants in one contract")
	}

	if t[GrantTypePeerRegistration] > 1 {
		return fmt.Errorf("contract cannot contain more than 1 peer registration grant")
	}

	return nil
}

func (g grants) Types() map[GrantType]bool {
	t := make(map[GrantType]bool)

	for _, grant := range g {
		switch grant.(type) {
		case *GrantPeerRegistration:
			t[GrantTypePeerRegistration] = true
		case *GrantServicePublication:
			t[GrantTypeServicePublication] = true
		case *GrantServiceConnection:
			t[GrantTypeServiceConnection] = true
		case *GrantDelegatedServiceConnection:
			t[GrantTypeDelegatedServiceConnection] = true
		case *GrantDelegatedServicePublication:
			t[GrantTypeDelegatedServicePublication] = true
		}
	}

	return t
}

func (g grants) ServiceConnectionGrants() []*GrantServiceConnection {
	result := []*GrantServiceConnection{}

	for _, grant := range g {
		serviceConnectionGrant, ok := grant.(*GrantServiceConnection)
		if !ok {
			continue
		}

		result = append(result, serviceConnectionGrant)
	}

	return result
}

func (g grants) DelegatedServiceConnectionGrants() []*GrantDelegatedServiceConnection {
	result := []*GrantDelegatedServiceConnection{}

	for _, grant := range g {
		g, ok := grant.(*GrantDelegatedServiceConnection)
		if !ok {
			continue
		}

		result = append(result, g)
	}

	return result
}

func (g grants) ServicePublicationGrants() []*GrantServicePublication {
	result := []*GrantServicePublication{}

	for _, grant := range g {
		serviceServicePublication, ok := grant.(*GrantServicePublication)
		if !ok {
			continue
		}

		result = append(result, serviceServicePublication)
	}

	return result
}

func (g grants) PeerRegistrationGrant() *GrantPeerRegistration {
	for _, grant := range g {
		peerRegistration, ok := grant.(*GrantPeerRegistration)
		if !ok {
			continue
		}

		return peerRegistration
	}

	return nil
}

func (g grants) DelegatedServicePublicationGrants() []*GrantDelegatedServicePublication {
	result := []*GrantDelegatedServicePublication{}

	for _, grant := range g {
		delegatedServiceServicePublication, ok := grant.(*GrantDelegatedServicePublication)
		if !ok {
			continue
		}

		result = append(result, delegatedServiceServicePublication)
	}

	return result
}
