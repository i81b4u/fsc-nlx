// Copyright © VNG Realisatie 2023
// Licensed under the EUPL

package contract

import (
	"bytes"
	"sort"

	"github.com/pkg/errors"
)

type grantDelegatedServicePublicationHash Hash

func newGrantDelegatedServicePublicationHash(pID contentID, alg HashAlg, gc *GrantDelegatedServicePublication) (*grantDelegatedServicePublicationHash, error) {
	h, err := newHash(alg, HashTypeGrantDelegatedServicePublication, getSortedGrantDelegatedPublicationBytes(pID, gc))
	if err != nil {
		return nil, errors.Wrap(err, "could not create grant delegated service connection hash")
	}

	return (*grantDelegatedServicePublicationHash)(h), nil
}

func getSortedGrantDelegatedPublicationBytes(pID contentID, gc *GrantDelegatedServicePublication) []byte {
	byteArrays := make([][]byte, 0)

	byteArrays = append(byteArrays,
		pID.Bytes(),
		[]byte(gc.directory.peer.id),
		[]byte(gc.delegator.peer.id),
		[]byte(gc.service.peer.id),
		[]byte(gc.service.name),
	)

	sort.Slice(byteArrays, func(i, j int) bool {
		return bytes.Compare(byteArrays[i], byteArrays[j]) < 0
	})

	bytesToHash := make([]byte, 0)
	for _, byteArray := range byteArrays {
		bytesToHash = append(bytesToHash, byteArray...)
	}

	return bytesToHash
}

func (h grantDelegatedServicePublicationHash) String() string {
	return Hash(h).String()
}
