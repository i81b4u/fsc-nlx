// Copyright © VNG Realisatie 2023
// Licensed under the EUPL

package contract

import "fmt"

type NewGrantDelegatedServiceConnectionArgs struct {
	Outway    *NewGrantDelegatedServiceConnectionOutwayArgs
	Service   *NewGrantDelegatedServiceConnectionServiceArgs
	Delegator *NewGrantDelegatedServiceConnectionDelegatorArgs
}

type NewGrantDelegatedServiceConnectionOutwayArgs struct {
	Peer                  *NewPeerArgs
	CertificateThumbprint string
}

type NewGrantDelegatedServiceConnectionServiceArgs struct {
	Peer *NewPeerArgs
	Name string
}

type NewGrantDelegatedServiceConnectionDelegatorArgs struct {
	Peer *NewPeerArgs
}

type GrantDelegatedServiceConnection struct {
	hash      *grantDelegatedServiceConnectionHash
	outway    *grantDelegatedServiceConnectionOutway
	service   *grantDelegatedServiceConnectionService
	delegator *grantDelegatedServiceConnectionDelegator
}

type grantDelegatedServiceConnectionOutway struct {
	peer                  *Peer
	certificateThumbprint CertificateThumbprint
}

type grantDelegatedServiceConnectionService struct {
	peer *Peer
	name serviceName
}

type grantDelegatedServiceConnectionDelegator struct {
	peer *Peer
}

func (g *GrantDelegatedServiceConnection) Outway() *grantDelegatedServiceConnectionOutway {
	return g.outway
}

func (g *GrantDelegatedServiceConnection) Hash() *grantDelegatedServiceConnectionHash {
	return g.hash
}

func (g *GrantDelegatedServiceConnection) Service() *grantDelegatedServiceConnectionService {
	return g.service
}

func (g *GrantDelegatedServiceConnection) Delegator() *grantDelegatedServiceConnectionDelegator {
	return g.delegator
}

func (s *grantDelegatedServiceConnectionService) Peer() *Peer {
	return s.peer
}

func (s *grantDelegatedServiceConnectionService) Name() string {
	return string(s.name)
}

func (c *grantDelegatedServiceConnectionOutway) Peer() *Peer {
	return c.peer
}

func (s *grantDelegatedServiceConnectionDelegator) Peer() *Peer {
	return s.peer
}

func (c *grantDelegatedServiceConnectionOutway) CertificateThumbprint() CertificateThumbprint {
	return c.certificateThumbprint
}

// nolint:dupl // grants are definitively similar but still have distinctive properties
func newGrantDelegatedServiceConnection(pID contentID, alg HashAlg, args *NewGrantDelegatedServiceConnectionArgs) (*GrantDelegatedServiceConnection, error) {
	if args == nil {
		return nil, fmt.Errorf("new delegated service connection args cannot be nil")
	}

	outway, err := newGrantDelegatedServiceConnectionOutway(args.Outway)
	if err != nil {
		return nil, err
	}

	service, err := newGrantDelegatedServiceConnectionService(args.Service)
	if err != nil {
		return nil, err
	}

	delegator, err := newGrantDelegatedServiceConnectionDelegator(args.Delegator)
	if err != nil {
		return nil, err
	}

	h, err := newGrantDelegatedServiceConnectionHash(pID, alg, &GrantDelegatedServiceConnection{
		outway:    outway,
		service:   service,
		delegator: delegator,
	})
	if err != nil {
		return nil, err
	}

	return &GrantDelegatedServiceConnection{
		hash:      h,
		outway:    outway,
		service:   service,
		delegator: delegator,
	}, nil
}

func newGrantDelegatedServiceConnectionService(args *NewGrantDelegatedServiceConnectionServiceArgs) (*grantDelegatedServiceConnectionService, error) {
	if args == nil {
		return nil, fmt.Errorf("new grant delegated service connection service args cannot be nil")
	}

	p, err := NewPeer(args.Peer)
	if err != nil {
		return nil, err
	}

	name, err := newServiceName(args.Name)
	if err != nil {
		return nil, err
	}

	return &grantDelegatedServiceConnectionService{
		peer: p,
		name: name,
	}, nil
}

func newGrantDelegatedServiceConnectionOutway(args *NewGrantDelegatedServiceConnectionOutwayArgs) (*grantDelegatedServiceConnectionOutway, error) {
	if args == nil {
		return nil, fmt.Errorf("new grant delegated service connection outway args cannot be nil")
	}

	p, err := NewPeer(args.Peer)
	if err != nil {
		return nil, err
	}

	thumbprint, err := NewCertificateThumbprint(args.CertificateThumbprint)
	if err != nil {
		return nil, err
	}

	return &grantDelegatedServiceConnectionOutway{
		peer:                  p,
		certificateThumbprint: thumbprint,
	}, nil
}

func newGrantDelegatedServiceConnectionDelegator(args *NewGrantDelegatedServiceConnectionDelegatorArgs) (*grantDelegatedServiceConnectionDelegator, error) {
	if args == nil {
		return nil, fmt.Errorf("new grant delegated service connection delegator args cannot be nil")
	}

	p, err := NewPeer(args.Peer)
	if err != nil {
		return nil, err
	}

	return &grantDelegatedServiceConnectionDelegator{
		peer: p,
	}, nil
}
