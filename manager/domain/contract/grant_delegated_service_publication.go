// Copyright © VNG Realisatie 2023
// Licensed under the EUPL

package contract

import "fmt"

type NewGrantDelegatedServicePublicationArgs struct {
	Directory *NewGrantDelegatedServicePublicationDirectoryArgs
	Service   *NewGrantDelegatedServicePublicationServiceArgs
	Delegator *NewGrantDelegatedServicePublicationDelegatorArgs
}

type NewGrantDelegatedServicePublicationDirectoryArgs struct {
	Peer *NewPeerArgs
}

type NewGrantDelegatedServicePublicationServiceArgs struct {
	Peer *NewPeerArgs
	Name string
}

type NewGrantDelegatedServicePublicationDelegatorArgs struct {
	Peer *NewPeerArgs
}

type GrantDelegatedServicePublication struct {
	hash      *grantDelegatedServicePublicationHash
	directory *grantDelegatedServicePublicationDirectory
	service   *grantDelegatedServicePublicationService
	delegator *grantDelegatedServicePublicationDelegator
}

type grantDelegatedServicePublicationDirectory struct {
	peer *Peer
}

type grantDelegatedServicePublicationService struct {
	peer *Peer
	name serviceName
}

type grantDelegatedServicePublicationDelegator struct {
	peer *Peer
}

func (g *GrantDelegatedServicePublication) Directory() *grantDelegatedServicePublicationDirectory {
	return g.directory
}

func (g *GrantDelegatedServicePublication) Hash() *grantDelegatedServicePublicationHash {
	return g.hash
}

func (g *GrantDelegatedServicePublication) Service() *grantDelegatedServicePublicationService {
	return g.service
}

func (g *GrantDelegatedServicePublication) Delegator() *grantDelegatedServicePublicationDelegator {
	return g.delegator
}

func (s *grantDelegatedServicePublicationService) Peer() *Peer {
	return s.peer
}

func (s *grantDelegatedServicePublicationService) Name() string {
	return string(s.name)
}

func (c *grantDelegatedServicePublicationDirectory) Peer() *Peer {
	return c.peer
}

func (s *grantDelegatedServicePublicationDelegator) Peer() *Peer {
	return s.peer
}

// nolint:dupl // grants are definitively similar but still have distinctive properties
func newGrantDelegatedServicePublication(pID contentID, alg HashAlg, args *NewGrantDelegatedServicePublicationArgs) (*GrantDelegatedServicePublication, error) {
	if args == nil {
		return nil, fmt.Errorf("new delegated service publication args cannot be nil")
	}

	directory, err := newGrantDelegatedServicePublicationDirectory(args.Directory)
	if err != nil {
		return nil, err
	}

	service, err := newGrantDelegatedServicePublicationService(args.Service)
	if err != nil {
		return nil, err
	}

	delegator, err := newGrantDelegatedServicePublicationDelegator(args.Delegator)
	if err != nil {
		return nil, err
	}

	h, err := newGrantDelegatedServicePublicationHash(pID, alg, &GrantDelegatedServicePublication{
		directory: directory,
		service:   service,
		delegator: delegator,
	})
	if err != nil {
		return nil, err
	}

	return &GrantDelegatedServicePublication{
		hash:      h,
		directory: directory,
		service:   service,
		delegator: delegator,
	}, nil
}

func newGrantDelegatedServicePublicationService(args *NewGrantDelegatedServicePublicationServiceArgs) (*grantDelegatedServicePublicationService, error) {
	if args == nil {
		return nil, fmt.Errorf("new grant delegated service publication service args cannot be nil")
	}

	p, err := NewPeer(args.Peer)
	if err != nil {
		return nil, err
	}

	name, err := newServiceName(args.Name)
	if err != nil {
		return nil, err
	}

	return &grantDelegatedServicePublicationService{
		peer: p,
		name: name,
	}, nil
}

func newGrantDelegatedServicePublicationDirectory(args *NewGrantDelegatedServicePublicationDirectoryArgs) (*grantDelegatedServicePublicationDirectory, error) {
	if args == nil {
		return nil, fmt.Errorf("new grant delegated service publication directory args cannot be nil")
	}

	p, err := NewPeer(args.Peer)
	if err != nil {
		return nil, err
	}

	return &grantDelegatedServicePublicationDirectory{
		peer: p,
	}, nil
}

func newGrantDelegatedServicePublicationDelegator(args *NewGrantDelegatedServicePublicationDelegatorArgs) (*grantDelegatedServicePublicationDelegator, error) {
	if args == nil {
		return nil, fmt.Errorf("new grant delegated service publication delegator args cannot be nil")
	}

	p, err := NewPeer(args.Peer)
	if err != nil {
		return nil, err
	}

	return &grantDelegatedServicePublicationDelegator{
		peer: p,
	}, nil
}
