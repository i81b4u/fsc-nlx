// Copyright © VNG Realisatie 2023
// Licensed under the EUPL

package contract

import (
	"bytes"
	"sort"

	"github.com/pkg/errors"
)

type grantPublicationHash Hash

func newGrantServicePublicationHash(pID contentID, alg HashAlg, gc *GrantServicePublication) (*grantPublicationHash, error) {
	h, err := newHash(alg, HashTypeGrantServicePublication, getSortedGrantPublicationBytes(pID, gc))
	if err != nil {
		return nil, errors.Wrap(err, "could not create grant service publication hash")
	}

	return (*grantPublicationHash)(h), nil
}

func getSortedGrantPublicationBytes(pID contentID, gc *GrantServicePublication) []byte {
	byteArrays := make([][]byte, 0)

	byteArrays = append(byteArrays,
		pID.Bytes(),
		[]byte(gc.directory.peer.id),
		[]byte(gc.service.peer.id),
		[]byte(gc.service.name),
	)

	sort.Slice(byteArrays, func(i, j int) bool {
		return bytes.Compare(byteArrays[i], byteArrays[j]) < 0
	})

	bytesToHash := make([]byte, 0)
	for _, byteArray := range byteArrays {
		bytesToHash = append(bytesToHash, byteArray...)
	}

	return bytesToHash
}

func (h grantPublicationHash) String() string {
	return Hash(h).String()
}
