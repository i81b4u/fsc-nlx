// Copyright © VNG Realisatie 2023
// Licensed under the EUPL

package postgresadapter

import (
	"context"
	"crypto/x509"
	"fmt"

	"go.nlx.io/nlx/manager/adapters/storage/postgres/queries"
	"go.nlx.io/nlx/manager/domain/contract"
)

func (r *PostgreSQLRepository) ListContractsForOutwayConfig(ctx context.Context, certificateThumbprint string) ([]*contract.Contract, error) {
	contractRows, err := r.queries.ListContractsForOutwayConfig(ctx, certificateThumbprint)
	if err != nil {
		return nil, fmt.Errorf("could not list contracts for peer with service connection grant from database: %v", err)
	}

	if len(contractRows) == 0 {
		return []*contract.Contract{}, nil
	}

	result := make([]*contract.Contract, len(contractRows))

	for i, contractRow := range contractRows {
		model, contractErr := getContractFromDB(ctx, r.queries, r.trustedRootCAs, &createContractArgs{
			ID:            contractRow.ContentID,
			contentHash:   contractRow.ContentHash,
			groupID:       contractRow.ContentGroupID,
			hashAlgorithm: contractRow.ContentHashAlgorithm,
			createdAt:     contractRow.ContentCreatedAt,
			notBefore:     contractRow.ContentValidNotBefore,
			notAfter:      contractRow.ContentValidNotAfter,
		})
		if contractErr != nil {
			return nil, fmt.Errorf("invalid contract in database: %v", contractErr)
		}

		result[i] = model
	}

	return result, nil
}

func getPeersCertsByContractHash(ctx context.Context, trustedRootCAs *x509.CertPool, q *queries.Queries, contentHash string) (contract.PeersCertificates, error) {
	certificateRows, err := q.ListPeerCertificatesForContract(ctx, contentHash)
	if err != nil {
		return nil, err
	}

	peerCertificatesMap := make(contract.NewPeersCertificatesArgs)

	for _, certificateRow := range certificateRows {
		certificateBytes := make([][]byte, 0)
		certificateBytes = append(certificateBytes, certificateRow.Certificate)
		peerCertificatesMap[certificateRow.PeerID] = certificateBytes
	}

	peersCerts, err := contract.NewPeersCertificates(trustedRootCAs, peerCertificatesMap)
	if err != nil {
		return nil, err
	}

	return peersCerts, nil
}
