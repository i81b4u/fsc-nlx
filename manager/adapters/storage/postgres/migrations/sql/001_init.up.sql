-- Copyright © VNG Realisatie 2023
-- Licensed under the EUPL


BEGIN transaction;

CREATE SCHEMA contracts;
CREATE TYPE contracts.content_hash_algorithm AS ENUM ('sha3_512');

CREATE SCHEMA peers;

CREATE TABLE contracts.grants_service_connection (
    hash VARCHAR(150) NOT NULL,
    content_hash VARCHAR(150) NOT NULL,
    consumer_peer_id VARCHAR(20) NOT NULL,
    service_peer_id VARCHAR(20) NOT NULL,
    service_name varchar(255) NOT NULL,
    certificate_thumbprint varchar(512) NOT NULL,
    CONSTRAINT grants_service_connection_pk PRIMARY KEY (hash)
);

CREATE TABLE peers.certificates (
    certificate_thumbprint varchar(512) NOT NULL,
    peer_id VARCHAR(20) NOT NULL,
    certificate bytea NOT NULL,
    CONSTRAINT certificates_pk PRIMARY KEY (certificate_thumbprint)

);

CREATE TABLE peers.peers (
    id VARCHAR(20) NOT NULL,
    name varchar(60) NULL,
    manager_address varchar(255) NULL,
    CONSTRAINT peers_pk PRIMARY KEY (id)

);

CREATE TABLE contracts.content (
    hash VARCHAR(150) NOT NULL,
    hash_algorithm contracts.content_hash_algorithm NOT NULL,
    id uuid NOT NULL, -- UUID v7
    group_id varchar(255) NOT NULL,
    valid_not_before timestamptz NOT NULL,
    valid_not_after timestamptz NOT NULL,
    created_at timestamptz NOT NULL,
    CONSTRAINT content_pk PRIMARY KEY (hash)

);

CREATE TABLE contracts.grants_delegated_service_connection (
    hash VARCHAR(150) NOT NULL,
    content_hash VARCHAR(150) NOT NULL,
    outway_peer_id VARCHAR(20) NOT NULL,
    delegator_peer_id VARCHAR(20) NOT NULL,
    service_peer_id VARCHAR(20) NOT NULL,
    service_name varchar(255) NOT NULL,
    certificate_thumbprint varchar(512) NOT NULL,
    CONSTRAINT grants_delegated_service_connection_pk PRIMARY KEY (hash)
);


CREATE TABLE contracts.grants_service_publication (
    hash VARCHAR(150) NOT NULL,
    content_hash VARCHAR(150) NOT NULL,
    directory_peer_id VARCHAR(20) NOT NULL,
    service_peer_id VARCHAR(20) NOT NULL,
    service_name varchar(255) NOT NULL,
    CONSTRAINT grants_service_publication_pk PRIMARY KEY (hash)

);

CREATE TABLE contracts.grants_peer_registration (
    hash VARCHAR(150) NOT NULL,
    content_hash VARCHAR(150) NOT NULL,
    directory_peer_id VARCHAR(20) NOT NULL,
    peer_id VARCHAR(20) NOT NULL,
    peer_name varchar(255) NOT NULL,
    CONSTRAINT grants_peer_registration_pk PRIMARY KEY (hash)

);

CREATE TABLE contracts.grants_delegated_service_publication (
  hash VARCHAR(150) NOT NULL,
  content_hash VARCHAR(150) NOT NULL,
  directory_peer_id VARCHAR(20) NOT NULL,
  delegator_peer_id VARCHAR(20) NOT NULL,
  service_peer_id VARCHAR(20) NOT NULL,
  service_name varchar(255) NOT NULL,
  CONSTRAINT grants_delegated_service_publication_pk PRIMARY KEY (hash)
);

CREATE TYPE contracts.signature_type AS
    ENUM ('accept','reject','revoke');

CREATE TABLE contracts.contract_signatures (
    content_hash VARCHAR(150) NOT NULL,
    signature_type contracts.signature_type NOT NULL,
    peer_id VARCHAR(20) NOT NULL,
    certificate_thumbprint varchar(512) NOT NULL,
    signature varchar(2048) NOT NULL,
    signed_at timestamptz NOT NULL,
    CONSTRAINT contract_signatures_pk PRIMARY KEY (content_hash,signature_type,peer_id)

);

CREATE INDEX certificates_peer_id_idx ON peers.certificates
    USING btree
    (
    peer_id
    );

CREATE INDEX certificates_certificate_thumbprint_idx ON peers.certificates
    USING btree
    (
    certificate_thumbprint
    );

CREATE INDEX grants_service_connection_content_hash_idx ON contracts.grants_service_connection
    USING btree
    (
    content_hash
    );

CREATE INDEX grants_service_connection_consumer_peer_id_idx ON contracts.grants_service_connection
    USING btree
    (
    consumer_peer_id
    );

CREATE INDEX grants_service_connection_service_peer_id_idx ON contracts.grants_service_connection
    USING btree
    (
    service_peer_id
    );

CREATE INDEX grants_delegated_service_connection_content_hash_idx ON contracts.grants_delegated_service_connection
    USING btree
    (
    content_hash
    );

CREATE INDEX grants_delegated_service_connection_delegator_peer_id_idx ON contracts.grants_delegated_service_connection
    USING btree
    (
    delegator_peer_id
    );

CREATE INDEX grants_delegated_service_connection_outway_peer_id_idx ON contracts.grants_delegated_service_connection
    USING btree
    (
    outway_peer_id
    );

CREATE INDEX grants_delegated_service_connection_service_peer_id_idx ON contracts.grants_delegated_service_connection
    USING btree
    (
    service_peer_id
    );

CREATE INDEX grants_service_publication_service_peer_id_idx ON contracts.grants_service_publication
    USING btree
    (
    service_peer_id
    );

CREATE INDEX grants_service_publication_directory_peer_id_idx ON contracts.grants_service_publication
    USING btree
    (
    directory_peer_id
    );

CREATE INDEX grants_peer_registration_directory_peer_id_idx ON contracts.grants_peer_registration
    USING btree
    (
    directory_peer_id
    );

CREATE INDEX grants_peer_registration_peer_id_idx ON contracts.grants_peer_registration
    USING btree
    (
    peer_id
    );

ALTER TABLE contracts.grants_service_connection ADD CONSTRAINT grants_service_connections_consumer_peer_id_fk FOREIGN KEY (consumer_peer_id)
    REFERENCES peers.peers (id) MATCH FULL
    ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE contracts.grants_service_connection ADD CONSTRAINT grants_service_connections_service_peer_id_fk FOREIGN KEY (service_peer_id)
    REFERENCES peers.peers (id) MATCH FULL
    ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE contracts.grants_service_connection ADD CONSTRAINT grants_service_connection_content_hash_fk FOREIGN KEY (content_hash)
    REFERENCES contracts.content (hash) MATCH FULL
    ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE peers.certificates ADD CONSTRAINT certificates_peer_id_fk FOREIGN KEY (peer_id)
    REFERENCES peers.peers (id) MATCH FULL
    ON DELETE CASCADE ON UPDATE NO ACTION;

ALTER TABLE contracts.grants_delegated_service_connection ADD CONSTRAINT grants_delegated_service_connection_content_hash_fk FOREIGN KEY (content_hash)
    REFERENCES contracts.content (hash) MATCH FULL
    ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE contracts.grants_delegated_service_connection ADD CONSTRAINT grants_delegated_service_connection_delegator_peer_id_fk FOREIGN KEY (delegator_peer_id)
    REFERENCES peers.peers (id) MATCH FULL
    ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE contracts.grants_delegated_service_connection ADD CONSTRAINT grants_delegated_service_connection_outway_peer_id_fk FOREIGN KEY (outway_peer_id)
    REFERENCES peers.peers (id) MATCH FULL
    ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE contracts.grants_delegated_service_connection ADD CONSTRAINT grants_delegated_service_connection_service_peer_id_fk FOREIGN KEY (service_peer_id)
    REFERENCES peers.peers (id) MATCH FULL
    ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE contracts.grants_service_publication ADD CONSTRAINT grants_service_publication_service_peer_id_fk FOREIGN KEY (service_peer_id)
    REFERENCES peers.peers (id) MATCH FULL
    ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE contracts.grants_service_publication ADD CONSTRAINT grants_service_publication_directory_peer_id_fk FOREIGN KEY (directory_peer_id)
    REFERENCES peers.peers (id) MATCH FULL
    ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE contracts.grants_peer_registration ADD CONSTRAINT grants_peer_registration_directory_peer_id_fk FOREIGN KEY (directory_peer_id)
    REFERENCES peers.peers (id) MATCH FULL
    ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE contracts.grants_peer_registration ADD CONSTRAINT grants_peer_registration_peer_id_fk FOREIGN KEY (peer_id)
    REFERENCES peers.peers (id) MATCH FULL
    ON DELETE CASCADE ON UPDATE CASCADE;


ALTER TABLE contracts.contract_signatures ADD CONSTRAINT contract_signatures_content_hash_fk FOREIGN KEY (content_hash)
    REFERENCES contracts.content (hash) MATCH FULL
    ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE contracts.contract_signatures ADD CONSTRAINT contract_signatures_peer_id_fk FOREIGN KEY (peer_id)
    REFERENCES peers.peers (id) MATCH FULL
    ON DELETE CASCADE ON UPDATE CASCADE;

CREATE VIEW contracts.valid_contracts AS
    SELECT content.hash
    FROM contracts.content as content
    WHERE
        content.valid_not_before < NOW() AND
        content.valid_not_after > NOW() AND

        -- contract is invalid when it contains a reject signature
        (SELECT COUNT(*) FROM contracts.contract_signatures as s WHERE s.signature_type = 'reject' AND s.content_hash = content.hash) = 0 AND

        -- contract is invalid when it contains a revoke signature
        (SELECT COUNT(*) FROM contracts.contract_signatures as s WHERE s.signature_type = 'revoke' AND s.content_hash = content.hash) = 0 AND

        -- contract is invalid when the contract is missing an accept signature from a peer
        (SELECT COUNT(DISTINCT id)
            FROM (
                -- the UNIONs will put all peer ids in the same column named 'id'
                SELECT directory_peer_id as id FROM contracts.grants_peer_registration WHERE content_hash = content.hash
                UNION
                SELECT peer_id as id FROM contracts.grants_peer_registration WHERE content_hash = content.hash
                UNION
                SELECT directory_peer_id as id FROM contracts.grants_service_publication WHERE content_hash = content.hash
                UNION
                SELECT service_peer_id as id FROM contracts.grants_service_publication WHERE content_hash = content.hash
                UNION
                SELECT consumer_peer_id as id FROM contracts.grants_service_connection WHERE content_hash = content.hash
                UNION
                SELECT service_peer_id as id FROM contracts.grants_service_connection WHERE content_hash = content.hash
                UNION
                SELECT service_peer_id as id FROM contracts.grants_service_connection WHERE content_hash = content.hash
                UNION
                SELECT service_peer_id as id FROM contracts.grants_delegated_service_connection WHERE content_hash = content.hash
                UNION
                SELECT outway_peer_id as id FROM contracts.grants_delegated_service_connection WHERE content_hash = content.hash
                UNION
                SELECT delegator_peer_id as id FROM contracts.grants_delegated_service_connection WHERE content_hash = content.hash
                UNION
                SELECT service_peer_id as id FROM contracts.grants_delegated_service_publication WHERE content_hash = content.hash
                UNION
                SELECT directory_peer_id as id FROM contracts.grants_delegated_service_publication WHERE content_hash = content.hash
                UNION
                SELECT delegator_peer_id as id FROM contracts.grants_delegated_service_publication WHERE content_hash = content.hash
            ) as p
        ) = (SELECT COUNT(*) FROM contracts.contract_signatures as s WHERE s.signature_type = 'accept' AND s.content_hash = content.hash)
    ;

COMMIT;
