// Copyright © VNG Realisatie 2023
// Licensed under the EUPL

package postgresadapter

import (
	"context"
	"fmt"

	"github.com/pkg/errors"

	"go.nlx.io/nlx/manager/adapters/storage/postgres/queries"
	"go.nlx.io/nlx/manager/domain/contract"
)

//nolint:gocyclo // complexity because of the filters cannot be easily reduced
func (r *PostgreSQLRepository) ListServices(ctx context.Context, peerID *contract.PeerID, serviceName, paginationStartID string, paginationLimit uint32, paginationSortOrder contract.SortOrder) ([]*contract.Service, error) {
	var pID string

	if peerID != nil {
		pID = peerID.Value()
	}

	if serviceName != "" {
		serviceName = fmt.Sprintf("%%%s%%", serviceName) // sqlc does not support LIKE with %
	}

	if peerID != nil || serviceName != "" {
		rows, err := r.queries.ListServicesWithFilter(ctx, &queries.ListServicesWithFilterParams{
			Limit:             int32(paginationLimit),
			PaginationStartID: paginationStartID,
			OrderDirection:    string(paginationSortOrder),
			ServiceName:       serviceName,
			PeerID:            pID,
		})
		if err != nil {
			return nil, errors.Wrap(err, "could not list services from database")
		}

		services := make([]*contract.Service, len(rows))

		if len(rows) == 0 {
			return services, nil
		}

		for i, r := range rows {
			services[i] = &contract.Service{
				PeerID:             r.ServicePeerID,
				PeerName:           r.PeerName.String,
				Name:               r.ServiceName,
				PeerManagerAddress: r.ManagerAddress.String,
				DelegatorPeerID:    r.DelegatorPeerID,
				DelegatorPeerName:  r.DelegatorName,
				ContractID:         r.ID.String(),
			}
		}

		return services, nil
	}

	rows, err := r.queries.ListServices(ctx, &queries.ListServicesParams{
		Limit:             int32(paginationLimit),
		PaginationStartID: paginationStartID,
		OrderDirection:    string(paginationSortOrder),
	})
	if err != nil {
		return nil, errors.Wrap(err, "could not list services from database")
	}

	services := make([]*contract.Service, len(rows))

	if len(rows) == 0 {
		return services, nil
	}

	for i, r := range rows {
		services[i] = &contract.Service{
			PeerID:             r.ServicePeerID,
			PeerName:           r.PeerName.String,
			Name:               r.ServiceName,
			PeerManagerAddress: r.ManagerAddress.String,
			DelegatorPeerID:    r.DelegatorPeerID,
			DelegatorPeerName:  r.DelegatorName,
			ContractID:         r.ID.String(),
		}
	}

	return services, nil
}
