// Copyright © VNG Realisatie 2023
// Licensed under the EUPL

package postgresadapter

import (
	"context"
	"crypto/x509"
	"fmt"

	"go.nlx.io/nlx/manager/adapters/storage/postgres/queries"
	"go.nlx.io/nlx/manager/domain/contract"
)

func getContractFromDB(ctx context.Context, q *queries.Queries, certs *x509.CertPool, args *createContractArgs) (*contract.Contract, error) {
	grants, err := getGrantsByContractHash(ctx, q, args.contentHash)
	if err != nil {
		return nil, err
	}

	signatures, err := getSignaturesByContractHash(ctx, q, args.contentHash)
	if err != nil {
		return nil, err
	}

	peersCerts, err := getPeersCertsByContractHash(ctx, certs, q, args.contentHash)
	if err != nil {
		return nil, err
	}

	model, err := contract.NewContract(&contract.NewContractArgs{
		Content: &contract.NewContentArgs{
			ID:            args.ID.Bytes(),
			HashAlgorithm: hashAlgToModel(args.hashAlgorithm),
			GroupID:       args.groupID,
			Validity: &contract.NewValidityArgs{
				NotBefore: args.notBefore,
				NotAfter:  args.notAfter,
			},
			Grants:    grants,
			CreatedAt: args.createdAt,
		},
		SignaturesAccepted: signatures.accepted,
		SignaturesRejected: signatures.rejected,
		SignaturesRevoked:  signatures.revoked,
		PeersCerts:         peersCerts,
	})
	if err != nil {
		return nil, fmt.Errorf("invalid contract in database: %v", err)
	}

	return model, nil
}

func hashAlgToModel(a queries.ContractsContentHashAlgorithm) contract.HashAlg {
	switch a {
	case queries.ContractsContentHashAlgorithmSha3512:
		return contract.HashAlgSHA3_512
	default:
		return contract.HashAlg(0)
	}
}
