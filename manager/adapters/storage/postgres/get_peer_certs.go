// Copyright © VNG Realisatie 2022
// Licensed under the EUPL

package postgresadapter

import (
	"context"
	"fmt"

	"github.com/pkg/errors"

	"go.nlx.io/nlx/manager/adapters/storage/postgres/queries"
	"go.nlx.io/nlx/manager/domain/contract"
)

func (r *PostgreSQLRepository) GetPeerCertsByCertThumbprints(ctx context.Context, id contract.PeerID, thumbprints contract.CertificateThumbprints) (*contract.PeerCertificates, error) {
	rows, err := r.queries.GetCertificatesForPeerByCertThumbprints(ctx, &queries.GetCertificatesForPeerByCertThumbprintsParams{
		PeerID:                 id.Value(),
		CertificateThumbprints: thumbprints.Value(),
	})
	if err != nil {
		return nil, errors.Wrap(err, "could not get certificates from database")
	}

	peerCertificates := make(contract.RawDERPeerCertificates, len(rows))

	count := copy(peerCertificates, rows)
	if count != len(rows) {
		return nil, fmt.Errorf("could not copy rows to peer certificates, invalid amount copied, got: %d, want %d", count, len(rows))
	}

	model, err := contract.NewPeerCertificates(r.trustedRootCAs, peerCertificates)
	if err != nil {
		return nil, errors.Wrap(err, "could not create contract certificates from database certificates")
	}

	return model, nil
}

func (r *PostgreSQLRepository) GetPeerCerts(ctx context.Context, id contract.PeerID) (*contract.PeerCertificates, error) {
	rows, err := r.queries.GetCertificatesForPeer(ctx, id.Value())
	if err != nil {
		return nil, errors.Wrap(err, "could not get certificates database")
	}

	peerCertificates := make(contract.RawDERPeerCertificates, len(rows))

	count := copy(peerCertificates, rows)
	if count != len(rows) {
		return nil, fmt.Errorf("could not copy rows to peer certificates, invalid amount copied, got: %d, want %d", count, len(rows))
	}

	model, err := contract.NewPeerCertificates(r.trustedRootCAs, peerCertificates)
	if err != nil {
		return nil, errors.Wrap(err, "could not create contract certificates from database certificates")
	}

	return model, nil
}
