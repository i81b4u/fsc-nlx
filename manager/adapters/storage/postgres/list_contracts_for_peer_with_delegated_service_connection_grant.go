// Copyright © VNG Realisatie 2023
// Licensed under the EUPL

package postgresadapter

import (
	"context"
	"fmt"

	"go.nlx.io/nlx/manager/adapters/storage/postgres/queries"
	"go.nlx.io/nlx/manager/domain/contract"
)

// nolint:dupl // similar but not duplicate
func (r *PostgreSQLRepository) ListContractsForPeerWithDelegatedServiceConnectionGrant(ctx context.Context, peerID contract.PeerID, paginationStartID string, paginationLimit uint32, paginationSortOrder contract.SortOrder) ([]*contract.Contract, error) {
	contractRows, err := r.queries.ListContractsForPeerWithDelegatedServiceConnectionGrant(ctx, &queries.ListContractsForPeerWithDelegatedServiceConnectionGrantParams{
		PaginationStartID: paginationStartID,
		PeerID:            string(peerID),
		Limit:             int32(paginationLimit),
		OrderDirection:    string(paginationSortOrder),
	})
	if err != nil {
		return nil, fmt.Errorf("could not list contracts for peer with service connection grant from database: %v", err)
	}

	if len(contractRows) == 0 {
		return []*contract.Contract{}, nil
	}

	result := make([]*contract.Contract, len(contractRows))

	for i, contractRow := range contractRows {
		model, contractErr := getContractFromDB(ctx, r.queries, r.trustedRootCAs, &createContractArgs{
			ID:            contractRow.ContentID,
			contentHash:   contractRow.ContentHash,
			groupID:       contractRow.ContentGroupID,
			hashAlgorithm: contractRow.ContentHashAlgorithm,
			createdAt:     contractRow.ContentCreatedAt,
			notBefore:     contractRow.ContentValidNotBefore,
			notAfter:      contractRow.ContentValidNotAfter,
		})
		if contractErr != nil {
			return nil, fmt.Errorf("invalid contract in database: %v", contractErr)
		}

		result[i] = model
	}

	return result, nil
}
