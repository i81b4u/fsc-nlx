// Copyright © VNG Realisatie 2023
// Licensed under the EUPL

package postgresadapter

import (
	"context"
	"fmt"

	"github.com/pkg/errors"

	"go.nlx.io/nlx/manager/domain/contract"
)

func (r *PostgreSQLRepository) GetTokenProviderPeer(ctx context.Context, grantHash *contract.Hash) (contract.PeerID, error) {
	var stringPeerID string

	switch grantHash.Type() {
	case contract.HashTypeGrantServiceConnection:
		var err error

		stringPeerID, err = r.queries.GetTokenProviderPeerIDServiceConnection(ctx, grantHash.String())
		if err != nil {
			return "", errors.Wrap(err, "could not get token peerID for service connection")
		}
	case contract.HashTypeGrantDelegatedServiceConnection:
		var err error

		stringPeerID, err = r.queries.GetTokenProviderPeerIDDelegatedServiceConnection(ctx, grantHash.String())
		if err != nil {
			return "", errors.Wrap(err, "could not get token peerID for delegated service connection")
		}
	default:
		return "", fmt.Errorf("invalid grant hash type, only service connection and delegated service connection types are allowed")
	}

	pID, err := contract.NewPeerID(stringPeerID)
	if err != nil {
		return "", errors.Wrap(err, "invalid peer ID in database")
	}

	return pID, nil
}
