// Copyright © VNG Realisatie 2023
// Licensed under the EUPL

package postgresadapter

import (
	"context"
	"fmt"

	"github.com/pkg/errors"

	"go.nlx.io/nlx/manager/domain/contract"
)

// nolint:dupl // code is indeed the same, abstract once the delegation PoC has been finalized
func (r *PostgreSQLRepository) ListAllContracts(ctx context.Context) ([]*contract.Contract, error) {
	contractRows, err := r.queries.ListAllContracts(ctx)
	if err != nil {
		return nil, errors.Wrap(err, "could not list all contracts from database")
	}

	if len(contractRows) == 0 {
		return []*contract.Contract{}, nil
	}

	result := make([]*contract.Contract, len(contractRows))

	for i, contractRow := range contractRows {
		model, contractErr := getContractFromDB(ctx, r.queries, r.trustedRootCAs, &createContractArgs{
			ID:            contractRow.ContentID,
			contentHash:   contractRow.ContentHash,
			groupID:       contractRow.ContentGroupID,
			hashAlgorithm: contractRow.ContentHashAlgorithm,
			createdAt:     contractRow.ContentCreatedAt,
			notBefore:     contractRow.ContentValidNotBefore,
			notAfter:      contractRow.ContentValidNotAfter,
		})
		if contractErr != nil {
			return nil, fmt.Errorf("invalid contract in database: %v", contractErr)
		}

		result[i] = model
	}

	return result, nil
}
