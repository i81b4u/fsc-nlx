// Code generated by sqlc. DO NOT EDIT.
// versions:
//   sqlc v1.19.1
// source: list_contracts_for_inway_config.sql

package queries

import (
	"context"
	"time"

	"github.com/gofrs/uuid"
	"github.com/lib/pq"
)

const listContractsForInwayConfig = `-- name: ListContractsForInwayConfig :many

SELECT
    c.id AS content_id,
    c.hash AS content_hash,
    c.hash_algorithm AS content_hash_algorithm,
    c.group_id AS content_group_id,
    c.valid_not_before AS content_valid_not_before,
    c.valid_not_after AS content_valid_not_after,
    c.created_at AS content_created_at
FROM contracts.content AS c
    LEFT JOIN contracts.grants_service_connection AS gc
        ON gc.content_hash = c.hash
    LEFT JOIN contracts.grants_delegated_service_connection AS gdsc
         ON gdsc.content_hash = c.hash
    JOIN contracts.valid_contracts
        ON contracts.valid_contracts.hash = c.hash
WHERE
    gc.service_name = ANY($1::varchar[]) OR
    gdsc.service_name = ANY($1::varchar[])
`

type ListContractsForInwayConfigRow struct {
	ContentID             uuid.UUID
	ContentHash           string
	ContentHashAlgorithm  ContractsContentHashAlgorithm
	ContentGroupID        string
	ContentValidNotBefore time.Time
	ContentValidNotAfter  time.Time
	ContentCreatedAt      time.Time
}

// Copyright © VNG Realisatie 2023
// Licensed under the EUPL
func (q *Queries) ListContractsForInwayConfig(ctx context.Context, serviceNames []string) ([]*ListContractsForInwayConfigRow, error) {
	rows, err := q.query(ctx, q.listContractsForInwayConfigStmt, listContractsForInwayConfig, pq.Array(serviceNames))
	if err != nil {
		return nil, err
	}
	defer rows.Close()
	items := []*ListContractsForInwayConfigRow{}
	for rows.Next() {
		var i ListContractsForInwayConfigRow
		if err := rows.Scan(
			&i.ContentID,
			&i.ContentHash,
			&i.ContentHashAlgorithm,
			&i.ContentGroupID,
			&i.ContentValidNotBefore,
			&i.ContentValidNotAfter,
			&i.ContentCreatedAt,
		); err != nil {
			return nil, err
		}
		items = append(items, &i)
	}
	if err := rows.Close(); err != nil {
		return nil, err
	}
	if err := rows.Err(); err != nil {
		return nil, err
	}
	return items, nil
}
