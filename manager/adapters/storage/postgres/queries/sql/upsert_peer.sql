-- Copyright © VNG Realisatie 2023
-- Licensed under the EUPL

-- name: UpsertPeer :exec
INSERT INTO peers.peers (
    id,
    name,
    manager_address
)
VALUES ($1, $2, $3)
ON CONFLICT (id) DO UPDATE
    SET name = EXCLUDED.name,
        manager_address = EXCLUDED.manager_address
;
