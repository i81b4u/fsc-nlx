-- Copyright © VNG Realisatie 2023
-- Licensed under the EUPL

-- name: ListPeersAsc :many
SELECT
    p.id,
    p.name,
    p.manager_address
FROM peers.peers as p
WHERE
    p.name IS NOT NULL AND
    p.manager_address IS NOT NULL AND
    p.id > sqlc.arg(pagination_start)::text
ORDER BY
    p.id ASC
LIMIT $1;

-- name: ListPeersDesc :many
SELECT
    p.id,
    p.name,
    p.manager_address
FROM peers.peers as p
WHERE
    p.name IS NOT NULL AND
    p.manager_address IS NOT NULL AND
    p.id < sqlc.arg(pagination_start)::text
ORDER BY
    p.id DESC
LIMIT $1;
