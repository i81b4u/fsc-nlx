-- Copyright © VNG Realisatie 2023
-- Licensed under the EUPL

-- name: ListSignaturesForContract :many
SELECT
    signature_type as type,
    peer_id,
    p.name as peer_name,
    certificate_thumbprint,
    signature as jws,
    cs.signed_at
FROM
    contracts.contract_signatures as cs
LEFT JOIN
    peers.peers as p
ON
    p.id = cs.peer_id
WHERE
    cs.content_hash = $1
;

-- name: ListServiceConnectionGrantsForContract :many
SELECT
    gc.consumer_peer_id as consumer_peer_id,
    gc.service_peer_id as service_peer_id,
    gc.service_name as service_name,
    gc.certificate_thumbprint as certificate_thumbprint
FROM
    contracts.grants_service_connection as gc
WHERE
        gc.content_hash = $1
GROUP BY
    gc.hash
;

-- name: ListDelegatedServiceConnectionGrantsForContract :many
SELECT
    gdsc.outway_peer_id as outway_peer_id,
    gdsc.service_peer_id as service_peer_id,
    gdsc.service_name as service_name,
    gdsc.delegator_peer_id as delegator_peer_id,
    gdsc.certificate_thumbprint as certificate_thumbprint
FROM
    contracts.grants_delegated_service_connection as gdsc
WHERE
        gdsc.content_hash = $1
GROUP BY
    gdsc.hash
;

-- name: ListDelegatedServicePublicationGrantsForContract :many
SELECT
    gdsp.directory_peer_id as directory_peer_id,
    gdsp.service_peer_id as service_peer_id,
    gdsp.service_name as service_name,
    gdsp.delegator_peer_id as delegator_peer_id
FROM
    contracts.grants_delegated_service_publication as gdsp
WHERE
    gdsp.content_hash = $1
GROUP BY
    gdsp.hash
;


-- name: ListPeerRegistrationGrantsForContract :many
SELECT
    gpr.hash as hash,
    gpr.directory_peer_id as directory_peer_id,
    gpr.peer_id as peer_id,
    gpr.peer_name as peer_name
FROM
    contracts.grants_peer_registration as gpr
WHERE
        gpr.content_hash = $1
;


-- name: ListServicePublicationGrantsForContract :many
SELECT
    gp.hash as hash,
    gp.directory_peer_id as directory_peer_id,
    gp.service_peer_id as service_peer_id,
    gp.service_name as service_name
FROM
    contracts.grants_service_publication as gp
WHERE
        gp.content_hash = $1
;
