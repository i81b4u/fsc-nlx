-- Copyright © VNG Realisatie 2022
-- Licensed under the EUPL

-- name: GetTokenInfoServiceConnection :one
SELECT
    gsc.service_name as service_name,
    gsc.certificate_thumbprint as certificate_thumbprint,
    gdsp.delegator_peer_id as service_delegator_peer_id
FROM
    contracts.grants_service_connection as gsc
    JOIN contracts.valid_contracts as vc
        ON vc.hash = gsc.content_hash
    JOIN contracts.valid_contracts as vcd
        ON vcd.hash = gsc.content_hash
    LEFT JOIN contracts.grants_delegated_service_publication as gdsp
              ON gdsp.service_peer_id = @self_peer_id::text
            AND gdsp.service_name = gsc.service_name
WHERE
    gsc.hash = @hash::text
    AND gsc.consumer_peer_id = @outway_peer_id::text
    AND gsc.service_peer_id = @self_peer_id::text;

-- name: GetTokenInfoDelegatedServiceConnection :one
SELECT
    gdsc.service_name as service_name,
    gdsc.certificate_thumbprint as certificate_thumbprint,
    gdsc.delegator_peer_id as outway_delegator_peer_id,
    gdsp.delegator_peer_id as service_delegator_peer_id
FROM
    contracts.grants_delegated_service_connection as gdsc
    JOIN contracts.valid_contracts as vc
        ON vc.hash = gdsc.content_hash
    JOIN contracts.valid_contracts as vcd
        ON vcd.hash = gdsc.content_hash
    LEFT JOIN contracts.grants_delegated_service_publication as gdsp
         ON gdsp.service_peer_id = @self_peer_id::text
            AND gdsp.service_name = gdsc.service_name
WHERE
    gdsc.hash = @hash::text
    AND gdsc.outway_peer_id = @outway_peer_id::text
    AND gdsc.service_peer_id = @self_peer_id::text;
