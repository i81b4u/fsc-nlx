-- Copyright © VNG Realisatie 2023
-- Licensed under the EUPL

-- name: ListPeersByID :many
SELECT
    p.id,
    p.name,
    p.manager_address
FROM peers.peers as p
WHERE
    p.name IS NOT NULL AND
    p.manager_address IS NOT NULL AND
    p.id = ANY(sqlc.arg(PeerIDs)::varchar[]);
