-- Copyright © VNG Realisatie 2023
-- Licensed under the EUPL

-- name: ListContractsWithDelegatedServicePublicationGrant :many
SELECT
    c.id as content_id,
    c.hash as content_hash,
    c.hash_algorithm as content_hash_algorithm,
    c.group_id as content_group_id,
    c.valid_not_before as content_valid_not_before,
    c.valid_not_after as content_valid_not_after,
    c.created_at as content_created_at
FROM contracts.content as c
         INNER JOIN contracts.grants_delegated_service_publication as gc
                    ON gc.content_hash = c.hash
WHERE
        c.hash > sqlc.arg(pagination_start_id)::text
ORDER BY
-- NOTE: unable to use ASC/DESC dynamically so we use a switch case
CASE
    WHEN @order_direction::text = 'asc' THEN c.hash END ASC,
CASE
    WHEN @order_direction::text = 'desc' THEN c.hash END DESC
LIMIT $1;


-- name: ListContractsForPeerWithDelegatedServicePublicationGrant :many
SELECT
    c.id as content_id,
    c.hash as content_hash,
    c.hash_algorithm as content_hash_algorithm,
    c.group_id as content_group_id,
    c.valid_not_before as content_valid_not_before,
    c.valid_not_after as content_valid_not_after,
    c.created_at as content_created_at
FROM contracts.content as c
         INNER JOIN contracts.grants_delegated_service_publication as gp
                    ON gp.content_hash = c.hash
WHERE
    (
                gp.service_peer_id = sqlc.arg(peer_id)::text OR
                gp.directory_peer_id = sqlc.arg(peer_id)::text OR
                gp.delegator_peer_id = sqlc.arg(peer_id)::text
        ) AND
    (
                @pagination_start_id::text = ''
            OR
                (@order_direction::text = 'asc' AND c.id > @pagination_start_id::uuid)
            OR
                (@order_direction::text = 'desc' AND c.id < @pagination_start_id::uuid)
        )
ORDER BY
-- NOTE: unable to use ASC/DESC dynamically so we use a switch case
CASE
    WHEN @order_direction::text = 'asc' THEN c.id END ASC,
CASE
    WHEN @order_direction::text = 'desc' THEN c.id END DESC
LIMIT $1;
