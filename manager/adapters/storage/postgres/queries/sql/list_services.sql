-- Copyright © VNG Realisatie 2023
-- Licensed under the EUPL

-- name: ListServices :many
SELECT
    gp.service_name, gp.service_peer_id, gp.delegator_peer_id, gp.delegator_name, p.name as peer_name, p.manager_address, cc.id
FROM
    (
        SELECT
            service_name,
            service_peer_id,
            content_hash,
            '' as delegator_peer_id,
            '' as delegator_name
        FROM
            contracts.grants_service_publication
        UNION
        SELECT
            service_name,
            service_peer_id,
            content_hash,
            delegator_peer_id as delegator_peer_id,
            dp.name as delegator_name
        FROM
            contracts.grants_delegated_service_publication
                join
            peers.peers dp on (grants_delegated_service_publication.delegator_peer_id = dp.id)
    ) as gp
        JOIN
    contracts.valid_contracts as cvc on (cvc.hash = gp.content_hash)
        JOIN
    peers.peers as p on (p.id = gp.service_peer_id)
        JOIN
    contracts.content cc on (cc.hash = gp.content_hash)
WHERE
                @pagination_start_id::text = ''
            OR
                (@order_direction::text = 'asc' AND cc.id > @pagination_start_id::uuid)
            OR
                (@order_direction::text = 'desc' AND cc.id < @pagination_start_id::uuid)

ORDER BY
    CASE
        WHEN @order_direction::text = 'asc' THEN cc.id END ASC,
    CASE
        WHEN @order_direction::text = 'desc' THEN cc.id END DESC
LIMIT $1;

-- name: ListServicesWithFilter :many
SELECT
    gp.service_name, gp.service_peer_id, gp.delegator_peer_id, gp.delegator_name, p.name as peer_name, p.manager_address, cc.id
FROM
    (
        SELECT
            service_name,
            service_peer_id,
            content_hash,
            '' as delegator_peer_id,
            '' as delegator_name
        FROM
            contracts.grants_service_publication
        UNION
        SELECT
            service_name,
            service_peer_id,
            content_hash,
            delegator_peer_id as delegator_peer_id,
            dp.name as delegator_name
        FROM
            contracts.grants_delegated_service_publication
                join
            peers.peers dp on (grants_delegated_service_publication.delegator_peer_id = dp.id)
    ) as gp
        JOIN
    contracts.valid_contracts as cvc on (cvc.hash = gp.content_hash)
        JOIN
    peers.peers as p on (p.id = gp.service_peer_id)
        JOIN
    contracts.content cc on (cc.hash = gp.content_hash)
WHERE
    (
                @pagination_start_id::text = ''
            OR
                (@order_direction::text = 'asc' AND cc.id > @pagination_start_id::uuid)
            OR
                (@order_direction::text = 'desc' AND cc.id < @pagination_start_id::uuid)
        )
  AND
    gp.service_peer_id = @peer_id::text
    OR
     gp.service_name like @service_name::text
ORDER BY
    CASE
        WHEN @order_direction::text = 'asc' THEN cc.id END ASC,
    CASE
        WHEN @order_direction::text = 'desc' THEN cc.id END DESC
LIMIT $1;
