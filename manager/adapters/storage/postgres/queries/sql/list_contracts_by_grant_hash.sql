-- Copyright © VNG Realisatie 2023
-- Licensed under the EUPL

-- name: ListContractsByGrantHash :many
SELECT
    c.id AS content_id,
    c.hash AS content_hash,
    c.hash_algorithm AS content_hash_algorithm,
    c.group_id AS content_group_id,
    c.valid_not_before AS content_valid_not_before,
    c.valid_not_after AS content_valid_not_after,
    c.created_at AS content_created_at
FROM contracts.content AS c
         LEFT JOIN contracts.grants_peer_registration as gpr
                   ON gpr.content_hash = c.hash
         LEFT JOIN contracts.grants_service_connection as gc
                   ON gc.content_hash = c.hash
         LEFT JOIN contracts.grants_delegated_service_connection as gdc
                   ON gdc.content_hash = c.hash
         LEFT JOIN contracts.grants_service_publication as gp
                   ON gp.content_hash = c.hash
         LEFT JOIN contracts.grants_delegated_service_publication as gdp
                   ON gdp.content_hash = c.hash
WHERE
    (
                gpr.peer_id = sqlc.arg(peer_id)::text OR
                gpr.directory_peer_id = sqlc.arg(peer_id)::text OR
                gc.service_peer_id = sqlc.arg(peer_id)::text OR
                gc.consumer_peer_id = sqlc.arg(peer_id)::text OR
                gdc.service_peer_id = sqlc.arg(peer_id)::text OR
                gdc.outway_peer_id = sqlc.arg(peer_id)::text OR
                gdc.delegator_peer_id = sqlc.arg(peer_id)::text OR
                gp.service_peer_id = sqlc.arg(peer_id)::text OR
                gp.directory_peer_id = sqlc.arg(peer_id)::text OR
                gdp.service_peer_id = sqlc.arg(peer_id)::text OR
                gdp.directory_peer_id = sqlc.arg(peer_id)::text OR
                gdp.delegator_peer_id = sqlc.arg(peer_id)::text
        )
AND
    (
                gpr.hash = ANY(sqlc.arg(grant_hashes)::varchar[]) OR
                gc.hash = ANY(sqlc.arg(grant_hashes)::varchar[]) OR
                gdc.hash = ANY(sqlc.arg(grant_hashes)::varchar[]) OR
                gp.hash = ANY(sqlc.arg(grant_hashes)::varchar[]) OR
                gdp.hash = ANY(sqlc.arg(grant_hashes)::varchar[])
        );
