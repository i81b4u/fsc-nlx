-- Copyright © VNG Realisatie 2022
-- Licensed under the EUPL

-- name: GetContentByHash :one
SELECT
    c.hash as content_hash,
    c.hash_algorithm as content_hash_algorithm,
    c.id as content_id,
    c.group_id as content_group_id,
    c.valid_not_before as content_valid_not_before,
    c.valid_not_after as content_valid_not_after,
    c.created_at as content_created_at
FROM contracts.content as c
WHERE c.hash = $1;
