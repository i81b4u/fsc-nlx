-- Copyright © VNG Realisatie 2023
-- Licensed under the EUPL

-- name: ListContractsForInwayConfig :many
SELECT
    c.id AS content_id,
    c.hash AS content_hash,
    c.hash_algorithm AS content_hash_algorithm,
    c.group_id AS content_group_id,
    c.valid_not_before AS content_valid_not_before,
    c.valid_not_after AS content_valid_not_after,
    c.created_at AS content_created_at
FROM contracts.content AS c
    LEFT JOIN contracts.grants_service_connection AS gc
        ON gc.content_hash = c.hash
    LEFT JOIN contracts.grants_delegated_service_connection AS gdsc
         ON gdsc.content_hash = c.hash
    JOIN contracts.valid_contracts
        ON contracts.valid_contracts.hash = c.hash
WHERE
    gc.service_name = ANY(sqlc.arg(service_names)::varchar[]) OR
    gdsc.service_name = ANY(sqlc.arg(service_names)::varchar[])
;
