-- Copyright © VNG Realisatie 2023
-- Licensed under the EUPL

-- name: ListPeerCertificatesForContract :many
SELECT
    peer_id,
    certificate_thumbprint,
    certificate
FROM peers.certificates
WHERE certificate_thumbprint in (
    SELECT
        certificate_thumbprint
    FROM contracts.contract_signatures
    WHERE
        content_hash = $1
    )
ORDER by peer_id DESC;
