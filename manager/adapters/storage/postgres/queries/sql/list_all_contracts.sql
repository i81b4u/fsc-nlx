-- Copyright © VNG Realisatie 2023
-- Licensed under the EUPL

-- name: ListAllContracts :many
SELECT
    c.id AS content_id,
    c.hash AS content_hash,
    c.hash_algorithm AS content_hash_algorithm,
    c.group_id AS content_group_id,
    c.valid_not_before AS content_valid_not_before,
    c.valid_not_after AS content_valid_not_after,
    c.created_at AS content_created_at
FROM contracts.content AS c;
