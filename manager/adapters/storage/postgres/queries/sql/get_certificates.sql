-- Copyright © VNG Realisatie 2022
-- Licensed under the EUPL

-- name: GetCertificatesForPeersByCertThumbprints :many
SELECT
    peer_id,
    certificate
FROM peers.certificates
WHERE certificate_thumbprint = ANY(sqlc.arg(certificate_thumbprints)::varchar[]);

-- name: GetCertificatesForPeerByCertThumbprints :many
SELECT
    certificate
FROM peers.certificates
WHERE peer_id = $1
    AND certificate_thumbprint = ANY(sqlc.arg(certificate_thumbprints)::varchar[]);

-- name: GetCertificatesForPeer :many
SELECT
    certificate
FROM peers.certificates
WHERE peer_id = $1;
