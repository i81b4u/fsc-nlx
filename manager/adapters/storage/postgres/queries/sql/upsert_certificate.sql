-- Copyright © VNG Realisatie 2023
-- Licensed under the EUPL

-- name: UpsertCertificate :exec
WITH upsert_peer AS (
INSERT INTO peers.peers (id)
VALUES ($1)
ON CONFLICT DO NOTHING)
INSERT INTO peers.certificates (
    certificate_thumbprint,
    certificate,
    peer_id
)
VALUES ($2, $3, $1)
ON CONFLICT DO NOTHING;
