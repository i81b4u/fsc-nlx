-- Copyright © VNG Realisatie 2022
-- Licensed under the EUPL

-- name: GetTokenProviderPeerIDServiceConnection :one
SELECT
    gsc.service_peer_id as peer_id
FROM
    contracts.grants_service_connection as gsc
    JOIN contracts.valid_contracts as vc
        ON vc.hash = gsc.content_hash
WHERE
    gsc.hash = @hash::text
;

-- name: GetTokenProviderPeerIDDelegatedServiceConnection :one
SELECT
    gsc.service_peer_id as peer_id
FROM
    contracts.grants_delegated_service_connection as gsc
    JOIN contracts.valid_contracts as vc
        ON vc.hash = gsc.content_hash
WHERE
    gsc.hash = @hash::text
;
