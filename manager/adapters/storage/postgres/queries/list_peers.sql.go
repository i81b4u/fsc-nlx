// Code generated by sqlc. DO NOT EDIT.
// versions:
//   sqlc v1.19.1
// source: list_peers.sql

package queries

import (
	"context"
)

const listPeersAsc = `-- name: ListPeersAsc :many

SELECT
    p.id,
    p.name,
    p.manager_address
FROM peers.peers as p
WHERE
    p.name IS NOT NULL AND
    p.manager_address IS NOT NULL AND
    p.id > $2::text
ORDER BY
    p.id ASC
LIMIT $1
`

type ListPeersAscParams struct {
	Limit           int32
	PaginationStart string
}

// Copyright © VNG Realisatie 2023
// Licensed under the EUPL
func (q *Queries) ListPeersAsc(ctx context.Context, arg *ListPeersAscParams) ([]*PeersPeer, error) {
	rows, err := q.query(ctx, q.listPeersAscStmt, listPeersAsc, arg.Limit, arg.PaginationStart)
	if err != nil {
		return nil, err
	}
	defer rows.Close()
	items := []*PeersPeer{}
	for rows.Next() {
		var i PeersPeer
		if err := rows.Scan(&i.ID, &i.Name, &i.ManagerAddress); err != nil {
			return nil, err
		}
		items = append(items, &i)
	}
	if err := rows.Close(); err != nil {
		return nil, err
	}
	if err := rows.Err(); err != nil {
		return nil, err
	}
	return items, nil
}

const listPeersDesc = `-- name: ListPeersDesc :many
SELECT
    p.id,
    p.name,
    p.manager_address
FROM peers.peers as p
WHERE
    p.name IS NOT NULL AND
    p.manager_address IS NOT NULL AND
    p.id < $2::text
ORDER BY
    p.id DESC
LIMIT $1
`

type ListPeersDescParams struct {
	Limit           int32
	PaginationStart string
}

func (q *Queries) ListPeersDesc(ctx context.Context, arg *ListPeersDescParams) ([]*PeersPeer, error) {
	rows, err := q.query(ctx, q.listPeersDescStmt, listPeersDesc, arg.Limit, arg.PaginationStart)
	if err != nil {
		return nil, err
	}
	defer rows.Close()
	items := []*PeersPeer{}
	for rows.Next() {
		var i PeersPeer
		if err := rows.Scan(&i.ID, &i.Name, &i.ManagerAddress); err != nil {
			return nil, err
		}
		items = append(items, &i)
	}
	if err := rows.Close(); err != nil {
		return nil, err
	}
	if err := rows.Err(); err != nil {
		return nil, err
	}
	return items, nil
}
