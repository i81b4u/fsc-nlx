// Code generated by sqlc. DO NOT EDIT.
// versions:
//   sqlc v1.19.1
// source: get_token_peer.sql

package queries

import (
	"context"
)

const getTokenProviderPeerIDDelegatedServiceConnection = `-- name: GetTokenProviderPeerIDDelegatedServiceConnection :one
SELECT
    gsc.service_peer_id as peer_id
FROM
    contracts.grants_delegated_service_connection as gsc
    JOIN contracts.valid_contracts as vc
        ON vc.hash = gsc.content_hash
WHERE
    gsc.hash = $1::text
`

func (q *Queries) GetTokenProviderPeerIDDelegatedServiceConnection(ctx context.Context, hash string) (string, error) {
	row := q.queryRow(ctx, q.getTokenProviderPeerIDDelegatedServiceConnectionStmt, getTokenProviderPeerIDDelegatedServiceConnection, hash)
	var peer_id string
	err := row.Scan(&peer_id)
	return peer_id, err
}

const getTokenProviderPeerIDServiceConnection = `-- name: GetTokenProviderPeerIDServiceConnection :one

SELECT
    gsc.service_peer_id as peer_id
FROM
    contracts.grants_service_connection as gsc
    JOIN contracts.valid_contracts as vc
        ON vc.hash = gsc.content_hash
WHERE
    gsc.hash = $1::text
`

// Copyright © VNG Realisatie 2022
// Licensed under the EUPL
func (q *Queries) GetTokenProviderPeerIDServiceConnection(ctx context.Context, hash string) (string, error) {
	row := q.queryRow(ctx, q.getTokenProviderPeerIDServiceConnectionStmt, getTokenProviderPeerIDServiceConnection, hash)
	var peer_id string
	err := row.Scan(&peer_id)
	return peer_id, err
}
