// Code generated by sqlc. DO NOT EDIT.
// versions:
//   sqlc v1.19.1
// source: upsert_peer.sql

package queries

import (
	"context"
	"database/sql"
)

const upsertPeer = `-- name: UpsertPeer :exec

INSERT INTO peers.peers (
    id,
    name,
    manager_address
)
VALUES ($1, $2, $3)
ON CONFLICT (id) DO UPDATE
    SET name = EXCLUDED.name,
        manager_address = EXCLUDED.manager_address
`

type UpsertPeerParams struct {
	ID             string
	Name           sql.NullString
	ManagerAddress sql.NullString
}

// Copyright © VNG Realisatie 2023
// Licensed under the EUPL
func (q *Queries) UpsertPeer(ctx context.Context, arg *UpsertPeerParams) error {
	_, err := q.exec(ctx, q.upsertPeerStmt, upsertPeer, arg.ID, arg.Name, arg.ManagerAddress)
	return err
}
