// Copyright © VNG Realisatie 2023
// Licensed under the EUPL

package rest

import (
	"context"
	"encoding/base64"
	"fmt"
	"net/http"
	"time"

	"github.com/pkg/errors"

	common_tls "go.nlx.io/nlx/common/tls"
	"go.nlx.io/nlx/manager/adapters/manager"
	"go.nlx.io/nlx/manager/domain/contract"
	"go.nlx.io/nlx/manager/ports/ext/rest/api"
	"go.nlx.io/nlx/manager/ports/ext/rest/api/models"
)

var contractGrantTypes = map[contract.GrantType]models.FSCCoreGrantType{
	contract.GrantTypeServicePublication:          models.GRANTTYPESERVICEPUBLICATION,
	contract.GrantTypeServiceConnection:           models.GRANTTYPESERVICECONNECTION,
	contract.GrantTypePeerRegistration:            models.GRANTTYPEPEERREGISTRATION,
	contract.GrantTypeDelegatedServicePublication: models.GRANTTYPEDELEGATEDSERVICEPUBLICATION,
	contract.GrantTypeDelegatedServiceConnection:  models.GRANTTYPEDELEGATEDSERVICECONNECTION,
}

type Client struct {
	manager.Manager
	c           *api.ClientWithResponses
	cert        *common_tls.CertificateBundle
	selfAddress string
}

const fetchLimit = 20

func NewClient(selfAddress, address string, cert *common_tls.CertificateBundle) (*Client, error) {
	if selfAddress == "" {
		return nil, fmt.Errorf("self address required")
	}

	if address == "" {
		return nil, fmt.Errorf("address required")
	}

	if cert == nil {
		return nil, fmt.Errorf("cert required")
	}

	c, err := api.NewClientWithResponses(address, func(c *api.Client) error {
		t := &http.Transport{
			TLSClientConfig: cert.TLSConfig(),
		}
		c.Client = &http.Client{Transport: t}
		return nil
	})

	if err != nil {
		return nil, err
	}

	return &Client{c: c, selfAddress: selfAddress, cert: cert}, nil
}

func (c *Client) GetCertificates(ctx context.Context) (*contract.PeerCertificates, error) {
	response, err := c.c.GetJSONWebKeySetWithResponse(ctx)
	if err != nil {
		return nil, errors.Wrap(err, "could not get certificates")
	}

	if response.StatusCode() != http.StatusOK {
		return nil, fmt.Errorf("could not get certificates, received invalid status code %d: %s", response.StatusCode(), string(response.Body))
	}

	rawCerts := make([][]byte, 0)

	for _, cert := range *response.JSON200.Keys {
		certBytes := make([]byte, 0)

		for _, c := range cert.X5c {
			var b []byte

			b, err = base64.URLEncoding.DecodeString(c)
			if err != nil {
				return nil, errors.Wrap(err, "unable to decode certificate base64 string")
			}

			certBytes = append(certBytes, b...)
		}

		rawCerts = append(rawCerts, certBytes)
	}

	peerCertificates, err := contract.NewPeerCertificates(c.cert.RootCAs(), rawCerts)
	if err != nil {
		return nil, err
	}

	return peerCertificates, nil
}

func (c *Client) GetToken(ctx context.Context, grantHash string) (string, error) {
	response, err := c.c.GetTokenWithFormdataBodyWithResponse(ctx, models.GetTokenFormdataRequestBody{
		GrantType: models.ClientCredentials,
		Scope:     grantHash,
	})
	if err != nil {
		return "", errors.Wrap(err, "could not get token")
	}

	if response.StatusCode() != http.StatusOK {
		return "", fmt.Errorf("could not get token: received invalid status code %d: %s", response.StatusCode(), string(response.Body))
	}

	if response.JSON200.TokenType != models.Bearer {
		return "", fmt.Errorf("invalid token type: %s", response.JSON200.TokenType)
	}

	return response.JSON200.AccessToken, nil
}

func (c *Client) GetContracts(ctx context.Context, grantType *contract.GrantType, grantHashes *[]string) ([]*contract.Contract, error) {
	sortOrder := models.FSCCoreSortOrderSORTORDERDESCENDING

	var grantTypeFilter *models.FSCCoreGrantType

	if grantType != nil {
		apiGrantType, ok := contractGrantTypes[*grantType]
		if !ok {
			return nil, fmt.Errorf("unknown grant type: %d", *grantType)
		}

		grantTypeFilter = &apiGrantType
	}

	limit := models.FSCCoreQueryPaginationLimit(fetchLimit)

	response, err := c.c.GetContractsWithResponse(ctx, &models.GetContractsParams{
		Limit:     &limit,
		SortOrder: &sortOrder,
		GrantType: grantTypeFilter,
		GrantHash: grantHashes,
	})
	if err != nil {
		return nil, err
	}

	if response.StatusCode() != http.StatusOK {
		return nil, fmt.Errorf("received invalid status code %d: %s", response.StatusCode(), string(response.Body))
	}

	return nil, err
}

func (c *Client) Announce(ctx context.Context) error {
	resp, err := c.c.AnnounceWithResponse(ctx, &models.AnnounceParams{
		FscManagerAddress: c.selfAddress,
	})
	if err != nil {
		return errors.Wrap(err, "could not announce to peer")
	}

	if resp.StatusCode() != http.StatusOK {
		return fmt.Errorf("received invalid status code %d: %s", resp.StatusCode(), string(resp.Body))
	}

	return nil
}

func (c *Client) SubmitContract(ctx context.Context, contractContent *contract.Content, signature *contract.Signature) error {
	cc, err := contractContentToAPIModel(contractContent)
	if err != nil {
		return errors.Wrap(err, "could not submit contract")
	}

	resp, err := c.c.SubmitContractWithResponse(ctx, &models.SubmitContractParams{
		FscManagerAddress: c.selfAddress,
	},
		models.SubmitContractJSONRequestBody(models.SubmitContractJSONBody{
			ContractContent: *cc,
			Signature:       signature.JWS(),
		}),
	)
	if err != nil {
		return errors.Wrap(err, "could not submit contract")
	}

	if resp.StatusCode() != http.StatusCreated {
		return fmt.Errorf("received invalid status code %d: %s", resp.StatusCode(), string(resp.Body))
	}

	return nil
}

func (c *Client) AcceptContract(ctx context.Context, contractContent *contract.Content, signature *contract.Signature) error {
	cc, err := contractContentToAPIModel(contractContent)
	if err != nil {
		return errors.Wrap(err, "could not accept contract")
	}

	resp, err := c.c.AcceptContractWithResponse(ctx, contractContent.Hash().String(), &models.AcceptContractParams{
		FscManagerAddress: c.selfAddress,
	},
		models.AcceptContractJSONRequestBody(models.SubmitContractJSONBody{
			ContractContent: *cc,
			Signature:       signature.JWS(),
		}),
	)
	if err != nil {
		return errors.Wrap(err, "could not accept contract")
	}

	if resp.StatusCode() != http.StatusCreated {
		return fmt.Errorf("received invalid status code %d: %s", resp.StatusCode(), string(resp.Body))
	}

	return nil
}

func (c *Client) RevokeContract(ctx context.Context, contractContent *contract.Content, signature *contract.Signature) error {
	cc, err := contractContentToAPIModel(contractContent)
	if err != nil {
		return errors.Wrap(err, "could not revoke contract")
	}

	resp, err := c.c.RevokeContractWithResponse(ctx, contractContent.Hash().String(), &models.RevokeContractParams{
		FscManagerAddress: c.selfAddress,
	},
		models.RevokeContractJSONRequestBody(models.RevokeContractJSONBody{
			ContractContent: *cc,
			Signature:       signature.JWS(),
		}),
	)
	if err != nil {
		return errors.Wrap(err, "could not accept contract")
	}

	if resp.StatusCode() != http.StatusCreated {
		return fmt.Errorf("received invalid status code %d: %s", resp.StatusCode(), string(resp.Body))
	}

	return nil
}

func (c *Client) RejectContract(ctx context.Context, contractContent *contract.Content, signature *contract.Signature) error {
	cc, err := contractContentToAPIModel(contractContent)
	if err != nil {
		return errors.Wrap(err, "could not reject contract")
	}

	resp, err := c.c.RejectContractWithResponse(ctx, contractContent.Hash().String(), &models.RejectContractParams{
		FscManagerAddress: c.selfAddress,
	},
		models.RejectContractJSONRequestBody(models.RejectContractJSONBody{
			ContractContent: *cc,
			Signature:       signature.JWS(),
		}),
	)
	if err != nil {
		return errors.Wrap(err, "could not reject contract")
	}

	if resp.StatusCode() != http.StatusCreated {
		return fmt.Errorf("received invalid status code %d: %s", resp.StatusCode(), string(resp.Body))
	}

	return nil
}

func (c *Client) GetPeerInfo(ctx context.Context) (*contract.Peer, error) {
	resp, err := c.c.GetPeerInfoWithResponse(ctx)

	if err != nil {
		return nil, err
	}

	if resp.StatusCode() != http.StatusOK {
		return nil, fmt.Errorf("received invalid status code %d: %s", resp.StatusCode(), string(resp.Body))
	}

	p, err := contract.NewPeer(&contract.NewPeerArgs{
		ID:   resp.JSON200.PeerId,
		Name: resp.JSON200.PeerName,
	})
	if err != nil {
		return nil, errors.Wrap(err, "could not create new peer")
	}

	return p, nil
}

func (c *Client) GetPeers(ctx context.Context, peerIDs []contract.PeerID) ([]*contract.Peer, error) {
	sortOrder := models.FSCCoreSortOrderSORTORDERDESCENDING

	peerIDFiler := make([]models.FSCCorePeerID, len(peerIDs))

	for i, p := range peerIDs {
		peerIDFiler[i] = p.Value()
	}

	limit := models.FSCCoreQueryPaginationLimit(fetchLimit)

	resp, err := c.c.GetPeersWithResponse(ctx, &models.GetPeersParams{
		SortOrder: &sortOrder,
		PeerId:    &peerIDFiler,
		Limit:     &limit,
	})

	if err != nil {
		return nil, err
	}

	if resp.StatusCode() != http.StatusOK {
		return nil, fmt.Errorf("received invalid status code %d: %s", resp.StatusCode(), string(resp.Body))
	}

	peers := make([]*contract.Peer, len(resp.JSON200.Peers))

	for i, p := range resp.JSON200.Peers {
		peer, errNewPeer := contract.NewPeer(&contract.NewPeerArgs{
			ID:             p.Id,
			Name:           p.Name,
			ManagerAddress: p.ManagerAddress,
		})
		if errNewPeer != nil {
			return nil, errors.Wrap(errNewPeer, "could not create new peer")
		}

		peers[i] = peer
	}

	return peers, nil
}

func (c *Client) GetServices(ctx context.Context, peerID *contract.PeerID, serviceName string) ([]*contract.Service, error) {
	sortOrder := models.FSCCoreSortOrderSORTORDERDESCENDING

	var peerIDFilter *models.FSCCorePeerID

	if peerID != nil {
		pf := models.FSCCorePeerID(*peerID)
		peerIDFilter = &pf
	}

	var serviceNameFilter *models.FSCCoreServiceName

	if serviceName != "" {
		serviceNameFilter = &serviceName
	}

	limit := models.FSCCoreQueryPaginationLimit(fetchLimit)

	resp, err := c.c.GetServicesWithResponse(ctx, &models.GetServicesParams{
		SortOrder:   &sortOrder,
		PeerId:      peerIDFilter,
		Limit:       &limit,
		ServiceName: serviceNameFilter,
	})

	if err != nil {
		return nil, err
	}

	if resp.StatusCode() != http.StatusOK {
		return nil, fmt.Errorf("received invalid status code %d: %s", resp.StatusCode(), string(resp.Body))
	}

	servicesList := make([]*contract.Service, len(resp.JSON200.Services))

	for i, s := range resp.JSON200.Services {
		switch s.Type {
		case models.TYPEDELEGATEDSERVICE:
			data, err := s.Data.AsFSCCoreServiceListingDelegatedService()
			if err != nil {
				return nil, errors.Wrap(err, "could not cast to service listing")
			}

			servicesList[i] = &contract.Service{
				Name:              data.Name,
				PeerID:            data.Peer.Id,
				PeerName:          data.Peer.Name,
				DelegatorPeerID:   data.Delegator.PeerId,
				DelegatorPeerName: data.Delegator.PeerName,
			}
		case models.TYPESERVICE:
			data, err := s.Data.AsFSCCoreServiceListingService()
			if err != nil {
				return nil, errors.Wrap(err, "could not cast to service listing")
			}

			servicesList[i] = &contract.Service{
				Name:     data.Name,
				PeerID:   data.Peer.Id,
				PeerName: data.Peer.Name,
			}
		}
	}

	return servicesList, nil
}

func (c *Client) GetTXLogRecords(ctx context.Context) (contract.TXLogRecords, error) {
	sortOrder := models.FSCLoggingSortOrderSORTORDERDESCENDING

	resp, err := c.c.GetLogsWithResponse(ctx, &models.GetLogsParams{
		SortOrder: &sortOrder,
		Limit:     fetchLimit,
	})
	if err != nil {
		return nil, err
	}

	if resp.StatusCode() != http.StatusOK {
		return nil, fmt.Errorf("received invalid status code %d: %s", resp.StatusCode(), string(resp.Body))
	}

	records := make(contract.TXLogRecords, len(resp.JSON200.Records))

	for i := range resp.JSON200.Records {
		r := resp.JSON200.Records[i]

		source, destination, err := mapSourceAndDestinationToModel(&r)
		if err != nil {
			return nil, fmt.Errorf("could not map source and destination: %w", err)
		}

		records[i] = &contract.TXLogRecord{
			TransactionID: r.TransactionId.String(),
			GrantHash:     r.GrantHash,
			ServiceName:   r.ServiceName,
			Direction:     mapDirection(r.Direction),
			Source:        source,
			Destination:   destination,
			CreatedAt:     time.Unix(r.CreatedAt, 0),
		}
	}

	return records, nil
}

func mapSourceAndDestinationToModel(r *models.FSCLoggingLogRecord) (source, destination interface{}, err error) {
	// nolint:dupl // not the same
	switch r.SourceType {
	case models.TYPESOURCE:
		var s models.FSCLoggingSource

		s, err = r.Source.AsFSCLoggingSource()
		if err != nil {
			return nil, nil, errors.Wrap(err, "could not cast to source")
		}

		source = &contract.TXLogRecordSource{
			OutwayPeerID: s.OutwayPeerId,
		}
	case models.TYPEDELEGATEDSOURCE:
		var s models.FSCLoggingSourceDelegated

		s, err = r.Source.AsFSCLoggingSourceDelegated()
		if err != nil {
			return nil, nil, errors.Wrap(err, "could not cast to delegated source")
		}

		source = &contract.TXLogRecordDelegatedSource{
			OutwayPeerID:    s.OutwayPeerId,
			DelegatorPeerID: s.DelegatorPeerId,
		}
	}

	// nolint:dupl // not the same
	switch r.DestinationType {
	case models.TYPEDESTINATION:
		var d models.FSCLoggingDestination

		d, err = r.Destination.AsFSCLoggingDestination()
		if err != nil {
			return nil, nil, errors.Wrap(err, "could not cast to destination")
		}

		destination = &contract.TXLogRecordDestination{
			ServicePeerID: d.ServicePeerId,
		}
	case models.TYPEDELEGATEDDESTINATION:
		var d models.FSCLoggingDestinationDelegated

		d, err = r.Destination.AsFSCLoggingDestinationDelegated()
		if err != nil {
			return nil, nil, errors.Wrap(err, "could not cast to delegated destination")
		}

		destination = &contract.TXLogRecordDelegatedDestination{
			ServicePeerID:   d.ServicePeerId,
			DelegatorPeerID: d.DelegatorPeerId,
		}
	}

	return source, destination, nil
}

func mapDirection(d models.FSCLoggingLogRecordDirection) contract.TXLogDirection {
	switch d {
	case models.DIRECTIONINCOMING:
		return contract.TXLogDirectionIn
	case models.DIRECTIONOUTGOING:
		return contract.TXLogDirectionOut
	default:
		return contract.TXLogDirectionUnspecified
	}
}

func contractContentToAPIModel(p *contract.Content) (*models.FSCCoreContractContent, error) {
	grants := p.Grants()

	grantAPIModels := make([]models.FSCCoreGrant, len(grants))

	for i, grant := range grants {
		switch g := grant.(type) {
		case *contract.GrantPeerRegistration:
			grantAPIModels[i] = peerRegistrationGrantToAPIModel(g)
		case *contract.GrantServicePublication:
			grantAPIModels[i] = servicePublicationGrantToAPIModel(g)
		case *contract.GrantServiceConnection:
			grantAPIModels[i] = serviceConnectionGrantToAPIModel(g)
		case *contract.GrantDelegatedServiceConnection:
			grantAPIModels[i] = delegatedServiceConnectionGrantToAPIModel(g)
		case *contract.GrantDelegatedServicePublication:
			grantAPIModels[i] = delegatedServicePublicationGrantToAPIModel(g)
		default:
			return nil, fmt.Errorf("unsupported grant type: %T", g)
		}
	}

	return &models.FSCCoreContractContent{
		HashAlgorithm: models.HASHALGORITHMSHA3512,
		Id:            p.ID().String(),
		GroupId:       p.GroupID(),
		Validity: models.FSCCoreValidity{
			NotBefore: p.NotBefore().Unix(),
			NotAfter:  p.NotAfter().Unix(),
		},
		Grants:    grantAPIModels,
		CreatedAt: p.CreatedAt().Unix(),
	}, nil
}

func peerRegistrationGrantToAPIModel(g *contract.GrantPeerRegistration) models.FSCCoreGrant {
	data := models.FSCCoreGrant_Data{}
	_ = data.FromFSCCoreGrantPeerRegistration(models.FSCCoreGrantPeerRegistration{
		Directory: models.FSCCoreDirectory{
			PeerId: g.Directory().PeerID().Value(),
		},
		Peer: models.FSCCorePeerRegistration{
			Id:   g.Peer().ID().Value(),
			Name: g.Peer().Name(),
		},
	})

	return models.FSCCoreGrant{
		Data: data,
		Type: models.GRANTTYPEPEERREGISTRATION,
	}
}

func servicePublicationGrantToAPIModel(g *contract.GrantServicePublication) models.FSCCoreGrant {
	data := models.FSCCoreGrant_Data{}
	_ = data.FromFSCCoreGrantServicePublication(models.FSCCoreGrantServicePublication{
		Directory: models.FSCCoreDirectory{
			PeerId: g.Directory().Peer().ID().Value(),
		},
		Service: models.FSCCoreServicePublication{
			Name:   g.Service().Name(),
			PeerId: g.Service().Peer().ID().Value(),
		},
	})

	return models.FSCCoreGrant{
		Data: data,
		Type: models.GRANTTYPESERVICEPUBLICATION,
	}
}

func serviceConnectionGrantToAPIModel(g *contract.GrantServiceConnection) models.FSCCoreGrant {
	data := models.FSCCoreGrant_Data{}
	_ = data.FromFSCCoreGrantServiceConnection(models.FSCCoreGrantServiceConnection{
		Outway: models.FSCCoreOutway{
			PeerId:                g.Outway().Peer().ID().Value(),
			CertificateThumbprint: g.Outway().CertificateThumbprint().Value(),
		},
		Service: models.FSCCoreService{
			Name:   g.Service().Name(),
			PeerId: g.Service().Peer().ID().Value(),
		},
	})

	return models.FSCCoreGrant{
		Data: data,
		Type: models.GRANTTYPESERVICECONNECTION,
	}
}

func delegatedServiceConnectionGrantToAPIModel(g *contract.GrantDelegatedServiceConnection) models.FSCCoreGrant {
	data := models.FSCCoreGrant_Data{}
	_ = data.FromFSCCoreGrantDelegatedServiceConnection(models.FSCCoreGrantDelegatedServiceConnection{
		Delegator: models.FSCCoreDelegator{
			PeerId: g.Delegator().Peer().ID().Value(),
		},
		Outway: models.FSCCoreOutway{
			PeerId:                g.Outway().Peer().ID().Value(),
			CertificateThumbprint: g.Outway().CertificateThumbprint().Value(),
		},
		Service: models.FSCCoreService{
			Name:   g.Service().Name(),
			PeerId: g.Service().Peer().ID().Value(),
		},
	})

	return models.FSCCoreGrant{
		Data: data,
		Type: models.GRANTTYPEDELEGATEDSERVICECONNECTION,
	}
}

func delegatedServicePublicationGrantToAPIModel(g *contract.GrantDelegatedServicePublication) models.FSCCoreGrant {
	data := models.FSCCoreGrant_Data{}

	_ = data.FromFSCCoreGrantDelegatedServicePublication(models.FSCCoreGrantDelegatedServicePublication{
		Delegator: models.FSCCoreDelegator{
			PeerId: g.Delegator().Peer().ID().Value(),
		},
		Directory: models.FSCCoreDirectory{
			PeerId: g.Directory().Peer().ID().Value(),
		},
		Service: models.FSCCoreServicePublication{
			Name:   g.Service().Name(),
			PeerId: g.Service().Peer().ID().Value(),
		},
	})

	return models.FSCCoreGrant{
		Data: data,
		Type: models.GRANTTYPEDELEGATEDSERVICEPUBLICATION,
	}
}
