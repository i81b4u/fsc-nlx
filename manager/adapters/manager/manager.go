// Copyright © VNG Realisatie 2023
// Licensed under the EUPL

package manager

import (
	"context"

	"go.nlx.io/nlx/manager/domain/contract"
)

type Manager interface {
	Announce(ctx context.Context) error
	GetCertificates(ctx context.Context) (*contract.PeerCertificates, error)
	GetContracts(ctx context.Context, grantTypes *contract.GrantType, grantHashes *[]string) ([]*contract.Contract, error)
	SubmitContract(ctx context.Context, contractContent *contract.Content, signature *contract.Signature) error
	AcceptContract(ctx context.Context, contractContent *contract.Content, signature *contract.Signature) error
	RejectContract(ctx context.Context, contractContent *contract.Content, signature *contract.Signature) error
	RevokeContract(ctx context.Context, contractContent *contract.Content, signature *contract.Signature) error
	GetPeerInfo(ctx context.Context) (*contract.Peer, error)
	GetPeers(ctx context.Context, peerIDs []contract.PeerID) ([]*contract.Peer, error)
	GetServices(ctx context.Context, peerIDs *contract.PeerID, services string) ([]*contract.Service, error)
	GetToken(ctx context.Context, grantHash string) (string, error)
	GetTXLogRecords(ctx context.Context) (contract.TXLogRecords, error)
}

type Factory interface {
	New(ctx context.Context, address string) (Manager, error)
}
