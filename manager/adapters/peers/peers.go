// Copyright © VNG Realisatie 2023
// Licensed under the EUPL

package peers

import (
	"context"

	"go.nlx.io/nlx/manager/domain/contract"
)

type Peers interface {
	SubmitContract(ctx context.Context, c *contract.Content, sig *contract.Signature) error
	AcceptContract(ctx context.Context, c *contract.Content, sig *contract.Signature) error
	RejectContract(ctx context.Context, c *contract.Content, sig *contract.Signature) error
	RevokeContract(ctx context.Context, c *contract.Content, sig *contract.Signature) error

	GetToken(ctx context.Context, peerID contract.PeerID, grantHash string, outwayCertThumbprint string) (string, error)
}
