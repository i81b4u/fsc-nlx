// Copyright © VNG Realisatie 2023
// Licensed under the EUPL

package grpctxlog

import (
	"context"
	"fmt"
	"time"

	"github.com/pkg/errors"
	"go.nlx.io/nlx/common/nlxversion"
	common_tls "go.nlx.io/nlx/common/tls"
	"go.nlx.io/nlx/manager/adapters/txlog"
	"go.nlx.io/nlx/txlog-api/api"
	"go.nlx.io/nlx/txlog-api/domain/record"
	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials"
)

type GrpcTXLog struct {
	client           api.TXLogServiceClient
	cancelTimeoutCtx context.CancelFunc
}

func New(ctx context.Context, address string, cert *common_tls.CertificateBundle) (*GrpcTXLog, error) {
	if address == "" {
		return nil, errors.New("address is required")
	}

	timeoutCtx, cancel := context.WithTimeout(nlxversion.NewGRPCContext(ctx, "manager"), 1*time.Minute)

	txlogAPIConn, err := grpc.DialContext(timeoutCtx, address, grpc.WithTransportCredentials(credentials.NewTLS(cert.TLSConfig())))
	if err != nil {
		cancel()
		return nil, errors.Wrap(err, "failed to connect with the txlog api")
	}

	return &GrpcTXLog{
		client:           api.NewTXLogServiceClient(txlogAPIConn),
		cancelTimeoutCtx: cancel,
	}, nil
}

func (g *GrpcTXLog) ListRecords(ctx context.Context, req *txlog.ListRecordsRequest) ([]*txlog.Record, error) {
	resp, err := g.client.ListRecords(ctx, reqToProto(req))
	if err != nil {
		return nil, errors.Wrap(err, "could not get records from txlog api")
	}

	return protoToRecords(resp)
}

func (g *GrpcTXLog) Shutdown(context.Context) error {
	g.cancelTimeoutCtx()

	return nil
}

func protoToRecords(resp *api.ListRecordsResponse) ([]*txlog.Record, error) {
	records := make([]*txlog.Record, len(resp.Records))

	for i, r := range resp.Records {
		var src interface{}
		switch s := r.Source.Data.(type) {
		case *api.TransactionLogRecordSource_Source_:
			src = &txlog.RecordSource{
				OutwayPeerID: s.Source.OutwayPeerId,
			}
		case *api.TransactionLogRecordSource_DelegatedSource_:
			src = &txlog.RecordDelegatedSource{
				OutwayPeerID:    s.DelegatedSource.OutwayPeerId,
				DelegatorPeerID: s.DelegatedSource.DelegatorPeerId,
			}
		default:
			return nil, fmt.Errorf("unknown source type: %T", s)
		}

		var dest interface{}
		switch d := r.Destination.Data.(type) {
		case *api.TransactionLogRecordDestination_Destination_:
			dest = &txlog.RecordDestination{
				ServicePeerID: d.Destination.ServicePeerId,
			}
		case *api.TransactionLogRecordDestination_DelegatedDestination_:
			dest = &txlog.RecordDelegatedDestination{
				ServicePeerID:   d.DelegatedDestination.ServicePeerId,
				DelegatorPeerID: d.DelegatedDestination.DelegatorPeerId,
			}
		default:
			return nil, fmt.Errorf("unknown destination type: %T", d)
		}

		records[i] = &txlog.Record{
			ID:            r.Id,
			TransactionID: r.TransactionId,
			GrantHash:     r.GrantHash,
			Direction:     directionProtoToModel(r.Direction),
			ServiceName:   r.ServiceName,
			Source:        src,
			Destination:   dest,
			CreatedAt:     time.Unix(r.CreatedAt, 0),
		}
	}

	return records, nil
}

func directionProtoToModel(d api.TransactionLogRecord_Direction) record.Direction {
	switch d {
	case api.TransactionLogRecord_DIRECTION_IN:
		return record.DirectionIn
	case api.TransactionLogRecord_DIRECTION_OUT:
		return record.DirectionOut
	default:
		return record.DirectionUnspecified
	}
}

func reqToProto(req *txlog.ListRecordsRequest) *api.ListRecordsRequest {
	return &api.ListRecordsRequest{
		Pagination: &api.Pagination{
			StartId: req.Pagination.StartID,
			Limit:   req.Pagination.Limit,
			Order:   sortOrderToProto(req.Pagination.SortOrder),
		},
	}
}

func sortOrderToProto(o txlog.SortOrder) api.SortOrder {
	switch o {
	case txlog.SortOrderAscending:
		return api.SortOrder_SORT_ORDER_ASCENDING
	case txlog.SortOrderDescending:
		return api.SortOrder_SORT_ORDER_DESCENDING
	default:
		return api.SortOrder_SORT_ORDER_UNSPECIFIED
	}
}
