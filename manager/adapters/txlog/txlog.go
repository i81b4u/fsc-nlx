// Copyright © VNG Realisatie 2023
// Licensed under the EUPL

package txlog

import (
	"context"
	"time"

	"go.nlx.io/nlx/txlog-api/domain/record"
)

type TXLog interface {
	ListRecords(ctx context.Context, args *ListRecordsRequest) ([]*Record, error)
}

type SortOrder uint32

const (
	SortOrderUnspecified SortOrder = iota
	SortOrderAscending
	SortOrderDescending
)

type Pagination struct {
	StartID   uint64
	Limit     uint32
	SortOrder SortOrder
}

type Period struct {
	Start time.Time
	End   time.Time
}

type Filter struct {
	Period        *Period
	TransactionID *record.TransactionID
	ServiceName   string
	GrantHash     string
	PeerID        string
}

type ListRecordsRequest struct {
	Pagination *Pagination
	Filters    []*Filter
}

type Record struct {
	ID            uint64
	TransactionID string
	GrantHash     string
	ServiceName   string
	Direction     record.Direction
	Source        interface{}
	Destination   interface{}
	CreatedAt     time.Time
}

type RecordSource struct {
	OutwayPeerID string
}

type RecordDelegatedSource struct {
	OutwayPeerID    string
	DelegatorPeerID string
}

type RecordDestination struct {
	ServicePeerID string
}

type RecordDelegatedDestination struct {
	ServicePeerID   string
	DelegatorPeerID string
}
