// Copyright © VNG Realisatie 2023
// Licensed under the EUPL

package controller

import (
	"context"
)

type Controller interface {
	GetInwayAddressForService(ctx context.Context, serviceName string) (string, error)
	GetServiceEndpointURL(ctx context.Context, inwayAddress string, serviceName string) (string, error)
}
