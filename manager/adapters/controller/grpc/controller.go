// Copyright © VNG Realisatie 2023
// Licensed under the EUPL

package grpccontroller

import (
	"context"
	"time"

	"github.com/pkg/errors"
	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials"

	"go.nlx.io/nlx/common/nlxversion"
	common_tls "go.nlx.io/nlx/common/tls"
	controllerapi "go.nlx.io/nlx/controller/ports/internalgrpc/api"
)

type GrpcController struct {
	groupID          string
	client           controllerapi.ControllerServiceClient
	cancelTimeoutCtx context.CancelFunc
}

func New(ctx context.Context, groupID, address string, cert *common_tls.CertificateBundle) (*GrpcController, error) {
	if groupID == "" {
		return nil, errors.New("groupID is required")
	}

	if address == "" {
		return nil, errors.New("address is required")
	}

	timeoutCtx, cancel := context.WithTimeout(nlxversion.NewGRPCContext(ctx, "manager"), 1*time.Minute)

	controllerAPIConn, err := grpc.DialContext(timeoutCtx, address, grpc.WithTransportCredentials(credentials.NewTLS(cert.TLSConfig())))
	if err != nil {
		cancel()
		return nil, errors.Wrap(err, "failed to connect with the controller api")
	}

	return &GrpcController{
		groupID:          groupID,
		client:           controllerapi.NewControllerServiceClient(controllerAPIConn),
		cancelTimeoutCtx: cancel,
	}, nil
}

func (m *GrpcController) GetInwayAddressForService(ctx context.Context, serviceName string) (string, error) {
	resp, err := m.client.GetInwayAddressForService(ctx, &controllerapi.GetInwayAddressForServiceRequest{
		GroupId:     m.groupID,
		ServiceName: serviceName,
	})
	if err != nil {
		return "", errors.Wrap(err, "failed to retrieve inway address for service")
	}

	return resp.Address, nil
}

func (m *GrpcController) GetServiceEndpointURL(ctx context.Context, inwayAddress, serviceName string) (string, error) {
	result, err := m.client.GetServiceEndpointURL(ctx, &controllerapi.GetServiceEndpointURLRequest{
		InwayAddress: inwayAddress,
		ServiceName:  serviceName,
	})
	if err != nil {
		return "", errors.Wrap(err, "failed to retrieve service endpoint url for inway")
	}

	return result.EndpointUrl, nil
}

func (m *GrpcController) Shutdown(context.Context) error {
	m.cancelTimeoutCtx()

	return nil
}
