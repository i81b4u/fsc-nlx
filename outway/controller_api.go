// Copyright © VNG Realisatie 2022
// Licensed under the EUPL

package outway

import (
	"context"

	"github.com/cenkalti/backoff/v4"
	"go.uber.org/zap"

	"go.nlx.io/nlx/outway/adapters/controller"
)

func (o *Outway) announceToControllerAPI(ctx context.Context) {
	register := func() error {
		err := o.controller.RegisterOutway(ctx, &controller.RegisterOutwayArgs{
			Name:                  o.name,
			CertificateThumbprint: o.externalCert.CertificateThumbprint(),
		})
		if err != nil {
			o.logger.Error("failed to register to controller api", zap.Error(err))

			return err
		}

		o.logger.Info("controller api registration successful")

		return nil
	}

	err := backoff.Retry(register, backoff.WithContext(backoff.NewExponentialBackOff(), ctx))
	if err != nil {
		o.logger.Error("permanently failed to register to controller api", zap.Error(err))
	}
}
