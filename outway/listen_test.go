// Copyright © VNG Realisatie 2018
// Licensed under the EUPL

package outway

import (
	"path/filepath"
)

var pkiDir = filepath.Join("..", "testing", "pki")

// Disabled the tests because it is unclear which tests are useful. Once the POC is completed the tests should be reviewed.

//func testRequests(t *testing.T, tests map[string]struct {
//	url        string
//	statusCode int
//	wantErr    *httperrors.NLXNetworkError
//}) {
//	client := http.Client{}
//
//	for name, tt := range tests {
//		tt := tt
//
//		t.Run(name, func(t *testing.T) {
//			req, err := http.NewRequest("GET", tt.url, http.NoBody)
//			if err != nil {
//				t.Fatal("error creating http request", err)
//			}
//
//			resp, err := client.Do(req)
//			if err != nil {
//				t.Fatal("error making http request", err)
//			}
//			defer resp.Body.Close()
//
//			assert.Equal(t, tt.statusCode, resp.StatusCode)
//			contents, err := io.ReadAll(resp.Body)
//
//			if err != nil {
//				t.Fatal("error reading response body", err)
//			}
//
//			if tt.wantErr != nil {
//				gotError := &httperrors.NLXNetworkError{}
//				err := json.Unmarshal(contents, gotError)
//				assert.NoError(t, err)
//
//				assert.Equal(t, tt.wantErr, gotError)
//			}
//		})
//	}
//}
//
////nolint:funlen // test function
//func TestOutwayListen(t *testing.T) {
//	// TODO: rework test
//	t.Skipf("test is using internals which will be reworked while implementing FSC")
//
//logger := zap.NewNop()
//
//outway := &Outway{
//	servicesHTTP:      make(map[string]HTTPService),
//	servicesDirectory: make(map[string]*directoryapi.ListServicesResponse_Service),
//	logger:            logger,
//	txlogger:          transactionlog.NewDiscardTransactionLogger(),
//}
//
//outway.requestHTTPHandler = outway.handleRequest
//
//ctrl := gomock.NewController(t)
//defer ctrl.Finish()
//
//mockService := mock.NewMockHTTPService(ctrl)
//mockFailService := mock.NewMockHTTPService(ctrl)
//
//mockService.EXPECT().HandleRequest(gomock.Any(), gomock.Any()).Do(
//	func(w http.ResponseWriter, r *http.Request) {
//		w.WriteHeader(http.StatusOK)
//	},
//)
//
//mockService.EXPECT().HandleRequest(gomock.Any(), gomock.Any()).Do(
//	func(w http.ResponseWriter, r *http.Request) {
//		w.WriteHeader(http.StatusOK)
//	},
//)
//
//mockFailService.EXPECT().HandleRequest(gomock.Any(), gomock.Any()).Do(
//	func(w http.ResponseWriter, r *http.Request) {
//		w.WriteHeader(httperrors.StatusNLXNetworkError)
//	},
//)
//
//for i := 0; i < 11; i++ {
//	outway.servicesHTTP["00000000000000000001.mockservice"+strconv.Itoa(i)] = mockService
//	inwayMessage := directoryapi.ListServicesResponse_Service{
//		Name: "mockservice" + strconv.Itoa(i),
//		Organization: &directoryapi.Organization{
//			ID: "00000000000000000001",
//			Name:         "test-org",
//		},
//		Inways: []*directoryapi.Inway{
//			{
//				Address: "mock-service-a-1:123",
//				State:   directoryapi.Inway_STATE_UP,
//			},
//		},
//	}
//	outway.servicesDirectory["00000000000000000001.mockservice"+strconv.Itoa(i)] = &inwayMessage
//}
//
//// Setup a Failing mock service.
//outway.servicesHTTP["00000000000000000001.mockservicefail"] = mockFailService
//inwayMessage := directoryapi.ListServicesResponse_Service{
//	Name: "mockservicefail",
//	Organization: &directoryapi.Organization{
//		ID: "00000000000000000001",
//		Name:         "test-org",
//	},
//	Inways: []*directoryapi.Inway{
//		{
//			Address: "mock-service-fail-1:123",
//			State:   directoryapi.Inway_STATE_UP,
//		},
//	},
//}
//outway.servicesDirectory["00000000000000000001.mockservicefail"] = &inwayMessage
//
//// Setup mock http server with the outway as http handler
//mockServer := httptest.NewServer(outway)
//defer mockServer.Close()
//
//// Test http responses
//tests := map[string]struct {
//	url        string
//	statusCode int
//	wantErr    *httperrors.NLXNetworkError
//}{
//	"when_service_fails": {
//		url:        fmt.Sprintf("%s/00000000000000000001/mockservicefail/", mockServer.URL),
//		statusCode: httperrors.StatusNLXNetworkError,
//	},
//	"happy_flow": {
//		url:        fmt.Sprintf("%s/00000000000000000001/mockservice0/", mockServer.URL),
//		statusCode: http.StatusOK,
//	},
//	"happy_flow_without_trailing_slash": {
//		url:        fmt.Sprintf("%s/00000000000000000001/mockservice0", mockServer.URL),
//		statusCode: http.StatusOK,
//	},
//}
//
//testRequests(t, tests)
//}
//
//func createMockOutway() *Outway {
//	return &Outway{
//		logger:   zap.NewNop(),
//		txlogger: transactionlog.NewDiscardTransactionLogger(),
//	}
//}
//
//func TestHandleOnNLXExceptions(t *testing.T) {
//	outway := createMockOutway()
//
//	// Setup mock httpservice
//	ctrl := gomock.NewController(t)
//	defer ctrl.Finish()
//
//	mockService := mock.NewMockHTTPService(ctrl)
//	outway.servicesHTTP["00000000000000000001.mockservice"] = mockService
//
//	tests := map[string]struct {
//		authEnabled        bool
//		txLogger           func(ctrl *gomock.Controller) transactionlog.TransactionLogger
//		dataSubjectHeader  string
//		wantHTTPStatusCode int
//		wantErr            *httperrors.NLXNetworkError
//	}{
//		"with_failing_auth_settings": {
//			authEnabled: true,
//			txLogger: func(ctrl *gomock.Controller) transactionlog.TransactionLogger {
//				txLogger := mock_transaction_logger.NewMockTransactionLogger(t)
//				return txLogger
//			},
//			dataSubjectHeader:  "",
//			wantHTTPStatusCode: httperrors.StatusNLXNetworkError,
//			wantErr: &httperrors.NLXNetworkError{
//				Source:   httperrors.Outway,
//				Location: httperrors.OAS1,
//				Code:     httperrors.ErrorWhileAuthorizingRequestErr,
//				Message:  "error authorizing request",
//			},
//		},
//		"with_failing_transactionlogger": {
//			authEnabled: false,
//			txLogger: func(ctrl *gomock.Controller) transactionlog.TransactionLogger {
//				txLogger := mock_transaction_logger.NewMockTransactionLogger(t)
//				txLogger.EXPECT().AddRecord(gomock.Any()).Return(fmt.Errorf("cannot add transaction record"))
//
//				return txLogger
//			},
//			dataSubjectHeader:  "",
//			wantHTTPStatusCode: httperrors.StatusNLXNetworkError,
//			wantErr: &httperrors.NLXNetworkError{
//				Source:   httperrors.Outway,
//				Location: httperrors.C1,
//				Code:     httperrors.ServerErrorErr,
//				Message:  "server error: unable to add record to database: cannot add transaction record",
//			},
//		},
//		"with_invalid_datasubject header": {
//			authEnabled: false,
//			txLogger: func(ctrl *gomock.Controller) transactionlog.TransactionLogger {
//				txLogger := mock_transaction_logger.NewMockTransactionLogger(t)
//				return txLogger
//			},
//			dataSubjectHeader:  "invalid",
//			wantHTTPStatusCode: httperrors.StatusNLXNetworkError,
//			wantErr: &httperrors.NLXNetworkError{
//				Source:   httperrors.Outway,
//				Location: httperrors.C1,
//				Code:     httperrors.InvalidDataSubjectHeaderErr,
//				Message:  "invalid data subject header",
//			},
//		},
//	}
//
//	for name, tt := range tests {
//		tt := tt
//
//		t.Run(name, func(t *testing.T) {
//			recorder := httptest.NewRecorder()
//
//			tCtrl := gomock.NewController(t)
//			defer tCtrl.Finish()
//
//			outway.txlogger = tt.txLogger(tCtrl)
//
//			outway.plugins = []plugins.Plugin{
//				plugins.NewLogRecordPlugin("00000000000000000001", outway.txlogger),
//				plugins.NewStripHeadersPlugin("00000000000000000001"),
//			}
//
//			if tt.authEnabled {
//				outway.plugins = append([]plugins.Plugin{
//					plugins.NewAuthorizationPlugin(&plugins.NewAuthorizationPluginArgs{
//						CA:                  nil,
//						ServiceURL:          "",
//						AuthorizationClient: http.Client{},
//					}),
//				}, outway.plugins...)
//			}
//
//			req := httptest.NewRequest("GET", "http://mockservice.00000000000000000001.services.nlx.local", nil)
//			req.Header.Add("X-NLX-Request-Data-Subject", tt.dataSubjectHeader)
//
//			outway.handleOnNLX(outway.logger, &plugins.Destination{
//				PeerID: "00000000000000000001",
//				Service:                  "mockservice",
//				Path:                     "/",
//			}, recorder, req)
//
//			assert.Equal(t, tt.wantHTTPStatusCode, recorder.Code)
//
//			contents, err := io.ReadAll(recorder.Body)
//			if err != nil {
//				t.Fatal("error parsing result.body", err)
//			}
//
//			if tt.wantErr != nil {
//				gotError := &httperrors.NLXNetworkError{}
//				err := json.Unmarshal(contents, gotError)
//				assert.NoError(t, err)
//
//				assert.Equal(t, tt.wantErr, gotError)
//			}
//		})
//	}
//}
//
//type failingRoundTripper struct{}
//
//func (failingRoundTripper) RoundTrip(*http.Request) (*http.Response, error) {
//	return nil, errors.New("some error")
//}
//
//func (o *Outway) setFailingTransport() {
//	// Change connection Transport to Failing Transports.
//	// for all proxies
//	for _, s := range o.servicesHTTP {
//		if rrlbs, ok := s.(*HTTPService); ok {
//			for _, p := range rrlbs.proxies {
//				p.Transport = new(failingRoundTripper)
//			}
//		}
//	}
//}
//
//// TestFailingTransport tests the error handling when there are
//// network problems to reach the advertised service from the outway
////
//// client -> outway -> [FAIL] inway -> service
////
//// The test creates a service with failing transport.
//// and expecting a 503 service temporarily unavailable status code
//// when service gets called
//func TestFailingTransport(t *testing.T) {
//	// TODO: rework test
//	t.Skipf("test is using internals which will be reworked while implementing FSC")
//
//	logger := zap.NewNop()
//	// during tests: logger, _ := zap.NewDevelopment()
//	// defer logger.Sync()
//
//	// Create a outway with a mock service
//	outway := &Outway{
//		servicesHTTP:      make(map[string]HTTPService),
//		servicesDirectory: make(map[string]*directoryapi.ListServicesResponse_Service),
//		logger:            logger,
//		txlogger:          transactionlog.NewDiscardTransactionLogger(),
//	}
//
//	outway.requestHTTPHandler = outway.handleRequest
//
//	// Setup mock http server with the outway as http handler
//	mockServer := httptest.NewServer(outway)
//	defer mockServer.Close()
//
//	tests := map[string]struct {
//		url        string
//		statusCode int
//		wantErr    *httperrors.NLXNetworkError
//	}{
//		"when_request_to_inway_fails": {
//			url:        fmt.Sprintf("%s/00000000000000000001/mockservice/", mockServer.URL),
//			statusCode: httperrors.StatusNLXNetworkError,
//			wantErr: &httperrors.NLXNetworkError{
//				Source:   httperrors.Outway,
//				Location: httperrors.O1,
//				Code:     httperrors.ServiceUnreachableErr,
//				Message:  "failed API request to https://inway.00000000000000000001/mockservice/ try again later. service api down/unreachable. check error at https://docs.nlx.io/support/common-errors/",
//			},
//		},
//	}
//
//	inwayMessage := directoryapi.ListServicesResponse_Service{
//		Name: "mockservice",
//		Organization: &directoryapi.Organization{
//			ID: "00000000000000000001",
//			Name:         "test-org",
//		},
//		Inways: []*directoryapi.Inway{
//			{
//				Address: "mock-service-:123",
//				State:   directoryapi.Inway_STATE_UP,
//			},
//		},
//	}
//
//	// Setup mock httpservice
//	outway.servicesDirectory["00000000000000000001.mockservice"] = &inwayMessage
//
//	certBundle, err := common_testing.GetCertificateBundle(pkiDir, common_testing.OrgNLXTest)
//	require.NoError(t, err)
//
//	l, err := NewHTTPService(
//		zap.NewNop(), certBundle,
//		"00000000000000000001", "mockservice",
//		[]directoryapi.Inway{{
//			Address: "inway.00000000000000000001",
//			State:   directoryapi.Inway_STATE_UP,
//		}})
//
//	assert.Nil(t, err)
//
//	outway.servicesHTTP["00000000000000000001.mockservice"] = l
//	// set transports to fail.
//	outway.setFailingTransport()
//	testRequests(t, tests)
//}
//
//func TestRunServer(t *testing.T) {
//	t.Parallel()
//
//	logger := zap.NewNop()
//
//	certificate, _ := tls.LoadX509KeyPair(
//		filepath.Join("..", "testing", "pki", "org-nlx-test-chain.pem"),
//		filepath.Join("..", "testing", "pki", "org-nlx-test-key.pem"),
//	)
//
//	tests := map[string]struct {
//		listenAddress            string
//		listenAddressGRPC        string
//		monitoringServiceAddress string
//		certificate              *tls.Certificate
//		errorMessage             string
//	}{
//		"invalid_listen_address": {
//			listenAddress:            "invalid",
//			listenAddressGRPC:        "127.0.0.1:8082",
//			monitoringServiceAddress: "localhost:8081",
//			certificate:              nil,
//			errorMessage:             "error listening on server: listen tcp: address invalid: missing port in address",
//		},
//		"invalid_listen_address with TLS": {
//			listenAddress:            "invalid",
//			listenAddressGRPC:        "127.0.0.1:8083",
//			monitoringServiceAddress: "localhost:8082",
//			certificate:              &certificate,
//			errorMessage:             "error listening on server: listen tcp: address invalid: missing port in address",
//		},
//	}
//
//	for name, tt := range tests {
//		tt := tt
//
//		t.Run(name, func(t *testing.T) {
//			monitorService, err := monitoring.NewMonitoringService(tt.monitoringServiceAddress, logger)
//			assert.Nil(t, err)
//
//			o := &Outway{
//				ctx:            context.Background(),
//				logger:         logger,
//				monitorService: monitorService,
//				grpcServer:     grpc.NewServer(),
//			}
//
//			err = o.RunServer(tt.listenAddress, tt.listenAddressGRPC, tt.certificate)
//			assert.EqualError(t, err, tt.errorMessage)
//
//			err = monitorService.Stop()
//			assert.NoError(t, err)
//		})
//	}
//}
