// Copyright © VNG Realisatie 2023
// Licensed under the EUPL

package outway

import (
	"sync"

	"github.com/pkg/errors"

	common_tls "go.nlx.io/nlx/common/tls"
)

type HTTPProxies struct {
	mut     sync.RWMutex
	proxies map[string]*HTTPService
	orgCert *common_tls.CertificateBundle
}

func NewHTTPProxies(orgCert *common_tls.CertificateBundle) (*HTTPProxies, error) {
	if orgCert == nil {
		return nil, errors.New("organization certificate is required")
	}

	return &HTTPProxies{
		mut:     sync.RWMutex{},
		proxies: make(map[string]*HTTPService),
		orgCert: orgCert,
	}, nil
}

func (p *HTTPProxies) Get(inwayAddress string) (*HTTPService, error) {
	p.mut.RLock()
	proxy, ok := p.proxies[inwayAddress]
	p.mut.RUnlock()

	if ok {
		return proxy, nil
	}

	proxy, err := NewHTTPService(p.orgCert, inwayAddress)
	if err != nil {
		return nil, errors.Wrapf(err, "could not create proxy to inway: %s", inwayAddress)
	}

	p.mut.Lock()
	p.proxies[inwayAddress] = proxy
	p.mut.Unlock()

	return proxy, nil
}
