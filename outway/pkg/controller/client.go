// Copyright © VNG Realisatie 2023
// Licensed under the EUPL

package controller

import (
	"context"
	"time"

	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials"

	"go.nlx.io/nlx/common/nlxversion"
	common_tls "go.nlx.io/nlx/common/tls"
	"go.nlx.io/nlx/common/version"
	controllerapi "go.nlx.io/nlx/controller/ports/internalgrpc/api"
)

const component = "outway"

var (
	userAgent = component + "/" + version.BuildVersion
)

type Client interface {
	controllerapi.ControllerServiceClient
	Close() error
}

type client struct {
	controllerapi.ControllerServiceClient
	conn *grpc.ClientConn
}

func NewClient(ctx context.Context, controllerAddress string, cert *common_tls.CertificateBundle) (Client, error) {
	dialCredentials := credentials.NewTLS(cert.TLSConfig())
	dialOptions := []grpc.DialOption{
		grpc.WithTransportCredentials(dialCredentials),
		grpc.WithUserAgent(userAgent),
	}

	var grpcTimeout = 10 * time.Second

	timeoutCtx, cancel := context.WithTimeout(ctx, grpcTimeout)
	defer cancel()

	grpcCtx := nlxversion.NewGRPCContext(timeoutCtx, component)

	controllerConnection, err := grpc.DialContext(grpcCtx, controllerAddress, dialOptions...)
	if err != nil {
		return nil, err
	}

	c := &client{
		ControllerServiceClient: controllerapi.NewControllerServiceClient(controllerConnection),
		conn:                    controllerConnection,
	}

	return c, nil
}

func (c *client) Close() error {
	return c.conn.Close()
}
