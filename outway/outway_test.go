// Copyright © VNG Realisatie 2018
// Licensed under the EUPL

package outway

import (
	"fmt"
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	"go.uber.org/zap"

	"go.nlx.io/nlx/common/clock"
	mock_repository "go.nlx.io/nlx/outway/domain/config/mock"
	common_testing "go.nlx.io/nlx/testing/testingutils"
)

// nolint:funlen // this is a test
func TestNewOutwayExeception(t *testing.T) {
	orgCertWithoutName, err := common_testing.GetCertificateBundle(pkiDir, common_testing.OrgWithoutName)
	require.NoError(t, err)

	orgCert, err := common_testing.GetCertificateBundle(pkiDir, common_testing.OrgNLXTest)
	require.NoError(t, err)

	orgCertWithoutSerialNumber, err := common_testing.GetCertificateBundle(pkiDir, common_testing.OrgWithoutSerialNumber)
	require.NoError(t, err)

	internalCert, err := common_testing.GetCertificateBundle(pkiDir, common_testing.NLXTestInternal)
	require.NoError(t, err)

	tests := map[string]struct {
		args            *NewOutwayArgs
		wantError       error
		wantPluginCount int
	}{
		"certificate_without_organization": {
			args: &NewOutwayArgs{
				Logger:            zap.NewNop(),
				ExternalCert:      orgCertWithoutName,
				InternalCert:      internalCert,
				MonitoringAddress: "localhost:8080",
				AuthServiceURL:    "",
				AuthCAPath:        "",
				ConfigRepository:  mock_repository.NewMockRepository(t),
			},
			wantError: fmt.Errorf("cannot obtain organization name from self cert"),
		},
		"certificate_without_organization_serial_number": {
			args: &NewOutwayArgs{
				Logger:            zap.NewNop(),
				ExternalCert:      orgCertWithoutSerialNumber,
				InternalCert:      internalCert,
				MonitoringAddress: "localhost:8080",
				AuthServiceURL:    "",
				AuthCAPath:        "",
				ConfigRepository:  mock_repository.NewMockRepository(t),
			},
			wantError: fmt.Errorf("validation error for subject serial number from cert: cannot be empty"),
		},
		// "authorization_service_URL_set_but_no_CA_for_authorization_provided": {
		//	args: &NewOutwayArgs{
		//		Logger:            zap.NewNop(),
		//		ExternalCert:      orgCert,
		//		InternalCert:      internalCert,
		//		MonitoringAddress: "localhost:8080",
		//		AuthServiceURL:    "http://auth.nlx.io",
		//		AuthCAPath:        "",
		//		ConfigRepository:  config_mock.NewMockRepository(gomock.NewController(t)),
		//	},
		//	wantError: fmt.Errorf("authorization service URL set but no CA for authorization provided"),
		// },
		// "authorization_service_URL_is_not_'https'": {
		//	args: &NewOutwayArgs{
		//		Logger:            zap.NewNop(),
		//		ExternalCert:      orgCert,
		//		InternalCert:      internalCert,
		//		MonitoringAddress: "localhost:8080",
		//		AuthServiceURL:    "http://auth.nlx.io",
		//		AuthCAPath:        "/path/to",
		//		ConfigRepository:  config_mock.NewMockRepository(gomock.NewController(t)),
		//	},
		//	wantError: fmt.Errorf("scheme of authorization service URL is not 'https'"),
		// },
		// "invalid_monitioring_service_address": {
		//	args: &NewOutwayArgs{
		//		Logger:            zap.NewNop(),
		//		ExternalCert:      orgCert,
		//		InternalCert:      internalCert,
		//		MonitoringAddress: "",
		//		AuthServiceURL:    "",
		//		AuthCAPath:        "",
		//		ConfigRepository:  config_mock.NewMockRepository(gomock.NewController(t)),
		//	},
		//	wantError: fmt.Errorf("unable to create monitoring service: address required"),
		//},
		//"happy_flow_with_authorization_plugin": {
		//	args: &NewOutwayArgs{
		//		Logger:            zap.NewNop(),
		//		ExternalCert:      orgCert,
		//		InternalCert:      internalCert,
		//		MonitoringAddress: "localhost:8080",
		//		AuthServiceURL:    "https://auth.nlx.io",
		//		AuthCAPath:        "../testing/pki/ca-root.pem",
		//		ConfigRepository:  config_mock.NewMockRepository(gomock.NewController(t)),
		//	},
		//	wantError:       nil,
		//	wantPluginCount: 3,
		// },
		"happy_flow": {
			args: &NewOutwayArgs{
				Logger:            zap.NewNop(),
				ExternalCert:      orgCert,
				InternalCert:      internalCert,
				MonitoringAddress: "localhost:8080",
				Clock:             clock.New(),
				ConfigRepository:  mock_repository.NewMockRepository(t),
			},
			wantError:       nil,
			wantPluginCount: 2,
		},
	}

	for name, tt := range tests {
		tt := tt

		t.Run(name, func(t *testing.T) {
			outway, err := New(tt.args)
			if tt.wantError != nil {
				assert.Equal(t, tt.wantError.Error(), err.Error())
			} else {
				assert.NoError(t, err)
				assert.NotNil(t, outway)
				assert.Equal(t, tt.wantPluginCount, len(outway.plugins))
			}
		})
	}
}
