// Copyright © VNG Realisatie 2018
// Licensed under the EUPL

package outway

import (
	"crypto/tls"
	"fmt"
	"net/http"
	"time"

	"github.com/pkg/errors"
	"go.uber.org/zap"

	"go.nlx.io/nlx/common/httperrors"
	common_tls "go.nlx.io/nlx/common/tls"
	outway_http "go.nlx.io/nlx/outway/http"
	"go.nlx.io/nlx/outway/plugins"
)

const (
	FscGrantHashHeader     = "Fsc-Grant-Hash"
	FscAuthorizationHeader = "Fsc-Authorization"
	readHeaderTimeout      = time.Second * 60
)

func (o *Outway) RunServer(listenAddress string, serverCertificate *tls.Certificate) error {
	o.httpServer = &http.Server{
		Addr:              listenAddress,
		Handler:           o,
		ReadHeaderTimeout: readHeaderTimeout,
	}

	errorChannel := make(chan error)

	if serverCertificate == nil {
		go func() {
			o.logger.Info(fmt.Sprintf("starting HTTP server on %s", listenAddress))
			errorChannel <- o.httpServer.ListenAndServe()
		}()
	} else {
		tlsConfig := common_tls.NewConfig(common_tls.WithTLS12())
		tlsConfig.Certificates = []tls.Certificate{*serverCertificate}

		o.httpServer.TLSConfig = tlsConfig

		go func() {
			o.logger.Info(fmt.Sprintf("starting HTTPS server on %s", listenAddress))
			errorChannel <- o.httpServer.ListenAndServeTLS("", "")
		}()
	}

	go func() {
		err := o.monitorService.Start()
		if err != nil {
			errorChannel <- errors.Wrap(err, "error listening on monitoring service")
		}
	}()

	o.monitorService.SetReady()

	err := <-errorChannel

	if err == http.ErrServerClosed {
		return nil
	}

	return errors.Wrap(err, "error listening on server")
}

func (o *Outway) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	ctx := r.Context()

	now := o.clk.Now()

	logger := o.logger.With(
		zap.String("request-path", r.URL.Path),
		zap.String("request-remote-address", r.RemoteAddr),
	)

	logger.Info("received request")

	grantHashHeaderValue := r.Header.Get(FscGrantHashHeader)

	exists, services, err := o.cfg.GrantHashExists(ctx, grantHashHeaderValue)
	if err != nil {
		fmt.Fprintf(w, "error while checking if grant hash exists, %s", err)
		return
	}

	if !exists {
		svcs := make([]*httperrors.Service, 0)

		for grantHash, s := range services {
			svcs = append(svcs, &httperrors.Service{
				GrantHash: grantHash,
				Name:      s.Name,
				PeerID:    s.PeerID,
			})
		}

		outway_http.WriteError(w, httperrors.C1, httperrors.UnknownGrantHashInHeader(grantHashHeaderValue, svcs))

		return
	}

	tokenInfo, err := o.cfg.GetTokenInfoForGrantHash(ctx, grantHashHeaderValue)
	if err != nil {
		o.logger.Warn("could not get token for service", zap.String("grant-hash", grantHashHeaderValue), zap.Error(err))

		outway_http.WriteError(w, httperrors.C1, httperrors.UnableToGetTokenForGrantHash(grantHashHeaderValue))

		return
	}

	if r.Header.Get(FscAuthorizationHeader) != "" {
		outway_http.WriteError(w, httperrors.C1, httperrors.AuthHeaderMustNotBeSet())

		return
	}

	r.Header.Add(FscAuthorizationHeader, tokenInfo.Token)

	proxy, err := o.proxies.Get(tokenInfo.ServiceInwayAddress)
	if err != nil {
		outway_http.WriteError(w, httperrors.O1, httperrors.CouldNotSetupConnToInway(tokenInfo.ServiceInwayAddress, err))
		return
	}

	chain := buildChain(func(context *plugins.Context) error {
		proxy.HandleRequest(context.Response, context.Request)
		return nil
	}, o.plugins...)

	customCtx := &plugins.Context{
		Response:         w,
		Request:          r,
		Logger:           o.logger,
		RequestCreatedAt: now,
		TokenInfo:        tokenInfo,
		LogData:          map[string]string{},
	}

	logger.Info(
		"forwarding API request",
		zap.String("grant-hash", tokenInfo.GrantHash),
		zap.String("service", tokenInfo.ServiceName),
		zap.String("path", r.URL.Path),
		zap.String("source-peer-id", tokenInfo.OutwayPeerID),
		zap.String("source-delegator-peer-id", tokenInfo.OutwayDelegatorPeerID),
		zap.String("destination-peer-id", tokenInfo.ServicePeerID),
		zap.String("destination-delegator-peer-id", tokenInfo.ServiceDelegatorPeerID),
	)

	if err := chain(customCtx); err != nil {
		logger.Error("error while handling API request", zap.Error(err))
	}
}

func buildChain(serve plugins.ServeFunc, pluginList ...plugins.Plugin) plugins.ServeFunc {
	if len(pluginList) == 0 {
		return serve
	}

	return pluginList[0].Serve(buildChain(serve, pluginList[1:]...))
}
