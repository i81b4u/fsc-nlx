// Copyright © VNG Realisatie 2018
// Licensed under the EUPL

package plugins

import (
	"bytes"
	"crypto/x509"
	"encoding/base64"
	"encoding/json"
	"fmt"
	"io"
	"net/http"

	"go.uber.org/zap"

	"go.nlx.io/nlx/common/httperrors"
	common_tls "go.nlx.io/nlx/common/tls"
	"go.nlx.io/nlx/manager/domain/contract"
	outway_http "go.nlx.io/nlx/outway/http"
)

const (
	HTTPHeaderAuthorization = "X-NLX-Authorization"
)

type authRequest struct {
	Input *authRequestInput `json:"input"`
}

type authRequestInput struct {
	Headers                http.Header `json:"headers"`
	Path                   string      `json:"path"`
	OutwayCertificateChain []string    `json:"outway_certificate_chain"`
}

type authResponse struct {
	Allowed bool               `json:"allowed"`
	Status  authResponseStatus `json:"status"`
}

type authResponseStatus struct {
	Reason string `json:"reason"`
}
type AuthorizationPlugin struct {
	ca                     *x509.CertPool
	serviceURL             string
	authorizationClient    http.Client
	outwayCertificateChain []string
}

type NewAuthorizationPluginArgs struct {
	CA                  *x509.CertPool
	ServiceURL          string
	AuthorizationClient http.Client
	ExternalCertificate *common_tls.CertificateBundle
}

func NewAuthorizationPlugin(args *NewAuthorizationPluginArgs) (*AuthorizationPlugin, error) {
	if args.ServiceURL == "" {
		return nil, fmt.Errorf("ServiceURL cannot be empty")
	}

	if args.ExternalCertificate == nil {
		return nil, fmt.Errorf("ExternalCertificate cannot be nil")
	}

	c, err := contract.NewPeerCertFromCertificate(args.ExternalCertificate.RootCAs(), args.ExternalCertificate.Cert().Certificate)
	if err != nil {
		return nil, fmt.Errorf("could not create peer cert from args: %w", err)
	}

	return &AuthorizationPlugin{
		ca:                     args.CA,
		serviceURL:             args.ServiceURL,
		authorizationClient:    args.AuthorizationClient,
		outwayCertificateChain: rawDERstoBase64(c.RawDERs()),
	}, nil
}

func (plugin *AuthorizationPlugin) Serve(next ServeFunc) ServeFunc {
	return func(context *Context) error {
		authResponse, authErr := plugin.authorizeRequest(context)
		if authErr != nil {
			context.Logger.Error("error authorizing request", zap.Error(authErr))

			outway_http.WriteError(context.Response, httperrors.OAS1, httperrors.ErrorWhileAuthorizingRequest())

			return nil
		}

		context.Logger.Info(
			"authorization result",
			zap.Bool("authorized", authResponse.Allowed),
		)

		if !authResponse.Allowed {
			outway_http.WriteError(context.Response, httperrors.OAS1, httperrors.Unauthorized(authResponse.Status.Reason))

			return nil
		}

		return next(context)
	}
}

func (plugin *AuthorizationPlugin) authorizeRequest(c *Context) (*authResponse, error) {
	req, err := http.NewRequest(http.MethodPost, plugin.serviceURL, http.NoBody)
	if err != nil {
		return nil, err
	}

	body, err := json.Marshal(&authRequest{
		Input: &authRequestInput{
			Headers:                c.Request.Header,
			Path:                   c.Request.URL.Path,
			OutwayCertificateChain: plugin.outwayCertificateChain,
		},
	})
	if err != nil {
		return nil, err
	}

	req.Body = io.NopCloser(bytes.NewBuffer(body))

	resp, err := plugin.authorizationClient.Do(req)
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()

	if resp.StatusCode != http.StatusOK {
		return nil, fmt.Errorf("authorization service return non 200 status code. status code: %d", resp.StatusCode)
	}

	authResponse := &authResponse{}

	err = json.NewDecoder(resp.Body).Decode(authResponse)
	if err != nil {
		return nil, err
	}

	return authResponse, nil
}

func rawDERstoBase64(rawDERs [][]byte) []string {
	certs := make([]string, len(rawDERs))

	for i, c := range rawDERs {
		certs[i] = base64.URLEncoding.EncodeToString(c)
	}

	return certs
}
