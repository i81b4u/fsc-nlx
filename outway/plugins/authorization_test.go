// Copyright © VNG Realisatie 2018
// Licensed under the EUPL

package plugins

import (
	"encoding/json"
	"io"
	"net/http"
	"net/http/httptest"
	"path/filepath"
	"testing"
	"time"

	"github.com/stretchr/testify/assert"

	"go.nlx.io/nlx/common/accesstoken"
	"go.nlx.io/nlx/common/httperrors"
	"go.nlx.io/nlx/manager/domain/contract"
	"go.nlx.io/nlx/outway/domain/config"
	common_testing "go.nlx.io/nlx/testing/testingutils"
)

//nolint:funlen // this is a test
func TestAuthorizationPlugin(t *testing.T) {
	tests := map[string]struct {
		args                         *authRequest
		authServerResponse           interface{}
		authServerResponseStatusCode int
		wantErr                      *httperrors.FSCNetworkError
		wantHTTPStatusCode           int
	}{
		"when_auth_server_returns_non_OK_status": {
			args: &authRequest{
				Input: &authRequestInput{
					Headers: http.Header{
						"Proxy-Authorization": []string{"Bearer abc"},
					},
				},
			},
			authServerResponse: &authResponse{
				Allowed: true,
			},
			authServerResponseStatusCode: http.StatusUnauthorized,
			wantHTTPStatusCode:           httperrors.StatusFSCNetworkError,
			wantErr: &httperrors.FSCNetworkError{
				Source:   httperrors.Outway,
				Location: httperrors.OAS1,
				Code:     httperrors.ErrorWhileAuthorizingRequestErr,
				Message:  "error authorizing request",
			},
		},
		"when_auth_server_returns_invalid_response": {
			args: &authRequest{
				Input: &authRequestInput{
					Headers: http.Header{
						"Proxy-Authorization": []string{"Bearer abc"},
					},
				},
			},
			authServerResponse: struct {
				Invalid string `json:"invalid"`
			}{
				Invalid: "this is an invalid response",
			},
			authServerResponseStatusCode: http.StatusOK,
			wantHTTPStatusCode:           httperrors.StatusFSCNetworkError,
			wantErr: &httperrors.FSCNetworkError{
				Source:   httperrors.Outway,
				Location: httperrors.OAS1,
				Code:     httperrors.UnauthorizedErr,
				Message:  "authorization server denied request: ",
			},
		},
		"when_auth_server_fails": {
			args: &authRequest{
				Input: &authRequestInput{
					Headers: http.Header{
						"Proxy-Authorization": []string{"Bearer abc"},
					},
				},
			},
			authServerResponseStatusCode: http.StatusInternalServerError,
			wantHTTPStatusCode:           httperrors.StatusFSCNetworkError,
			wantErr: &httperrors.FSCNetworkError{
				Source:   httperrors.Outway,
				Location: httperrors.OAS1,
				Code:     httperrors.ErrorWhileAuthorizingRequestErr,
				Message:  "error authorizing request",
			},
		},
		"when_auth_server_returns_no_access": {
			args: &authRequest{
				Input: &authRequestInput{
					Headers: http.Header{
						"Proxy-Authorization": []string{"Bearer abc"},
					},
				},
			},
			authServerResponse: &authResponse{
				Allowed: false,
				Status: authResponseStatus{
					Reason: "for some reason",
				},
			},
			authServerResponseStatusCode: http.StatusOK,
			wantHTTPStatusCode:           httperrors.StatusFSCNetworkError,
			wantErr: &httperrors.FSCNetworkError{
				Source:   httperrors.Outway,
				Location: httperrors.OAS1,
				Code:     httperrors.UnauthorizedErr,
				Message:  "authorization server denied request: for some reason",
			},
		},
		"happy_flow": {
			args: &authRequest{
				Input: func() *authRequestInput {
					externalCert, err := common_testing.GetCertificateBundle(filepath.Join("..", "..", "testing", "pki"), common_testing.OrgNLXTest)
					assert.NoError(t, err)

					c, err := contract.NewPeerCertFromCertificate(externalCert.RootCAs(), externalCert.Cert().Certificate)
					assert.NoError(t, err)

					return &authRequestInput{
						Headers: http.Header{
							"Proxy-Authorization": []string{"Bearer abc"},
						},
						Path:                   "/test",
						OutwayCertificateChain: rawDERstoBase64(c.RawDERs()),
					}
				}(),
			},
			authServerResponse: &authResponse{
				Allowed: true,
			},
			authServerResponseStatusCode: http.StatusOK,
			wantHTTPStatusCode:           http.StatusOK,
		},
	}

	for name, tt := range tests {
		tt := tt

		t.Run(name, func(t *testing.T) {
			context := fakeContext(&config.TokenInfo{
				UnsafeDecodedToken: &accesstoken.UnsafeDecodedToken{
					GrantHash:                   "gh",
					OutwayPeerID:                "12345678901234567890",
					OutwayDelegatorPeerID:       "12345678901234567890",
					OutwayCertificateThumbprint: "",
					ServiceName:                 "serviceName",
					ServiceInwayAddress:         "inway.local",
					ServicePeerID:               "12345678901234567890",
					ServiceDelegatorPeerID:      "12345678901234567890",
					ExpiryDate:                  time.Now(),
				},
			})

			for k, values := range tt.args.Input.Headers {
				for _, v := range values {
					context.Request.Header.Add(k, v)
				}
			}

			var gotAuthorizationServiceRequest []byte

			server := httptest.NewServer(
				http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
					body := r.Body

					var err error
					gotAuthorizationServiceRequest, err = io.ReadAll(body)
					assert.NoError(t, err)

					w.WriteHeader(tt.authServerResponseStatusCode)

					b, err := json.Marshal(tt.authServerResponse)
					assert.NoError(t, err)

					_, err = w.Write(b)
					assert.NoError(t, err)
				}),
			)

			externalCert, err := common_testing.GetCertificateBundle(filepath.Join("..", "..", "testing", "pki"), common_testing.OrgNLXTest)
			assert.NoError(t, err)

			plugin, err := NewAuthorizationPlugin(&NewAuthorizationPluginArgs{
				CA:                  nil,
				ServiceURL:          server.URL,
				AuthorizationClient: *http.DefaultClient,
				ExternalCertificate: externalCert,
			})
			assert.NoError(t, err)

			err = plugin.Serve(nopServeFunc)(context)
			assert.NoError(t, err)

			response := context.Response.(*httptest.ResponseRecorder).Result()

			defer response.Body.Close()

			contents, err := io.ReadAll(response.Body)
			assert.NoError(t, err)

			assert.Equal(t, tt.wantHTTPStatusCode, response.StatusCode)

			if tt.wantErr != nil {
				gotError := &httperrors.FSCNetworkError{}
				err := json.Unmarshal(contents, gotError)
				assert.NoError(t, err)

				assert.Equal(t, tt.wantErr, gotError)
			} else {
				wantAuthorizationServiceRequest, err := json.Marshal(tt.args)
				assert.NoError(t, err)

				assert.Equal(t, wantAuthorizationServiceRequest, gotAuthorizationServiceRequest)
			}
		})
	}
}
