// Copyright © VNG Realisatie 2018
// Licensed under the EUPL

package plugins

import (
	ctx_context "context"
	"github.com/pkg/errors"
	"go.uber.org/zap"

	"go.nlx.io/nlx/common/httperrors"
	"go.nlx.io/nlx/common/transactionlog"
	outway_http "go.nlx.io/nlx/outway/http"
	"go.nlx.io/nlx/txlog-api/domain/record"
)

type LogRecordPlugin struct {
	organizationPeerID string
	txLogger           transactionlog.TransactionLogger
}

func NewLogRecordPlugin(organizationPeerID string, txLogger transactionlog.TransactionLogger) *LogRecordPlugin {
	return &LogRecordPlugin{
		organizationPeerID: organizationPeerID,
		txLogger:           txLogger,
	}
}

func (plugin *LogRecordPlugin) Serve(next ServeFunc) ServeFunc {
	return func(context *Context) error {
		logRecordID, err := plugin.createLogRecord(context)
		if err != nil {
			context.Logger.Error("failed to store transactionlog record", zap.Error(err))

			outway_http.WriteError(context.Response, httperrors.C1, httperrors.ServerError(err))

			return nil
		}

		context.Request.Header.Set("Fsc-Transaction-Id", logRecordID.String())

		return next(context)
	}
}

func (plugin *LogRecordPlugin) createLogRecord(context *Context) (*transactionlog.TransactionID, error) {
	id, err := transactionlog.NewTransactionID()
	if err != nil {
		return nil, errors.Wrap(err, "could not get new request ID")
	}

	var source interface{}
	if context.TokenInfo.OutwayDelegatorPeerID == "" {
		source = &transactionlog.RecordSource{
			OutwayPeerID: context.TokenInfo.OutwayPeerID,
		}
	} else {
		source = &transactionlog.RecordDelegatedSource{
			OutwayPeerID:    context.TokenInfo.OutwayPeerID,
			DelegatorPeerID: context.TokenInfo.OutwayDelegatorPeerID,
		}
	}

	var destination interface{}
	if context.TokenInfo.ServiceDelegatorPeerID == "" {
		destination = &transactionlog.RecordDestination{
			ServicePeerID: context.TokenInfo.ServicePeerID,
		}
	} else {
		destination = &transactionlog.RecordDelegatedDestination{
			ServicePeerID:   context.TokenInfo.ServicePeerID,
			DelegatorPeerID: context.TokenInfo.ServiceDelegatorPeerID,
		}
	}

	rec := &transactionlog.Record{
		TransactionID: id,
		GrantHash:     context.TokenInfo.GrantHash,
		ServiceName:   context.TokenInfo.ServiceName,
		Direction:     record.DirectionOut,
		Source:        source,
		Destination:   destination,
		CreatedAt:     context.RequestCreatedAt,
		Data: map[string]interface{}{
			"request-path": context.Request.URL.Path,
		},
	}

	if err := plugin.txLogger.AddRecords(ctx_context.TODO(), []*transactionlog.Record{rec}); err != nil {
		return nil, errors.Wrap(err, "unable to add records to txlog")
	}

	return id, nil
}
