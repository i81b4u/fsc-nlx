// Copyright © VNG Realisatie 2023
// Licensed under the EUPL

package controller

import "context"

type Controller interface {
	RegisterOutway(ctx context.Context, args *RegisterOutwayArgs) error
}

type RegisterOutwayArgs struct {
	Name                  string
	CertificateThumbprint string
}
