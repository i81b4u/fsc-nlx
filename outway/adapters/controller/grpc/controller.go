// Copyright © VNG Realisatie 2023
// Licensed under the EUPL

package grpcmanagement

import (
	"context"
	"fmt"

	"github.com/pkg/errors"

	"go.nlx.io/nlx/controller/ports/internalgrpc/api"
	"go.nlx.io/nlx/outway/adapters/controller"
	mclient "go.nlx.io/nlx/outway/pkg/controller"
)

type grpcController struct {
	controller mclient.Client
}

func New(client mclient.Client) (*grpcController, error) {
	if client == nil {
		return nil, fmt.Errorf("client is required")
	}

	return &grpcController{
		controller: client,
	}, nil
}

func (m *grpcController) RegisterOutway(ctx context.Context, args *controller.RegisterOutwayArgs) error {
	_, err := m.controller.RegisterOutway(ctx, &api.RegisterOutwayRequest{
		Name:                  args.Name,
		CertificateThumbprint: args.CertificateThumbprint,
	})
	if err != nil {
		return errors.Wrap(err, "could not register outway in grpc manager")
	}

	return nil
}
