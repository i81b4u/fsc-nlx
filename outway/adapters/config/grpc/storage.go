// Copyright © VNG Realisatie 2023
// Licensed under the EUPL

package restconfig

import (
	"context"
	"fmt"

	"github.com/pkg/errors"

	common_tls "go.nlx.io/nlx/common/tls"
	"go.nlx.io/nlx/outway/domain/config"
	"go.nlx.io/nlx/outway/pkg/manager"
)

type Repository struct {
	managerClient          manager.Client
	externalCertThumbprint string
}

func New(ctx context.Context, managerAddress string, cert *common_tls.CertificateBundle, externalCertThumbprint string) (config.Repository, error) {
	if managerAddress == "" {
		return nil, errors.New("missing manager address")
	}

	if cert == nil {
		return nil, errors.New("missing cert")
	}

	if externalCertThumbprint == "" {
		return nil, fmt.Errorf("missing external certificate thumbprint")
	}

	managerClient, err := manager.NewClient(ctx, managerAddress, cert)
	if err != nil {
		return nil, errors.Wrap(err, "failed to setup manager managerClient")
	}

	return &Repository{
		managerClient:          managerClient,
		externalCertThumbprint: externalCertThumbprint,
	}, nil
}

func (r *Repository) Close() error {
	return r.managerClient.Close()
}
