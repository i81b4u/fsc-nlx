// Copyright © VNG Realisatie 2023
// Licensed under the EUPL

package restconfig

import (
	"context"
	"time"

	"github.com/pkg/errors"

	"go.nlx.io/nlx/common/accesstoken"
	"go.nlx.io/nlx/manager/ports/int/grpc/api"
	"go.nlx.io/nlx/outway/domain/config"
)

func (r *Repository) GetServices(ctx context.Context) (config.Services, error) {
	res, err := r.managerClient.GetServicesForOutway(ctx, &api.GetServicesForOutwayRequest{
		OutwayCertificateThumbprint: r.externalCertThumbprint,
	})
	if err != nil {
		return nil, errors.Wrap(err, "could not get services from manager")
	}

	services := make(config.Services)
	for grantHash, s := range res.Services {
		services[grantHash] = &config.Service{
			PeerID: s.PeerId,
			Name:   s.Name,
		}
	}

	return services, nil
}

func (r *Repository) GetToken(ctx context.Context, grantHash string) (*config.TokenInfo, error) {
	res, err := r.managerClient.GetToken(ctx, &api.GetTokenRequest{
		GrantHash:                   grantHash,
		OutwayCertificateThumbprint: r.externalCertThumbprint,
	})
	if err != nil {
		return nil, errors.Wrap(err, "could not get token from manager")
	}

	return &config.TokenInfo{
		Token: res.Token,
		UnsafeDecodedToken: &accesstoken.UnsafeDecodedToken{
			GrantHash:                   res.TokenInfo.GrantHash,
			OutwayPeerID:                res.TokenInfo.OutwayPeerId,
			OutwayDelegatorPeerID:       res.TokenInfo.OutwayDelegatorPeerId,
			OutwayCertificateThumbprint: res.TokenInfo.OutwayCertificateThumbprint,
			ServiceName:                 res.TokenInfo.ServiceName,
			ServiceInwayAddress:         res.TokenInfo.ServiceInwayAddress,
			ServicePeerID:               res.TokenInfo.ServicePeerId,
			ServiceDelegatorPeerID:      res.TokenInfo.ServiceDelegatorPeerId,
			ExpiryDate:                  time.Unix(res.TokenInfo.ExpiryDate, 0),
		},
	}, nil
}
