// Copyright © VNG Realisatie 2023
// Licensed under the EUPL

package config

import (
	"context"
	"fmt"
	"sync"

	"golang.org/x/sync/singleflight"

	"go.nlx.io/nlx/common/accesstoken"
	"go.nlx.io/nlx/common/clock"
)

type Tokens map[string]*TokenInfo

type TokenInfo struct {
	Token string
	*accesstoken.UnsafeDecodedToken
}

type tokens struct {
	tokens   Tokens
	mut      *sync.RWMutex
	getToken *singleflight.Group
}

type Services map[string]*Service

type Service struct {
	Name   string
	PeerID string
}

type services struct {
	services    Services
	mut         *sync.RWMutex
	getServices *singleflight.Group
}

type Config struct {
	clock    clock.Clock
	repo     Repository
	services *services
	tokens   *tokens
}

func New(c clock.Clock, repo Repository) (*Config, error) {
	if repo == nil {
		return nil, fmt.Errorf("repo is required")
	}

	if c == nil {
		return nil, fmt.Errorf("clock is required")
	}

	return &Config{
		repo: repo,
		services: &services{
			services:    make(Services),
			mut:         &sync.RWMutex{},
			getServices: &singleflight.Group{},
		},
		tokens: &tokens{
			tokens:   make(Tokens),
			mut:      &sync.RWMutex{},
			getToken: &singleflight.Group{},
		},
		clock: c,
	}, nil
}

func (c *Config) GetTokenInfoForGrantHash(ctx context.Context, grantHash string) (*TokenInfo, error) {
	if grantHash == "" {
		return nil, fmt.Errorf("grantHash cannot be empty")
	}

	c.tokens.mut.RLock()
	info, ok := c.tokens.tokens[grantHash]
	c.tokens.mut.RUnlock()

	if ok && info.ExpiryDate.After(c.clock.Now()) {
		return info, nil
	}

	// get token from repository via singleflight
	// this will prevent multiple calls for the same grant hash
	token, err, _ := c.tokens.getToken.Do(grantHash, func() (interface{}, error) {
		tokenInfo, err := c.repo.GetToken(ctx, grantHash)
		if err != nil {
			return nil, err
		}

		c.tokens.mut.Lock()
		c.tokens.tokens[grantHash] = tokenInfo
		c.tokens.mut.Unlock()

		return tokenInfo, nil
	})
	if err != nil {
		return nil, err
	}

	return token.(*TokenInfo), nil
}

// GrantHashExists checks if grant hash exists, returns true when grant hash exists
// otherwise will return false when grant hash does not exists and also will return a list of existing grant hashes
func (c *Config) GrantHashExists(ctx context.Context, grantHash string) (exists bool, svcs Services, e error) {
	if grantHash != "" {
		c.services.mut.RLock()
		_, ok := c.services.services[grantHash]
		c.services.mut.RUnlock()

		if ok {
			return true, nil, nil
		}
	}

	res, err, _ := c.services.getServices.Do("services", func() (interface{}, error) {
		services, err := c.repo.GetServices(ctx)
		if err != nil {
			return nil, err
		}

		c.services.mut.Lock()
		c.services.services = services
		c.services.mut.Unlock()

		return services, nil
	})
	if err != nil {
		return false, nil, err
	}

	services := res.(Services)

	_, ok := services[grantHash]
	if ok {
		return true, nil, nil
	}

	return false, services, nil
}
