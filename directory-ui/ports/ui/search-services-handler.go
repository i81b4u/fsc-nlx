// Copyright © VNG Realisatie 2023
// Licensed under the EUPL

package uiport

import (
	"context"
	"net/http"
)

func (s *Server) searchServicesHandler(w http.ResponseWriter, r *http.Request) {
	environment := r.PostFormValue("environment")

	if environment != s.environment {
		w.Header().Set(HxRedirectHeader, environmentNameToUrls[environment])
		return
	}

	services, err := s.app.Queries.ListServices.Handle(context.Background(), r.PostFormValue("search"))
	if err != nil {
		s.logger.Error("could not execute list services query", err)
		http.Error(w, http.StatusText(http.StatusInternalServerError), http.StatusInternalServerError)

		return
	}

	data := searchData{
		BasePage:      s.basePage,
		SearchResults: make([]*ServicesSearchResult, len(services)),
	}

	for i, service := range services {
		ssr := &ServicesSearchResult{
			ServiceName: service.Name,
			Provider: Peer{
				ID:   service.Provider.ID,
				Name: service.Provider.Name,
			},
		}

		if service.IsDelegatedPublication() {
			ssr.Delegator = &Peer{
				ID:   service.Delegator.ID,
				Name: service.Delegator.Name,
			}
		}

		data.SearchResults[i] = ssr
	}

	err = data.render(w)
	if err != nil {
		s.logger.Error("could not render search services page", err)
		http.Error(w, http.StatusText(http.StatusInternalServerError), http.StatusInternalServerError)

		return
	}
}
