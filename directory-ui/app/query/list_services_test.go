// Copyright © VNG Realisatie 2023
// Licensed under the EUPL

package query_test

import (
	"context"
	"errors"
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"

	"go.nlx.io/nlx/directory-ui/adapters/directory"
	"go.nlx.io/nlx/directory-ui/app/query"
)

//nolint:funlen,dupl // test
func TestListServices(t *testing.T) {
	t.Parallel()

	tests := map[string]struct {
		setup   func(context.Context, *mocks)
		args    string
		want    []*query.Service
		wantErr error
	}{
		"when_repo_list_services_errors": {
			args: "query",
			setup: func(ctx context.Context, m *mocks) {
				m.repository.EXPECT().
					ListServices(ctx).
					Return(nil, errors.New("arbitrary error"))
			},
			wantErr: &query.InternalError{},
		},
		"search_by_service_name": {
			args: "parkeerrechten",
			setup: func(ctx context.Context, m *mocks) {
				m.repository.EXPECT().
					ListServices(ctx).
					Return([]directory.DirectoryService{
						&directory.Service{
							Name: "basisregister-parkeerrechten-api",
							Provider: directory.Peer{
								ID:   "12345678901234567890",
								Name: "Gemeente Stijns",
							},
						},
						&directory.Service{
							Name: "basisregister-kentekens-api",
							Provider: directory.Peer{
								ID:   "12345678901234567890",
								Name: "Gemeente Stijns",
							},
						},
					}, nil)
			},
			want: []*query.Service{
				{
					Name: "basisregister-parkeerrechten-api",
					Provider: query.Peer{
						ID:   "12345678901234567890",
						Name: "Gemeente Stijns",
					},
					Delegator: nil,
				},
			},
			wantErr: nil,
		},
		"search_by_provider_name": {
			args: "stijns",
			setup: func(ctx context.Context, m *mocks) {
				m.repository.EXPECT().
					ListServices(ctx).
					Return([]directory.DirectoryService{
						&directory.Service{
							Name: "basisregister-parkeerrechten-api",
							Provider: directory.Peer{
								ID:   "12345678901234567890",
								Name: "Gemeente Stijns",
							},
						},
						&directory.Service{
							Name: "basisregister-kentekens-api",
							Provider: directory.Peer{
								ID:   "12345678901234567891",
								Name: "RvRD",
							},
						},
					}, nil)
			},
			want: []*query.Service{
				{
					Name: "basisregister-parkeerrechten-api",
					Provider: query.Peer{
						ID:   "12345678901234567890",
						Name: "Gemeente Stijns",
					},
					Delegator: nil,
				},
			},
			wantErr: nil,
		},
		"search_by_delegator_name": {
			args: "vergunningsoftware",
			setup: func(ctx context.Context, m *mocks) {
				m.repository.EXPECT().
					ListServices(ctx).
					Return([]directory.DirectoryService{
						&directory.DelegatedService{
							Name: "basisregister-parkeerrechten-api",
							Provider: directory.Peer{
								ID:   "12345678901234567890",
								Name: "Gemeente Stijns",
							},
							Delegator: directory.Peer{
								ID:   "12345678901234567892",
								Name: "Vergunningsoftware BV",
							},
						},
						&directory.Service{
							Name: "basisregister-kentekens-api",
							Provider: directory.Peer{
								ID:   "12345678901234567891",
								Name: "RvRD",
							},
						},
					}, nil)
			},
			want: []*query.Service{
				{
					Name: "basisregister-parkeerrechten-api",
					Provider: query.Peer{
						ID:   "12345678901234567890",
						Name: "Gemeente Stijns",
					},
					Delegator: &query.Peer{
						ID:   "12345678901234567892",
						Name: "Vergunningsoftware BV",
					},
				},
			},
			wantErr: nil,
		},
	}

	for name, tt := range tests {
		tt := tt

		t.Run(name, func(t *testing.T) {
			t.Parallel()

			mocks := newMocks(t)

			h, err := query.NewListServicesHandler(mocks.repository)
			require.NoError(t, err)

			ctx := context.Background()

			if tt.setup != nil {
				tt.setup(ctx, mocks)
			}

			actual, err := h.Handle(ctx, tt.args)

			if tt.wantErr == nil {
				assert.NoError(t, err)
				assert.Equal(t, tt.want, actual)
			} else {
				assert.ErrorAs(t, err, &tt.wantErr)
			}
		})
	}
}
