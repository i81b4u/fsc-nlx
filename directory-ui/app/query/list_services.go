// Copyright © VNG Realisatie 2023
// Licensed under the EUPL

package query

import (
	"context"
	"errors"
	"fmt"
	"strings"

	"go.nlx.io/nlx/directory-ui/adapters/directory"
)

type ListServicesHandler struct {
	repository directory.Repository
}

func NewListServicesHandler(repository directory.Repository) (*ListServicesHandler, error) {
	if repository == nil {
		return nil, errors.New("repository is required")
	}

	return &ListServicesHandler{
		repository: repository,
	}, nil
}

func (l *ListServicesHandler) Handle(ctx context.Context, searchQuery string) ([]*Service, error) {
	services, err := l.repository.ListServices(ctx)
	if err != nil {
		return nil, fmt.Errorf("%s: %w", newInternalError("could not list services from repository"), err)
	}

	result := make([]*Service, 0)

	for _, s := range services {
		var ps *Service

		switch service := s.(type) {
		case *directory.Service:
			ps = &Service{
				Name: service.Name,
				Provider: Peer{
					ID:   service.Provider.ID,
					Name: service.Provider.Name,
				},
			}
		case *directory.DelegatedService:
			ps = &Service{
				Name: service.Name,
				Provider: Peer{
					ID:   service.Provider.ID,
					Name: service.Provider.Name,
				},
				Delegator: &Peer{
					ID:   service.Delegator.ID,
					Name: service.Delegator.Name,
				},
			}
		}

		if searchQuery == "" || match(ps, searchQuery) {
			result = append(result, ps)
		}
	}

	return result, nil
}

func match(s *Service, searchQuery string) bool {
	searchLower := strings.ToLower(searchQuery)
	serviceNameLower := strings.ToLower(s.Name)
	providerNameLower := strings.ToLower(s.Provider.Name)

	if s.IsDelegatedPublication() && strings.Contains(strings.ToLower(s.Delegator.Name), searchLower) {
		return true
	}

	return strings.Contains(serviceNameLower, searchLower) || strings.Contains(providerNameLower, searchLower)
}
