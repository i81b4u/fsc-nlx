// Copyright © VNG Realisatie 2023
// Licensed under the EUPL

package directory

import (
	"context"
)

type Repository interface {
	ListServices(ctx context.Context) ([]DirectoryService, error)
	ListParticipants(ctx context.Context) ([]*Participant, error)

	Shutdown() error
}

type DirectoryService interface{}

type Service struct {
	Name     string
	Provider Peer
}

type DelegatedService struct {
	Name      string
	Provider  Peer
	Delegator Peer
}

type Peer struct {
	ID   string
	Name string
}

type Participant struct {
	ID   string
	Name string
}
