// Copyright © VNG Realisatie 2023
// Licensed under the EUPL

package rest

import (
	"context"
	"fmt"
	"net/http"

	"github.com/pkg/errors"

	common_tls "go.nlx.io/nlx/common/tls"
	"go.nlx.io/nlx/directory-ui/adapters/directory"
	"go.nlx.io/nlx/manager/ports/ext/rest/api"
	"go.nlx.io/nlx/manager/ports/ext/rest/api/models"
)

type Client struct {
	directory.Repository
	c    *api.ClientWithResponses
	cert *common_tls.CertificateBundle
}

const fetchLimit = 250

func NewClient(address string, cert *common_tls.CertificateBundle) (directory.Repository, error) {
	if address == "" {
		return nil, fmt.Errorf("address required")
	}

	if cert == nil {
		return nil, fmt.Errorf("cert required")
	}

	c, err := api.NewClientWithResponses(address, func(c *api.Client) error {
		t := &http.Transport{
			TLSClientConfig: cert.TLSConfig(),
		}
		c.Client = &http.Client{Transport: t}
		return nil
	})

	if err != nil {
		return nil, err
	}

	return &Client{c: c, cert: cert}, nil
}

func (c *Client) ListParticipants(ctx context.Context) ([]*directory.Participant, error) {
	sortOrder := models.FSCCoreSortOrderSORTORDERDESCENDING

	limit := models.FSCCoreQueryPaginationLimit(fetchLimit)

	resp, err := c.c.GetPeersWithResponse(ctx, &models.GetPeersParams{
		SortOrder: &sortOrder,
		Limit:     &limit,
	})

	if err != nil {
		return nil, err
	}

	if resp.StatusCode() != http.StatusOK {
		return nil, fmt.Errorf("received invalid status code %d: %s", resp.StatusCode(), string(resp.Body))
	}

	peers := make([]*directory.Participant, len(resp.JSON200.Peers))

	for i, p := range resp.JSON200.Peers {
		peers[i] = &directory.Participant{
			ID:   p.Id,
			Name: p.Name,
		}
	}

	return peers, nil
}

func (c *Client) ListServices(ctx context.Context) ([]directory.DirectoryService, error) {
	sortOrder := models.FSCCoreSortOrderSORTORDERDESCENDING

	limit := models.FSCCoreQueryPaginationLimit(fetchLimit)

	resp, err := c.c.GetServicesWithResponse(ctx, &models.GetServicesParams{
		SortOrder: &sortOrder,
		Limit:     &limit,
	})

	if err != nil {
		return nil, err
	}

	if resp.StatusCode() != http.StatusOK {
		return nil, fmt.Errorf("received invalid status code %d: %s", resp.StatusCode(), string(resp.Body))
	}

	servicesList := make([]directory.DirectoryService, len(resp.JSON200.Services))

	for i, s := range resp.JSON200.Services {
		switch s.Type {
		case models.TYPESERVICE:
			data, err := s.Data.AsFSCCoreServiceListingService()
			if err != nil {
				return nil, errors.Wrap(err, "could not cast to service listing")
			}

			servicesList[i] = &directory.Service{
				Provider: directory.Peer{
					ID:   data.Peer.Id,
					Name: data.Peer.Name,
				},
				Name: data.Name,
			}
		case models.TYPEDELEGATEDSERVICE:
			data, err := s.Data.AsFSCCoreServiceListingDelegatedService()
			if err != nil {
				return nil, errors.Wrap(err, "could not cast to service listing")
			}

			servicesList[i] = &directory.DelegatedService{
				Provider: directory.Peer{
					ID:   data.Peer.Id,
					Name: data.Peer.Name,
				},
				Delegator: directory.Peer{
					ID:   data.Delegator.PeerId,
					Name: data.Delegator.PeerName,
				},
				Name: data.Name,
			}
		}
	}

	return servicesList, nil
}

func (c *Client) Shutdown() error {
	return nil
}
